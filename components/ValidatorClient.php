<?php
namespace app\components;

use Yii;
use yii\rbac\Rule;
use app\models\User;

class ValidatorClient extends Rule
{
	public $name = 'validator_client';
	
	public function execute($user, $item, $params)
	{		
        $model = User::findOne($user);
        return $model && $user == $model->id;
	}
}
?>
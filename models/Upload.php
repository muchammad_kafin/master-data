<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\base\Behavior;
use yii\behaviors\TimestampBehavior;
use yii\web\UploadedFile;

/**
 * This is the model class for table "upload".
 *
 * @property integer $id
 * @property string $id_form
 * @property string $filename
 * @property string $filepath
 * @property string $stored_file
 * @property string $created_at
 * @property string $updated_at
 */
class Upload extends \yii\db\ActiveRecord
{
	public $getfile;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'upload';
    }
	
	public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // if you're using datetime instead of UNIX timestamp:
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_form', 'filename', 'filepath', 'stored_file'], 'string', 'max' => 255],
            [['getfile'], 'safe'],
            ['getfile',  'file', 'maxSize'=>'500000', 'skipOnEmpty' => false, 'maxFiles' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_form' => Yii::t('app', 'Id Form'),
            'filename' => Yii::t('app', 'Filename'),
            'filepath' => Yii::t('app', 'Filepath'),
            'stored_file' => Yii::t('app', 'Stored File'),
            'getfile' => Yii::t('app', 'Upload File'),
        ];
    }
	
}

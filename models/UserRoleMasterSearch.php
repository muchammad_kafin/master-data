<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserRoleMaster;

/**
 * UserRoleMasterSearch represents the model behind the search form about `app\models\UserRoleMaster`.
 */
class UserRoleMasterSearch extends UserRoleMaster
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_master', 'id_role'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserRoleMaster::find();
		$query->joinWith(['users', 'masters', 'roles']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		$dataProvider->sort->attributes['user'] = [
			'asc' => ['user.username' => SORT_ASC],
			'desc' => ['user.username' => SORT_DESC],
		];
		
		$dataProvider->sort->attributes['master'] = [
			'asc' => ['master_list.name' => SORT_ASC],
			'desc' => ['master_list.name' => SORT_DESC],
		];
		
		$dataProvider->sort->attributes['role'] = [
			'asc' => ['master_role.name' => SORT_ASC],
			'desc' => ['master_role.name' => SORT_DESC],
		];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_user' => $this->id_user,
            'id_master' => $this->id_master,
            'id_role' => $this->id_role,
        ]);

        return $dataProvider;
    }
}

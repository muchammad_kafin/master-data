<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\base\Behavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "master_vendor".
 *
 * @property integer $id
 * @property string $no_doc
 * @property string $status
 * @property string $req_date
 * @property string $requestor
 * @property string $email_requestor
 * @property integer $validator
 * @property string $email_validator
 * @property integer $approver
 * @property string $email_approver
 * @property integer $support
 * @property string $email_support
 * @property string $support_date
 * @property string $remark
 * @property string $remark_cancel
 * @property string $code
 * @property string $name
 * @property string $special_intru
 * @property string $tax_address_code
 * @property string $npwp
 * @property string $vendor_group
 * @property string $address_type
 * @property string $address_1
 * @property string $address_2
 * @property string $address_3
 * @property string $address_4
 * @property string $country
 * @property string $state
 * @property string $city
 * @property string $zip_code
 * @property string $firstname
 * @property string $lastname
 * @property string $phone_number
 * @property string $email
 * @property string $office_code
 * @property string $credit_term
 * @property string $billing_currency
 * @property string $account_code
 */
class MasterVendor extends \yii\db\ActiveRecord
{
	public $validator_name;
	public $acknowledges_name;
	public $support_name;
	public $country_name;
	public $state_name;
	public $city_name;
	public $getfile;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_vendor';
    }	
	
	public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // if you're using datetime instead of UNIX timestamp:
                'value' => new Expression('NOW()'),
            ],
        ];
    }
	
	public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
				$this->requestor = strtoupper($this->requestor);
				$this->code = strtoupper($this->code);
				$this->name = strtoupper($this->name);
				$this->tax_address_code = strtoupper($this->tax_address_code);
				$this->npwp = strtoupper($this->npwp);
				$this->vendor_group = strtoupper($this->vendor_group);
				$this->address_type = strtoupper($this->address_type);
				$this->address_1 = strtoupper($this->address_1);
				$this->address_2 = strtoupper($this->address_2);
				$this->address_3 = strtoupper($this->address_3);
				$this->address_4 = strtoupper($this->address_4);
				$this->special_intru = strtoupper($this->special_intru);				
				$this->state = strtoupper($this->state);
				$this->city = strtoupper($this->city);				
				$this->billing_currency = strtoupper($this->billing_currency);
				$this->account_code = strtoupper($this->account_code);
				$this->zip_code = strtoupper($this->zip_code);
				$this->remark = strtoupper($this->remark);				
				$this->email = strtoupper($this->email);
				$this->support = strtoupper($this->support);
				$this->email_requestor = strtolower($this->email_requestor);
				$this->account_name = strtoupper($this->account_name);
				$this->bank_name = strtoupper($this->bank_name);
				$this->branch = strtoupper($this->branch);
				$this->swift_code = strtoupper($this->swift_code);
				$map = implode(",", $this->office_code);
				$this->office_code = $map;
				$country = $this->country;
				if($country){
					$this->country = strtoupper($this->country);
				}else{
					$this->country = strtoupper($this->country_name);
				}
			}elseif(Yii::$app->controller->action->id == 'cancelvendor'){
				$this->remark_cancel = strtoupper($this->remark_cancel);
			}elseif(Yii::$app->controller->action->id == 'updatevendor' && Yii::$app->controller->id == 'validator'){
				$map = implode(",", $this->office_code);
				$this->office_code = $map;
			}
			return true;
		}
        
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_doc', 'status', 'req_date', 'supplier', 'requestor', 'email_requestor', 'validator', 'email_validator', 'acknowledges', 'email_acknowledges', 'email_support', 'support', 'code', 'tax_address_code', 'npwp', 'vendor_group', 'address_type', 'address_1', 'country', 'office_code', 'credit_term', 'billing_currency', 'account_code', 'bank_name', 'account_name', 'account_no', 'country_name'], 'required'],
            [['req_date', 'support_date', 'validator_name', 'acknowledges_name', 'support_name', 'city_name', 'state_name','state','city','zip_code','branch','swift_code','getfile'], 'safe'],
            [['remark', 'remark_cancel', 'special_intru', 'address_1', 'address_2', 'address_3', 'address_4'], 'string'],
            [['no_doc', 'code', 'name', 'tax_address_code', 'npwp', 'vendor_group', 'address_type', 'credit_term', 'billing_currency', 'account_code'], 'string', 'max' => 255],
            [['no_doc'], 'unique'],
			[['remark_cancel'],'required','on' => 'cancel_vendor'],
			[['support_date'],'required','on' => 'support_vendor'],
			[['address_1','address_2','address_3','address_4'], 'string', 'max' => 50],
			[['email_validator','email_requestor','email_acknowledges','email_support'], 'string', 'min' => 5],
			[['email','email_requestor'], 'email'],
			[['code','vendor_group','tax_address_code'], 'string', 'min' => 11, 'on'=>'validator_vendor'],
			['getfile',  'file', 'maxSize'=>'300000', 'skipOnEmpty' => true, 'maxFiles' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'no_doc' => Yii::t('app', 'No Doc'),
            'status' => Yii::t('app', 'Status'),
            'req_date' => Yii::t('app', 'Req Date'),
            'requestor' => Yii::t('app', 'Requestor'),
            'email_requestor' => Yii::t('app', 'Email Requestor'),
            'validator' => Yii::t('app', 'Validator'),
            'email_validator' => Yii::t('app', 'Email Validator'),
            'acknowledges' => Yii::t('app', 'Acknowledges'),
            'email_acknowledges' => Yii::t('app', 'Email Acknowledges'),
            'support' => Yii::t('app', 'Support'),
            'email_support' => Yii::t('app', 'Email Support'),
            'support_date' => Yii::t('app', 'Support Date'),
            'remark' => Yii::t('app', 'Remark'),
            'remark_cancel' => Yii::t('app', 'Remark Reject'),
            'code' => Yii::t('app', 'Code'),
            'name' => Yii::t('app', 'Name'),
            'special_intru' => Yii::t('app', 'Special Instructions'),
            'tax_address_code' => Yii::t('app', 'Tax Address Code'),
            'npwp' => Yii::t('app', 'Npwp'),
            'vendor_group' => Yii::t('app', 'Vendor Group'),
            'address_type' => Yii::t('app', 'Address Type'),
            'address_1' => Yii::t('app', 'Address 1'),
            'address_2' => Yii::t('app', 'Address 2'),
            'address_3' => Yii::t('app', 'Address 3'),
            'address_4' => Yii::t('app', 'Address 4'),
            'country' => Yii::t('app', 'Country'),
            'state' => Yii::t('app', 'State'),
            'city' => Yii::t('app', 'City'),
            'zip_code' => Yii::t('app', 'Zip Code'),
            // 'firstname' => Yii::t('app', 'Firstname'),
            // 'lastname' => Yii::t('app', 'Lastname'),
            // 'phone_type' => Yii::t('app', 'Phone Type'),
            // 'phone_number' => Yii::t('app', 'Phone Number'),
            'email' => Yii::t('app', 'Email'),
            'office_code' => Yii::t('app', 'Office Code'),
            'credit_term' => Yii::t('app', 'Credit Term'),
            'billing_currency' => Yii::t('app', 'Billing Currency'),
            'account_code' => Yii::t('app', 'Account Code'),
            'supplier' => Yii::t('app', 'Supplier Type'),
            'bank_name' => Yii::t('app', 'Bank Name'),
            'account_name' => Yii::t('app', 'Bank Account Name'),
            'branch' => Yii::t('app', 'Bank Branch'),
            'swift_code' => Yii::t('app', 'Bank Swift Code'),
            'account_no' => Yii::t('app', 'Bank Account Number'),
            'getfile' => Yii::t('app', 'Upload File'),
        ];
    }
	
	public function getDayArray(){
		$listData=\yii\helpers\ArrayHelper::map(\app\models\CreditTerm::find()->all(),'code','description');
		return $listData;
	}
	
	public function getSupArray(){		
		$listData=\yii\helpers\ArrayHelper::map(\app\models\SupplierType::find()->all(),'code','name');
		return $listData;
	}
}

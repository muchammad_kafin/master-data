<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "country".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code'], 'required'],
            [['name', 'code'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'code' => Yii::t('app', 'Code'),
        ];
    }
	
	public static function getState($state) {
		$data=\app\models\Region::find()
		   ->where(['country'=>$state])
		   ->select(['code as id','name'])->asArray()->all();

		return $data;
	}
	
	public static function getCity($country, $state) {
		$out = [];
        $data=\app\models\City::find()
		   ->where(['country'=>$country,'region'=>$state])
		   ->select(['id','name'])->asArray()->all();

        $selected = '';

        foreach ($data as $dat => $datas) {
            $out[] = ['id' => $datas['id'], 'name' => $datas['name']];

            if($dat == 0){
                    $aux = $datas['id'];
                }

            ($datas['name'] == $country) ? $selected = $country : $selected = $aux;

        }
        return $output = [
            'output' => $out,
            'selected' => $selected
        ];
	}
}

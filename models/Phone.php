<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "phone".
 *
 * @property integer $id
 * @property integer $id_contact
 * @property string $phone_type
 * @property string $phone_number
 *
 * @property Contact $idContact
 */
class Phone extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'phone';
    }
	
	public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
				// $this->lastname = strtoupper($this->lastname);
				// $this->firstname = strtoupper($this->firstname);
				$this->phone_type = strtoupper($this->phone_type);
			}
			return true;
		}
	}
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_contact'], 'integer'],
            [['phone_type', 'phone_number'], 'string', 'max' => 255],
            [['id_contact'], 'exist', 'skipOnError' => true, 'targetClass' => Contact::className(), 'targetAttribute' => ['id_contact' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_contact' => Yii::t('app', 'Id Contact'),
            'phone_type' => Yii::t('app', 'Phone Type'),
            'phone_number' => Yii::t('app', 'Phone Number'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdContact()
    {
        return $this->hasOne(Contact::className(), ['id' => 'id_contact']);
    }
}

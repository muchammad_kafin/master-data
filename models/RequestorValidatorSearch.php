<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RequestorValidator;

/**
 * RequestorValidatorSearch represents the model behind the search form about `app\models\RequestorValidator`.
 */
class RequestorValidatorSearch extends RequestorValidator
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_validator', 'requestor'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RequestorValidator::find();
		$query->joinWith(['valids','requests']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		$dataProvider->sort->attributes['valid'] = [
			'asc' => ['user.username' => SORT_ASC],
			'desc' => ['user.username' => SORT_DESC],
		];
		
		$dataProvider->sort->attributes['requestor'] = [
			'asc' => ['requestor.email_requestor' => SORT_ASC],
			'desc' => ['requestor.email_requestor' => SORT_DESC],
		];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 'id_validator', $this->id_validator])
            ->andFilterWhere(['like', 'requestor', $this->requestor]);

        return $dataProvider;
    }
}

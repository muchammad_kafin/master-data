<?php

namespace app\models;

use Yii;
use app\models\User;
use app\models\MasterRole;
use app\models\MasterList;

/**
 * This is the model class for table "user_role_master".
 *
 * @property integer $id_user
 * @property integer $id_master
 * @property integer $id_role
 */
class UserRoleMaster extends \yii\db\ActiveRecord
{
	public $user;
	public $role;
	public $master;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_role_master';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_master', 'id_role'], 'required'],
            [['user', 'master', 'role'], 'safe'],
            [['id_user', 'id_master', 'id_role'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_user' => Yii::t('app', 'Id User'),
            'id_master' => Yii::t('app', 'Id Master'),
            'id_role' => Yii::t('app', 'Id Role'),
        ];
    }
	
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUsers()
	{
		return $this->hasOne(User::className(), ['id' => 'id_user']);
	}
	
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getMasters()
	{
		return $this->hasOne(MasterList::className(), ['id' => 'id_master']);
	}
	
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getRoles()
	{
		return $this->hasOne(MasterRole::className(), ['id' => 'id_role']);
	}
}

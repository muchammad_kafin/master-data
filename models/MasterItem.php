<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\AttributeBehavior;
use yii\db\Expression;
use yii\base\Behavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "master_item".
 *
 * @property integer $id
 * @property string $no_doc
 * @property string $status
 * @property string $req_date
 * @property string $requestor
 * @property string $email_requestor
 * @property string $validator
 * @property string $email_validator
 * @property string $acknowledges
 * @property string $email_acknowledges
 * @property string $support
 * @property string $email_support
 * @property string $support_date
 * @property string $remark
 * @property string $remark_cancel
 * @property string $item_code
 * @property string $description
 * @property string $lookup_code
 * @property string $item_type
 * @property string $base_unit
 * @property string $base_location
 * @property string $label_code
 * @property string $unit_purchase
 */
class MasterItem extends \yii\db\ActiveRecord
{
	public $validator_name;
	public $acknowledges_name;
	public $support_name;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_item';
    }
	
	public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // if you're using datetime instead of UNIX timestamp:
                'value' => new Expression('NOW()'),
            ],
        ];
    }
	
	public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
				$this->requestor = strtoupper($this->requestor);
				$this->item_code = strtoupper($this->item_code);
				$this->item_type = strtoupper($this->item_type);
				$this->base_unit = strtoupper($this->base_unit);
				$this->base_location = strtoupper($this->base_location);
				$this->label_code = strtoupper($this->label_code);
				$this->unit_purchase = strtoupper($this->unit_purchase);
				$this->description = strtoupper($this->description);
				$this->lookup_code = strtoupper($this->lookup_code);
				$this->email_requestor = strtolower($this->email_requestor);
			}
			return true;
		}
        
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_doc', 'status', 'req_date', 'requestor', 'email_requestor', 'validator', 'email_validator', 'acknowledges', 'email_acknowledges', 'item_code', 'item_type', 'base_unit', 'base_location', 'label_code', 'unit_purchase'], 'required'],
            [['req_date', 'support_date','validator_name', 'acknowledges_name', 'support_name','description'], 'safe'],
            [['remark', 'remark_cancel'], 'string'],
			[['remark_cancel'],'required','on' => 'cancel_item'],
			[['support_date'],'required','on' => 'support_item'],
            [['no_doc', 'validator', 'email_validator', 'item_code', 'lookup_code', 'item_type', 'base_unit', 'base_location', 'label_code', 'unit_purchase','description'], 'string', 'max' => 255],
            [['no_doc'], 'unique'],
			[['email_validator','email_requestor','email_acknowledges','email_support'], 'string', 'min' => 5],
			[['email_requestor'], 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'no_doc' => Yii::t('app', 'No Doc'),
            'status' => Yii::t('app', 'Status'),
            'req_date' => Yii::t('app', 'Req Date'),
            'requestor' => Yii::t('app', 'Requestor'),
            'email_requestor' => Yii::t('app', 'Email Requestor'),
            'validator' => Yii::t('app', 'Validator'),
            'email_validator' => Yii::t('app', 'Email Validator'),
            'acknowledges' => Yii::t('app', 'Acknowledges'),
            'email_acknowledges' => Yii::t('app', 'Email Acknowledges'),
            'support' => Yii::t('app', 'Support'),
            'email_support' => Yii::t('app', 'Email Support'),
            'support_date' => Yii::t('app', 'Support Date'),
            'remark' => Yii::t('app', 'Remark'),
            'remark_cancel' => Yii::t('app', 'Remark Cancel'),
            'item_code' => Yii::t('app', 'Item Code'),
            'description' => Yii::t('app', 'Description'),
            'lookup_code' => Yii::t('app', 'Lookup Code'),
            'item_type' => Yii::t('app', 'Item Type'),
            'base_unit' => Yii::t('app', 'Base Unit'),
            'base_location' => Yii::t('app', 'Base Location'),
            'label_code' => Yii::t('app', 'Label Code'),
            'unit_purchase' => Yii::t('app', 'Unit Purchase'),
        ];
    }
}

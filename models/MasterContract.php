<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\AttributeBehavior;
use yii\db\Expression;
use yii\base\Behavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "master_contract".
 *
 * @property integer $id
 * @property string $no_doc
 * @property string $status
 * @property string $req_date
 * @property string $requestor
 * @property string $email_requestor
 * @property string $validator
 * @property string $email_validator
 * @property string $acknowledges
 * @property string $email_acknowledges
 * @property string $support
 * @property string $email_support
 * @property string $support_date
 * @property string $remark
 * @property string $remark_cancel
 * @property string $vendor_code
 * @property string $vendor_name
 * @property string $sk_no
 * @property string $start_periode
 * @property string $end_periode
 * @property string $jenis_sewa
 * @property string $item_code
 * @property string $uom
 */
class MasterContract extends \yii\db\ActiveRecord
{
	public $validator_name;
	public $acknowledges_name;
	public $support_name;
	public $getfile;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_contract';
    }
	
	public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // if you're using datetime instead of UNIX timestamp:
                'value' => new Expression('NOW()'),
            ],
        ];
    }
	
	public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
				$this->requestor = strtoupper($this->requestor);
				$this->item_code = strtoupper($this->item_code);
				$this->uom = strtoupper($this->uom);
				$this->vendor_code = strtoupper($this->vendor_code);
				$this->vendor_name = strtoupper($this->vendor_name);
				$this->sk_no = strtoupper($this->sk_no);
				$this->jenis_sewa = strtoupper($this->jenis_sewa);
				$this->email_requestor = strtolower($this->email_requestor);
			}
			return true;
		}
        
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_doc', 'status', 'req_date', 'requestor', 'email_requestor', 'validator', 'email_validator', 'acknowledges', 'email_acknowledges', 'vendor_code', 'vendor_name', 'item_code', 'uom', 'start_periode', 'end_periode', 'jenis_sewa'], 'required'],
            [['req_date', 'support_date','validator_name', 'acknowledges_name', 'support_name'], 'safe'],
            [['remark', 'remark_cancel'], 'string'],
			[['remark_cancel'],'required','on' => 'cancel_contract'],
			[['support_date'],'required','on' => 'support_contract'],
            [['no_doc', 'validator', 'email_validator', 'vendor_code', 'vendor_name', 'sk_no', 'jenis_sewa', 'item_code', 'uom'], 'string', 'max' => 255],            
            [['no_doc'], 'unique'],
			[['email_validator','email_requestor','email_acknowledges','email_support'], 'string', 'min' => 5],
			[['email_requestor'], 'email'],
			['getfile',  'file', 'maxSize'=>'300000', 'skipOnEmpty' => true, 'maxFiles' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'no_doc' => Yii::t('app', 'No Doc'),
            'status' => Yii::t('app', 'Status'),
            'req_date' => Yii::t('app', 'Req Date'),
            'requestor' => Yii::t('app', 'Requestor'),
            'email_requestor' => Yii::t('app', 'Email Requestor'),
            'validator' => Yii::t('app', 'Validator'),
            'email_validator' => Yii::t('app', 'Email Validator'),
            'acknowledges' => Yii::t('app', 'Acknowledges'),
            'email_acknowledges' => Yii::t('app', 'Email Acknowledges'),
            'support' => Yii::t('app', 'Support'),
            'email_support' => Yii::t('app', 'Email Support'),
            'support_date' => Yii::t('app', 'Support Date'),
            'remark' => Yii::t('app', 'Remark'),
            'remark_cancel' => Yii::t('app', 'Remark Cancel'),
            'vendor_code' => Yii::t('app', 'Vendor Code'),
            'vendor_name' => Yii::t('app', 'Vendor Name'),
            'sk_no' => Yii::t('app', 'NO Kontrak / NO Memo'),
            'start_periode' => Yii::t('app', 'Start Periode'),
            'end_periode' => Yii::t('app', 'End Periode'),
            'jenis_sewa' => Yii::t('app', 'Jenis Sewa'),
            'item_code' => Yii::t('app', 'Item Code'),
            'uom' => Yii::t('app', 'Uom'),
			'getfile' => Yii::t('app', 'Upload File'),
        ];
    }
}

<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\AttributeBehavior;
use yii\db\Expression;
use yii\base\Behavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "master_country".
 *
 * @property integer $id
 * @property string $no_doc
 * @property string $status
 * @property string $req_date
 * @property string $requestor
 * @property string $validator
 * @property string $approver
 * @property string $support
 * @property string $support_date
 * @property string $remark
 * @property string $name
 * @property string $code
 * @property string $isd
 * @property string $currency
 * @property string $continent
 */
class MasterCountry extends \yii\db\ActiveRecord
{
	public $validator_name;
	public $acknowledges_name;
	public $support_name;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_country';
    }
	
	public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // if you're using datetime instead of UNIX timestamp:
                'value' => new Expression('NOW()'),
            ],
        ];
    }
	
	public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
				$this->requestor = strtoupper($this->requestor);
				$this->code = strtoupper($this->code);
				$this->name = strtoupper($this->name);
				$this->continent = strtoupper($this->continent);
				$this->email_requestor = strtolower($this->email_requestor);
			}
			return true;
		}
        
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_doc', 'status', 'req_date', 'requestor', 'validator', 'acknowledges', 'name', 'code', 'email_acknowledges','email_requestor','email_support','email_validator'], 'required'],
            [['req_date', 'support_date', 'validator_name', 'acknowledges_name', 'support_name'], 'safe'],
            [['remark'], 'string'],
            [['no_doc', 'name', 'code', 'isd', 'currency', 'continent'], 'string', 'max' => 255],
            [['no_doc'], 'unique'],
			[['remark_cancel'],'required','on' => 'cancel_country'],
			[['support_date'],'required','on' => 'support_country'],
			[['email_validator','email_requestor','email_acknowledges','email_support'], 'string', 'min' => 5],
			[['email_requestor'], 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'no_doc' => Yii::t('app', 'No Doc'),
            'status' => Yii::t('app', 'Status'),
            'req_date' => Yii::t('app', 'Req Date'),
            'requestor' => Yii::t('app', 'Requestor'),
            'validator' => Yii::t('app', 'Validator'),
            'acknowledges' => Yii::t('app', 'Acknowledges'),
            'support' => Yii::t('app', 'Support'),
            'support_date' => Yii::t('app', 'Support Date'),
            'remark' => Yii::t('app', 'Remark'),
            'name' => Yii::t('app', 'Name'),
            'code' => Yii::t('app', 'Code'),
            'isd' => Yii::t('app', 'Isd'),
            'currency' => Yii::t('app', 'Currency'),
            'continent' => Yii::t('app', 'Continent'),
        ];
    }
}

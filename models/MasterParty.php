<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\AttributeBehavior;
use yii\db\Expression;
use yii\base\Behavior;
use yii\behaviors\TimestampBehavior;
use app\models\Contact;

/**
 * This is the model class for table "master_party".
 *
 * @property integer $id
 * @property string $no_doc
 * @property string $status
 * @property string $req_date
 * @property string $requestor
 * @property string $email_requestor
 * @property integer $validator
 * @property string $email_validator
 * @property integer $approver
 * @property string $email_approver
 * @property integer $support
 * @property string $email_support
 * @property string $support_date
 * @property string $remark
 * @property string $remark_cancel
 * @property string $client
 * @property string $code
 * @property string $name
 * @property string $office
 * @property string $address_1
 * @property string $address_2
 * @property string $country
 * @property string $state
 * @property string $city
 * @property string $zip_code
 * @property string $contact_person
 * @property string $phone_number
 * @property string $email
 * @property string $type
 */
class MasterParty extends \yii\db\ActiveRecord
{
	public $validator_name;
	public $acknowledges_name;
	public $support_name;
	public $country_name;
	public $state_name;
	public $city_name;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_party';
    }
	
	public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // if you're using datetime instead of UNIX timestamp:
                'value' => new Expression('NOW()'),
            ],
        ];
    }
	
	public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
				$this->requestor = strtoupper($this->requestor);
				$this->client = strtoupper($this->client);
				$this->code = strtoupper($this->code);
				$this->name = strtoupper($this->name);
				$this->address_1 = strtoupper($this->address_1);
				$this->address_2 = strtoupper($this->address_2);
				$this->state = strtoupper($this->state);
				$this->city = strtoupper($this->city);
				$this->zip_code = strtoupper($this->zip_code);
				$this->location = strtoupper($this->location);
				$this->contact_person = strtoupper($this->contact_person);
				$this->remark = strtoupper($this->remark);
				$this->email_requestor = strtolower($this->email_requestor);
				$type = implode(",", $this->type);
				$this->type = $type;
				$map = implode(",", $this->office_mapping);
				$this->office_mapping = $map;
				$country = $this->country;
				if($country){
					$this->country = strtoupper($this->country);
				}else{
					$this->country = strtoupper($this->country_name);
				}
			}elseif(Yii::$app->controller->action->id == 'cancelparty'){
				$this->remark_cancel = strtoupper($this->remark_cancel);
			}elseif(Yii::$app->controller->action->id == 'updateparty' && Yii::$app->controller->id == 'validator'){
				$map = implode(",", $this->office_mapping);
				$this->office_mapping = $map;
			}elseif(Yii::$app->controller->action->id == 'updateparty' && Yii::$app->controller->id == 'support'){
				$type = implode(",", $this->type);
				$this->type = $type;
			}
			return true;
		}
        
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_doc', 'status', 'req_date', 'country_name', 'requestor', 'validator', 'acknowledges', 'support', 'client', 'code', 'name', 'office', 'address_1', 'country', 'email_acknowledges','email_requestor','email_support','email_validator', 'remark_cancel', 'office_mapping', 'type'], 'required'],
            [['validator_name', 'acknowledges_name', 'support_name', 'city_name', 'state_name','state','city','location','zip_code','status_ubah'], 'safe'],
            [['remark', 'remark_cancel', 'address_1', 'address_2'], 'string'],
			[['remark_cancel'],'required','on' => 'cancel_party'],
			[['support_date'],'required','on' => 'support_party'],
            [['no_doc'], 'string', 'max' => 255],
            [['no_doc'], 'unique'],
            [['email_validator','email_requestor','email_acknowledges','email_support'], 'string', 'min' => 5],
			[['email_requestor'], 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'no_doc' => Yii::t('app', 'No Doc'),
            'status' => Yii::t('app', 'Status'),
            'req_date' => Yii::t('app', 'Req Date'),
            'requestor' => Yii::t('app', 'Requestor'),
            'email_requestor' => Yii::t('app', 'Email Requestor'),
            'validator' => Yii::t('app', 'Validator'),
            'email_validator' => Yii::t('app', 'Email Validator'),
            'acknowledges' => Yii::t('app', 'Acknowledges'),
            'email_acknowledges' => Yii::t('app', 'Email Acknowledges'),
            'support' => Yii::t('app', 'Support'),
            'email_support' => Yii::t('app', 'Email Support'),
            'support_date' => Yii::t('app', 'Support Date'),
            'remark' => Yii::t('app', 'Remark'),
            'remark_cancel' => Yii::t('app', 'Remark Reject'),
            'client' => Yii::t('app', 'Client'),
            'code' => Yii::t('app', 'Code'),
            'name' => Yii::t('app', 'Name'),
            'office' => Yii::t('app', 'Office'),
            'address_1' => Yii::t('app', 'Address 1'),
            'address_2' => Yii::t('app', 'Address 2'),
            'country' => Yii::t('app', 'Country'),
            'state' => Yii::t('app', 'State'),
            'city' => Yii::t('app', 'City'),
            'zip_code' => Yii::t('app', 'Zip Code'),
            'contact_person' => Yii::t('app', 'Contact Person'),
            'phone_number' => Yii::t('app', 'Phone Number'),
            'email' => Yii::t('app', 'Email'),
            'type' => Yii::t('app', 'Type'),
            'other' => Yii::t('app', 'Other'),
            'office_mapping' => Yii::t('app', 'Office Mapping'),
        ];
    }
	
	 /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasMany(Contact::className(), ['id_party' => 'id']);
    }
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasMany(Address::className(), ['id_party' => 'id']);
    }
	
	public static function getSearch()
	{
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand('
			SELECT DISTINCT `code` AS `query` FROM `master_party` 
			UNION
			SELECT DISTINCT `name` AS `query` FROM `master_party` 
			');

		$dataCountry = $command->queryAll();
		return $dataCountry;
	}
}

<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MasterVendor;

/**
 * MasterVendorSearch represents the model behind the search form about `app\models\MasterVendor`.
 */
class MasterVendorSearch extends MasterVendor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'validator', 'acknowledges', 'support'], 'safe'],
            [['no_doc', 'status', 'req_date', 'requestor', 'email_requestor', 'email_validator', 'email_acknowledges', 'email_support', 'support_date', 'remark', 'remark_cancel', 'code', 'name', 'special_intru', 'tax_address_code', 'npwp', 'vendor_group', 'address_type', 'address_1', 'address_2', 'address_3', 'address_4', 'country', 'state', 'city', 'zip_code', 'email', 'office_code', 'credit_term', 'billing_currency', 'account_code'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterVendor::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort' => [
				'defaultOrder' => [
					'req_date' => SORT_DESC,
				]
			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
		if(Yii::$app->controller->id == "finish"){
			$query->andFilterWhere(['like', 'status', $this->status]);
		}elseif(Yii::$app->user->identity->username == "admin"){
			$query->andFilterWhere(['like', 'status', $this->status]);
		}else{
			if(Yii::$app->controller->id == "validator"){
				$query->andFilterWhere(['and',['!=', 'status', 'reject'],['=', 'status', "validator"]]);
				$query->andFilterWhere(['like','email_validator',Yii::$app->user->identity->email]);
			}elseif(Yii::$app->controller->id == "approver"){
				$query->andFilterWhere(['and',['!=', 'status', 'reject'],['=', 'status', "approver"]]);
				$query->andFilterWhere(['like','email_validator',Yii::$app->user->identity->email]);
			}elseif(Yii::$app->controller->id == "support"){
				$query->andFilterWhere(['and',['!=', 'status', 'reject'],['=', 'status', "support"]]);
				$query->andFilterWhere(['like','email_support',Yii::$app->user->identity->email]);
			}
		}
		
        // grid filtering conditions
        /*$query->andFilterWhere([
            'id' => $this->id,
            'req_date' => $this->req_date,
            'validator' => $this->validator,
            'acknowledges' => $this->acknowledges,
            'support' => $this->support,
            'support_date' => $this->support_date,
        ]);*/

        $query->andFilterWhere(['like', 'no_doc', $this->no_doc])
            ->andFilterWhere(['like', 'requestor', $this->requestor])
            ->andFilterWhere(['like', 'email_requestor', $this->email_requestor])
            ->andFilterWhere(['like', 'remark', $this->remark])
            ->andFilterWhere(['like', 'remark_cancel', $this->remark_cancel])
            ->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'special_intru', $this->special_intru])
            ->andFilterWhere(['like', 'tax_address_code', $this->tax_address_code])
            ->andFilterWhere(['like', 'npwp', $this->npwp])
            ->andFilterWhere(['like', 'vendor_group', $this->vendor_group])
            ->andFilterWhere(['like', 'address_type', $this->address_type])
            ->andFilterWhere(['like', 'address_1', $this->address_1])
            ->andFilterWhere(['like', 'address_2', $this->address_2])
            ->andFilterWhere(['like', 'address_3', $this->address_3])
            ->andFilterWhere(['like', 'address_4', $this->address_4])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'zip_code', $this->zip_code])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'office_code', $this->office_code])
            ->andFilterWhere(['like', 'credit_term', $this->credit_term])
            ->andFilterWhere(['like', 'billing_currency', $this->billing_currency])
            ->andFilterWhere(['like', 'account_code', $this->account_code]);

        return $dataProvider;
    }
}

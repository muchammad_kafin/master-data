<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "item_type".
 *
 * @property integer $id
 * @property integer $analysis_code
 * @property string $name
 */
class ItemType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['analysis_code', 'name'], 'required'],
            [['analysis_code'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'analysis_code' => Yii::t('app', 'Analysis Code'),
            'name' => Yii::t('app', 'Name'),
        ];
    }
}

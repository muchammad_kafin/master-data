<?php

namespace app\models;

use Yii;
use app\models\Requestor;
use app\models\User;

/**
 * This is the model class for table "requestor_validator".
 *
 * @property string $id_validator
 * @property string $requestor
 */
class RequestorValidator extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'requestor_validator';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_validator', 'requestor'], 'required'],
            [['id_validator', 'requestor'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_validator' => Yii::t('app', 'Id Validator'),
            'requestor' => Yii::t('app', 'Requestor'),
        ];
    }
	
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getValids()
	{
		return $this->hasOne(User::className(), ['id' => 'id_validator']);
	}
	
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getRequests()
	{
		return $this->hasOne(Requestor::className(), ['id' => 'requestor']);
	}
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_list".
 *
 * @property integer $id
 * @property string $name
 */
class MasterList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }
	
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUrm()
	{
		return $this->hasOne(app\models\UserRoleMaster::className(), ['id_master' => 'id']);
	}
}

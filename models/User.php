<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use app\models\UserRoleMaster;
use app\models\MasterParty;
use app\models\MasterPartySearch;
use app\models\MasterClient;
use app\models\MasterClientSearch;
use app\models\MasterCountry;
use app\models\MasterCountrySearch;
use app\models\MasterDebtor;
use app\models\MasterDebtorSearch;
use app\models\MasterLocation;
use app\models\MasterLocationSearch;
use app\models\MasterRegion;
use app\models\MasterRegionSearch;
use app\models\MasterVendor;
use app\models\MasterVendorSearch;
use app\models\MasterItem;
use app\models\MasterItemSearch;
use app\models\MasterContract;
use app\models\MasterContractSearch;
use app\models\MasterEquipment;
use app\models\MasterEquipmentSearch;
use app\models\MasterProduct;
use app\models\MasterProductSearch;
use app\models\MasterPrinting;
use app\models\MasterPrintingSearch;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
	const ROLE_ADMIN = "admin";
	const ROLE_VALIDATOR = "validator";
	const ROLE_ACKNO = "acknowledges";
	const ROLE_SUPPORT = "support";

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			[['username','firstname','lastname'], 'required'],
			['username', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This username has already been taken.'],
			[['email'], 'email'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
	
	public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->setPassword($this->password_hash);
                $this->generateAuthKey();
                $this->generatePasswordResetToken();
            }elseif(Yii::$app->controller->action->id == 'resetpassword'){
				$this->setPassword($this->password_hash);
			}else{
				 unset($this->password_hash);
			}
            return true;
        }
        return false;
    }
	
	public static function getRoleArray(){
		$data=ArrayHelper::map(\app\models\MasterRole::find()->all(), 'id', 'name');
		return $data;
	}
	
	public static function getMasterArray() {
		$data=ArrayHelper::map(\app\models\MasterList::find()->all(), 'id', 'name');
		return $data;
	}
	
	public static function getUserArray() {
		$data=ArrayHelper::map(static::find()->all(), 'id', 'username');
		return $data;
	}
	
	public static function getRequestorArray(){
		$data=ArrayHelper::map(\app\models\Requestor::find()->all(), 'email_requestor', 'email_requestor');
		return $data;
	}
	
	public static function getValidator($role,$master) {
		$id = [];
		$username = [];
		$email = [];
		$validator = [];
		$data = static::findBySql('select u.id as id, u.email as email,u.username as username from user u RIGHT JOIN user_role_master urm on u.id = urm.id_user inner JOIN master_list m on urm.id_master = m.id INNER JOIN master_role mr on mr.id = urm.id_role where mr.name = :role and m.name = :master',[':master'=>$master, ':role'=>$role])->all();
		if($data){
			foreach ($data as $ids){
				$id[] = $ids->id;
			}
			$validator['id'] = implode(',', $id);
			
			foreach ($data as $usernames){
				$username[] = $usernames->username;
			}
			$validator['username'] = implode(',', $username);
			
			foreach ($data as $emails){
				$email[] = $emails->email;
			}
			$validator['email'] = implode(',', $email);
			
		}else{
			$validator = Yii::createObject([
				'class'=>'app\models\User',
				'attributes'=>['id'=>0,'username'=>"Need Assign user",'email'=>"Need Assign user"]
			]);
		}
		return $validator;
	}
	
	public static function getValid($requestor,$role,$master) {
		$id = [];
		$username = [];
		$email = [];
		$validator = [];
		$data = static::findBySql('
		select u.id as id, u.email as email,u.username as username from user u 
		INNER JOIN user_role_master urm on u.id = urm.id_user 
		inner JOIN master_list m on urm.id_master = m.id 
		INNER JOIN master_role mr on mr.id = urm.id_role
		INNER JOIN requestor_validator rv on rv.id_validator = u.id
		where mr.name = :role and m.name = :master and rv.requestor = :requestor',
		[':master'=>$master, ':role'=>$role, ':requestor' => $requestor])->all();
		if($data){
			foreach ($data as $ids){
				$id[] = $ids->id;
			}
			$validator['id'] = implode(',', $id);
			
			foreach ($data as $usernames){
				$username[] = $usernames->username;
			}
			$validator['username'] = implode(',', $username);
			
			foreach ($data as $emails){
				$email[] = $emails->email;
			}
			$validator['email'] = implode(',', $email);
			
		}else{
			$validator['id'] = '-';
			$validator['username'] = '-';
			$validator['email'] = '-';
		}
		return $validator;
	}
	
	public static function getUser($id) {
		$user = explode(',',$id);
		$valids =[];
		foreach($user as $key => $value) {
		   $valids[] = $value;
		}
		$valid = static::find()->where(['id'=>$valids])->all();
		$validsd =[];
		foreach($valid as $key => $value) {
		   $validsd[] = $value->username;
		}
		$results = implode(',',$validsd);
		
		return $results;
	}
	
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUrm()
	{
		return $this->hasOne(UserRoleMaster::className(), ['id_user' => 'id']);
	}
	
	public static function getSearch()
	{
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand('
			SELECT DISTINCT `requestor` AS `query` FROM `master_client` 
			UNION
			SELECT DISTINCT `requestor` AS `query` FROM `master_contract` 
			UNION
			SELECT DISTINCT `requestor` AS `query` FROM `master_country` 
			UNION
			SELECT DISTINCT `requestor` AS `query` FROM `master_debtor` 
			UNION
			SELECT DISTINCT `requestor` AS `query` FROM `master_equipment` 
			UNION
			SELECT DISTINCT `requestor` AS `query` FROM `master_item` 
			UNION
			SELECT DISTINCT `requestor` AS `query` FROM `master_location` 
			UNION
			SELECT DISTINCT `requestor` AS `query` FROM `master_party` 
			UNION
			SELECT DISTINCT `requestor` AS `query` FROM `master_printing` 
			UNION
			SELECT DISTINCT `requestor` AS `query` FROM `master_product` 
			UNION
			SELECT DISTINCT `requestor` AS `query` FROM `master_region` 
			UNION
			SELECT DISTINCT `requestor` AS `query` FROM `master_vendor`
			UNION
			SELECT DISTINCT `name` AS `query` FROM `master_client` 
			UNION
			SELECT DISTINCT vendor_name AS `query` FROM `master_contract` 
			UNION
			SELECT DISTINCT `name` AS `query` FROM `master_country` 
			UNION
			SELECT DISTINCT `name` AS `query` FROM `master_debtor` 
			UNION
			SELECT DISTINCT `name` AS `query` FROM `master_equipment` 
			UNION
			SELECT DISTINCT description AS `query` FROM `master_item` 
			UNION
			SELECT DISTINCT `name` AS `query` FROM `master_location` 
			UNION
			SELECT DISTINCT `name` AS `query` FROM `master_party` 
			UNION
			SELECT DISTINCT description AS `query` FROM `master_printing` 
			UNION
			SELECT DISTINCT `name` AS `query` FROM `master_product` 
			UNION
			SELECT DISTINCT `name` AS `query` FROM `master_region` 
			UNION
			SELECT DISTINCT `name` AS `query` FROM `master_vendor`');

		$dataCountry = $command->queryAll();
		return $dataCountry;
	}
	
	public static function getSearchcode()
	{
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand('
			SELECT DISTINCT `code` AS `query` FROM `client` where `status`=1
			UNION
			SELECT DISTINCT `code` AS `query` FROM `debtor` where `status`=1
			UNION
			SELECT DISTINCT `code` AS `query` FROM `party`  where `status`=1
			UNION
			SELECT DISTINCT `code` AS `query` FROM `vendor` where `status`=1
			UNION
			SELECT DISTINCT `name` AS `query` FROM `client` where `status`=1
			UNION
			SELECT DISTINCT `name` AS `query` FROM `debtor` where `status`=1
			UNION
			SELECT DISTINCT `name` AS `query` FROM `party` where `status`=1
			UNION
			SELECT DISTINCT `name` AS `query` FROM `vendor` where `status`=1
		');

		$dataCountry = $command->queryAll();
		return $dataCountry;
	}
	
	public static function getMastercode()
	{
		$dataCountry = [
				['id'=>'client','name'=>'Client'],
				['id'=>'debtor','name'=>'Debtor'],
				['id'=>'party','name'=>'Party'],
				['id'=>'vendor','name'=>'Vendor'],
			];
		return $dataCountry;
	}
	
}

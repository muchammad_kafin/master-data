<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\base\Behavior;
use yii\behaviors\TimestampBehavior;
use thyseus\validators\WordValidator;

/**
 * This is the model class for table "master_debtor".
 *
 * @property integer $id
 * @property string $no_doc
 * @property string $status
 * @property string $req_date
 * @property string $requestor
 * @property string $validator
 * @property string $approver
 * @property string $support
 * @property string $support_date
 * @property string $remark
 * @property string $code
 * @property string $name
 * @property string $special_intru
 * @property string $tax_address_code
 * @property string $npwp
 * @property string $debtor_group
 * @property string $address_type
 * @property string $address_1
 * @property string $address_2
 * @property string $address_3
 * @property string $address_4
 * @property string $country
 * @property string $state
 * @property string $city
 * @property string $zip_code
 * @property string $firstname
 * @property string $lastname
 * @property string $phone_number
 * @property string $email
 * @property string $office_code
 * @property string $credit_term
 * @property string $billing_currency
 * @property string $account_code
 * @property string $client_no
 * @property string $party_no
 */
class MasterDebtor extends \yii\db\ActiveRecord
{
	public $validator_name;
	public $acknowledges_name;
	public $support_name;
	public $country_name;
	public $state_name;
	public $city_name;
	public $getfile;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_debtor';
    }
	
	public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // if you're using datetime instead of UNIX timestamp:
                'value' => new Expression('NOW()'),
            ],
        ];
    }
	
	public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
				$this->requestor = strtoupper($this->requestor);
				$this->code = strtoupper($this->code);
				$this->name = strtoupper($this->name);
				$this->tax_address_code = strtoupper($this->tax_address_code);
				$this->npwp = strtoupper($this->npwp);
				$this->debtor_group = strtoupper($this->debtor_group);
				$this->address_type = strtoupper($this->address_type);
				$this->address_1 = strtoupper($this->address_1);
				$this->address_2 = strtoupper($this->address_2);
				$this->address_3 = strtoupper($this->address_3);
				$this->address_4 = strtoupper($this->address_4);
				$this->special_intru = strtoupper($this->special_intru);
				$this->country = strtoupper($this->country);
				$this->state = strtoupper($this->state);
				$this->city = strtoupper($this->city);
				$this->billing_currency = strtoupper($this->billing_currency);
				$this->account_code = strtoupper($this->account_code);
				$this->zip_code = strtoupper($this->zip_code);
				$this->account_name = strtoupper($this->account_name);
				$this->bank_name = strtoupper($this->bank_name);
				$this->branch = strtoupper($this->branch);
				$this->swift_code = strtoupper($this->swift_code);
				$this->remark = strtoupper($this->remark);
				$this->email_requestor = strtolower($this->email_requestor);
				// $map = implode(",", $this->office_code);
				// $this->office_code = $map;
				$country = $this->country;
				if($country){
					$this->country = strtoupper($this->country);
				}else{
					$this->country = strtoupper($this->country_name);
				}
			}elseif(Yii::$app->controller->action->id == 'canceldebtor'){
				$this->remark_cancel = strtoupper($this->remark_cancel);
			}elseif(Yii::$app->controller->action->id == 'updatedebtor' && Yii::$app->controller->id == 'validator'){
				$map = implode(",", $this->office_code);
				$this->office_code = $map;
			}
			return true;
		}
        
    }
	
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_doc', 'status', 'req_date', 'country_name', 'sector', 'requestor', 'validator', 'acknowledges', 'support', 'email_acknowledges','email_requestor','email_support','email_validator', 'code', 'tax_address_code', 'npwp', 'debtor_group', 'address_type', 'address_1', 'country', 'firstname', 'lastname', 'phone_number', 'phone_type', 'office_code', 'credit_term', 'billing_currency', 'name', 'account_code', 'bank_name', 'account_name', 'account_no'], 'required'],
            [['getfile', 'req_date', 'account_code', 'support_date', 'validator_name', 'acknowledges_name', 'support_name', 'city_name', 'state_name', 'zip_code','state','city','branch','swift_code'], 'safe'],
            [['remark', 'special_intru', 'address_1', 'address_2', 'address_3', 'address_4'], 'string'],
            [['no_doc'], 'string', 'max' => 255],
            [['no_doc'], 'unique'],
			[['remark_cancel'],'required','on' => 'cancel_debtor'],
			[['support_date'],'required','on' => 'support_debtor'],
			[['email','email_requestor'], 'email'],
			[['email_validator','email_requestor','email_acknowledges','email_support'], 'string', 'min' => 5],
			[['code','debtor_group','tax_address_code'], 'string', 'min' => 11, 'on'=>'validator_debtor'],
			[['address_1','address_2','address_3','address_4'], 'string', 'max' => 50],
			['getfile',  'file', 'maxSize'=>'300000', 'skipOnEmpty' => true, 'maxFiles' => 5],
			// ['requestor', 'in', 'range' => $this->getBlacklist(),'not'=>true],
			[['requestor'], WordValidator::className(), 'blacklist' => $this->getBlacklist(),'messages'  => ['blacklist'=>'Not Valid']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'no_doc' => Yii::t('app', 'No Doc'),
            'status' => Yii::t('app', 'Status'),
            'req_date' => Yii::t('app', 'Req Date'),
            'requestor' => Yii::t('app', 'Requestor'),
            'validator' => Yii::t('app', 'Validator'),
            'acknowledges' => Yii::t('app', 'Acknowledges'),
            'support' => Yii::t('app', 'Support'),
            'support_date' => Yii::t('app', 'Support Date'),
            'remark' => Yii::t('app', 'Remark'),
            'remark_cancel' => Yii::t('app', 'Remark Reject'),
            'code' => Yii::t('app', 'Code'),
            'name' => Yii::t('app', 'Name'),
            'special_intru' => Yii::t('app', 'Special Instructions'),
            'tax_address_code' => Yii::t('app', 'Tax Address Code'),
            'npwp' => Yii::t('app', 'Npwp'),
            'debtor_group' => Yii::t('app', 'Debtor Group'),
            'address_type' => Yii::t('app', 'Address Type'),
            'address_1' => Yii::t('app', 'Address 1'),
            'address_2' => Yii::t('app', 'Address 2'),
            'address_3' => Yii::t('app', 'Address 3'),
            'address_4' => Yii::t('app', 'Address 4'),
            'country' => Yii::t('app', 'Country'),
            'state' => Yii::t('app', 'State'),
            'city' => Yii::t('app', 'City'),
            'zip_code' => Yii::t('app', 'Zip Code'),
            'firstname' => Yii::t('app', 'Firstname'),
            'lastname' => Yii::t('app', 'Lastname'),
            'phone_type' => Yii::t('app', 'Phone Type'),
            'phone_number' => Yii::t('app', 'Phone Number'),
            'email' => Yii::t('app', 'Email'),
            'office_code' => Yii::t('app', 'Office Code'),
            'credit_term' => Yii::t('app', 'Credit Term'),
            'billing_currency' => Yii::t('app', 'Billing Currency'),
            'account_code' => Yii::t('app', 'Account Code'),
			'bank_name' => Yii::t('app', 'Bank Name'),
            'account_name' => Yii::t('app', 'Bank Account Name'),
            'branch' => Yii::t('app', 'Bank Branch'),
            'swift_code' => Yii::t('app', 'Bank Swift Code'),
            'account_no' => Yii::t('app', 'Bank Account Number'),
            // 'client_no' => Yii::t('app', 'Client'),
            // 'party_no' => Yii::t('app', 'Party'),
            'getfile' => Yii::t('app', 'Upload File'),
        ];
    }
	
	public function getDayArray(){		
		$listData=\yii\helpers\ArrayHelper::map(\app\models\CreditTerm::find()->all(),'code','description');
		return $listData;
	}
	
	public function getSecArray(){		
		$listData=\yii\helpers\ArrayHelper::map(\app\models\Sector::find()->all(),'code','name');
		return $listData;
	}
	
	private function getBlacklist(){
		$list = \yii\helpers\ArrayHelper::map(Yii::$app->db->CreateCommand('select name from blacklist')->queryAll(),'name','name');
		return $list;
	}
}

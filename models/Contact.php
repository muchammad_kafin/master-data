<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contact".
 *
 * @property integer $id
 * @property integer $id_vendor
 * @property string $firstname
 * @property string $lastname
 * @property integer $id_debtor
 * @property integer $id_party
 * @property integer $id_client
 *
 * @property MasterClient $idClient
 * @property MasterDebtor $idDebtor
 * @property MasterParty $idParty
 * @property MasterVendor $idVendor
 * @property Phone[] $phones
 */
class Contact extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contact';
    }
	
	public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
				$this->lastname = strtoupper($this->lastname);
				$this->firstname = strtoupper($this->firstname);
			}
			return true;
		}
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_vendor', 'id_debtor', 'id_party', 'id_client'], 'integer'],
            [['firstname'], 'required'],
            [['firstname', 'lastname'], 'string', 'max' => 255],
            [['id_client'], 'exist', 'skipOnError' => true, 'targetClass' => MasterClient::className(), 'targetAttribute' => ['id_client' => 'id']],
            [['id_debtor'], 'exist', 'skipOnError' => true, 'targetClass' => MasterDebtor::className(), 'targetAttribute' => ['id_debtor' => 'id']],
            [['id_party'], 'exist', 'skipOnError' => true, 'targetClass' => MasterParty::className(), 'targetAttribute' => ['id_party' => 'id']],
            [['id_vendor'], 'exist', 'skipOnError' => true, 'targetClass' => MasterVendor::className(), 'targetAttribute' => ['id_vendor' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_vendor' => Yii::t('app', 'Id Vendor'),
            'firstname' => Yii::t('app', 'Firstname'),
            'lastname' => Yii::t('app', 'Lastname'),
            'id_debtor' => Yii::t('app', 'Id Debtor'),
            'id_party' => Yii::t('app', 'Id Party'),
            'id_client' => Yii::t('app', 'Id Client'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdClient()
    {
        return $this->hasOne(MasterClient::className(), ['id' => 'id_client']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdDebtor()
    {
        return $this->hasOne(MasterDebtor::className(), ['id' => 'id_debtor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdParty()
    {
        return $this->hasOne(MasterParty::className(), ['id' => 'id_party']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdVendor()
    {
        return $this->hasOne(MasterVendor::className(), ['id' => 'id_vendor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhones()
    {
        return $this->hasMany(Phone::className(), ['id_contact' => 'id']);
    }
}

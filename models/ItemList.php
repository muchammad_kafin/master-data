<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "item_list".
 *
 * @property integer $id
 * @property string $item_code
 * @property string $description
 */
class ItemList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_code', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'item_code' => Yii::t('app', 'Item Code'),
            'description' => Yii::t('app', 'Description'),
        ];
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "charge_list".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $job_type
 * @property string $office_map
 * @property string $printing_code
 * @property string $printing_sequence
 * @property string $revenue_posting
 * @property string $cost_posting
 * @property string $currency
 * @property string $reimbursable
 * @property string $commissionable
 * @property string $profit_shared
 * @property string $tax_group
 * @property string $applicable_for
 * @property string $based_on
 */
class ChargeList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'charge_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'name', 'job_type', 'office_map', 'printing_code', 'printing_sequence', 'revenue_posting', 'cost_posting', 'currency', 'reimbursable', 'commissionable', 'profit_shared', 'tax_group', 'applicable_for', 'based_on'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'code' => Yii::t('app', 'Code'),
            'name' => Yii::t('app', 'Name'),
            'job_type' => Yii::t('app', 'Job Type'),
            'office_map' => Yii::t('app', 'Office Map'),
            'printing_code' => Yii::t('app', 'Printing Code'),
            'printing_sequence' => Yii::t('app', 'Printing Sequence'),
            'revenue_posting' => Yii::t('app', 'Revenue Posting'),
            'cost_posting' => Yii::t('app', 'Cost Posting'),
            'currency' => Yii::t('app', 'Currency'),
            'reimbursable' => Yii::t('app', 'Reimbursable'),
            'commissionable' => Yii::t('app', 'Commissionable'),
            'profit_shared' => Yii::t('app', 'Profit Shared'),
            'tax_group' => Yii::t('app', 'Tax Group'),
            'applicable_for' => Yii::t('app', 'Applicable For'),
            'based_on' => Yii::t('app', 'Based On'),
        ];
    }
}

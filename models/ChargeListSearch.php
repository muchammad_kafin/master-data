<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ChargeList;

/**
 * ChargeListSearch represents the model behind the search form about `app\models\ChargeList`.
 */
class ChargeListSearch extends ChargeList
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['code', 'name', 'job_type', 'office_map', 'printing_code', 'printing_sequence', 'revenue_posting', 'cost_posting', 'currency', 'reimbursable', 'commissionable', 'profit_shared', 'tax_group', 'applicable_for', 'based_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ChargeList::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'job_type', $this->job_type])
            ->andFilterWhere(['like', 'office_map', $this->office_map])
            ->andFilterWhere(['like', 'printing_code', $this->printing_code])
            ->andFilterWhere(['like', 'printing_sequence', $this->printing_sequence])
            ->andFilterWhere(['like', 'revenue_posting', $this->revenue_posting])
            ->andFilterWhere(['like', 'cost_posting', $this->cost_posting])
            ->andFilterWhere(['like', 'currency', $this->currency])
            ->andFilterWhere(['like', 'reimbursable', $this->reimbursable])
            ->andFilterWhere(['like', 'commissionable', $this->commissionable])
            ->andFilterWhere(['like', 'profit_shared', $this->profit_shared])
            ->andFilterWhere(['like', 'tax_group', $this->tax_group])
            ->andFilterWhere(['like', 'applicable_for', $this->applicable_for])
            ->andFilterWhere(['like', 'based_on', $this->based_on]);

        return $dataProvider;
    }
}

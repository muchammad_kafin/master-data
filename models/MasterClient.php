<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\AttributeBehavior;
use yii\db\Expression;
use yii\base\Behavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "master_client".
 *
 * @property integer $id
 * @property string $no_doc
 * @property string $status
 * @property string $req_date
 * @property string $requestor
 * @property string $validator
 * @property string $approver
 * @property string $support
 * @property string $support_date
 * @property string $remark
 * @property string $code
 * @property string $name
 * @property string $address_1
 * @property string $address_2
 * @property string $country
 * @property string $state
 * @property string $city
 * @property string $zip_code
 * @property string $web
 * @property string $firstname
 * @property string $lastname
 * @property string $phone_number
 * @property string $email
 * @property string $base_currency
 */
class MasterClient extends \yii\db\ActiveRecord
{
	public $validator_name;
	public $acknowledges_name;
	public $support_name;
	public $country_name;
	public $state_name;
	public $city_name;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_client';
    }
	
	public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // if you're using datetime instead of UNIX timestamp:
                'value' => new Expression('NOW()'),
            ],
        ];
    }
	
	public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
				$this->requestor = strtoupper($this->requestor);
				$this->code = strtoupper($this->code);
				$this->name = strtoupper($this->name);
				$this->address_1 = strtoupper($this->address_1);
				$this->address_2 = strtoupper($this->address_2);
				$this->country = strtoupper($this->country);
				$this->state = strtoupper($this->state);
				$this->city = strtoupper($this->city);
				$this->phone_type = strtoupper($this->phone_type);
				$this->base_currency = strtoupper($this->base_currency);
				$this->remark = strtoupper($this->remark);
				$this->lastname = strtoupper($this->lastname);
				$this->firstname = strtoupper($this->firstname);
				$this->web = strtoupper($this->web);
				$this->zip_code = strtoupper($this->zip_code);
				$this->email = strtoupper($this->email);
				$this->email_requestor = strtolower($this->email_requestor);
				$country = $this->country;
				if($country){
					$this->country = strtoupper($this->country);
				}else{
					$this->country = strtoupper($this->country_name);
				}
			}
			return true;
		}
        
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_doc', 'status', 'req_date', 'country_name', 'requestor', 'acknowledges', 'support','email_requestor' , 'email_support','email_validator', 'validator',  'code', 'name', 'address_1', 'country', 'base_currency','email_acknowledges'], 'required'],
            [['req_date', 'support_date', 'validator_name',  'acknowledges_name', 'support_name', 'city_name', 'state_name', 'city', 'state', 'zip_code', 'web'], 'safe'],
            [['remark', 'remark_cancel', 'address_1', 'address_2'], 'string'],
            [['no_doc'], 'string', 'max' => 255],
            [['status', 'requestor', 'support', 'code', 'name', 'country', 'state', 'city', 'zip_code', 'web', 'firstname', 'lastname', 'phone_number', 'email', 'base_currency'], 'string', 'max' => 50],
            [['no_doc'], 'unique'],
			[['remark_cancel'],'required','on' => 'cancel_client'],
			[['email_validator','email_requestor','email_acknowledges','email_support'], 'string', 'min' => 5],
			// ['web', 'url', 'defaultScheme' => 'http'],
			[['support_date'],'required','on' => 'support_client'],
			[['email_requestor'], 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'no_doc' => Yii::t('app', 'No Doc'),
            'status' => Yii::t('app', 'Status'),
            'req_date' => Yii::t('app', 'Req Date'),
            'requestor' => Yii::t('app', 'Requestor'),
            'validator' => Yii::t('app', 'Validator'),
            'acknowledges' => Yii::t('app', 'Acknowledges'),
            'support' => Yii::t('app', 'Support'),
            'support_date' => Yii::t('app', 'Support Date'),
            'remark' => Yii::t('app', 'Remark'),
            'code' => Yii::t('app', 'Code'),
            'name' => Yii::t('app', 'Name'),
            'address_1' => Yii::t('app', 'Address 1'),
            'address_2' => Yii::t('app', 'Address 2'),
            'country' => Yii::t('app', 'Country'),
            'state' => Yii::t('app', 'State'),
            'city' => Yii::t('app', 'City'),
            'zip_code' => Yii::t('app', 'Zip Code'),
            'web' => Yii::t('app', 'Web'),
            'firstname' => Yii::t('app', 'Firstname'),
            'lastname' => Yii::t('app', 'Lastname'),
            'phone_type' => Yii::t('app', 'Phone Type'),
            'phone_number' => Yii::t('app', 'Phone Number'),
            'email' => Yii::t('app', 'Email'),
            'base_currency' => Yii::t('app', 'Base Currency'),
        ];
    }
}

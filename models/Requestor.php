<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "requestor".
 *
 * @property integer $id
 * @property string $email_requestor
 */
class Requestor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'requestor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email_requestor'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'email_requestor' => Yii::t('app', 'Email Requestor'),
        ];
    }
}

<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MasterItem;

/**
 * MasterItemSearch represents the model behind the search form about `app\models\MasterItem`.
 */
class MasterItemSearch extends MasterItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['no_doc', 'status', 'req_date', 'requestor', 'email_requestor', 'validator', 'email_validator', 'acknowledges', 'email_acknowledges', 'support', 'email_support', 'support_date', 'remark', 'remark_cancel', 'item_code', 'description', 'lookup_code', 'item_type', 'base_unit', 'base_location', 'label_code', 'unit_purchase'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterItem::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort' => [
				'defaultOrder' => [
					'req_date' => SORT_DESC,
				]
			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
		if(Yii::$app->controller->id == "finish"){
			$query->andFilterWhere(['like', 'status', $this->status]);
		}elseif(Yii::$app->user->identity->username == "admin"){
			$query->andFilterWhere(['like', 'status', $this->status]);
		}else{
			if(Yii::$app->controller->id == "validator"){
				$query->andFilterWhere(['and',['!=', 'status', 'reject'],['=', 'status', "validator"]]);
				$query->andFilterWhere(['like','email_validator',Yii::$app->user->identity->email]);
			}elseif(Yii::$app->controller->id == "approver"){
				$query->andFilterWhere(['and',['!=', 'status', 'reject'],['=', 'status', "approver"]]);
				$query->andFilterWhere(['like','email_validator',Yii::$app->user->identity->email]);
			}elseif(Yii::$app->controller->id == "support"){
				$query->andFilterWhere(['and',['!=', 'status', 'reject'],['=', 'status', "support"]]);
				$query->andFilterWhere(['like','email_support',Yii::$app->user->identity->email]);
			}
		}

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'req_date' => $this->req_date,
            'support_date' => $this->support_date,
        ]);

        $query->andFilterWhere(['like', 'no_doc', $this->no_doc])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'requestor', $this->requestor])
            ->andFilterWhere(['like', 'email_requestor', $this->email_requestor])
            ->andFilterWhere(['like', 'validator', $this->validator])
            ->andFilterWhere(['like', 'email_validator', $this->email_validator])
            ->andFilterWhere(['like', 'acknowledges', $this->acknowledges])
            ->andFilterWhere(['like', 'email_acknowledges', $this->email_acknowledges])
            ->andFilterWhere(['like', 'support', $this->support])
            ->andFilterWhere(['like', 'email_support', $this->email_support])
            ->andFilterWhere(['like', 'remark', $this->remark])
            ->andFilterWhere(['like', 'remark_cancel', $this->remark_cancel])
            ->andFilterWhere(['like', 'item_code', $this->item_code])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'lookup_code', $this->lookup_code])
            ->andFilterWhere(['like', 'item_type', $this->item_type])
            ->andFilterWhere(['like', 'base_unit', $this->base_unit])
            ->andFilterWhere(['like', 'base_location', $this->base_location])
            ->andFilterWhere(['like', 'label_code', $this->label_code])
            ->andFilterWhere(['like', 'unit_purchase', $this->unit_purchase]);

        return $dataProvider;
    }
}

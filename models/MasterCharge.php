<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\base\Behavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "master_charge".
 *
 * @property integer $id
 * @property string $no_doc
 * @property string $status
 * @property string $req_date
 * @property string $requestor
 * @property string $email_requestor
 * @property string $validator
 * @property string $email_validator
 * @property string $acknowledges
 * @property string $email_acknowledges
 * @property string $support
 * @property string $email_support
 * @property string $support_date
 * @property string $remark
 * @property string $remark_cancel
 * @property string $code
 * @property string $name
 * @property string $job_type
 * @property string $office_map
 * @property string $printing_code
 * @property string $printing_sequence
 * @property string $revenue_posting
 * @property string $cost_posting
 * @property string $currency
 * @property string $reimbursable
 * @property string $commissionable
 * @property string $profit_shared
 * @property string $tax_group
 * @property string $applicable_for
 * @property string $based_on
 * @property string $created_at
 * @property string $updated_at
 */
class MasterCharge extends \yii\db\ActiveRecord
{
	public $validator_name;
	public $acknowledges_name;
	public $support_name;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_charge';
    }
	
	public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // if you're using datetime instead of UNIX timestamp:
                'value' => new Expression('NOW()'),
            ],
        ];
    }
	
	public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
				$this->requestor = strtoupper($this->requestor);
				$this->code = strtoupper($this->code);
				$this->name = strtoupper($this->name);
				$this->remark = strtoupper($this->remark);
				$this->tax_group = strtoupper($this->tax_group);
				$this->printing_code = strtoupper($this->printing_code);
				$this->printing_sequence = strtoupper($this->printing_sequence);
				$this->revenue_posting = strtoupper($this->revenue_posting);
				$this->cost_posting = strtoupper($this->cost_posting);
				$this->support = strtoupper($this->support);
				$this->email_requestor = strtolower($this->email_requestor);
				$job = implode(",", $this->job_type);
				$this->job_type = $job;
			}elseif(Yii::$app->controller->action->id == 'cancelcharge'){
				$this->remark_cancel = strtoupper($this->remark_cancel);
			}elseif(Yii::$app->controller->action->id == 'updatecharge' && Yii::$app->controller->id == 'validator'){
				$map = implode(",", $this->office_map);
				$this->office_map = $map;
				$job = implode(",", $this->job_type);
				$this->job_type = $job;
			}
			return true;
		}
        
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_doc', 'status', 'req_date', 'requestor', 'email_requestor', 'validator', 'email_validator', 'code', 'name', 'job_type', 'office_map', 'tax_group', 'applicable_for', 'based_on', 'acknowledges', 'support', 'email_acknowledges', 'email_support'], 'required'],
            [['req_date', 'support_date', 'validator_name', 'acknowledges_name', 'support_name'], 'safe'],
            [['remark', 'remark_cancel'], 'string'],
            [['no_doc', 'email_requestor', 'email_validator', 'email_acknowledges', 'email_support', 'code', 'name', 'printing_code', 'printing_sequence', 'revenue_posting', 'cost_posting', 'currency', 'reimbursable', 'commissionable', 'profit_shared', 'tax_group', 'applicable_for', 'based_on'], 'string', 'max' => 255],
            [['no_doc'], 'unique'],
			[['remark_cancel'],'required','on' => 'cancel_charge'],
			[['support_date'],'required','on' => 'support_charge'],
			[['email_validator','email_requestor','email_acknowledges','email_support'], 'string', 'min' => 5],
			[['code'], 'string', 'min' => 11, 'on'=>'validator_charge'],
			[['email_requestor'], 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'no_doc' => Yii::t('app', 'No Doc'),
            'status' => Yii::t('app', 'Status'),
            'req_date' => Yii::t('app', 'Req Date'),
            'requestor' => Yii::t('app', 'Requestor'),
            'email_requestor' => Yii::t('app', 'Email Requestor'),
            'validator' => Yii::t('app', 'Validator'),
            'email_validator' => Yii::t('app', 'Email Validator'),
            'acknowledges' => Yii::t('app', 'Acknowledges'),
            'email_acknowledges' => Yii::t('app', 'Email Acknowledges'),
            'support' => Yii::t('app', 'Support'),
            'email_support' => Yii::t('app', 'Email Support'),
            'support_date' => Yii::t('app', 'Support Date'),
            'remark' => Yii::t('app', 'Remark'),
            'remark_cancel' => Yii::t('app', 'Remark Cancel'),
            'code' => Yii::t('app', 'Code'),
            'name' => Yii::t('app', 'Name'),
            'job_type' => Yii::t('app', 'Job Type'),
            'office_map' => Yii::t('app', 'Office Map'),
            'printing_code' => Yii::t('app', 'Printing Code'),
            'printing_sequence' => Yii::t('app', 'Printing Sequence'),
            'revenue_posting' => Yii::t('app', 'Revenue Posting'),
            'cost_posting' => Yii::t('app', 'Cost Posting'),
            'currency' => Yii::t('app', 'Currency'),
            'reimbursable' => Yii::t('app', 'Reimbursable'),
            'commissionable' => Yii::t('app', 'Commissionable'),
            'profit_shared' => Yii::t('app', 'Profit Shared'),
            'tax_group' => Yii::t('app', 'Tax Group'),
            'applicable_for' => Yii::t('app', 'Applicable For'),
            'based_on' => Yii::t('app', 'Based On'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}

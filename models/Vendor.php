<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vendor".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 */
class Vendor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vendor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'name'], 'required'],
            [['code', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'code' => Yii::t('app', 'Supplier Code'),
            'name' => Yii::t('app', 'Name'),
        ];
    }
	
	public static function getVendor($code) {
		$data=\app\models\Vendor::find()
		   ->where(['code'=>$code])
		   ->select(['code as id','name'])->asArray()->all();

		return $data;
	}
	
	public static function getVendor1($code) {
		$data=\app\models\Vendor::find()
		   ->where(['code'=>$code])
		   ->select(['name'])->asArray()->one();

		return $data;
	}
	
	public function getVendorcode()
	{
			return $this->code.' <-> '.$this->name;
	}
}

<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\base\Behavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "master_equipment".
 *
 * @property integer $id
 * @property string $no_doc
 * @property string $status
 * @property string $req_date
 * @property string $requestor
 * @property string $email_requestor
 * @property string $validator
 * @property string $email_validator
 * @property string $acknowledges
 * @property string $email_acknowledges
 * @property string $support
 * @property string $email_support
 * @property string $support_date
 * @property string $remark
 * @property string $remark_cancel
 * @property string $code
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 */
class MasterEquipment extends \yii\db\ActiveRecord
{
	public $validator_name;
	public $acknowledges_name;
	public $support_name;
	public $getfile;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_equipment';
    }
	
	public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // if you're using datetime instead of UNIX timestamp:
                'value' => new Expression('NOW()'),
            ],
        ];
    }
	
	public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
				$this->requestor = strtoupper($this->requestor);
				$this->code = strtoupper($this->code);
				$this->name = strtoupper($this->name);
				$this->remark = strtoupper($this->remark);
				$this->support = strtoupper($this->support);
				$this->email_requestor = strtolower($this->email_requestor);
			}elseif(Yii::$app->controller->action->id == 'cancelequipment'){
				$this->remark_cancel = strtoupper($this->remark_cancel);
			}
			return true;
		}
        
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_doc', 'status', 'req_date', 'requestor', 'email_requestor', 'validator', 'email_validator', 'code'], 'required'],
            [['req_date', 'support_date', 'name', 'validator_name', 'acknowledges_name', 'support_name', 'getfile'], 'safe'],
            [['remark', 'remark_cancel'], 'string'],
            [['no_doc', 'email_requestor', 'email_validator', 'email_acknowledges', 'email_support', 'code', 'name'], 'string', 'max' => 255],
            [['status', 'requestor', 'validator', 'acknowledges', 'support'], 'string', 'max' => 50],
            [['no_doc'], 'unique'],
			[['remark_cancel'],'required','on' => 'cancel_equipment'],
			[['support_date'],'required','on' => 'support_equipment'],
			[['email_requestor'], 'email'],
			[['email_validator','email_requestor','email_acknowledges','email_support'], 'string', 'min' => 5],
			['getfile',  'file', 'maxSize'=>'300000', 'skipOnEmpty' => true, 'maxFiles' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'no_doc' => Yii::t('app', 'No Doc'),
            'status' => Yii::t('app', 'Status'),
            'req_date' => Yii::t('app', 'Req Date'),
            'requestor' => Yii::t('app', 'Requestor'),
            'email_requestor' => Yii::t('app', 'Email Requestor'),
            'validator' => Yii::t('app', 'Validator'),
            'email_validator' => Yii::t('app', 'Email Validator'),
            'acknowledges' => Yii::t('app', 'Acknowledges'),
            'email_acknowledges' => Yii::t('app', 'Email Acknowledges'),
            'support' => Yii::t('app', 'Support'),
            'email_support' => Yii::t('app', 'Email Support'),
            'support_date' => Yii::t('app', 'Support Date'),
            'remark' => Yii::t('app', 'Remark'),
            'remark_cancel' => Yii::t('app', 'Remark Cancel'),
            'code' => Yii::t('app', 'Code'),
            'name' => Yii::t('app', 'Name'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'getfile' => Yii::t('app', 'Upload File'),
        ];
    }
}

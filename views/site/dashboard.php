<?php
use sjaakp\gcharts\PieChart;
use sjaakp\gcharts\ColumnChart;
use sjaakp\gcharts\LineChart;
use sjaakp\gcharts\AreaChart;
use sjaakp\gcharts\BarChart;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use kartik\export\ExportMenu;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\widgets\Pjax;

/* @var $this yii\web\View */

$this->title = 'Master Data Management';
?>
<div class="site-index">
	<h1><i class="fa fa-pie-chart"></i>  Dashboard Overview</h1>
	<div class="row">
	<?= Html::beginForm(['site/dashboard'], 'get', ['class' => 'form-inline']); ?>
		<div class="col-lg-12">
			<div class="col-lg-3">
				<?php echo '<label>Start Date</label>';?>
				<?php
					echo yii\jui\DatePicker::widget( [
					// 'pluginOptions' => [
					// 'autoClose' => true,
					// 'format' => 'yyyy-mm-dd',
					// ],
					'options' => ['class' => 'form-control'],
					'value' => $start,
					'name' => 'start',
					'dateFormat' => 'yyyy-MM-dd',
					'clientOptions' =>[
						'numberOfMonths' => 1,
						'changeMonth' => true,
					]
				]) ?>
			</div>
			<div class="col-lg-4">
				<?php echo '<label>End Date</label>';?>
				<?php
					echo yii\jui\DatePicker::widget( [
					// 'pluginOptions' => [
					// 'autoClose' => true,
					// 'format' => 'yyyy-mm-dd',
					// ],
					'options' => ['class' => 'form-control'],
					'value' => $end,
					'name' => 'end',
					'dateFormat' => 'yyyy-MM-dd',
					'clientOptions' =>[
						'numberOfMonths' => 1,
						'changeMonth' => true,
					]
				]) ?>
				<?= Html::submitButton('Check', ['class' => 'btn btn-sm btn-primary']) ?>
			</div>
		</div>
		<?= Html::endForm() ?>
	</div>
	<p></p>
	<div class="row">
		<div class="col-lg-6">
			<div class="box box-danger">
				<div class="box-header">
					<h3 class="box-title"><b>New Request</b></h3>
				</div>
				<div class="box-body">
					<?php
						echo GridView::widget([
							'columns'=>[
								[
									'label' => 'Master List',
									'attribute'=>'master',
								],
								 [
									'label' => 'Created',
									'value'=>'created',
								 ],
								 [
									'label' => 'Validator',
									'value'=>'validator',
								 ],
								 [
									'label' => 'Support',
									'value'=>'support',
								 ],
								 [
									'label' => 'Finish',
									'value'=>'finish',
								 ],
								 [
									'label' => 'Reject',
									'attribute'=>'reject',
								 ],
							],
							'dataProvider'=>$dataProvider,
						]);
					?>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="box box-danger">
				<div class="box-header">
					<h3 class="box-title"><b>Update Request</b></h3>
				</div>
				<div class="box-body">
					<?php
						echo GridView::widget([
							'columns'=>[
								[
									'label' => 'Master List',
									'attribute'=>'master',
								],
								 [
									'label' => 'Created',
									'value'=>'created',
								 ],
								 [
									'label' => 'Validator',
									'value'=>'validator',
								 ],
								 [
									'label' => 'Support',
									'value'=>'support',
								 ],
								 [
									'label' => 'Finish',
									'value'=>'finish',
								 ],
								 [
									'label' => 'Reject',
									'attribute'=>'reject',
								 ],
							],
							'dataProvider'=>$updateProvider,
						]);
					?>
				</div>
			</div>
		</div>
	</div>
</div>

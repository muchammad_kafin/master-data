<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Test Email';
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        This is the test Email page.
    </p>
	<?= Html::a('Send', ['/site/email'], ['class'=>'btn btn-primary']) ?>
</div>

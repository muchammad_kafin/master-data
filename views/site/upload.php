<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
use yii\helpers\ArrayHelper;
use kartik\widgets\Typeahead;
use kartik\widgets\TypeaheadBasic;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
use yii\widgets\MaskedInput;
use yii\bootstrap\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\jui\AutoComplete;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\masterdebtor */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Manual Upload';
?>
<style> 
input[type=text] {
	text-transform: uppercase;
}
textarea {
	text-transform: uppercase;
}
</style>
<div class="master-vendor-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>
	
	<div class="row">
		<div class="col-lg-3">
		<p><strong>Please Fill with Capital Letter !</strong></p>
		</div>
		<div class="col-lg-3">
		</div>
		<div class="col-lg-2">
		</div>
		<div class="col-lg-12">
			<div class="row">
				<div class="col-lg-2">
					<?= Html::activeLabel($model, 'id_form'); ?>		
				</div>
				<div class="col-lg-3">
					<?= $form->field($model, 'id_form')->textInput(['maxlength' => true])->label(false) ?>
				</div>
			</div>
			
			<?= $form->field($model, 'getfile[]')->widget(\kartik\widgets\FileInput::classname(), [
					'options' => ['multiple' => true],
					'pluginOptions' => ['previewFileType' => 'any','showUpload' => false]
			]); ?>

			<div class="form-group">
				<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Upload') : Yii::t('app', 'Upload'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
			</div>

			<?php ActiveForm::end(); ?>

		</div>
	</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PartyType */

$this->title = Yii::t('app', 'Create Party Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Party Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="party-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

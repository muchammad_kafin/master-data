<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MasterVendor */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Master Vendors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-vendor-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'no_doc',
            'status',
            'req_date',
            'requestor',
            'email_requestor:email',
            'validator',
            'email_validator:email',
            'approver',
            'email_approver:email',
            'support',
            'email_support:email',
            'support_date',
            'remark:ntext',
            'remark_cancel:ntext',
            'code',
            'name',
            'special_intru:ntext',
            'tax_address_code',
            'npwp',
            'vendor_group',
            'address_type',
            'address_1:ntext',
            'address_2:ntext',
            'address_3:ntext',
            'address_4:ntext',
            'country',
            'state',
            'city',
            'zip_code',
            'firstname',
            'lastname',
            'phone_number',
            'email:email',
            'office_code',
            'credit_term',
            'billing_currency',
            'account_code',
        ],
    ]) ?>

</div>

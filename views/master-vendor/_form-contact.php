<?php
 
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
use yii\helpers\ArrayHelper;
use kartik\widgets\Typeahead;
use kartik\widgets\TypeaheadBasic;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
use yii\widgets\MaskedInput;
use yii\bootstrap\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
?>
 
<?php DynamicFormWidget::begin([
    'widgetContainer' => 'dynamicform_inner',
    'widgetBody' => '.container-rooms',
    'widgetItem' => '.room-item',
    'limit' => 3,
    'min' => 1,
    'insertButton' => '.add-room',
    'deleteButton' => '.remove-room',
    'model' => $modelsPhone[0],
    'formId' => 'dynamic-form',
    'formFields' => [
        'phone_type',
        'phone_number',
    ],
]); ?>
<table class="table table-bordered">
    <thead>
        <tr>
            <th>Phone</th>
            <th class="text-center">
                <button type="button" class="add-room btn btn-success btn-xs"><span class="glyphicon glyphicon-plus"></span></button>
            </th>
        </tr>
    </thead>
    <tbody class="container-rooms">
    <?php foreach ($modelsPhone as $indexRoom => $modelPhone): ?>
        <tr class="room-item">
            <td class="vcenter">
                <?php
                    // necessary for update action.
                    if (!$modelPhone->isNewRecord) {
                        echo Html::activeHiddenInput($modelPhone, "[{$indexHouse}][{$indexRoom}]id");
                    }
                ?>
                <?= $form->field($modelPhone, "[{$indexHouse}][{$indexRoom}]phone_type")->textInput(['maxlength' => true]) ?>
                <?= $form->field($modelPhone, "[{$indexHouse}][{$indexRoom}]phone_number")->textInput(['maxlength' => true]) ?>
            </td>
            <td class="text-center vcenter" style="width: 90px;">
                <button type="button" class="remove-room btn btn-danger btn-xs"><span class="glyphicon glyphicon-minus"></span></button>
            </td>
        </tr>
     <?php endforeach; ?>
    </tbody>
</table>
<?php DynamicFormWidget::end(); ?>
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MasterVendorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-vendor-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'no_doc') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'req_date') ?>

    <?= $form->field($model, 'requestor') ?>

    <?php // echo $form->field($model, 'email_requestor') ?>

    <?php // echo $form->field($model, 'validator') ?>

    <?php // echo $form->field($model, 'email_validator') ?>

    <?php // echo $form->field($model, 'approver') ?>

    <?php // echo $form->field($model, 'email_approver') ?>

    <?php // echo $form->field($model, 'support') ?>

    <?php // echo $form->field($model, 'email_support') ?>

    <?php // echo $form->field($model, 'support_date') ?>

    <?php // echo $form->field($model, 'remark') ?>

    <?php // echo $form->field($model, 'remark_cancel') ?>

    <?php // echo $form->field($model, 'code') ?>

    <?php // echo $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'special_intru') ?>

    <?php // echo $form->field($model, 'tax_address_code') ?>

    <?php // echo $form->field($model, 'npwp') ?>

    <?php // echo $form->field($model, 'vendor_group') ?>

    <?php // echo $form->field($model, 'address_type') ?>

    <?php // echo $form->field($model, 'address_1') ?>

    <?php // echo $form->field($model, 'address_2') ?>

    <?php // echo $form->field($model, 'address_3') ?>

    <?php // echo $form->field($model, 'address_4') ?>

    <?php // echo $form->field($model, 'country') ?>

    <?php // echo $form->field($model, 'state') ?>

    <?php // echo $form->field($model, 'city') ?>

    <?php // echo $form->field($model, 'zip_code') ?>

    <?php // echo $form->field($model, 'firstname') ?>

    <?php // echo $form->field($model, 'lastname') ?>

    <?php // echo $form->field($model, 'phone_number') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'office_code') ?>

    <?php // echo $form->field($model, 'credit_term') ?>

    <?php // echo $form->field($model, 'billing_currency') ?>

    <?php // echo $form->field($model, 'account_code') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

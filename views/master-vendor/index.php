<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MasterVendorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Master Vendors');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-vendor-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Master Vendor'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'no_doc',
            'status',
            'req_date',
            'requestor',
            // 'email_requestor:email',
            // 'validator',
            // 'email_validator:email',
            // 'approver',
            // 'email_approver:email',
            // 'support',
            // 'email_support:email',
            // 'support_date',
            // 'remark:ntext',
            // 'remark_cancel:ntext',
            // 'code',
            // 'name',
            // 'special_intru:ntext',
            // 'tax_address_code',
            // 'npwp',
            // 'vendor_group',
            // 'address_type',
            // 'address_1:ntext',
            // 'address_2:ntext',
            // 'address_3:ntext',
            // 'address_4:ntext',
            // 'country',
            // 'state',
            // 'city',
            // 'zip_code',
            // 'firstname',
            // 'lastname',
            // 'phone_number',
            // 'email:email',
            // 'office_code',
            // 'credit_term',
            // 'billing_currency',
            // 'account_code',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

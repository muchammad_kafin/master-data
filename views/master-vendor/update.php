<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterVendor */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Master Vendor',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Master Vendors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="master-vendor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

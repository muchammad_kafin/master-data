<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UpdateForm */
/* @var $form yii\widgets\ActiveForm */
$this->title = $title;
?>
<style> 
input[type=text] {
	text-transform: uppercase;
}
textarea {
	text-transform: uppercase;
}
</style>

<div class="update-form-form">
	<h1><center><?= Html::encode($title) ?></center></h1>
    <?php $form = ActiveForm::begin(); ?>
	
	<div class="row">
		<div class="col-lg-3">
		<p><strong>Please Fill with Capital Letter !</strong></p>
		</div>
		<div class="col-lg-3">
		</div>
		<div class="col-lg-2">
		</div>
		<div class="col-lg-4">
			<div class="row">
				<div class="col-lg-4">
					<?= Html::activeLabel($model, 'no_doc'); ?>		
				</div>
				<div class="col-lg-8">
					<?= $form->field($model, 'no_doc')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
				</div>
			</div>
			<div class="row hide">
				<div class="col-lg-4">
					<?= Html::activeLabel($model, 'status'); ?>		
				</div>
				<div class="col-lg-8">
					<?= $form->field($model, 'status')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
					<?= $form->field($model, 'validator')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
					<?= $form->field($model, 'acknowledges')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
					<?= $form->field($model, 'support')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
					<?= $form->field($model, 'master')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4">
					<?= Html::activeLabel($model, 'req_date'); ?>		
				</div>
				<div class="col-lg-8">
					<?= $form->field($model, 'req_date')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
				</div>
			</div>
		</div>
	</div>
	
	<div class ="row">
		<div class="col-lg-4">
			<?= $form->field($model, 'code')->textInput(['maxlength' => true,'readOnly' => true]) ?>
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'name')->textInput(['maxlength' => true,'readOnly' => true]) ?>
		</div>
		<div class="col-lg-4">
		</div>
	</div>
	
	<div class ="row">
		<div class="col-lg-12">
			<?= $form->field($model, 'request')->textarea(['rows' => 2,'readOnly' => true]) ?>			
		</div>
		<div class="col-lg-12">
			<?= $form->field($model, 'reason')->textarea(['rows' => 2,'readOnly' => true]) ?>
		</div>
		<div class="col-lg-12">
			<?= $form->field($model, 'remark')->textarea(['rows' => 2,'readOnly' => true]) ?>
		</div>
		<div class="col-lg-12">
			<?= $form->field($model, 'remark_support')->textarea(['rows' => 2]) ?>
		</div>
		<div class="col-lg-12">
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Uploaded File</th>
					</tr>
				</thead>
					<?php foreach ($modelsUpload as $files => $modelUpload): ?>
						<?= yii\widgets\DetailView::widget([
								'model' => $modelUpload,
								'attributes' => [						
									[
										'attribute' => 'filename',
										'format' => 'html',
										'value' =>Html::a($modelUpload->filename, ['site/download','file'=>$modelUpload->stored_file,'path'=>$modelUpload->filepath]),
									],
								],
								'options'=>['class' => 'table table-bordered']
							]) ?>
					 <?php endforeach; ?>
			</table>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-3">
			<?= $form->field($model, 'requestor')->textInput(['maxlength' => true,'readOnly' => true]) ?>			
			<?= $form->field($model, 'email_requestor')->textInput(['maxlength' => true,'readOnly' => true]) ?>
		</div>
		<div class="col-lg-3">
		<?= $form->field($model, 'validator_name')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		<?= $form->field($model, 'email_validator')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-3">
		<?= $form->field($model, 'acknowledges_name')->textInput(['maxlength' => true,'readOnly' => true]) ?>
		<?= $form->field($model, 'email_acknowledges')->textInput(['maxlength' => true,'readOnly'=> true]) ?>		
		</div>
		<div class="col-lg-3">	
		<?= $form->field($model, 'support_name')->textInput(['maxlength' => true,'readOnly' => true]) ?>
		<?= $form->field($model, 'email_support')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		<?= $form->field($model, 'support_date')->widget(\kartik\widgets\DateTimePicker::classname(), [
			'options' => ['placeholder' => 'Enter event time ...'],
			'pluginOptions' => [
				'autoclose' => true,
				'format' => 'yyyy-mm-dd hh:ii:ss'
			]
		]); ?>
		</div>
	</div>    

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\User;
use yii\helpers\ArrayHelper;
use kartik\widgets\Typeahead;
use kartik\widgets\TypeaheadBasic;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\MasterClient */
/* @var $form yii\widgets\ActiveForm */
$this->title = $title;
?>
<style> 
input[type=text] {
	text-transform: uppercase;
}
textarea {
	text-transform: uppercase;
}
</style>
<div class="master-client-form">
	<h1><center><?= Html::encode($this->title) ?></center></h1>
    <?php $form = ActiveForm::begin(); ?>
	
	<div class="row">
		<div class="col-lg-3">
		</div>
		<div class="col-lg-3">
		</div>
		<div class="col-lg-2">
		</div>
		<div class="col-lg-4">
			<div class="row">
				<div class="col-lg-4">
					<?= Html::activeLabel($model, 'no_doc'); ?>		
				</div>
				<div class="col-lg-8">
					<?= $form->field($model, 'no_doc')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
				</div>
			</div>
			<div class="row hide">
				<div class="col-lg-4">
					<?= Html::activeLabel($model, 'status'); ?>		
				</div>
				<div class="col-lg-8">
					<?= $form->field($model, 'status')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
					<?= $form->field($model, 'validator')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
					<?= $form->field($model, 'acknowledges')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
					<?= $form->field($model, 'support')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4">
					<?= Html::activeLabel($model, 'req_date'); ?>		
				</div>
				<div class="col-lg-8">
					<?= $form->field($model, 'req_date')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<?= $form->field($model, 'code')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-6">
			<?= $form->field($model, 'name')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<?= $form->field($model, 'address_1')->textarea(['rows' => 2,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-6">
			 <?= $form->field($model, 'address_2')->textarea(['rows' => 2,'readOnly'=> true]) ?>
		</div>
	</div>
    <div class="row">
		<div class="col-lg-3">
			<?= $form->field($model, 'country_name')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-3">
			<?= $form->field($model, 'state_name')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-3">
			<?= $form->field($model, 'city_name')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-3">
			<?= $form->field($model, 'zip_code')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			<?= $form->field($model, 'web')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'email')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'base_currency')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
	</div>
	<div class="row">
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>Contact Name</th>
					<th>Contact Phone</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($modelsContact as $indexHouse => $modelContact): ?>
			<tr>
					<td>
						<?= yii\widgets\DetailView::widget([
								'model' => $modelContact,
								'attributes' => [
									'firstname',
									'lastname',
								],
								'options'=>['class' => 'table table-bordered']
							]) ?>
					</td>
					<?php 
						$modelsPhone = \app\models\Phone::find()->where(['id_contact'=>$modelContact->id])->all();
					?>
					<td>
						<table>
							<tbody>
							<?php foreach ($modelsPhone as $indexRoom => $modelPhone): ?>
								<tr>
									<td>
										<?= yii\widgets\DetailView::widget([
											'model' => $modelPhone,
											'attributes' => [
												'phone_type',
												'phone_number',
											],
											'options'=>['class' => 'table table-bordered']
										]) ?>
									</td>
								</tr>
							 <?php endforeach; ?>
							</tbody>
						</table>
					</td>
			</tr>
			 <?php endforeach; ?>
			</tbody>
		</table>
	</div>
	<?= $form->field($model, 'remark')->textarea(['rows' => 2]) ?>
	<div class="row">
		<div class="col-lg-3">
			<?= $form->field($model, 'requestor')->textInput(['maxlength' => true,'readOnly'=> true]) ?>			
			<?= $form->field($model, 'email_requestor')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-3">
		<?= $form->field($model, 'validator_name')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		<?= $form->field($model, 'email_validator')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-3">
		<?= $form->field($model, 'acknowledges_name')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		<?= $form->field($model, 'email_acknowledges')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-3">
		<?= $form->field($model, 'support_name')->textInput(['maxlength' => true,'readOnly' => true]) ?>
		<?= $form->field($model, 'email_support')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		<?= $form->field($model, 'support_date')->widget(\kartik\widgets\DateTimePicker::classname(), [
			'options' => ['placeholder' => 'Enter event time ...'],
			'pluginOptions' => [
				'autoclose' => true,
				'format' => 'yyyy-mm-dd hh:ii:ss'
			]
		]); ?>
		</div>
	</div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
		<?php if ($model->validator == "Approve By System"){ ?>
		<?= Html::a(Yii::t('app', 'Reject'), ['support/cancelclient','id'=>$model->id], ['title' => 'Reject', 'class' => 'btn btn-danger']); ?>
		<?php }else{
			echo "";
		} ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MasterPartySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $title;
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-party-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php //echo Html::a(Yii::t('app', 'Create Master Party'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'no_doc',
            'req_date',
            'requestor',
            'status',
            'name',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}',
				'buttons'=>[
					'update' => function ($url, $model, $key) {
						return $model->status != 'reject' ? Html::a('<span class="fa fa-check-circle"></span>',
										['support/updateproduct','id'=>$model->id],
										[
											'title' => 'Support',
										]
							):'';
					},
				],
			],
        ],
    ]); ?>
</div>

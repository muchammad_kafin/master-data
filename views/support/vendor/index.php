<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MasterPartySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-party-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php //echo Html::a(Yii::t('app', 'Create Master Party'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<div class="box box-default">
			<div class="box-header with-border">
			  <h3 class="box-title">New Request</h3>
			  <div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
				</button>
			  </div>
			</div>
				<div class="box-body">
					<?= GridView::widget([
						'dataProvider' => $dataProvider,
						// 'filterModel' => $searchModel,
						'columns' => [
							['class' => 'yii\grid\SerialColumn'],

							// 'id',
							'no_doc',
							// 'status',
							'req_date',
							'requestor',
							'status',
							// 'validator',
							// 'approver',
							// 'support',
							// 'support_date',
							// 'remark:ntext',
							// 'client',
							// 'code',
							'name',
							// 'office',
							// 'address_1:ntext',
							// 'address_2:ntext',
							// 'country',
							// 'state',
							// 'city',
							// 'zip_code',
							// 'contact_person',
							// 'phone_number',
							// 'email:email',
							// 'type',

							['class' => 'yii\grid\ActionColumn', 'template' => '{update}',
								'buttons'=>[
									'update' => function ($url, $model, $key) {
										return $model->status != 'reject' ? Html::a('<span class="fa fa-check-circle"></span>',
														['support/updatevendor','id'=>$model->id],
														[
															'title' => 'Validation',
														]
											):'';
									},
								],
							],
						],
					]); ?>
				</div>
	</div>
	<div class="box box-default">
			<div class="box-header with-border">
			  <h3 class="box-title">Update Request</h3>
			  <div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
				</button>
			  </div>
			</div>
				<div class="box-body">
					<?= GridView::widget([
					'dataProvider' => $updateProvider,
					// 'filterModel' => $searchModel,
					'columns' => [
						['class' => 'yii\grid\SerialColumn'],

						// 'id',
						'no_doc',
						// 'status',
						'req_date',
						'requestor',
						'status',
						// 'validator',
						// 'approver',
						// 'support',
						// 'support_date',
						// 'remark:ntext',
						// 'client',
						// 'code',
						'name',
						// 'office',
						// 'address_1:ntext',
						// 'address_2:ntext',
						// 'country',
						// 'state',
						// 'city',
						// 'zip_code',
						// 'contact_person',
						// 'phone_number',
						// 'email:email',
						// 'type',

						['class' => 'yii\grid\ActionColumn', 'template' => '{update}',
							'buttons'=>[
								'update' => function ($url, $model, $key) {
									return $model['status'] != 'reject' ? Html::a('<span class="fa fa-check-circle"></span>',
													['support/updateform','id'=>$model['no_doc']],
													[
														'title' => 'Validation',
													]
										):'';
								},
							],
						],
					],
				]); ?>
				</div>
	</div>
</div>

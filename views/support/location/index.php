<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MasterLocationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Master Locations');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-location-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php //echo Html::a(Yii::t('app', 'Create Master Location'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'no_doc',
            'status',
            'req_date',
            'requestor',
            // 'validator',
            // 'approver',
            // 'support',
            // 'support_date',
            // 'remark:ntext',
            // 'code',
            // 'name',
            // 'type',
            // 'latitude',
            // 'longitude',
            // 'country',
            // 'city',
            // 'timezone',
            // 'un_code',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}',
				'buttons'=>[
					'update' => function ($url, $model, $key) {
						return $model->status != 'reject' ? Html::a('<span class="fa fa-check-circle"></span>',
										['support/updatelocation','id'=>$model->id],
										[
											'title' => 'Support',
										]
							):'';
					},
				],
			],
        ],
    ]); ?>
</div>

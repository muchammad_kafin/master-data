<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Transaction */
/* @var $form yii\widgets\ActiveForm */
$this->title="Reject" ;
?>
<style> 
input[type=text] {
	text-transform: uppercase;
}
textarea {
	text-transform: uppercase;
}
</style>
<div class="cancel-form">

    <?php $form = ActiveForm::begin(); ?>
	
	<div class="row">
		<?= $form->field($model, 'status')->hiddenInput()->label(false) ?>
			<div class="box box-default">
				<div class="box-header with-border">
				  <h3 class="box-title"><?php echo ($model->getAttributeLabel('remark_cancel'));?></h3>

				  <!--<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
					</button>
				  </div>
				  <!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<?= $form->field($model, 'remark_cancel')->textarea(['rows' => 3])->label(false) ?>
				</div>
				<!-- /.box-body -->
			</div>
			
	</div>
	
    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-danger']) ?>
        <?= Html::a( 'Back', Yii::$app->request->referrer,['class' => 'btn btn-primary']); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

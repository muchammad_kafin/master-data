<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Upload */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="upload-form">

    <?php $form = ActiveForm::begin([
		'options'=>['enctype'=>'multipart/form-data'] // important
	]); ?>

    <?= $form->field($model, 'id_form')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'filename')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'filepath')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'stored_file')->textInput(['maxlength' => true]) ?>
	
    <?= $form->field($model, 'getfile[]')->widget(\kartik\widgets\FileInput::classname(), [
		    'options' => ['multiple' => true],
			'pluginOptions' => ['previewFileType' => 'any']
	]); ?>
	
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

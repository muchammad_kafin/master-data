<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MasterClient */

$this->title = Yii::t('app', 'Request Master Data Client');
?>
<div class="master-client-create">

    <h1><center><?= Html::encode($this->title) ?></center></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelsContact' => $modelsContact,
        'modelsPhone' => $modelsPhone,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MasterClient */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Master Clients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-client-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'no_doc',
            'status',
            'req_date',
            'requestor',
            'validator',
            'approver',
            'support',
            'support_date',
            'remark:ntext',
            'code',
            'name',
            'address_1:ntext',
            'address_2:ntext',
            'country',
            'state',
            'city',
            'zip_code',
            'web',
            'firstname',
            'lastname',
            'phone_number',
            'email:email',
            'base_currency',
        ],
    ]) ?>

</div>

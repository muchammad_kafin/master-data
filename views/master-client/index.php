<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MasterClientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Master Clients');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-client-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php //echo Html::a(Yii::t('app', 'Create Master Client'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'no_doc',
            'status',
            'req_date',
            'requestor',
            // 'validator',
            // 'approver',
            // 'support',
            // 'support_date',
            // 'remark:ntext',
            // 'code',
            // 'name',
            // 'address_1:ntext',
            // 'address_2:ntext',
            // 'country',
            // 'state',
            // 'city',
            // 'zip_code',
            // 'web',
            // 'firstname',
            // 'lastname',
            // 'phone_number',
            // 'email:email',
            // 'base_currency',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

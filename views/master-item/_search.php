<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MasterItemSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-item-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'no_doc') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'req_date') ?>

    <?= $form->field($model, 'requestor') ?>

    <?php // echo $form->field($model, 'email_requestor') ?>

    <?php // echo $form->field($model, 'validator') ?>

    <?php // echo $form->field($model, 'email_validator') ?>

    <?php // echo $form->field($model, 'acknowledges') ?>

    <?php // echo $form->field($model, 'email_acknowledges') ?>

    <?php // echo $form->field($model, 'support') ?>

    <?php // echo $form->field($model, 'email_support') ?>

    <?php // echo $form->field($model, 'support_date') ?>

    <?php // echo $form->field($model, 'remark') ?>

    <?php // echo $form->field($model, 'remark_cancel') ?>

    <?php // echo $form->field($model, 'item_code') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'lookup_code') ?>

    <?php // echo $form->field($model, 'item_type') ?>

    <?php // echo $form->field($model, 'base_unit') ?>

    <?php // echo $form->field($model, 'base_location') ?>

    <?php // echo $form->field($model, 'label_code') ?>

    <?php // echo $form->field($model, 'unit_purchase') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\User;
use yii\helpers\ArrayHelper;
use kartik\widgets\Typeahead;
use kartik\widgets\TypeaheadBasic;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\MasterItem */
/* @var $form yii\widgets\ActiveForm */
?>
<style> 
input[type=text] {
	text-transform: uppercase;
}
textarea {
	text-transform: uppercase;
}
</style>
<div class="master-item-form">

    <?php $form = ActiveForm::begin(); ?>
	
	<div class="row">
		<div class="col-lg-3">
		<p><strong>Please Fill with Capital Letter !</strong></p>
		</div>
		<div class="col-lg-3">
		</div>
		<div class="col-lg-2">
		</div>
		<div class="col-lg-4">
			<div class="row">
				<div class="col-lg-4">
					<?= Html::activeLabel($model, 'no_doc'); ?>		
				</div>
				<div class="col-lg-8">
					<?= $form->field($model, 'no_doc')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
				</div>
			</div>
			<div class="row hide">
				<div class="col-lg-4">
					<?= Html::activeLabel($model, 'status'); ?>		
				</div>
				<div class="col-lg-8">
					<?= $form->field($model, 'status')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
					<?= $form->field($model, 'validator')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
					<?= $form->field($model, 'acknowledges')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
					<?= $form->field($model, 'support')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4">
					<?= Html::activeLabel($model, 'req_date'); ?>		
				</div>
				<div class="col-lg-8">
					<?= $form->field($model, 'req_date')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			<?= $form->field($model, 'item_code')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'lookup_code')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			<?php $dataItem = ArrayHelper::map(\app\models\ItemType::find()->all(), 'analysis_code', 'name'); ?>
			<?= $form->field($model, 'item_type')->widget(Select2::classname(), [
				'data' => $dataItem,
				'options' => ['placeholder' => 'Select a Item Type ...'],
				'pluginOptions' => [
					'allowClear' => true
				],
			]); ?>
		</div>
		<div class="col-lg-4">
			<?php $dataUnit = ArrayHelper::map(\app\models\Uom::find()->all(), 'code', 'description'); ?>
			<?= $form->field($model, 'base_unit')->widget(Select2::classname(), [
				'data' => $dataUnit,
				'options' => ['placeholder' => 'Select a Base Unit ...'],
				'pluginOptions' => [
					'allowClear' => true
				],
			]); ?>
		</div>
		<div class="col-lg-4">
			<?php $dataOffice = ArrayHelper::map(\app\models\OfficeType::find()->all(), 'code', 'name'); ?>
			<?= $form->field($model, 'base_location')->widget(Select2::classname(), [
				'data' => $dataOffice,
				'options' => ['placeholder' => 'Select a Base Location ...'],
				'pluginOptions' => [
					'allowClear' => true
				],
			]); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<?php $dataLabel = ArrayHelper::map(\app\models\Uom::find()->all(), 'code', 'description'); ?>
			<?= $form->field($model, 'label_code')->widget(Select2::classname(), [
				'data' => $dataLabel,
				'options' => ['placeholder' => 'Select a Label Code ...'],
				'pluginOptions' => [
					'allowClear' => true
				],
			]); ?>
		</div>
		<div class="col-lg-6">
			<?php $dataPurchase = ArrayHelper::map(\app\models\Uom::find()->all(), 'code', 'description'); ?>
			<?= $form->field($model, 'unit_purchase')->widget(Select2::classname(), [
				'data' => $dataPurchase,
				'options' => ['placeholder' => 'Select a Unit Purchase ...'],
				'pluginOptions' => [
					'allowClear' => true
				],
			]); ?>
		</div>
	</div>
	
	<?= $form->field($model, 'remark')->textarea(['rows' => 2]) ?>
	<div class="row">
		<div class="col-lg-3">
			<?= $form->field($model, 'requestor')->textInput(['maxlength' => true]) ?>			
			<?= $form->field($model, 'email_requestor')->textInput(['maxlength' => true,
				'onKeyUp'=>'
						$.post( "'.Yii::$app->urlManager->createUrl('site/validator').'&requestor='.'"+$(this).val()+"'.'&role=validator&master=item", function( data ) {							
						  $( "input#masteritem-validator_name" ).val( data.username);
						  $( "input#masteritem-email_validator" ).val( data.email);
						  $( "input#masteritem-validator" ).val( data.id);
						});
			']) ?>
		</div>
		<div class="col-lg-3">
		<?= $form->field($model, 'validator_name')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		<?= $form->field($model, 'email_validator')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-3">
		<?= $form->field($model, 'acknowledges_name')->textInput(['maxlength' => true,'readOnly' => true]) ?>
		<?= $form->field($model, 'email_acknowledges')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-3">
		<?= $form->field($model, 'support_name')->textInput(['maxlength' => true,'readOnly' => true]) ?>
		<?= $form->field($model, 'email_support')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		<?= $form->field($model, 'support_date')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
	</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

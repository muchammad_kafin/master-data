<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MasterItem */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Master Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-item-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'no_doc',
            'status',
            'req_date',
            'requestor',
            'email_requestor:email',
            'validator',
            'email_validator:email',
            'acknowledges',
            'email_acknowledges:email',
            'support',
            'email_support:email',
            'support_date',
            'remark:ntext',
            'remark_cancel:ntext',
            'item_code',
            'description',
            'lookup_code',
            'item_type',
            'base_unit',
            'base_location',
            'label_code',
            'unit_purchase',
        ],
    ]) ?>

</div>

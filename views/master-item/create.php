<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MasterItem */

$this->title = Yii::t('app', 'Request Master Data Items');
// $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Master Items'), 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-item-create">

    <h1><center><?= Html::encode($this->title) ?></center></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

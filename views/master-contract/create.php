<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MasterContract */

$this->title = Yii::t('app', 'Request Master Data Contract');
// $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Master Contracts'), 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-contract-create">

     <h1><center><?= Html::encode($this->title) ?></center></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

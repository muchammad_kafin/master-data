<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\User;
use yii\helpers\ArrayHelper;
use kartik\widgets\Typeahead;
use kartik\widgets\TypeaheadBasic;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\MasterContract */
/* @var $form yii\widgets\ActiveForm */
?>
<style> 
input[type=text] {
	text-transform: uppercase;
}
textarea {
	text-transform: uppercase;
}
</style>
<div class="master-contract-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form','options'=>['enctype'=>'multipart/form-data']]); ?>
	
	<div class="row">
		<div class="col-lg-3">
		<p><strong>Please Fill with Capital Letter !</strong></p>
		</div>
		<div class="col-lg-3">
		</div>
		<div class="col-lg-2">
		</div>
		<div class="col-lg-4">
			<div class="row">
				<div class="col-lg-4">
					<?= Html::activeLabel($model, 'no_doc'); ?>		
				</div>
				<div class="col-lg-8">
					<?= $form->field($model, 'no_doc')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
				</div>
			</div>
			<div class="row hide">
				<div class="col-lg-4">
					<?= Html::activeLabel($model, 'status'); ?>		
				</div>
				<div class="col-lg-8">
					<?= $form->field($model, 'status')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
					<?= $form->field($model, 'validator')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
					<?= $form->field($model, 'acknowledges')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
					<?= $form->field($model, 'support')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4">
					<?= Html::activeLabel($model, 'req_date'); ?>		
				</div>
				<div class="col-lg-8">
					<?= $form->field($model, 'req_date')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			<?php $dataVendorcode = ArrayHelper::map(\app\models\Vendor::find()->where(['status' => 1])->all(), 'code', 'Vendorcode'); ?>
			<?= $form->field($model, 'vendor_code')->widget(Select2::classname(), [
				'data' => $dataVendorcode,
				'options' => ['placeholder' => 'Select a Supplier Code ...',
					'onchange'=>'
						$.post( "'.Yii::$app->urlManager->createUrl('site/vendor1').'&code='.'"+$(this).val(), function( data ) {							
						  $( "input#mastercontract-vendor_name" ).val(data.name);
						});'
				],
				'pluginOptions' => [
					'allowClear' => true
				],
			]); ?>
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'vendor_name')->textInput(['maxlength' => true,'readOnly' => true]) ?>
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'sk_no')->textInput(['maxlength' => true])->hint('NO Kontrak/ NO Memo/ NO Penunjukkan Vendor/ NO Pelanggan') ?>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			<?= $form->field($model, 'start_periode')->widget(\yii\jui\DatePicker::classname(), [
				//'language' => 'ru',
				'dateFormat' => 'php:Y-m-d',
				'options' =>[
				'class'=>'form-control',
			]
		]) ?>
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'end_periode')->widget(\yii\jui\DatePicker::classname(), [
				//'language' => 'ru',
				'dateFormat' => 'php:Y-m-d',
				'options' =>[
				'class'=>'form-control',
			]
		]) ?>
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'jenis_sewa')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<?php $dataItemcode = ArrayHelper::map(\app\models\ItemList::find()->all(), 'item_code', 'description'); ?>
			<?= $form->field($model, 'item_code')->widget(Select2::classname(), [
				'data' => $dataItemcode,
				'options' => ['placeholder' => 'Select a Item Code ...'],
				'pluginOptions' => [
					'allowClear' => true
				],
			]); ?>
		</div>
		<div class="col-lg-6">
			<?php $dataUnit = ArrayHelper::map(\app\models\Uom::find()->all(), 'code', 'description'); ?>
			<?= $form->field($model, 'uom')->widget(Select2::classname(), [
				'data' => $dataUnit,
				'options' => ['placeholder' => 'Select a UOM ...'],
				'pluginOptions' => [
					'allowClear' => true
				],
			]); ?>
		</div>
	</div>
	<?= $form->field($model, 'getfile[]')->widget(\kartik\widgets\FileInput::classname(), [
		    'options' => ['multiple' => true],
			'pluginOptions' => ['previewFileType' => 'any','showUpload' => false]
	]); ?>
	<?= $form->field($model, 'remark')->textarea(['rows' => 2]) ?>
	<div class="row">
		<div class="col-lg-3">
			<?= $form->field($model, 'requestor')->textInput(['maxlength' => true]) ?>			
			<?= $form->field($model, 'email_requestor')->textInput(['maxlength' => true,
				'onKeyUp'=>'
						$.post( "'.Yii::$app->urlManager->createUrl('site/validator').'&requestor='.'"+$(this).val()+"'.'&role=validator&master=contract", function( data ) {							
						  $( "input#mastercontract-validator_name" ).val( data.username);
						  $( "input#mastercontract-email_validator" ).val( data.email);
						  $( "input#mastercontract-validator" ).val( data.id);
						});
			']) ?>
		</div>
		<div class="col-lg-3">
		<?= $form->field($model, 'validator_name')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		<?= $form->field($model, 'email_validator')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-3">
		<?= $form->field($model, 'acknowledges_name')->textInput(['maxlength' => true,'readOnly' => true]) ?>
		<?= $form->field($model, 'email_acknowledges')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-3">
		<?= $form->field($model, 'support_name')->textInput(['maxlength' => true,'readOnly' => true]) ?>
		<?= $form->field($model, 'email_support')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		<?= $form->field($model, 'support_date')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
	</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MasterCountrySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-country-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'no_doc') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'req_date') ?>

    <?= $form->field($model, 'requestor') ?>

    <?php // echo $form->field($model, 'validator') ?>

    <?php // echo $form->field($model, 'approver') ?>

    <?php // echo $form->field($model, 'support') ?>

    <?php // echo $form->field($model, 'support_date') ?>

    <?php // echo $form->field($model, 'remark') ?>

    <?php // echo $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'code') ?>

    <?php // echo $form->field($model, 'isd') ?>

    <?php // echo $form->field($model, 'currency') ?>

    <?php // echo $form->field($model, 'continent') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MasterCountry */

$this->title = Yii::t('app', 'Request Master Data Country');
?>
<div class="master-country-create">

    <h1><center><?= Html::encode($this->title) ?></center></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

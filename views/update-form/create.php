<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UpdateForm */

$this->title = Yii::t('app', 'Request Update Data '.$master);
?>
<div class="update-form-create">

   <h1><center><?= Html::encode($this->title) ?></center></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'master' => $master,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UpdateFormSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Update Forms');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="update-form-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Update Form'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'no_doc',
            'status',
            'req_date',
            'requestor',
            // 'email_requestor:email',
            // 'validator',
            // 'email_validator:email',
            // 'acknowledges',
            // 'email_acknowledges:email',
            // 'support',
            // 'email_support:email',
            // 'support_date',
            // 'remark:ntext',
            // 'reason:ntext',
            // 'request:ntext',
            // 'remark_cancel:ntext',
            // 'code',
            // 'name',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

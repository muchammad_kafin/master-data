<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\bootstrap\Modal;
use yii\grid\GridView;


/**
 * @link https://github.com/himiklab/yii2-search-component-v2
 * @copyright Copyright (c) 2014 HimikLab
 * @license http://opensource.org/licenses/MIT MIT
 */
/** @var yii\web\View $this */
/** @var ZendSearch\Lucene\Search\QueryHit[] $hits */
/** @var string $query */
/** @var yii\data\Pagination $pagination */
$this->title = "Results for \"$q\"";
?>
<script type="text/javascript">
    //get the click of modal button to create / update item
    //we get the button by class not by ID because you can only have one id on a page and you can
    //have multiple classes therefore you can have multiple open modal buttons on a page all with or without
    //the same link.
//we use on so the dom element can be called again if they are nested, otherwise when we load the content once it kills the dom element and wont let you load anther modal on click without a page refresh
      $(document).on('click', '.showModalButton', function(){
        //check if the modal is open. if it's open just reload content not whole modal
        //also this allows you to nest buttons inside of modals to reload the content it is in
        //the if else are intentionally separated instead of put into a function to get the 
        //button since it is using a class not an #id so there are many of them and we need
        //to ensure we get the right button and content. 
        if ($('#modal').data('bs.modal').isShown) {
            $('#modal').find('#modalContent')
                    .load($(this).attr('value'));
            //dynamiclly set the header for the modal via title tag
            document.getElementById('modalHeader').innerHTML = '<h4>' + $(this).attr('title') + '</h4>';
        } else {
            //if modal isn't open; open it and load content
            $('#modal').modal('show')
                    .find('#modalContent')
                    .load($(this).attr('value'));
             //dynamiclly set the header for the modal via title tag
            document.getElementById('modalHeader').innerHTML = '<h4>' + $(this).attr('title') + '</h4>';
        }
    });
</script>
<?php
	yii\bootstrap\Modal::begin([
		'headerOptions' => ['id' => 'modalHeader'],
		'id' => 'modal',
		'size' => 'modal-lg',
		
	]);
	echo "<div id='modalContent'></div>";
	yii\bootstrap\Modal::end();
?>
<h1><?= \yii\helpers\Html::encode($this->title) ?></h1>
<div class="row">
	<div class="form-group" role="search">
				<?php $form = ActiveForm::begin([
					'action' => ['update-form/search'],
					'method' => 'get',
				]); ?>
				<?php 
					$dataCountry = \app\models\User::getSearchcode();
				?>
				<div class="col-lg-4">
					<div class="form-group">
						<label for="q">Code / Name</label>						
						<?php
						echo \yii\jui\AutoComplete::widget([
							'name' => 'q',
							'options' => ['class' => 'form-control'],
							'clientOptions' => [
								'source' => \yii\helpers\ArrayHelper::getColumn(\app\models\User::getSearchcode(), 'query'),
							],
						]); ?>
					</div>
				</div>
				<div class="col-lg-4">
					<label for="master">Master List</label>
					<div class="input-group">
						<?= Html::dropDownList('master',$master,\yii\helpers\ArrayHelper::map(\app\models\User::getMastercode(), 'id', 'name'), ['class'=>'form-control']) ?>
						<span class="input-group-btn">
							<?= Html::submitButton('<i class="fa fa-search"></i>', ['class' => 'btn btn-flat']) ?>
						</span>
					</div>
				</div>
			<?php ActiveForm::end(); ?>
	</div>
</div>
</br>
<div class="row">
<?php //var_dump($model[0]['id']); ?>
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'code',
            'name',
            // 'address_1',
            // 'type',
            // 'validator',
            // 'approver',
            // 'support',
            // 'support_date',
            // 'remark:ntext',
            // 'client',
            // 'code',
            // 'name',
            // 'office',
            // 'address_1:ntext',
            // 'address_2:ntext',
            // 'country',
            // 'state',
            // 'city',
            // 'zip_code',
            // 'contact_person',
            // 'phone_number',
            // 'email:email',
            // 'type',

            [						
				'class' => 'yii\grid\ActionColumn',
				'header' => 'Actions',
				'buttons' => [
					'update' => function ($url,$model,$key) {
						return Html::a(
							'<span class="fa fa-check-square-o"></span>',
							['create','code'=>$model['code'],'master'=>$model['master']], 
							[
								'title' => 'Update',
							]
						);
					},
				],
				'template' => '{update}',
			],
        ],
    ]); ?>
</div>
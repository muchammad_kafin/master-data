<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MasterEquipment */

$this->title = Yii::t('app', 'Request Master Data Equipment');
?>
<div class="master-equipment-create">

    <h1><center><?= Html::encode($this->title) ?></center></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

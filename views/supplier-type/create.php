<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SupplierType */

$this->title = Yii::t('app', 'Create Supplier Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Supplier Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="supplier-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

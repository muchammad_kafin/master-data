<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

	<div class="box box-primary">
	<div class="box-body">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
	
    <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>
	
    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

    <?php //echo $form->field($model, 'auth_key')->textInput(['maxlength' => true]) ?>

    <?php //echo $form->field($model, 'password_reset_token')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
	
	<?php 
		$data = [10 => 'active',0=>'not active'];
	?>
    <?= $form->field($model, 'status')->dropDownList($data,['prompt'=>'Select...']) ?>
	
    <?php //echo $form->field($model, 'role')->dropDownList($model->roleArray,['prompt'=>'Select...']) ?>
	
    <?php //echo $form->field($model, 'master')->dropDownList($model->masterArray,['prompt'=>'Select...']) ?>

    <?php //echo $form->field($model, 'created_at')->textInput() ?>

    <?php //echo $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
	</div>

    <?php ActiveForm::end(); ?>
    </div>
	</div>

</div>

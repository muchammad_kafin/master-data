<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data User';
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="box box-primary">
    <div class="box-body">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'username',
            // 'auth_key',
            // 'password_hash',
            // 'password_reset_token',
            'email:email',
            [
				'attribute'=>'status',
				 'value' => function($model, $key, $index)
				{   
					if($model->status == 10)
					{
						return 'Active';
					}
					else
					{   
						return 'Not Active';
					}
				},
			],
            // 'created_at',
            // 'updated_at',

            [
				'class' => 'yii\grid\ActionColumn',
				'template' => '{update}{password}',
				'buttons' => [
					'update' => function ($url,$model) {
						return Html::a(
							'<span class="fa fa-check-square-o">&nbsp;</span>',
							['update','id'=>$model->id], 
							[
								'title' => 'Update',
							]
						);
					},
					'password' => function ($url,$model) {
						return Html::a(
							'<span class="fa fa-info"></span>',
							['resetpassword','id'=>$model->id], 
							[
								'title' => 'Reset Password',
							]
						);
					},
				],
			],
        ],
    ]); ?>
    </div>
    </div>
</div>

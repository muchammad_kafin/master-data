<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\User;
use yii\helpers\ArrayHelper;
use kartik\widgets\Typeahead;
use kartik\widgets\TypeaheadBasic;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
use yii\widgets\MaskedInput;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\jui\AutoComplete;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\MasterParty */
/* @var $form yii\widgets\ActiveForm */
?>
<style> 
input[type=text] {
	text-transform: uppercase;
}
textarea {
	text-transform: uppercase;
}
</style>
<div class="master-party-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>
	<div class="row">
		<div class="col-lg-3">
		<p><strong>Please Fill with Capital Letter !</strong></p>
		</div>
		<div class="col-lg-3">
		</div>
		<div class="col-lg-2">
		</div>
		<div class="col-lg-4">
			<div class="row">
				<div class="col-lg-4">
					<?= Html::activeLabel($model, 'no_doc'); ?>		
				</div>
				<div class="col-lg-8">
					<?= $form->field($model, 'no_doc')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
				</div>
			</div>
			<div class="row hide">
				<div class="col-lg-4">
					<?= Html::activeLabel($model, 'status'); ?>		
				</div>
				<div class="col-lg-8">
					<?= $form->field($model, 'status')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
					<?= $form->field($model, 'validator')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
					<?= $form->field($model, 'acknowledges')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
					<?= $form->field($model, 'support')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4">
					<?= Html::activeLabel($model, 'req_date'); ?>		
				</div>
				<div class="col-lg-8">
					<?= $form->field($model, 'req_date')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
				</div>
			</div>
		</div>
	</div>
	<div class ="row">
		<div class="col-lg-4">
			<?php $dataClient = ArrayHelper::map(\app\models\Clients::find()->all(), 'name', 'name'); ?>
			<?= $form->field($model, 'client')->widget(Select2::classname(), [
				'data' => $dataClient,
				'options' => ['placeholder' => 'Select a Client ...'],
				'pluginOptions' => [
					'allowClear' => true
				],
			]); ?>
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'code')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-12">
		<fieldset>
				<legend>Address & Contact Details</legend>
			<div class="col-lg-12">
				<div class="col-lg-4">
					<?php $dataOffice = ArrayHelper::map(\app\models\OfficeType::find()->all(), 'code', 'name'); ?>
					<?= $form->field($model, 'office')->widget(Select2::classname(), [
						'data' => $dataOffice,
						'options' => ['placeholder' => 'Select a Type ...'],
						'pluginOptions' => [
							'allowClear' => true
						],
					]); ?>
				</div>
				<div class="col-lg-4">
					<?= $form->field($model, 'address_1')->textarea(['rows' => 2]) ?>
				</div>
				<div class="col-lg-4">
					<?= $form->field($model, 'address_2')->textarea(['rows' => 2]) ?>
				</div>
			</div>
			<div class="col-lg-12">
				<div class="col-lg-4">
					<?php $dataCountry = \app\models\Country::find()->select('name as value, code as id')->asArray()->all(); ?>
					<?= $form->field($model, 'country_name')->widget(AutoComplete::className(), [
					'options' => ['class' => 'form-control'], 
					'clientOptions' => ['source' =>  $dataCountry ,
						'select' => new JsExpression("function( event, ui ) {
						$('#". Html::getInputId($model, 'country')."').val(ui.item.id).trigger('change');
						$('#masterparty-country_name').val(ui.item.value);
						console.log($('#". Html::getInputId($model, 'country')."').val());
							}"),
					'autoFill'=>true,]]) ?>
					<?= Html::activeHiddenInput($model, 'country')?>
					
				</div>
				<div class="col-lg-4">
					<?= $form->field($model, 'state')->textInput(['maxlength' => true]) ?>
				</div>
				<div class="col-lg-4">
					<?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
				</div>
				
			</div>
			<div class="col-lg-12">
				<div class="col-lg-4">
				<?= $form->field($model, 'zip_code')->widget(MaskedInput::classname(), [
					'mask' => '*{1,10}'
				]) ?>
				</div>
				<div class="col-lg-4">
				<?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>
				</div>
				<div class="col-lg-4">
				<?= $form->field($model, 'email')->widget(MaskedInput::classname(), [
					'clientOptions' => [
						'alias' =>  'email',
					],
				]) ?>
				</div>
			</div>
			<div class="row">
					<div class="padding-v-md">
						<div class="line line-dashed"></div>
					</div>
				 
					<?php DynamicFormWidget::begin([
						'widgetContainer' => 'dynamicform_wrapper',
						'widgetBody' => '.container-items',
						'widgetItem' => '.contact-item',
						'limit' => 5,
						'min' => 1,
						'insertButton' => '.add-contact',
						'deleteButton' => '.remove-contact',
						'model' => $modelsContact[0],
						'formId' => 'dynamic-form',
						'formFields' => [
							'firstname',
							'lastname',
						],
					]); ?>
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Contact Name</th>
								<th style="width: 450px;">Contact Phone</th>
								<th class="text-center" style="width: 90px;">
									<button type="button" class="add-contact btn btn-success btn-xs"><span class="fa fa-plus"></span></button>
								</th>
							</tr>
						</thead>
						<tbody class="container-items">
						<?php foreach ($modelsContact as $indexHouse => $modelContact): ?>
							<tr class="contact-item">
								<td class="vcenter">
									<?php
										// necessary for update action.
										if (!$modelContact->isNewRecord) {
											echo Html::activeHiddenInput($modelContact, "[{$indexHouse}]id");
										}
									?>
									<?= $form->field($modelContact, "[{$indexHouse}]firstname")->textInput(['maxlength' => true]) ?>
									<?= $form->field($modelContact, "[{$indexHouse}]lastname")->textInput(['maxlength' => true]) ?>
								</td>
								<td>
									<?= $this->render('_form-contact', [
										'form' => $form,
										'indexHouse' => $indexHouse,
										'modelsPhone' => $modelsPhone[$indexHouse],
									]) ?>
								</td>
								<td class="text-center vcenter" style="width: 90px; verti">
									<button type="button" class="remove-contact btn btn-danger btn-xs"><span class="fa fa-minus"></span></button>
								</td>
							</tr>
						 <?php endforeach; ?>
						</tbody>
					</table>
					<?php DynamicFormWidget::end(); ?>
				</div>
			</div>
		</fieldset>
	</div>
	<fieldset>
		<legend>Party Type</legend>
		<?php $dataType = ArrayHelper::map(\app\models\PartyType::find()->all(), 'name', 'name'); ?>
		<?= $form->field($model, 'type',[
				'template' => "<div>{label}</div>\n<div class=\"col-lg-offset-2 col-lg-9\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
			])->checkboxList($dataType, ['unselect'=>NULL,'separator' => '&emsp;&emsp;&emsp;']);?>
	</fieldset>	
	<?php $dataMap = ArrayHelper::map(\app\models\OfficeMap::find()->all(), 'code', 'name'); ?>
    <?= $form->field($model, 'office_mapping')->widget(Select2::classname(), [
			'data' => $dataMap,
			'options' => ['placeholder' => 'Select Office ...', 'multiple' => true],
			'pluginOptions' => [
				'allowClear' => true
			],
		]); ?>
	
	<?= $form->field($model, 'remark')->textarea(['rows' => 4]) ?>
	<div class="row">
		<div class="col-lg-3">
			<?= $form->field($model, 'requestor')->textInput(['maxlength' => true]) ?>			
			<?= $form->field($model, 'email_requestor')->textInput(['maxlength' => true,
				'onKeyUp'=>'
						$.post( "'.Yii::$app->urlManager->createUrl('site/validator').'&requestor='.'"+$(this).val()+"'.'&role=validator&master=party", function( data ) {							
						  $( "input#masterparty-validator_name" ).val( data.username);
						  $( "input#masterparty-email_validator" ).val( data.email);
						  $( "input#masterparty-validator" ).val( data.id);
						});
			']) ?>
		</div>
		<div class="col-lg-3">
		<?= $form->field($model, 'validator_name')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		<?= $form->field($model, 'email_validator')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-3">
		<?= $form->field($model, 'acknowledges_name')->textInput(['maxlength' => true,'readOnly' => true]) ?>
		<?= $form->field($model, 'email_acknowledges')->textInput(['maxlength' => true,'readOnly'=> true]) ?>		
		</div>
		<div class="col-lg-3">	
		<?= $form->field($model, 'support_name')->textInput(['maxlength' => true,'readOnly' => true]) ?>
		<?= $form->field($model, 'email_support')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		<?= $form->field($model, 'support_date')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
	</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Submit') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

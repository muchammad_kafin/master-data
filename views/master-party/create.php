<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MasterParty */

$this->title = Yii::t('app', 'Request Master Data Party');
?>
<div class="master-party-create">

    <h1><center><?= Html::encode($this->title) ?></center></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelsContact' => $modelsContact,
        'modelsPhone' => $modelsPhone,
    ]) ?>

</div>

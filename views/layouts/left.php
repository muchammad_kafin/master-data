<?php
use mdm\admin\components\Helper;
use mdm\admin\components\MenuHelper;
use yii\bootstrap\Nav;
?>
<aside class="main-sidebar">

    <section class="sidebar">
		<center><p><strong>Welcome <?php if (Yii::$app->user->isGuest){echo "Guest";}else{echo strtoupper(Yii::$app->user->identity->firstname." ".Yii::$app->user->identity->lastname);} ?></strong></p></center>
        <!-- Sidebar user panel 
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="<?php if (Yii::$app->user->isGuest){echo "";}else{echo Yii::$app->user->identity->username;} ?>" />				
            </div>
            <div class="pull-left info">
                <p><?php if (Yii::$app->user->isGuest){echo "";}else{echo Yii::$app->user->identity->username;} ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form 
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
		<?php
		$callback = function($menu){
			$data = $menu['data']; 
			//if have syntax error, unexpected 'fa' (T_STRING)  Errorexception,can use
		   //$data = $menu['data'];
			return [
				'label' => $menu['name'],
				'url' => [$menu['route']],
				'option' => $data,
				'icon' => $menu['data'],
				'items' => $menu['children'],
			];
		};

		$items = MenuHelper::getAssignedMenu(Yii::$app->user->id, null, $callback, true);
		// $items[] = [
			// 'label'=>'Logout',
			// 'url'=>['/site/logout'],
			// 'template' => '<a href="{url}" data-method="post">{icon} {label}</a>', 
			// 'visible' => !Yii::$app->user->isGuest
		// ];
			echo dmstr\widgets\Menu::widget([
			'options' => ['class' => 'sidebar-menu'],
			'items' => $items,
		]);
?>
		<?php 
			$menuItems = [
				['label' => 'Menu', 'options' => ['class' => 'header'],'visible' => Yii::$app->user->isGuest],
				// ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii']],
				// ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug']],
				// ['label' => 'About', 'icon' => 'fa fa-dashboard', 'url' => ['/site/about']],				
				[
					'label' => 'SCM Form',
					'icon' => 'fa fa-envelope',
					'url' => '#',
					'visible' => Yii::$app->user->isGuest,
					'items' => [
						['label' => 'Master Charge', 'icon' => 'fa fa-envelope-square', 'url' => ['/master-charge/create'],'visible' => Yii::$app->user->isGuest],
						['label' => 'Master Client', 'icon' => 'fa fa-envelope-square', 'url' => ['/master-client/create'],'visible' => Yii::$app->user->isGuest],
						['label' => 'Master Country', 'icon' => 'fa fa-envelope-square', 'url' => ['/master-country/create'],'visible' => Yii::$app->user->isGuest],
						['label' => 'Master Debtor', 'icon' => 'fa fa-envelope-square', 'url' => ['/master-debtor/create'],'visible' => Yii::$app->user->isGuest],
						['label' => 'Master Equipment', 'icon' => 'fa fa-envelope-square', 'url' => ['/master-equipment/create'],'visible' => Yii::$app->user->isGuest],
						['label' => 'Master Location', 'icon' => 'fa fa-envelope-square', 'url' => ['/master-location/create'],'visible' => Yii::$app->user->isGuest],
						['label' => 'Master Party', 'icon' => 'fa fa-envelope-square', 'url' => ['/master-party/create'],'visible' => Yii::$app->user->isGuest],
						['label' => 'Master Printing', 'icon' => 'fa fa-envelope-square', 'url' => ['/master-printing/create'],'visible' => Yii::$app->user->isGuest],
						['label' => 'Master Product', 'icon' => 'fa fa-envelope-square', 'url' => ['/master-product/create'],'visible' => Yii::$app->user->isGuest],
						['label' => 'Master Region', 'icon' => 'fa fa-envelope-square', 'url' => ['/master-region/create'],'visible' => Yii::$app->user->isGuest],
						['label' => 'Master Vendor', 'icon' => 'fa fa-envelope-square', 'url' => ['/master-vendor/create'],'visible' => Yii::$app->user->isGuest],
						['label' => 'Request Update', 'icon' => 'fa fa-envelope-square', 'url' => ['/update-form/search'],'visible' => Yii::$app->user->isGuest],
					],
				],
				[
					'label' => 'SUN Form',
					'icon' => 'fa fa-envelope',
					'url' => '#',
					'visible' => Yii::$app->user->isGuest,
					'items' => [
						['label' => 'Master Item', 'icon' => 'fa fa-envelope-square', 'url' => ['/master-item/create'],'visible' => Yii::$app->user->isGuest],
						['label' => 'Master Contract', 'icon' => 'fa fa-envelope-square', 'url' => ['/master-contract/create'],'visible' => Yii::$app->user->isGuest],     
					],
				],
				/*[
					'label' => 'Status',
					'icon' => 'fa fa-envelope',
					'url' => '#',
					'visible' => Yii::$app->user->isGuest,
					'items' => [
						['label' => 'Master Client', 'icon' => 'fa fa-info', 'url' => ['/finish/client'],'visible' => Yii::$app->user->isGuest],
						['label' => 'Master Country', 'icon' => 'fa fa-info', 'url' => ['/finish/country'],'visible' => Yii::$app->user->isGuest],
						['label' => 'Master Debtor', 'icon' => 'fa fa-info', 'url' => ['/finish/debtor'],'visible' => Yii::$app->user->isGuest],
						['label' => 'Master Location', 'icon' => 'fa fa-info', 'url' => ['/finish/location'],'visible' => Yii::$app->user->isGuest],
						['label' => 'Master Party', 'icon' => 'fa fa-info', 'url' => ['/finish/party'],'visible' => Yii::$app->user->isGuest],
						['label' => 'Master Region', 'icon' => 'fa fa-info', 'url' => ['/finish/region'],'visible' => Yii::$app->user->isGuest],
						['label' => 'Master Vendor', 'icon' => 'fa fa-info', 'url' => ['/finish/vendor'],'visible' => Yii::$app->user->isGuest],        
						['label' => 'Master Contract', 'icon' => 'fa fa-info', 'url' => ['/finish/contract'],'visible' => Yii::$app->user->isGuest],        
						['label' => 'Master Item', 'icon' => 'fa fa-info', 'url' => ['/finish/item'],'visible' => Yii::$app->user->isGuest],        
					],
				],*/
				// ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
				['label' => 'Logout', 'url' => ['site/logout'], 'icon' => 'fa fa-arrow-left', 'template' => '<a href="{url}" data-method="post">{icon} {label}</a>', 'visible' => !Yii::$app->user->isGuest],
			];
		?>
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => $menuItems,
            ]
        ) ?>
		 <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => 
				[
					['label' => 'Login', 'icon' => 'fa fa-sign-in', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
				],
            ]
        ) ?>
    </section>
</aside>

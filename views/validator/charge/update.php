<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\User;
use yii\helpers\ArrayHelper;
use kartik\widgets\Typeahead;
use kartik\widgets\TypeaheadBasic;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\MasterVendor */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Validation Master Charge';
?>
<style> 
input[type=text] {
	text-transform: uppercase;
}
textarea {
	text-transform: uppercase;
}
</style>
<div class="master-vendor-form">
<h1><center><?= Html::encode($this->title) ?></center></h1>
    <?php $form = ActiveForm::begin(); ?>
	
	<div class="row">
		<div class="col-lg-3">
		</div>
		<div class="col-lg-3">
		</div>
		<div class="col-lg-2">
		</div>
		<div class="col-lg-4">
			<div class="row">
				<div class="col-lg-4">
					<?= Html::activeLabel($model, 'no_doc'); ?>		
				</div>
				<div class="col-lg-8">
					<?= $form->field($model, 'no_doc')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
				</div>
			</div>
			<div class="row hide">
				<div class="col-lg-4">
					<?= Html::activeLabel($model, 'status'); ?>		
				</div>
				<div class="col-lg-8">
					<?= $form->field($model, 'status')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
					<?= $form->field($model, 'validator')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
					<?= $form->field($model, 'acknowledges')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
					<?= $form->field($model, 'support')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4">
					<?= Html::activeLabel($model, 'req_date'); ?>		
				</div>
				<div class="col-lg-8">
					<?= $form->field($model, 'req_date')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			<?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'name')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-4">
			<?php $arrayJob = [
						["code"=>"SEA","name"=>"SEA"],
						["code"=>"AIR","name"=>"AIR"],
						["code"=>"ROAD","name"=>"ROAD"],
						["code"=>"WAREHOUSE","name"=>"WAREHOUSE"],
						];
				$job = ArrayHelper::map($arrayJob, 'code', 'name');
			?>
			<?= $form->field($model, 'job_type')->widget(Select2::classname(), [
					'data' => $job,
					'options' => ['placeholder' => 'Select Job ...', 'multiple' => true],
					'pluginOptions' => [
						'allowClear' => true
					],
				]); ?>
		</div>		
	</div>
	
	<div class="row">
		<div class="col-lg-4">
			<?php $dataMap = ArrayHelper::map(\app\models\OfficeMap::find()->all(), 'code', 'name'); ?>
			<?= $form->field($model, 'office_map')->widget(Select2::classname(), [
					'data' => $dataMap,
					'options' => ['placeholder' => 'Select Office ...', 'multiple' => true],
					'pluginOptions' => [
						'allowClear' => true
					],
				]); ?>
		</div>	
		<div class="col-lg-4">
			<?= $form->field($model, 'printing_code')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'printing_sequence')->textInput(['maxlength' => true]) ?>
		</div>		
	</div>
	
	<div class="row">
		<div class="col-lg-4">
					<?= $form->field($model, 'currency')->textInput(['maxlength' => true,'readOnly'=> true]); ?>
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'cost_posting')->textInput(['maxlength' => true]) ?>
		</div>	
		<div class="col-lg-4">
			<?= $form->field($model, 'revenue_posting')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">		
			<div class="col-lg-3">
				<?= $form->field($model, 'reimbursable')->checkbox(['disabled'=> true]); ?>
			</div>
			<div class="col-lg-3">
				<?= $form->field($model, 'commissionable')->checkbox(['disabled'=> true]); ?>
			</div>
			<div class="col-lg-4">
				<?= $form->field($model, 'profit_shared')->checkbox(['disabled'=> true]); ?>
			</div>
		</div>
		<div class="col-lg-3">
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-4">
			<?php $tax = ["NA"=>"NA","VAT1"=>"VAT1","VAT10"=>"VAT10"]; ?>
			<?= $form->field($model, 'tax_group')->widget(Select2::classname(), [
					'data' => $tax,
					'options' => ['placeholder' => 'Select TAX ...'],
					'pluginOptions' => [
						'allowClear' => true
					],
				]); ?>
		</div>
		<div class="col-lg-4">
			<?php $app = ["COST"=>"COST","REVENUE"=>"REVENUE"]; ?>
			<?= $form->field($model, 'applicable_for')->widget(Select2::classname(), [
					'data' => $app,
					'options' => ['placeholder' => 'Select ...'],
					'pluginOptions' => [
						'allowClear' => true
					],
				]); ?>
		</div>
		<div class="col-lg-4">
			<?php $base = ["NA"=>"NA","VALUE"=>"VALUE"]; ?>
			<?= $form->field($model, 'based_on')->widget(Select2::classname(), [
					'data' => $base,
					'options' => ['placeholder' => 'Select ...'],
					'pluginOptions' => [
						'allowClear' => true
					],
				]); ?>
		</div>
	</div>
	
	<?= $form->field($model, 'remark')->textarea(['rows' => 2]) ?>
	<div class="row">
		<div class="col-lg-3">
			<?= $form->field($model, 'requestor')->textInput(['maxlength' => true,'readOnly'=> true]) ?>			
			<?= $form->field($model, 'email_requestor')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-3">
		<?= $form->field($model, 'validator_name')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		<?= $form->field($model, 'email_validator')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-3">
		<?= $form->field($model, 'acknowledges_name')->textInput(['maxlength' => true,'readOnly' => true]) ?>
		<?= $form->field($model, 'email_acknowledges')->textInput(['maxlength' => true,'readOnly'=> true]) ?>		
		</div>
		<div class="col-lg-3">
		<?= $form->field($model, 'support_name')->textInput(['maxlength' => true,'readOnly' => true]) ?>
		<?= $form->field($model, 'email_support')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		<?= $form->field($model, 'support_date')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
	</div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Reject'), ['validator/cancelcharge','id'=>$model->id], ['title' => 'Reject', 'class' => 'btn btn-danger']); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

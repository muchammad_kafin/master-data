<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\OfficeMap */

$this->title = Yii::t('app', 'Create Office Map');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Office Maps'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="office-map-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

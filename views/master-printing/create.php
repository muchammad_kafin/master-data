<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MasterPrinting */

$this->title = Yii::t('app', 'Request Master Data Printing');
?>
<div class="master-printing-create">

    <h1><center><?= Html::encode($this->title) ?></center></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RequestorValidator */

$this->title = Yii::t('app', 'Create Requestor Validator');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Requestor Validators'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="requestor-validator-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RequestorValidator */

$this->title = $model->id_validator;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Requestor Validators'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="requestor-validator-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id_validator' => $model->id_validator, 'requestor' => $model->requestor], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id_validator' => $model->id_validator, 'requestor' => $model->requestor], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_validator',
            'requestor',
        ],
    ]) ?>

</div>

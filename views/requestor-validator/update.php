<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RequestorValidator */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Requestor Validator',
]) . $model->id_validator;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Requestor Validators'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_validator, 'url' => ['view', 'id_validator' => $model->id_validator, 'requestor' => $model->requestor]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="requestor-validator-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

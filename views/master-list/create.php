<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MasterList */

$this->title = Yii::t('app', 'Create Master List');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Master Lists'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-list-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

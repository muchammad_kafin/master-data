<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\assets\AppAsset;
AppAsset::register($this);
dmstr\web\AdminLteAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\MasterClient */

$this->title = $model->no_doc;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="master-client-view">
	<div class="row">
		<div class="col-lg-6">
		<?= DetailView::widget([
			'model' => $model,
			'attributes' => [
				'no_doc',
				// 'status',
				'req_date',
				'requestor',
				'support_date',
				'remark:ntext',
				'remark_cancel:ntext',
				'client',
				'code',
				'name',
				'office',           
				'office_mapping',           
			],
		]) ?>
		</div>
		<div class="col-lg-6">
		<?= DetailView::widget([
			'model' => $model,
			'attributes' => [
				'address_1:ntext',
				'address_2:ntext',
				'country',
				'state',
				'city',
				'zip_code',
				'email:email',
				'type',
				'location',
			],
		]) ?>
		</div>
	</div>
	<div class="row">
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>Contact Name</th>
					<th>Contact Phone</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($modelsContact as $indexHouse => $modelContact): ?>
			<tr>
					<td>
						<?= yii\widgets\DetailView::widget([
								'model' => $modelContact,
								'attributes' => [
									'firstname',
									'lastname',
								],
								'options'=>['class' => 'table table-bordered']
							]) ?>
					</td>
					<?php 
						$modelsPhone = \app\models\Phone::find()->where(['id_contact'=>$modelContact->id])->all();
					?>
					<td>
						<table>
							<tbody>
							<?php foreach ($modelsPhone as $indexRoom => $modelPhone): ?>
								<tr>
									<td>
										<?= yii\widgets\DetailView::widget([
											'model' => $modelPhone,
											'attributes' => [
												'phone_type',
												'phone_number',
											],
											'options'=>['class' => 'table table-bordered']
										]) ?>
									</td>
								</tr>
							 <?php endforeach; ?>
							</tbody>
						</table>
					</td>
			</tr>
			 <?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MasterPartySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $title;
?>

<?php
		yii\bootstrap\Modal::begin([
			'headerOptions' => ['id' => 'modalHeader'],
			'id' => 'modal',
			'size' => 'modal-lg',
			
		]);
		echo "<div id='modalContent'></div>";
		yii\bootstrap\Modal::end();
	?>
<div class="master-party-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php //echo Html::a(Yii::t('app', 'Create Master Party'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<div class="box box-default">
			<div class="box-header with-border">
			  <h3 class="box-title">New Request</h3>
			  <div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
				</button>
			  </div>
			</div>
				<div class="box-body">
				<?= GridView::widget([
					'dataProvider' => $dataProvider,
					// 'filterModel' => $searchModel,
					'columns' => [
						['class' => 'yii\grid\SerialColumn'],

						// 'id',
						'no_doc',
						'status',
						'req_date',
						'updated_at',
						'requestor',            
						// 'validator',
						// 'approver',
						// 'support',
						// 'support_date',
						// 'remark:ntext',
						// 'client',
						// 'code',
						'name',
						// 'office',
						// 'address_1:ntext',
						// 'address_2:ntext',
						// 'country',
						// 'state',
						// 'city',
						// 'zip_code',
						// 'contact_person',
						// 'phone_number',
						// 'email:email',
						// 'type',

						['class' => 'yii\grid\ActionColumn', 'template' => '{update}',
							'buttons'=>[
								'update' => function ($url, $model, $key) {
									return Html::button('Detail', ['value' => Url::to(['/finish/detailvendor','id'=>$model->id]), 'title' => 'Detail '.$model->no_doc, 'class' => 'showModalButton btn btn-link']);
								},
							],
						],
					],
				]); ?>
				</div>
	</div>
	<div class="box box-default">
			<div class="box-header with-border">
			  <h3 class="box-title">Update Request</h3>
			  <div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
				</button>
			  </div>
			</div>
				<div class="box-body">
					<?= GridView::widget([
					'dataProvider' => $updateProvider,
					// 'filterModel' => $searchModel,
					'columns' => [
						['class' => 'yii\grid\SerialColumn'],

						// 'id',
						'no_doc',
						// 'status',
						'req_date',
						'requestor',
						'status',
						// 'validator',
						// 'approver',
						// 'support',
						// 'support_date',
						// 'remark:ntext',
						// 'client',
						// 'code',
						'name',
						// 'office',
						// 'address_1:ntext',
						// 'address_2:ntext',
						// 'country',
						// 'state',
						// 'city',
						// 'zip_code',
						// 'contact_person',
						// 'phone_number',
						// 'email:email',
						// 'type',

						['class' => 'yii\grid\ActionColumn', 'template' => '{update}',
							'buttons'=>[
								'update' => function ($url, $model, $key) {
									return Html::button('Detail', ['value' => Url::to(['/finish/detailupdate','id'=>$model->no_doc]), 'title' => 'Detail '.$model->no_doc, 'class' => 'showModalButton btn btn-link']);
								},
							],
						],
					],
				]); ?>
				</div>
	</div>
	<script type="text/javascript">
    //get the click of modal button to create / update item
    //we get the button by class not by ID because you can only have one id on a page and you can
    //have multiple classes therefore you can have multiple open modal buttons on a page all with or without
    //the same link.
//we use on so the dom element can be called again if they are nested, otherwise when we load the content once it kills the dom element and wont let you load anther modal on click without a page refresh
      $(document).on('click', '.showModalButton', function(){
        //check if the modal is open. if it's open just reload content not whole modal
        //also this allows you to nest buttons inside of modals to reload the content it is in
        //the if else are intentionally separated instead of put into a function to get the 
        //button since it is using a class not an #id so there are many of them and we need
        //to ensure we get the right button and content. 
        if ($('#modal').data('bs.modal').isShown) {
            $('#modal').find('#modalContent')
                    .load($(this).attr('value'));
            //dynamiclly set the header for the modal via title tag
            document.getElementById('modalHeader').innerHTML = '<h4> <?php echo $title; ?> </h4>';
        } else {
            //if modal isn't open; open it and load content
            $('#modal').modal('show')
                    .find('#modalContent')
                    .load($(this).attr('value'));
             //dynamiclly set the header for the modal via title tag
            document.getElementById('modalHeader').innerHTML = '<h4> <?php echo $title; ?> </h4>';
        }
    });
</script>
</div>

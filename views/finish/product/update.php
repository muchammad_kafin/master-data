<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\assets\AppAsset;
AppAsset::register($this);
dmstr\web\AdminLteAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\MasterClient */

$this->title = $model->no_doc;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="master-client-view">
	<div class="row">
		<div class="col-lg-6">
			<?= DetailView::widget([
				'model' => $model,
				'attributes' => [
					'no_doc',
					'created_at',
					'req_date',
					'remark',
					'remark_cancel',
				],
			]) ?>
		</div>
		<div class="col-lg-6">
			<?= DetailView::widget([
				'model' => $model,
				'attributes' => [
					'requestor',
					'code',
					'name',
					'parent_category',
				],
			]) ?>
		</div>
	</div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

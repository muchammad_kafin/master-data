<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MasterPartySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Master Parties');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-party-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php //echo Html::a(Yii::t('app', 'Create Master Party'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'no_doc',
            'status',
            'req_date',
            'requestor',
            // 'validator',
            // 'approver',
            // 'support',
            // 'support_date',
            // 'remark:ntext',
            // 'client',
            // 'code',
            // 'name',
            // 'office',
            // 'address_1:ntext',
            // 'address_2:ntext',
            // 'country',
            // 'state',
            // 'city',
            // 'zip_code',
            // 'contact_person',
            // 'phone_number',
            // 'email:email',
            // 'type',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

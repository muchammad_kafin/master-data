<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\assets\AppAsset;
AppAsset::register($this);
dmstr\web\AdminLteAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\MasterClient */

$this->title = $model->no_doc;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="master-client-view">
	<div class="row">
	<div class="col-lg-6">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'no_doc',
            // 'status',
            'req_date',
            'requestor',
            'support_date',
            'remark:ntext',
            'remark_cancel:ntext',
        ],
    ]) ?>
	</div>
	<div class="col-lg-6">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'vendor_code',
            'vendor_name',
            'sk_no',
            'start_periode',
            'end_periode',
            'jenis_sewa',
            'item_code',
            'uom',
        ],
    ]) ?>
	</div>
	</div>
	<div class="row">
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>Uploaded File</th>
				</tr>
			</thead>
				<?php foreach ($modelsUpload as $files => $modelUpload): ?>
					<?= yii\widgets\DetailView::widget([
							'model' => $modelUpload,
							'attributes' => [						
								[
									'label' => $modelUpload->filename,
									'format' => 'html',
									'value' =>Html::a($modelUpload->filename, ['site/download','file'=>$modelUpload->stored_file,'path'=>$modelUpload->filepath]),
								],
							],
							'options'=>['class' => 'table table-bordered']
						]) ?>
				 <?php endforeach; ?>
		</table>
	</div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\assets\AppAsset;
AppAsset::register($this);
dmstr\web\AdminLteAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\MasterClient */

$this->title = $model->no_doc;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="master-client-view">
	<div class="row">
		<?= DetailView::widget([
			'model' => $model,
			'attributes' => [
				'no_doc',
				'status',
				'req_date',
				'requestor',
				'support_date',
				'remark:ntext',
				'reason:ntext',
				'request:ntext',
				'code',
				'name',
				'remark_cancel:ntext',
				'remark_validator:ntext',
				'remark_support:ntext',
			],
		]) ?>
	</div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

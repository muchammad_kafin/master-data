<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\assets\AppAsset;
AppAsset::register($this);
dmstr\web\AdminLteAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\MasterClient */

$this->title = $model->no_doc;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="master-charge-view">
	<div class="row">
		<div class="col-lg-6">
		<?= DetailView::widget([
			'model' => $model,
			'attributes' => [
				'no_doc',
				'status',
				'req_date',
				'requestor',
				'support_date',
				'remark:ntext',
				'remark_cancel:ntext',
				'reimbursable',
				'commissionable',
				'profit_shared',
				'tax_group',
			],
		]) ?>
		</div>
		<div class="col-lg-6">
		<?= DetailView::widget([
			'model' => $model,
			'attributes' => [
				'code',
				'name',
				'job_type',
				'office_map',
				'printing_code',
				'printing_sequence',
				'revenue_posting',
				'cost_posting',
				'currency',
				'applicable_for',
				'based_on',
				'created_at',
				'updated_at',
			],
		]) ?>
		</div>
	</div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

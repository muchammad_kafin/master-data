<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MasterDebtor */

$this->title = Yii::t('app', 'Request Master Data Debtor');
?>
<div class="master-debtor-create">

    <h1><center><?= Html::encode($this->title) ?></center></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelsContact' => $modelsContact,
        'modelsPhone' => $modelsPhone,
    ]) ?>

</div>

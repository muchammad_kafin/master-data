<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
use yii\helpers\ArrayHelper;
use kartik\widgets\Typeahead;
use kartik\widgets\TypeaheadBasic;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
use yii\widgets\MaskedInput;
use yii\bootstrap\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\jui\AutoComplete;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\masterdebtor */
/* @var $form yii\widgets\ActiveForm */
?>
<style> 
input[type=text] {
	text-transform: uppercase;
}
textarea {
	text-transform: uppercase;
}
</style>
<div class="master-vendor-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form','options'=>['enctype'=>'multipart/form-data']]); ?>
	
	<div class="row">
		<div class="col-lg-3">
		<p><strong>Please Fill with Capital Letter !</strong></p>
		</div>
		<div class="col-lg-3">
		</div>
		<div class="col-lg-2">
		</div>
		<div class="col-lg-4">
			<div class="row">
				<div class="col-lg-4">
					<?= Html::activeLabel($model, 'no_doc'); ?>		
				</div>
				<div class="col-lg-8">
					<?= $form->field($model, 'no_doc')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
				</div>
			</div>
			<div class="row hide">
				<div class="col-lg-4">
					<?= Html::activeLabel($model, 'status'); ?>		
				</div>
				<div class="col-lg-8">
					<?= $form->field($model, 'status')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
					<?= $form->field($model, 'validator')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
					<?= $form->field($model, 'acknowledges')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
					<?= $form->field($model, 'support')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4">
					<?= Html::activeLabel($model, 'req_date'); ?>		
				</div>
				<div class="col-lg-8">
					<?= $form->field($model, 'req_date')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			<?= $form->field($model, 'sector')->widget(Select2::classname(), [
				'data' => $model->getSecArray(),
				'options' => ['placeholder' => 'Select a Sector ...',
							'onchange'=>'
							JS: var value = (this.value);
							$( "input#masterdebtor-code" ).val("C."+value+".XXXXX.011");
							$( "input#masterdebtor-tax_address_code" ).val("C."+value);
							$( "input#masterdebtor-debtor_group" ).val("C."+value);
							$( "input#masterdebtor-account_code" ).val("1.1.03.01.00100");
						'],
				'pluginOptions' => [
					'allowClear' => true
				],
			]); ?>
			<?= $form->field($model, 'code')->textInput(['maxlength' => true,'readOnly'=> true]) ?>			
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'tax_address_code')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
			<?= $form->field($model, 'account_code')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'debtor_group')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
			<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			<?= $form->field($model, 'npwp')->widget(MaskedInput::classname(), [
				'mask' => '99.999.999.9-999.999',
			]) ?>
			<?= $form->field($model, 'special_intru')->textarea(['rows' => 2]) ?>
			<?php $dataOffice = ArrayHelper::map(\app\models\OfficeType::find()->all(), 'code', 'name'); ?>
				<?= $form->field($model, 'address_type')->widget(Select2::classname(), [
					'data' => $dataOffice,
					'options' => ['placeholder' => 'Select a Type ...'],
					'pluginOptions' => [
						'allowClear' => true
					],
				]); ?>
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'address_1')->textarea(['rows' => 2,'maxlength' => true]) ?>
			<?= $form->field($model, 'address_3')->textarea(['rows' => 2,'maxlength' => true]) ?>
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'address_2')->textarea(['rows' => 2,'maxlength' => true]) ?>
			<?= $form->field($model, 'address_4')->textarea(['rows' => 2,'maxlength' => true]) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			<?php $dataCountry = \app\models\Country::find()->select('name as value, code as id')->asArray()->all(); ?>
					<?= $form->field($model, 'country_name')->widget(AutoComplete::className(), [
					'options' => ['class' => 'form-control'], 
					'clientOptions' => ['source' =>  $dataCountry ,
						'select' => new JsExpression("function( event, ui ) {
						$('#". Html::getInputId($model, 'country')."').val(ui.item.id).trigger('change');
						$('#masterdebtor-country_name').val(ui.item.value);
						console.log($('#". Html::getInputId($model, 'country')."').val());
							}"),
					'autoFill'=>true,]]) ?>
					<?= Html::activeHiddenInput($model, 'country')?>
			
			<?= $form->field($model, 'email')->widget(MaskedInput::classname(), [
				'clientOptions' => [
					'alias' =>  'email',
				],
			]) ?>
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'state')->textInput(['maxlength' => true]) ?>
			
			<?= $form->field($model, 'zip_code')->widget(MaskedInput::classname(), [
				'mask' => '*{1,10}'
			]) ?>
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
			
			<?php $dataMap = ArrayHelper::map(\app\models\OfficeMap::find()->all(), 'code', 'name'); ?>
			<?= $form->field($model, 'office_code')->widget(Select2::classname(), [
					'data' => $dataMap,
					'options' => ['placeholder' => 'Select Office ...', 'multiple' => true],
					'pluginOptions' => [
						'allowClear' => true
					],
				]); ?>
		</div>
	</div>
	<div class="row">
		<div class="padding-v-md">
			<div class="line line-dashed"></div>
		</div>
	 
		<?php DynamicFormWidget::begin([
			'widgetContainer' => 'dynamicform_wrapper',
			'widgetBody' => '.container-items',
			'widgetItem' => '.contact-item',
			'limit' => 5,
			'min' => 1,
			'insertButton' => '.add-contact',
			'deleteButton' => '.remove-contact',
			'model' => $modelsContact[0],
			'formId' => 'dynamic-form',
			'formFields' => [
				'firstname',
				'lastname',
			],
		]); ?>
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>Contact Name</th>
					<th style="width: 450px;">Contact Phone</th>
					<th class="text-center" style="width: 90px;">
						<button type="button" class="add-contact btn btn-success btn-xs"><span class="fa fa-plus"></span></button>
					</th>
				</tr>
			</thead>
			<tbody class="container-items">
			<?php foreach ($modelsContact as $indexHouse => $modelContact): ?>
				<tr class="contact-item">
					<td class="vcenter">
						<?php
							// necessary for update action.
							if (!$modelContact->isNewRecord) {
								echo Html::activeHiddenInput($modelContact, "[{$indexHouse}]id");
							}
						?>
						<?= $form->field($modelContact, "[{$indexHouse}]firstname")->textInput(['maxlength' => true]) ?>
						<?= $form->field($modelContact, "[{$indexHouse}]lastname")->textInput(['maxlength' => true]) ?>
					</td>
					<td>
						<?= $this->render('_form-contact', [
							'form' => $form,
							'indexHouse' => $indexHouse,
							'modelsPhone' => $modelsPhone[$indexHouse],
						]) ?>
					</td>
					<td class="text-center vcenter" style="width: 90px; verti">
						<button type="button" class="remove-contact btn btn-danger btn-xs"><span class="fa fa-minus"></span></button>
					</td>
				</tr>
			 <?php endforeach; ?>
			</tbody>
		</table>
		<?php DynamicFormWidget::end(); ?>
	</div>
	<div class="row">
		<div class="col-lg-3">
			<?= $form->field($model, 'credit_term')->widget(Select2::classname(), [
					'data' => $model->getDayArray(),
					'options' => ['placeholder' => 'Select ...'],
					'pluginOptions' => [
						'allowClear' => true
					],
				]); ?>
		</div>
		<div class="col-lg-3">
			<?php $dataCurrency = ArrayHelper::map(\app\models\Currency::find()->all(), 'code', 'code'); ?>
					<?= $form->field($model, 'billing_currency')->widget(TypeaheadBasic::classname(), [
						'data' => $dataCurrency,
						'options' => ['placeholder' => 'Type a Currency ...'],
						'pluginOptions' => ['highlight'=>true],
					]); ?>
		</div>
		<div class="col-lg-3">
			
		</div>	
		<div class="col-lg-3">
			<?= $form->field($model, 'bank_name')->textInput(['maxlength' => true]) ?>
		</div>				
	</div>
	<div class="row">
		<div class="col-lg-3">
			<?= $form->field($model, 'account_name')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-lg-3">
			<?= $form->field($model, 'account_no')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-lg-3">
			<?= $form->field($model, 'branch')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-lg-3">
			<?= $form->field($model, 'swift_code')->textInput(['maxlength' => true]) ?>
		</div>	
	</div>
	<?= $form->field($model, 'getfile[]')->widget(\kartik\widgets\FileInput::classname(), [
		    'options' => ['multiple' => true],
			'pluginOptions' => ['previewFileType' => 'any','showUpload' => false]
	]); ?>
	<?= $form->field($model, 'remark')->textarea(['rows' => 2]) ?>
	<div class="row">
		<div class="col-lg-3">
			<?= $form->field($model, 'requestor')->textInput(['maxlength' => true]) ?>			
			<?= $form->field($model, 'email_requestor')->textInput(['maxlength' => true,
				'onKeyUp'=>'
						$.post( "'.Yii::$app->urlManager->createUrl('site/validator').'&requestor='.'"+$(this).val()+"'.'&role=validator&master=debtor", function( data ) {							
						  $( "input#masterdebtor-validator_name" ).val( data.username);
						  $( "input#masterdebtor-email_validator" ).val( data.email);
						  $( "input#masterdebtor-validator" ).val( data.id);
						});
			']) ?>
		</div>
		<div class="col-lg-3">
		<?= $form->field($model, 'validator_name')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		<?= $form->field($model, 'email_validator')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-3">
		<?= $form->field($model, 'acknowledges_name')->textInput(['maxlength' => true,'readOnly' => true]) ?>
		<?= $form->field($model, 'email_acknowledges')->textInput(['maxlength' => true,'readOnly'=> true]) ?>				
		</div>
		<div class="col-lg-3">
		<?= $form->field($model, 'support_name')->textInput(['maxlength' => true,'readOnly' => true]) ?>
		<?= $form->field($model, 'email_support')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		<?= $form->field($model, 'support_date')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
	</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

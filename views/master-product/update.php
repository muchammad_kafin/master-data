<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterProduct */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Master Product',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Master Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="master-product-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

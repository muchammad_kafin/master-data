<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\User;
use yii\helpers\ArrayHelper;
use kartik\widgets\Typeahead;
use kartik\widgets\TypeaheadBasic;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\MasterClient */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-client-form">

    <?php $form = ActiveForm::begin(); ?>
	
	<div class="row">
		<div class="col-lg-3">
		<p><strong>Please Fill with Capital Letter !</strong></p>
		</div>
		<div class="col-lg-3">
		</div>
		<div class="col-lg-2">
		</div>
		<div class="col-lg-4">
			<div class="row">
				<div class="col-lg-4">
					<?= Html::activeLabel($model, 'no_doc'); ?>		
				</div>
				<div class="col-lg-8">
					<?= $form->field($model, 'no_doc')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
				</div>
			</div>
			<div class="row hide">
				<div class="col-lg-4">
					<?= Html::activeLabel($model, 'status'); ?>		
				</div>
				<div class="col-lg-8">
					<?= $form->field($model, 'status')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
					<?= $form->field($model, 'validator')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
					<?= $form->field($model, 'approver')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
					<?= $form->field($model, 'support')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4">
					<?= Html::activeLabel($model, 'req_date'); ?>		
				</div>
				<div class="col-lg-8">
					<?= $form->field($model, 'req_date')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<?= $form->field($model, 'code')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-6">
			<?= $form->field($model, 'name')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<?= $form->field($model, 'address_1')->textarea(['rows' => 2,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-6">
			 <?= $form->field($model, 'address_2')->textarea(['rows' => 2,'readOnly'=> true]) ?>
		</div>
	</div>
    <div class="row">
		<div class="col-lg-3">
			<?= $form->field($model, 'country_name')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-3">
			<?= $form->field($model, 'state_name')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-3">
			<?= $form->field($model, 'city_name')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-3">
			<?= $form->field($model, 'zip_code')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			<?= $form->field($model, 'web')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-8">
			<fieldset>
				<legend>Contact Person</legend>
				<?= $form->field($model, 'firstname')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
				<?= $form->field($model, 'lastname')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
			</fieldset>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			<div class="col-lg-4">
					<?= $form->field($model, 'phone_type')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
			</div>
			<div class="col-lg-8">
				<?= $form->field($model, 'phone_number')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
			</div>
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'email')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'base_currency')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
	</div>
	<?= $form->field($model, 'remark')->textarea(['rows' => 2,'readOnly'=> true]) ?>
	<div class="row">
		<div class="col-lg-3">
			<?= $form->field($model, 'requestor')->textInput(['maxlength' => true,'readOnly'=> true]) ?>			
			<?= $form->field($model, 'email_requestor')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-3">
		<?= $form->field($model, 'validator_name')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		<?= $form->field($model, 'email_validator')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-3">
		<?= $form->field($model, 'approver_name')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		<?= $form->field($model, 'email_approver')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-3">
		<?= $form->field($model, 'support_name')->textInput(['maxlength' => true,'readOnly' => true]) ?>
		<?= $form->field($model, 'email_support')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		<?= $form->field($model, 'support_date')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
	</div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

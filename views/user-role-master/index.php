<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserRoleMasterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'User Role Masters');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-role-master-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create User Role Master'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
			 'attribute' => 'user',
			 'value' => 'users.username'
			],
			[
			 'attribute' => 'master',
			 'value' => 'masters.name'
			],
            [
			 'attribute' => 'role',
			 'value' => 'roles.name'
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserRoleMaster */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-role-master-form">

    <?php $form = ActiveForm::begin(); ?>
	
	 <?php echo $form->field($model, 'id_user')->dropDownList(\app\models\User::getUserArray(),['prompt'=>'Select...']) ?>
	 
	 <?php echo $form->field($model, 'id_role')->dropDownList(\app\models\User::getRoleArray(),['prompt'=>'Select...']) ?>
	
    <?php echo $form->field($model, 'id_master')->dropDownList(\app\models\User::getMasterArray(),['prompt'=>'Select...']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

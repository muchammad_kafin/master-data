<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UserRoleMaster */

$this->title = Yii::t('app', 'Create User Role Master');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Role Masters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-role-master-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

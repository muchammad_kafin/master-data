<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserRoleMaster */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'User Role Master',
]) . $model->id_user;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Role Masters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_user, 'url' => ['view', 'id_user' => $model->id_user, 'id_master' => $model->id_master, 'id_role' => $model->id_role]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="user-role-master-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

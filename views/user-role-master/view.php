<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\UserRoleMaster */

$this->title = $model->id_user;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Role Masters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-role-master-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id_user' => $model->id_user, 'id_master' => $model->id_master, 'id_role' => $model->id_role], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id_user' => $model->id_user, 'id_master' => $model->id_master, 'id_role' => $model->id_role], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
			 'attribute' => 'users',
			 'value' => strtoupper($model->users->username)
			],
			[
			 'attribute' => 'masters',
			 'value' => strtoupper($model->masters->name)
			],
            [
			 'attribute' => 'roles',
			 'value' => strtoupper($model->roles->name)
			],
        ],
    ]) ?>

</div>

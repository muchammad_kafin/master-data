<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Timezone;
use yii\helpers\ArrayHelper;
use kartik\widgets\Typeahead;
use kartik\widgets\TypeaheadBasic;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\MasterLocation */
/* @var $form yii\widgets\ActiveForm */
?>
<style> 
input[type=text] {
	text-transform: uppercase;
}
textarea {
	text-transform: uppercase;
}
</style>
<div class="master-location-form">

    <?php $form = ActiveForm::begin(); ?>
	
	<?= \nifak\latlang\LatLngFinder::widget([
		'model' => $model,              // model object
		'latAttribute' => 'latitude',        // Latitude attribute
		'lngAttribute' => 'longitude',        // Longitude attribute
		'cityAttribute' => 'city',        // Longitude attribute
		'mapCanvasId' => 'map',         // Map Canvas id
		'mapWidth' => 1050,              // Map Canvas width
		'mapHeight' => 600,             // Map Canvas mapHeight
		'defaultLat' => -1.379749171598816,        // Default latitude for the map
		'defaultLng' =>117.27978515625,         // Default Longitude for the map
		'defaultZoom' => 5,             // Default zoom for the map
		'enableZoomField' => false,      // True: for assigning zoom values to the zoom field, False: Do not assign zoom value to the zoom field
	]); ?>
	
	<div class="row">
		<div class="col-lg-3">
		<p><strong>Please Fill with Capital Letter !</strong></p>
		</div>
		<div class="col-lg-3">
		</div>
		<div class="col-lg-2">
		</div>
		<div class="col-lg-4">
			<div class="row">
				<div class="col-lg-4">
					<?= Html::activeLabel($model, 'no_doc'); ?>		
				</div>
				<div class="col-lg-8">
					<?= $form->field($model, 'no_doc')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
				</div>
			</div>
			<div class="row hide">
				<div class="col-lg-4">
					<?= Html::activeLabel($model, 'status'); ?>		
				</div>
				<div class="col-lg-8">
					<?= $form->field($model, 'status')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
					<?= $form->field($model, 'validator')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
					<?= $form->field($model, 'acknowledges')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
					<?= $form->field($model, 'support')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4">
					<?= Html::activeLabel($model, 'req_date'); ?>		
				</div>
				<div class="col-lg-8">
					<?= $form->field($model, 'req_date')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			<?= $form->field($model, 'code')->widget(MaskedInput::classname(), [
				'mask' => '*{1,10}',
			]) ?>
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
		</div>			
		<div class="col-lg-4">
			<?php $dataType = ["AIRPORT"=>"AIRPORT","INLAND PORT"=>"INLAND PORT","LOCATION"=>"LOCATION","SEAPORT"=>"SEAPORT","STATION"=>"STATION","TERMINAL"=>"TERMINAL"]; ?>
			<?= $form->field($model, 'type')->widget(Select2::classname(), [
				'data' => $dataType,
				'options' => ['placeholder' => 'Select a Type ...'],
				'pluginOptions' => [
					'allowClear' => true
				],
			]); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			<?= $form->field($model, 'latitude')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'longitude')->textInput(['maxlength' => true]) ?>
		</div>			
		<div class="col-lg-4">
			<?php $dataTime = ArrayHelper::map(\app\models\Timezone::find()->orderBy('name')->all(), 'name', 'name'); ?>
			<?= $form->field($model, 'timezone')->widget(Select2::classname(), [
				'data' => $dataTime,
				'options' => ['placeholder' => 'Select a Timezone ...'],
				'pluginOptions' => [
					'allowClear' => true
				],
			]); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			<?= $form->field($model, 'country')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
		</div>			
		<div class="col-lg-4">
			<?= $form->field($model, 'un_code')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
	<?= $form->field($model, 'remark')->textarea(['rows' => 2]) ?>
	<div class="row">
		<div class="col-lg-3">
			<?= $form->field($model, 'requestor')->textInput(['maxlength' => true]) ?>			
			<?= $form->field($model, 'email_requestor')->textInput(['maxlength' => true,
				'onKeyUp'=>'
						$.post( "'.Yii::$app->urlManager->createUrl('site/validator').'&requestor='.'"+$(this).val()+"'.'&role=validator&master=location", function( data ) {							
						  $( "input#masterlocation-validator_name" ).val( data.username);
						  $( "input#masterlocation-email_validator" ).val( data.email);
						  $( "input#masterlocation-validator" ).val( data.id);
						});
			']) ?>
		</div>
		<div class="col-lg-3">
		<?= $form->field($model, 'validator_name')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		<?= $form->field($model, 'email_validator')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-3">
		<?= $form->field($model, 'acknowledges_name')->textInput(['maxlength' => true,'readOnly' => true]) ?>
		<?= $form->field($model, 'email_acknowledges')->textInput(['maxlength' => true,'readOnly'=> true]) ?>		
		</div>
		<div class="col-lg-3">
		<?= $form->field($model, 'support_name')->textInput(['maxlength' => true,'readOnly' => true]) ?>
		<?= $form->field($model, 'email_support')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		<?= $form->field($model, 'support_date')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
	</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

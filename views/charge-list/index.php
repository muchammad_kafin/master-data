<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ChargeListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Charge Lists');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="charge-list-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Charge List'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'code',
            'name',
            'job_type',
            'office_map',
            // 'printing_code',
            // 'printing_sequence',
            // 'revenue_posting',
            // 'cost_posting',
            // 'currency',
            // 'reimbursable',
            // 'commissionable',
            // 'profit_shared',
            // 'tax_group',
            // 'applicable_for',
            // 'based_on',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

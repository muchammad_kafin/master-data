<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ChargeList */

$this->title = Yii::t('app', 'Create Charge List');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Charge Lists'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="charge-list-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ChargeList */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="charge-list-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'job_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'office_map')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'printing_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'printing_sequence')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'revenue_posting')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cost_posting')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'currency')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reimbursable')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'commissionable')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'profit_shared')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tax_group')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'applicable_for')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'based_on')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

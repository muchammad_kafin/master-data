<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ChargeListSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="charge-list-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'code') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'job_type') ?>

    <?= $form->field($model, 'office_map') ?>

    <?php // echo $form->field($model, 'printing_code') ?>

    <?php // echo $form->field($model, 'printing_sequence') ?>

    <?php // echo $form->field($model, 'revenue_posting') ?>

    <?php // echo $form->field($model, 'cost_posting') ?>

    <?php // echo $form->field($model, 'currency') ?>

    <?php // echo $form->field($model, 'reimbursable') ?>

    <?php // echo $form->field($model, 'commissionable') ?>

    <?php // echo $form->field($model, 'profit_shared') ?>

    <?php // echo $form->field($model, 'tax_group') ?>

    <?php // echo $form->field($model, 'applicable_for') ?>

    <?php // echo $form->field($model, 'based_on') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MasterChargeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-charge-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'no_doc') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'req_date') ?>

    <?= $form->field($model, 'requestor') ?>

    <?php // echo $form->field($model, 'email_requestor') ?>

    <?php // echo $form->field($model, 'validator') ?>

    <?php // echo $form->field($model, 'email_validator') ?>

    <?php // echo $form->field($model, 'acknowledges') ?>

    <?php // echo $form->field($model, 'email_acknowledges') ?>

    <?php // echo $form->field($model, 'support') ?>

    <?php // echo $form->field($model, 'email_support') ?>

    <?php // echo $form->field($model, 'support_date') ?>

    <?php // echo $form->field($model, 'remark') ?>

    <?php // echo $form->field($model, 'remark_cancel') ?>

    <?php // echo $form->field($model, 'code') ?>

    <?php // echo $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'job_type') ?>

    <?php // echo $form->field($model, 'office_map') ?>

    <?php // echo $form->field($model, 'printing_code') ?>

    <?php // echo $form->field($model, 'printing_sequence') ?>

    <?php // echo $form->field($model, 'revenue_posting') ?>

    <?php // echo $form->field($model, 'cost_posting') ?>

    <?php // echo $form->field($model, 'currency') ?>

    <?php // echo $form->field($model, 'reimbursable') ?>

    <?php // echo $form->field($model, 'commissionable') ?>

    <?php // echo $form->field($model, 'profit_shared') ?>

    <?php // echo $form->field($model, 'tax_group') ?>

    <?php // echo $form->field($model, 'applicable_for') ?>

    <?php // echo $form->field($model, 'based_on') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

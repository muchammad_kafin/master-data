<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MasterCharge */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Master Charges'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-charge-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'no_doc',
            'status',
            'req_date',
            'requestor',
            'email_requestor:email',
            'validator',
            'email_validator:email',
            'acknowledges',
            'email_acknowledges:email',
            'support',
            'email_support:email',
            'support_date',
            'remark:ntext',
            'remark_cancel:ntext',
            'code',
            'name',
            'job_type',
            'office_map',
            'printing_code',
            'printing_sequence',
            'revenue_posting',
            'cost_posting',
            'currency',
            'reimbursable',
            'commissionable',
            'profit_shared',
            'tax_group',
            'applicable_for',
            'based_on',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>

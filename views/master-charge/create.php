<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MasterCharge */

$this->title = Yii::t('app', 'Request Master Data Charge Code');
?>
<div class="master-charge-create">

     <h1><center><?= Html::encode($this->title) ?></center></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

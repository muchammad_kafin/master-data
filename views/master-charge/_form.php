<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
use yii\helpers\ArrayHelper;
use kartik\widgets\Typeahead;
use kartik\widgets\TypeaheadBasic;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
use yii\widgets\MaskedInput;
use yii\bootstrap\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\jui\AutoComplete;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\MasterCharge */
/* @var $form yii\widgets\ActiveForm */
?>

<style> 
input[type=text] {
	text-transform: uppercase;
}
textarea {
	text-transform: uppercase;
}
</style>

<div class="master-charge-form">

    <?php $form = ActiveForm::begin(); ?>
	
	<div class="row">
		<div class="col-lg-3">
		<p><strong>Please Fill with Capital Letter !</strong></p>
		</div>
		<div class="col-lg-3">
		</div>
		<div class="col-lg-2">
		</div>
		<div class="col-lg-4">
			<div class="row">
				<div class="col-lg-4">
					<?= Html::activeLabel($model, 'no_doc'); ?>		
				</div>
				<div class="col-lg-8">
					<?= $form->field($model, 'no_doc')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
				</div>
			</div>
			<div class="row hide">
				<div class="col-lg-4">
					<?= Html::activeLabel($model, 'status'); ?>		
				</div>
				<div class="col-lg-8">
					<?= $form->field($model, 'status')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
					<?= $form->field($model, 'validator')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
					<?= $form->field($model, 'acknowledges')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
					<?= $form->field($model, 'support')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4">
					<?= Html::activeLabel($model, 'req_date'); ?>		
				</div>
				<div class="col-lg-8">
					<?= $form->field($model, 'req_date')->textInput(['maxlength' => true,'readOnly'=> true])->label(false) ?>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-4">
			<?= $form->field($model, 'code')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-4">
			<?php $dataName = ArrayHelper::map(\app\models\ChargeList::find()->all(), 'name', 'name'); ?>
			<?= $form->field($model, 'name')->widget(TypeaheadBasic::classname(), [
						'data' => $dataName,
						'options' => [
						'onKeyUp'=>'
						$.post( "'.Yii::$app->urlManager->createUrl('master-charge/exists').'&name='.'"+$(this).val(), 
						function( data ) {
							if(data.code){
								$( "input#mastercharge-code" ).val( data.code);
							}else{
								$( "input#mastercharge-code" ).val("-");
							}
						  $( "select#mastercharge-job_type" ).val( data.job_type);
						  $( "select#mastercharge-office_map" ).val( data.office_map);
						  $( "input#mastercharge-printing_code" ).val( data.printing_code);
						  $( "input#mastercharge-printing_sequence" ).val( data.printing_sequence);
						  $( "input#mastercharge-currency" ).val( data.currency);
						  $( "input#mastercharge-cost_posting" ).val( data.cost_posting);
						  $( "input#mastercharge-revenue_posting" ).val( data.revenue_posting);
						  if(data.reimbursable == 1){
							$( "#mastercharge-reimbursable" ).prop("checked", true);
						  }else{
							$( "#mastercharge-reimbursable" ).prop("checked", false);
						  }
						  $( "input#mastercharge-commissionable" ).val( data.commissionable);
						  $( "input#mastercharge-profit_shared" ).val( data.profit_shared);
						  $( "select#mastercharge-tax_group" ).val( data.tax_group);
						  $( "select#mastercharge-applicable_for" ).val( data.applicable_for);
						  $( "select#mastercharge-based_on" ).val( data.based_on);
						});',
						'placeholder' => 'Type a Name ...'],
						'pluginOptions' => ['highlight'=>true],
					]); ?>
		</div>
		<div class="col-lg-4">
			<?php $arrayJob = [
						["code"=>"SEA","name"=>"SEA"],
						["code"=>"AIR","name"=>"AIR"],
						["code"=>"ROAD","name"=>"ROAD"],
						["code"=>"WAREHOUSE","name"=>"WAREHOUSE"],
						];
				$job = ArrayHelper::map($arrayJob, 'code', 'name');
			?>
			<?= $form->field($model, 'job_type')->dropDownList($job,['multiple'=>'multiple']); ?>
		</div>		
	</div>
	
	<div class="row">
		<div class="col-lg-4">
			<?php $dataMap = ArrayHelper::map(\app\models\OfficeMap::find()->all(), 'code', 'name'); ?>
			<?= $form->field($model, 'office_map')->dropDownList($dataMap,['multiple'=>'multiple']); ?>
		</div>	
		<div class="col-lg-4">
			<?= $form->field($model, 'printing_code')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'printing_sequence')->textInput(['maxlength' => true]) ?>
		</div>		
	</div>
	
	<div class="row">
		<div class="col-lg-4">
			<?php $dataCurrency = ["IDR"=>"IDR","USD"=>"USD"]; ?>
			<?= $form->field($model, 'currency')->widget(TypeaheadBasic::classname(), [
						'data' => $dataCurrency,
						'options' => ['placeholder' => 'Type a Currency ...'],
						'pluginOptions' => ['highlight'=>true],
					]); ?>
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'cost_posting')->textInput(['maxlength' => true]) ?>
		</div>	
		<div class="col-lg-4">
			<?= $form->field($model, 'revenue_posting')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
		
		<div class="col-lg-3">
			<?= $form->field($model, 'reimbursable')->checkbox(); ?>
		</div>
		<div class="col-lg-3">
			<?= $form->field($model, 'commissionable')->checkbox(); ?>
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'profit_shared')->checkbox(); ?>
		</div>
		</div>
		<div class="col-lg-3">
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-4">
			<?php
				$tax1=[
					'tax_group'=>'NA',
				];
				$taxg = ArrayHelper::map(\app\models\ChargeList::find()->select('tax_group')->orderBy('tax_group ASC')->all(), 'tax_group', 'tax_group');
				$tax = ArrayHelper::merge($tax1,$taxg);	
			?>
			<?= $form->field($model, 'tax_group')->dropDownList($taxg); ?>
		</div>
		<div class="col-lg-4">
			<?php 
				$app1=[
					'NA'=>'NA',
				];
				$app2 = ArrayHelper::map(\app\models\ChargeList::find()->select('applicable_for')->orderBy('applicable_for ASC')->all(), 'applicable_for', 'applicable_for'); 
				$app = ArrayHelper::merge($app1,$app2); 
			?>
			<?= $form->field($model, 'applicable_for')->dropDownList($app); ?>
		</div>
		<div class="col-lg-4">
			<?php 
				$base1=[
				'NA'=>'NA',
				];
				$base2 = ArrayHelper::map(\app\models\ChargeList::find()->select('based_on')->orderBy('based_on ASC')->all(), 'based_on', 'based_on'); 
				$base = ArrayHelper::merge($base1,$base2);
				$array = ['NA'=>'NA','VALUE'=>'VALUE'];
			?>
			<?= $form->field($model, 'based_on')->dropDownList($array); ?>
		</div>
	</div>
	
	<?= $form->field($model, 'remark')->textarea(['rows' => 2]) ?>
	<div class="row">
		<div class="col-lg-3">
			<?= $form->field($model, 'requestor')->textInput(['maxlength' => true]) ?>			
			<?= $form->field($model, 'email_requestor')->textInput(['maxlength' => true,
				'onKeyUp'=>'
						$.post( "'.Yii::$app->urlManager->createUrl('site/validator').'&requestor='.'"+$(this).val()+"'.'&role=validator&master=charge", function( data ) {							
						  $( "input#mastercharge-validator_name" ).val( data.username);
						  $( "input#mastercharge-email_validator" ).val( data.email);
						  $( "input#mastercharge-validator" ).val( data.id);
						});
			']) ?>
		</div>
		<div class="col-lg-3">
		<?= $form->field($model, 'validator_name')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		<?= $form->field($model, 'email_validator')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
		<div class="col-lg-3">
		<?= $form->field($model, 'acknowledges_name')->textInput(['maxlength' => true,'readOnly' => true]) ?>
		<?= $form->field($model, 'email_acknowledges')->textInput(['maxlength' => true,'readOnly'=> true]) ?>		
		</div>
		<div class="col-lg-3">
		<?= $form->field($model, 'support_name')->textInput(['maxlength' => true,'readOnly' => true]) ?>
		<?= $form->field($model, 'email_support')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		<?= $form->field($model, 'support_date')->textInput(['maxlength' => true,'readOnly'=> true]) ?>
		</div>
	</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

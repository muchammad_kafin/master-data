<?php

use yii\db\Migration;
use yii\db\Schema;

class m161028_143757_sun extends Migration
{
    public function safeUp()
    {
		 $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%master_item}}', [
            'id' => $this->primaryKey(),
            'no_doc' => $this->string()->notNull()->unique(),
            'status' => $this->string(50)->notNull(),
            'req_date' => $this->date()->notNull(),
            'requestor' => $this->string(50)->notNull(),
            'email_requestor' => $this->string(50)->notNull(),
            'validator' => $this->string()->notNull(),
            'email_validator' => $this->string()->notNull(),
            'acknowledges' => $this->string(50)->notNull(),
            'email_acknowledges' => $this->string(50)->notNull(),
            'support' => $this->string(50),
            'email_support' => $this->string(50),
            'support_date' => $this->date(),
            'remark' => $this->text(),
            'remark_cancel' => $this->text(),
			'item_code' => $this->string()->notNull(),
			'description' => $this->string(50),
			'lookup_code' => $this->string(),
			'item_type' => $this->string()->notNull(),
			'base_unit' => $this->string()->notNull(),
			'base_location' => $this->string()->notNull(),
			'label_code' => $this->string()->notNull(),
			'unit_purchase' => $this->string()->notNull(),
        ], $tableOptions);
		
		$this->createTable('{{%master_contract}}', [
            'id' => $this->primaryKey(),
			'no_doc' => $this->string()->notNull()->unique(),
            'status' => $this->string(50)->notNull(),
            'req_date' => $this->date()->notNull(),
            'requestor' => $this->string(50)->notNull(),
            'email_requestor' => $this->string(50)->notNull(),
            'validator' => $this->string()->notNull(),
            'email_validator' => $this->string()->notNull(),
            'acknowledges' => $this->string(50)->notNull(),
            'email_acknowledges' => $this->string(50)->notNull(),
            'support' => $this->string(50),
            'email_support' => $this->string(50),
            'support_date' => $this->date(),
            'remark' => $this->text(),
            'remark_cancel' => $this->text(),
            'vendor_code' => $this->string()->notNull(),
			'vendor_name' => $this->string()->notNull(),
			'sk_no' => $this->string(),
			'start_periode' => $this->date(),
			'end_periode' => $this->date(),
			'jenis_sewa' => $this->string(),			
			'item_code' => $this->string()->notNull(),		
			'uom' => $this->string()->notNull(),		
        ], $tableOptions);
		
		$this->createTable('{{%item_type}}', [
            'id' => $this->primaryKey(),
			'analysis_code' => $this->integer()->notNull(),
			'name' => $this->string()->notNull(),
        ], $tableOptions);
		
		$this->createTable('{{%vendor}}', [
            'id' => $this->primaryKey(),
			'supplier_code' => $this->string()->notNull(),
			'name' => $this->string()->notNull(),
        ], $tableOptions);
		
		$this->createTable('{{%uom}}', [
            'id' => $this->primaryKey(),
			'code' => $this->string()->notNull(),
			'description' => $this->string()->notNull(),
        ], $tableOptions);
		
		$this->execute($this->getItemSql());
		
    }
	
	private function getItemSql()
    {
		$this->batchInsert('{{%item_type}}', ['analysis_code','name'], [
			['21','Goods'],
			['22','Non Inventory'],
			['23','Services'],
		]);
		
	}
	
    public function safeDown()
    {
       $this->dropTable('{{%master_item}}');
       $this->dropTable('{{%master_contract}}');
       $this->dropTable('{{%item_type}}');
       $this->dropTable('{{%vendor}}');
       $this->dropTable('{{%uom}}');
    }
}

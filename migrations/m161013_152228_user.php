<?php

use yii\db\Migration;
use yii\db\Schema;

class m161013_152228_user extends Migration
{
    public function safeUp()
    {
		 $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(50)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
		
		$this->createTable('{{%master_list}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
        ], $tableOptions);
		
		$this->execute($this->getUserSql());
		$this->execute($this->getListSql());
		$this->execute("set global sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';");
    }
	
	private function getUserSql()
    {
        $time = time();
		$password_hash = Yii::$app->security->generatePasswordHash('admin12345');
		$password_hash_user = Yii::$app->security->generatePasswordHash('request12345');
		$password_hash_valid = Yii::$app->security->generatePasswordHash('valid12345');
		$password_hash_app = Yii::$app->security->generatePasswordHash('app12345');
		$auth_key = Yii::$app->security->generateRandomString();
		$auth_key_user = Yii::$app->security->generateRandomString() . '_' . time();
		$auth_key_valid = Yii::$app->security->generateRandomString() . '_' . time();
		$auth_key_app = Yii::$app->security->generateRandomString() . '_' . time();
		$auth_key_supp = Yii::$app->security->generateRandomString() . '_' . time();
		$this->batchInsert('{{%user}}', ['username','auth_key','password_hash','password_reset_token','email', 'status','created_at','updated_at'], [
			['admin',$auth_key,$password_hash,$auth_key,'it.bs@kamadjaja.com', 10,$time,$time],
			['support',$auth_key_supp,$password_hash,$auth_key_supp,'it.bs@kamadjaja.com', 10,$time,$time],
			['requestor',$auth_key_user,$password_hash_user,$auth_key_user,'it.helpdesk@kamadjaja.com', 10,$time,$time],
			['validator',$auth_key_valid,$password_hash_valid,$auth_key_valid,'it.dse-east@kamadjaja.com', 10,$time,$time],
			['approver',$auth_key_app,$password_hash_app,$auth_key_app,'it.helpdesk@kamadjaja.com', 10,$time,$time],
		]);
		
	}
	
	private function getListSql()
	{
		$this->batchInsert('{{%master_list}}', ['name'], [
			['Party'],['Client'],['Country'],['Location'],['Region'],['Debtor'],['Vendor']
		]);
	}
	
    public function safeDown()
    {
       $this->dropTable('{{%user}}');
    }
}

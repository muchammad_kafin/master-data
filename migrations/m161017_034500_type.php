<?php

use yii\db\Migration;
use yii\db\Schema;

class m161017_034500_type extends Migration
{
    public function safeUp()
    {
		 $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%party_type}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'code' => $this->string()->notNull()->unique(),
        ], $tableOptions);
		
		$this->createTable('{{%office_type}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'code' => $this->string()->notNull()->unique(),
        ], $tableOptions);
		
		$this->createTable('{{%office_map}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'code' => $this->string()->notNull()->unique(),
        ], $tableOptions);
		
		$this->createTable('{{%clients}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'code' => $this->string()->notNull()->unique(),
        ], $tableOptions);
    }
	
    public function safeDown()
    {
       $this->dropTable('{{%party_type}}');
       $this->dropTable('{{%office_map}}');
       $this->dropTable('{{%office_type}}');
       $this->dropTable('{{%clients}}');
    }
}

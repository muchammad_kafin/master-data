<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\AppAsset;
use dmstr\web\AdminLteAsset;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */

app\assets\AppAsset::register($this);
dmstr\web\AdminLteAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    
    <div class="container-fluid">
		<div class="row">
			<div class="col-md-8">
				<div class="col-md-8">
					<h2>
						Dear <?= ucfirst(strtolower($request)); ?>,
					</h2>
					<p>
						Bersama ini kami informasikan bahwa permintaan <?= $master; ?> ini untuk diproses, 
						Mohon klik Link dibawah untuk perinciannya.
					</p>
					<p>
						<a class="btn btn-primary" href="http://mdm.kamadjaja.com/index.php">VIEW</a>
					</p>
					<p>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>
									FIELD
								</th>
								<th>
									VALUES
								</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									NOMOR DOC
								</td>
								<td>
									<?= $nodoc; ?>
								</td>
							</tr>
							<tr>
								<td>
									CODE
								</td>
								<td>
									<?= $code; ?>
								</td>
							</tr>
							<tr>
								<td>
									NAME
								</td>
								<td>
									<?= $name; ?>
								</td>
							</tr>							
							<tr>
								<td>
									REMARK
								</td>
								<td>
									<?= $remark; ?>
								</td>
							</tr>
						</tbody>
					</table>
					</p>
				</div>
				<br/>
				<address>
					 <strong>IT MDM</strong><br /> 
					 <br /> <img src="<?= $message->embed($image); ?>"/>
					 <br /> Email: <a href="mailto:it.mdm@kamadjaja.com">it.mdm@kamadjaja.com</a>
				</address>
			</div>
		</div>
	</div>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

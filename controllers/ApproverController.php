<?php

namespace app\controllers;

use Yii;
use app\models\MasterParty;
use app\models\MasterPartySearch;
use app\models\MasterClient;
use app\models\MasterClientSearch;
use app\models\MasterCountry;
use app\models\MasterCountrySearch;
use app\models\MasterDebtor;
use app\models\MasterDebtorSearch;
use app\models\MasterLocation;
use app\models\MasterLocationSearch;
use app\models\MasterRegion;
use app\models\MasterRegionSearch;
use app\models\MasterVendor;
use app\models\MasterVendorSearch;
use app\models\Country;
use app\models\Region;
use app\models\City;
use app\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * ApproverController implements the CRUD actions for MasterParty model.
 */
class ApproverController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
			// 'access' => [
                // 'class' => AccessControl::className(),
                // 'only' => ['party','client'],
                // 'rules' => [
                    // [
                        // 'actions' => ['party','client'],
                        // 'allow' => true,
                        // 'roles' => ['@'],
                    // ],
                // ],
            // ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionParty()
    {
        $searchModel = new MasterPartySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Party";

        return $this->render('party/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionClient()
    {
        $searchModel = new MasterClientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Client";

        return $this->render('client/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionDebtor()
    {
        $searchModel = new MasterDebtorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Debtor";

        return $this->render('debtor/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionCountry()
    {
        $searchModel = new MasterCountrySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Country";

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionLocation()
    {
        $searchModel = new MasterLocationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Location";

        return $this->render('location/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionRegion()
    {
        $searchModel = new MasterRegionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Region";

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionVendor()
    {
        $searchModel = new MasterVendorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Vendor";

        return $this->render('vendor/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdateparty($id)
    {
        $model = $this->findParty($id);
		$coun = Country::find()->where(['code'=>$model->country])->one();
		if($model->state != ''){
			$state = Region::find()->where(['country'=>$coun->code,'code'=>$model->state])->one();
			$region = $state->name;
		}else{
			$region = '';
		}
		if($model->city != ''){
			$city = City::find()->where(['country'=>$coun->code,'region'=>$state->code,'id'=>$model->city])->one();
			$namecity = $city->name;
		}else{
			$namecity = '';
		}
		
		$valid = User::find()->where(['id'=>$model->validator])->one();
		$approve = User::find()->where(['id'=>$model->approver])->one();
		$support = User::find()->where(['id'=>$model->support])->one();

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
            return $this->redirect('index');
        } else {
            return $this->render('party/update', [
                'model' => $model,
				$model->country_name = $coun->name,
				$model->validator_name = strtoupper($valid->username),
				$model->approver_name = strtoupper($approve->username),
				$model->support_name = strtoupper($support->username),
				$model->status = 'support',
				$model->state_name = $region,
				$model->city_name = $namecity,
            ]);
        }
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdatevendor($id)
    {
        $model = $this->findVendor($id);
		$coun = Country::find()->where(['code'=>$model->country])->one();
		if($model->state != ''){
			$state = Region::find()->where(['country'=>$coun->code,'code'=>$model->state])->one();
			$region = $state->name;
		}else{
			$region = '';
		}
		if($model->city != ''){
			$city = City::find()->where(['country'=>$coun->code,'region'=>$state->code,'id'=>$model->city])->one();
			$namecity = $city->name;
		}else{
			$namecity = '';
		}
		
		$valid = User::find()->where(['id'=>$model->validator])->one();
		$approve = User::find()->where(['id'=>$model->approver])->one();
		$support = User::find()->where(['id'=>$model->support])->one();

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			$email_request = $_POST["MasterVendor"]["email_requestor"];
			$email_valid = $_POST["MasterVendor"]["email_validator"];
			$email_approve = $_POST["MasterVendor"]["email_approver"];
			$email_support = $_POST["MasterVendor"]["email_support"];
			
			Yii::$app->mailer->compose()
			 ->setFrom('it.bs@kamadjaja.com')
			 ->setTo($email_support)
			 ->setCc('andra.wisata@kamadjaja.com')
			 ->setSubject('Support Master Vendor')
			 ->setHtmlBody(Html::a('Go to page', '192.168.1.172:8000/index.php?r=site/login'))
			 ->send();
			 
			Yii::$app->mailer->compose()
			 ->setFrom('it.bs@kamadjaja.com')
			 ->setTo([$email_request,$email_valid,$email_approve])
			 ->setCc('andra.wisata@kamadjaja.com')
			 ->setSubject('Master Vendor')
			 ->setHtmlBody('Your Request has been submit')
			 ->send();
			 
            return $this->redirect('index');
        } else {
            return $this->render('vendor/update', [
                'model' => $model,
				$model->country_name = $coun->name,
				$model->validator_name = strtoupper($valid->username),
				$model->approver_name = strtoupper($approve->username),
				$model->support_name = strtoupper($support->username),
				$model->status = 'support',
				$model->state_name = $region,
				$model->city_name = $namecity,
            ]);
        }
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdatedebtor($id)
    {
        $model = $this->findDebtor($id);
		$coun = Country::find()->where(['code'=>$model->country])->one();
		if($model->state != ''){
			$state = Region::find()->where(['country'=>$coun->code,'code'=>$model->state])->one();
			$region = $state->name;
		}else{
			$region = '';
		}
		if($model->city != ''){
			$city = City::find()->where(['country'=>$coun->code,'region'=>$state->code,'id'=>$model->city])->one();
			$namecity = $city->name;
		}else{
			$namecity = '';
		}
		
		$valid = User::find()->where(['id'=>$model->validator])->one();
		$approve = User::find()->where(['id'=>$model->approver])->one();
		$support = User::find()->where(['id'=>$model->support])->one();

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			$email_request = $_POST["MasterDebtor"]["email_requestor"];
			$email_valid = $_POST["MasterDebtor"]["email_validator"];
			$email_approve = $_POST["MasterDebtor"]["email_approver"];
			$email_support = $_POST["MasterDebtor"]["email_support"];
			
			Yii::$app->mailer->compose()
			 ->setFrom('it.bs@kamadjaja.com')
			 ->setTo($email_support)
			 ->setCc('andra.wisata@kamadjaja.com')
			 ->setSubject('Support Master Debtor')
			 ->setHtmlBody(Html::a('Go to page', '192.168.1.172:8000/index.php?r=site/login'))
			 ->send();
			 
			Yii::$app->mailer->compose()
			 ->setFrom('it.bs@kamadjaja.com')
			 ->setTo([$email_request,$email_valid,$email_approve])
			 ->setCc('andra.wisata@kamadjaja.com')
			 ->setSubject('Master Debtor')
			 ->setHtmlBody('Your Request has been submit')
			 ->send();
			 
            return $this->redirect('index');
        } else {
            return $this->render('debtor/update', [
                'model' => $model,
				$model->country_name = $coun->name,
				$model->validator_name = strtoupper($valid->username),
				$model->approver_name = strtoupper($approve->username),
				$model->support_name = strtoupper($support->username),
				$model->status = 'support',
				$model->state_name = $region,
				$model->city_name = $namecity,
            ]);
        }
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdateclient($id)
    {
        $model = $this->findClient($id);
		$coun = Country::find()->where(['code'=>$model->country])->one();
		if($model->state != ''){
			$state = Region::find()->where(['country'=>$coun->code,'code'=>$model->state])->one();
			$region = $state->name;
		}else{
			$region = '';
		}
		if($model->city != ''){
			$city = City::find()->where(['country'=>$coun->code,'region'=>$state->code,'id'=>$model->city])->one();
			$namecity = $city->name;
		}else{
			$namecity = '';
		}
		
		$valid = User::find()->where(['id'=>$model->validator])->one();
		$approve = User::find()->where(['id'=>$model->approver])->one();
		$support = User::find()->where(['id'=>$model->support])->one();

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			$email_request = $_POST["MasterClient"]["email_requestor"];
			$email_valid = $_POST["MasterClient"]["email_validator"];
			$email_approve = $_POST["MasterClient"]["email_approver"];
			$email_support = $_POST["MasterClient"]["email_support"];
			
			/*Yii::$app->mailer->compose()
			 ->setFrom('it.bs@kamadjaja.com')
			 ->setTo($email_support)
			 ->setCc('andra.wisata@kamadjaja.com')
			 ->setSubject('Support Master Debtor')
			 ->setHtmlBody(Html::a('Go to page', '192.168.1.172:8000/index.php?r=site/login'))
			 ->send();
			 
			Yii::$app->mailer->compose()
			 ->setFrom('it.bs@kamadjaja.com')
			 ->setTo([$email_request,$email_valid,$email_approve])
			 ->setCc('andra.wisata@kamadjaja.com')
			 ->setSubject('Master Vendor')
			 ->setHtmlBody('Your Request has been submit')
			 ->send();*/
			 
            return $this->redirect('index');
        } else {
            return $this->render('client/update', [
                'model' => $model,
				$model->country_name = $coun->name,
				$model->validator_name = strtoupper($valid->username),
				$model->approver_name = strtoupper($approve->username),
				$model->support_name = strtoupper($support->username),
				$model->status = 'support',
				$model->state_name = $region,
				$model->city_name = $namecity,
            ]);
        }
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdatelocation($id)
    {
        $model = $this->findLocation($id);
		
		$valid = User::find()->where(['id'=>$model->validator])->one();
		$approve = User::find()->where(['id'=>$model->approver])->one();
		$support = User::find()->where(['id'=>$model->support])->one();

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			$email_request = $_POST["MasterLocation"]["email_requestor"];
			$email_valid = $_POST["MasterLocation"]["email_validator"];
			$email_approve = $_POST["MasterLocation"]["email_approver"];
			$email_approve = $_POST["MasterLocation"]["email_support"];
			
			/*Yii::$app->mailer->compose()
			 ->setFrom('it.bs@kamadjaja.com')
			 ->setTo($email_approver)
			 ->setCc('andra.wisata@kamadjaja.com')
			 ->setSubject('Approve Master Vendor')
			 ->setHtmlBody(Html::a('Go to page', '192.168.1.172:8000/index.php?r=site/login'))
			 ->send();
			 
			Yii::$app->mailer->compose()
			 ->setFrom('it.bs@kamadjaja.com')
			 ->setTo([$email_request,$email_valid])
			 ->setCc('andra.wisata@kamadjaja.com')
			 ->setSubject('Master Vendor')
			 ->setHtmlBody('Your Request has been submit')
			 ->send();*/
			 
            return $this->redirect('index');
        } else {
            return $this->render('location/update', [
                'model' => $model,
				$model->validator_name = strtoupper($valid->username),
				$model->approver_name = strtoupper($approve->username),
				$model->support_name = strtoupper($support->username),
				$model->status = 'support',
            ]);
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findParty($id)
    {
        if (($model = MasterParty::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findClient($id)
    {
        if (($model = MasterClient::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findCountry($id)
    {
        if (($model = MasterCountry::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findLocation($id)
    {
        if (($model = MasterLocation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findDebtor($id)
    {
        if (($model = MasterDebtor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findRegion($id)
    {
        if (($model = MasterRegion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findVendor($id)
    {
        if (($model = MasterVendor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

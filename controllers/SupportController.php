<?php

namespace app\controllers;

use Yii;
use app\models\MasterParty;
use app\models\MasterPartySearch;
use app\models\MasterClient;
use app\models\MasterClientSearch;
use app\models\MasterCountry;
use app\models\MasterCountrySearch;
use app\models\MasterDebtor;
use app\models\MasterDebtorSearch;
use app\models\MasterLocation;
use app\models\MasterLocationSearch;
use app\models\MasterRegion;
use app\models\MasterRegionSearch;
use app\models\MasterVendor;
use app\models\MasterVendorSearch;
use app\models\MasterItem;
use app\models\MasterItemSearch;
use app\models\MasterContract;
use app\models\MasterContractSearch;
use app\models\MasterEquipment;
use app\models\MasterEquipmentSearch;
use app\models\MasterProduct;
use app\models\MasterProductSearch;
use app\models\MasterPrinting;
use app\models\MasterPrintingSearch;
use app\models\MasterCharge;
use app\models\MasterChargeSearch;
use app\models\UpdateForm;
use app\models\UpdateFormSearch;
use app\models\Country;
use app\models\Region;
use app\models\City;
use app\models\User;
use app\models\Contact;
use app\models\Phone;
use app\models\Upload;
use app\models\Vendor;
use app\models\ItemList;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * MasterPartyController implements the CRUD actions for MasterParty model.
 */
class SupportController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
			// 'access' => [
                // 'class' => AccessControl::className(),
                // 'only' => ['party','client'],
                // 'rules' => [
                    // [
                        // 'actions' => ['party','client'],
                        // 'allow' => true,
                        // 'roles' => ['@'],
                    // ],
                // ],
            // ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionParty()
    {
        $searchModel = new MasterPartySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$searchUpdate = new UpdateFormSearch();
		$queryParams = array_merge(array(),Yii::$app->request->queryParams);
        $queryParams["UpdateFormSearch"]["master"] = "PARTY";
        $dataUpdate = $searchUpdate->search($queryParams);
		$title = "Master Party";

        return $this->render('party/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'updateProvider' => $dataUpdate,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionClient()
    {
        $searchModel = new MasterClientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$searchUpdate = new UpdateFormSearch();
		$queryParams = array_merge(array(),Yii::$app->request->queryParams);
        $queryParams["UpdateFormSearch"]["master"] = "CLIENT";
        $dataUpdate = $searchUpdate->search($queryParams);
		$title = "Master Client";
		
        return $this->render('client/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'updateProvider' => $dataUpdate,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionDebtor()
    {
        $searchModel = new MasterDebtorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$searchUpdate = new UpdateFormSearch();
		$queryParams = array_merge(array(),Yii::$app->request->queryParams);
        $queryParams["UpdateFormSearch"]["master"] = "DEBTOR";
        $dataUpdate = $searchUpdate->search($queryParams);
		$title = "Master Debtor";

        return $this->render('debtor/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'updateProvider' => $dataUpdate,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionCountry()
    {
        $searchModel = new MasterCountrySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Country";

        return $this->render('country/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionLocation()
    {
        $searchModel = new MasterLocationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Location";

        return $this->render('location/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionRegion()
    {
        $searchModel = new MasterRegionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Region";

        return $this->render('region/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionVendor()
    {
        $searchModel = new MasterVendorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$searchUpdate = new UpdateFormSearch();
		$queryParams = array_merge(array(),Yii::$app->request->queryParams);
        $queryParams["UpdateFormSearch"]["master"] = "VENDOR";
        $dataUpdate = $searchUpdate->search($queryParams);
		$title = "Master Vendor";

        return $this->render('vendor/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'updateProvider' => $dataUpdate,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionItem()
    {
        $searchModel = new MasterItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Item";

        return $this->render('item/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionContract()
    {
        $searchModel = new MasterContractSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Contract";

        return $this->render('contract/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionEquipment()
    {
        $searchModel = new MasterEquipmentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Equipment";

        return $this->render('equipment/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionProduct()
    {
        $searchModel = new MasterProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Product";

        return $this->render('product/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionPrinting()
    {
        $searchModel = new MasterPrintingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Printing";

        return $this->render('printing/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionCharge()
    {
        $searchModel = new MasterChargeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Charge";

        return $this->render('charge/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdateparty($id)
    {
        $model = $this->findParty($id);
		$modelsContact = Contact::find()->where(['id_party'=>$id])->all();
		$model->scenario = "support_party";
		$coun = Country::find()->where(['code'=>$model->country])->one();
		if($coun){
			$couname = $coun->name;
		}else{
			$couname = $model->country;
		}
		// if($model->state != ''){
			// $state = Region::find()->where(['country'=>$coun->code,'code'=>$model->state])->one();
			// $region = $state->name;
		// }else{
			// $region = '';
		// }
		// if($model->city != ''){
			// $city = City::find()->where(['id'=>$model->city])->one();
			// $namecity = $city->name;
		// }else{
			// $namecity = '';
		// }
		
		if ($model->validator == "Approve By System"){
			$valids = "Approve By System";
		}else{
			$valids = User::getUser($model->validator);
		}
		$acknowledges = User::getUser($model->acknowledges);
		$support = User::getUser($model->support);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			$email_request = $_POST["MasterParty"]["email_requestor"];
			$email_valid = $_POST["MasterParty"]["email_validator"];
			$nodoc = $_POST["MasterParty"]["no_doc"];
			$email_support = $_POST["MasterParty"]["email_support"];
			$email_acknowledges = $_POST["MasterParty"]["email_acknowledges"];
			$requestor = $_POST["MasterParty"]["requestor"];
			$status = $_POST["MasterParty"]["status"];
			$code = $_POST["MasterParty"]["code"];
			$name = $_POST["MasterParty"]["name"];			
			$remark = $_POST["MasterParty"]["remark"];
			
			$emailv = explode(',',$email_valid);
			$emaila = explode(',',$email_acknowledges);
			$valid =[];
			$ackno =[];
			
			foreach($emailv as $key => $valuev) {
			   $valid[] = $valuev;
			}
			$valid[] = $email_request;
			$valid[] = $email_support;
			
			foreach($emaila as $key => $valuea) {
			   $ackno[] = $valuea;
			}
			$ackno[]="andra.wisata@kamadjaja.com";
			
			Yii::$app->mailer->compose('@app/mail/layouts/finish_party.php',[
				'request' => $requestor,
				'master' => 'Master Party',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'party_type' => $model->type,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc($ackno)
			 ->setSubject('Support Master Party ['.$nodoc.']')
			 ->send();
			 
			 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success support ".$nodoc);
            return $this->redirect(['party']);
        } else {
            return $this->render('party/update', [
                'model' => $model,
				'modelsContact' => $modelsContact,
				'title' => 'Support Master Data Party',
				$model->country_name = $couname,
				$model->validator_name = strtoupper($valids),
				$model->acknowledges_name = strtoupper($acknowledges),
				$model->support_name = strtoupper($support),
				$model->status = 'finish',
				$model->state_name = $model->state,
				$model->city_name = $model->city,
				$model->type = explode(',', $model->type),
            ]);
        }
    }
	
	public function actionCancelparty($id)
    {
        $model = $this->findParty($id);
		$modelsContact = Contact::find()->where(['id_party'=>$id])->all();
		$model->scenario = "cancel_party";
		if ($model->load(Yii::$app->request->post())) {
			if ($model->save(false)) {
				$email_request = $model->email_requestor;
				$email_valid = $model->email_validator;
				$email_support = $model->email_support;
				$email_acknowledges = $model->email_acknowledges;
				$requestor = $model->requestor;
				$status = $model->status;
				$code = $model->code;
				$name = $model->name;
				$nodoc = $model->no_doc;
				$remark = $model->remark_cancel;
				
				$emailv = explode(',',$email_valid);
				$emaila = explode(',',$email_acknowledges);
				$valid =[];
				$ackno =[];
				foreach($emailv as $key => $valuev) {
				   $valid[] = $valuev;
				}
				foreach($emaila as $key => $valuea) {
				   $ackno[] = $valuea;
				}
				$ackno[]=$model->email_support;
				
				Yii::$app->mailer->compose('@app/mail/layouts/reject.php',[
					'request' => $requestor,
					'master' => 'Master Party',
					'status' => $status,
					'code' => $code,
					'name' => $name,
					'nodoc' => $nodoc,
					'remark' => $remark,
					'image' => Yii::getAlias('@app/web/klog.png'),
				])
				 ->setFrom('it.mdm@kamadjaja.com')
				 ->setTo($email_request)
				 ->setCc(array_merge($valid,$ackno))
				 ->setSubject('Reject Master Party ['.$nodoc.']')
				 ->send();
				 
				 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success Reject ".$nodoc);
				return $this->redirect(['support/party']);
			}else{
				Yii::$app->session->setFlash('error', 'Error On ');
				return $this->redirect(['cancelparty','id'=>$model->id]);
			}
		}
		else {
			return $this->render('cancel', [
				'model' => $model,
				$model->status = 'reject',
			]);
		}
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdatevendor($id)
    {
        $model = $this->findVendor($id);
		$model->scenario = "support_vendor";
		$modelsContact = \app\models\Contact::find()->where(['id_vendor'=>$id])->all();
		$modelsUpload = Upload::find()->where(['id_form'=>$model->no_doc])->all();
		$coun = Country::find()->where(['code'=>$model->country])->one();
		if($coun){
			$couname = $coun->name;
		}else{
			$couname = $model->country;
		}
		// if($model->state != ''){
			// $state = Region::find()->where(['country'=>$coun->code,'code'=>$model->state])->one();
			// $region = $state->name;
		// }else{
			// $region = '';
		// }
		// if($model->city != ''){
			// $city = City::find()->where(['id'=>$model->city])->one();
			// $namecity = $city->name;
		// }else{
			// $namecity = '';
		// }
		
		$valid = User::getUser($model->validator);
		$acknowledges = User::getUser($model->acknowledges);
		$support = User::getUser($model->support);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			$email_request = $_POST["MasterVendor"]["email_requestor"];
			$email_valid = $_POST["MasterVendor"]["email_validator"];
			$nodoc = $_POST["MasterVendor"]["no_doc"];
			$email_support = $_POST["MasterVendor"]["email_support"];
			$email_acknowledges = $_POST["MasterVendor"]["email_acknowledges"];
			$requestor = $_POST["MasterVendor"]["requestor"];
			$status = $_POST["MasterVendor"]["status"];
			$code = $_POST["MasterVendor"]["code"];
			$name = $_POST["MasterVendor"]["name"];			
			$remark = $_POST["MasterVendor"]["remark"];
			
			$vendor = new Vendor();
			$cekvendor = Vendor::find()->where(['code'=>$code])->one();
			if($cekvendor === null){
				$vendor->code = $code;
				$vendor->name = $name;
				$vendor->save(false);
			}
			
			$emailv = explode(',',$email_valid);
			$emaila = explode(',',$email_acknowledges);
			$valid =[];
			$ackno =[];
			
			foreach($emailv as $key => $valuev) {
			   $valid[] = $valuev;
			}
			$valid[] = $email_request;
			$valid[] = $email_support;
			
			foreach($emaila as $key => $valuea) {
			   $ackno[] = $valuea;
			}
			$ackno[]="andra.wisata@kamadjaja.com";
			
			Yii::$app->mailer->compose('@app/mail/layouts/finish.php',[
				'request' => $requestor,
				'master' => 'Master Vendor',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc($ackno)
			 ->setSubject('Support Master Vendor ['.$nodoc.']')
			 ->send();
			 
			 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success support ".$nodoc);
			 return $this->redirect(['vendor']);
        } else {
            return $this->render('vendor/update', [
                'model' => $model,
				'title' => 'Support Master Data Vendor',
				'modelsContact' => $modelsContact,
				'modelsUpload' => $modelsUpload,
				$model->country_name = $couname,
				$model->validator_name = strtoupper($valid),
				$model->acknowledges_name = strtoupper($acknowledges),
				$model->support_name = strtoupper($support),
				$model->status = 'finish',
				$model->state_name = $model->state,
				$model->city_name = $model->city,
            ]);
        }
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdatedebtor($id)
    {
        $model = $this->findDebtor($id);
		$modelsContact = Contact::find()->where(['id_debtor'=>$id])->all();
		$modelsUpload = Upload::find()->where(['id_form'=>$model->no_doc])->all();
		$model->scenario = "support_debtor";
		$coun = Country::find()->where(['code'=>$model->country])->one();
		if($coun){
			$couname = $coun->name;
		}else{
			$couname = $model->country;
		}
		// if($model->state != ''){
			// $state = Region::find()->where(['country'=>$coun->code,'code'=>$model->state])->one();
			// $region = $state->name;
		// }else{
			// $region = '';
		// }
		// if($model->city != ''){
			// $city = City::find()->where(['id'=>$model->city])->one();
			// $namecity = $city->name;
		// }else{
			// $namecity = '';
		// }
		
		$valid = User::getUser($model->validator);
		$acknowledges = User::getUser($model->acknowledges);
		$support = User::getUser($model->support);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			$email_request = $_POST["MasterDebtor"]["email_requestor"];
			$email_valid = $_POST["MasterDebtor"]["email_validator"];
			$nodoc = $_POST["MasterDebtor"]["no_doc"];
			$email_support = $_POST["MasterDebtor"]["email_support"];
			$email_acknowledges = $_POST["MasterDebtor"]["email_acknowledges"];
			$requestor = $_POST["MasterDebtor"]["requestor"];
			$status = $_POST["MasterDebtor"]["status"];
			$code = $_POST["MasterDebtor"]["code"];
			$name = $_POST["MasterDebtor"]["name"];			
			$remark = $_POST["MasterDebtor"]["remark"];
			
			$emailv = explode(',',$email_valid);
			$emaila = explode(',',$email_acknowledges);
			$valid =[];
			$ackno =[];
			
			foreach($emailv as $key => $valuev) {
			   $valid[] = $valuev;
			}
			$valid[] = $email_request;
			$valid[] = $email_support;
			
			foreach($emaila as $key => $valuea) {
			   $ackno[] = $valuea;
			}
			$ackno[]="andra.wisata@kamadjaja.com";
			
			Yii::$app->mailer->compose('@app/mail/layouts/finish.php',[
				'request' => $requestor,
				'master' => 'Master Debtor',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc($ackno)
			 ->setSubject('Support Master Debtor ['.$nodoc.']')
			 ->send();
			 
			 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success support ".$nodoc);
            return $this->redirect(['debtor']);
        } else {
            return $this->render('debtor/update', [
                'model' => $model,
                'modelsContact' => $modelsContact,
                'modelsUpload' => $modelsUpload,
				'title' => 'Support Master Data Debtor',
				$model->country_name = $couname,
				$model->validator_name = strtoupper($valid),
				$model->acknowledges_name = strtoupper($acknowledges),
				$model->support_name = strtoupper($support),
				$model->status = 'finish',
				$model->state_name = $model->state,
				$model->city_name = $model->city,
            ]);
        }
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdateclient($id)
    {
        $model = $this->findClient($id);
		$modelsContact = Contact::find()->where(['id_client'=>$model->id])->all();
		$model->scenario = "support_client";
		$coun = Country::find()->where(['code'=>$model->country])->one();
		if($coun){
			$couname = $coun->name;
		}else{
			$couname = $model->country;
		}
		// if($model->state != ''){
			// $state = Region::find()->where(['country'=>$coun->code,'code'=>$model->state])->one();
			// $region = $state->name;
		// }else{
			// $region = '';
		// }
		// if($model->city != ''){
			// $city = City::find()->where(['id'=>$model->city])->one();
			// $namecity = $city->name;
		// }else{
			// $namecity = '';
		// }
		
		if ($model->validator == "Approve By System"){
			$valid = "Approve By System";
		}else{
			$valid = User::getUser($model->validator);
		}
		$acknowledges = User::getUser($model->acknowledges);
		$support = User::getUser($model->support);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			$email_request = $_POST["MasterClient"]["email_requestor"];
			$email_valid = $_POST["MasterClient"]["email_validator"];
			$nodoc = $_POST["MasterClient"]["no_doc"];
			$email_support = $_POST["MasterClient"]["email_support"];
			$email_acknowledges = $_POST["MasterClient"]["email_acknowledges"];
			$requestor = $_POST["MasterClient"]["requestor"];
			$status = $_POST["MasterClient"]["status"];
			$code = $_POST["MasterClient"]["code"];
			$name = $_POST["MasterClient"]["name"];			
			$remark = $_POST["MasterClient"]["remark"];
			
			$emailv = explode(',',$email_valid);
			$emaila = explode(',',$email_acknowledges);
			$valid =[];
			$ackno =[];
			
			foreach($emailv as $key => $valuev) {
			   $valid[] = $valuev;
			}
			$valid[] = $email_request;
			$valid[] = $email_support;
			
			foreach($emaila as $key => $valuea) {
			   $ackno[] = $valuea;
			}
			$ackno[]="andra.wisata@kamadjaja.com";
			
			Yii::$app->mailer->compose('@app/mail/layouts/finish.php',[
				'request' => $requestor,
				'master' => 'Master Client',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc($ackno)
			 ->setSubject('Support Master Client ['.$nodoc.']')
			 ->send();
			 
			 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success support ".$nodoc);
            return $this->redirect(['client']);
        } else {
            return $this->render('client/update', [
                'model' => $model,
                'modelsContact' => $modelsContact,
                'title' => 'Support Master Data Client',
				$model->country_name = $couname,
				$model->validator_name = strtoupper($valid),
				$model->acknowledges_name = strtoupper($acknowledges),
				$model->support_name = strtoupper($support),
				$model->status = 'finish',
				$model->state_name = $model->state,
				$model->city_name = $model->city,
            ]);
        }
    }
	
	public function actionCancelclient($id)
    {
        $model = $this->findClient($id);
		$model->scenario = "cancel_client";
		if ($model->load(Yii::$app->request->post())) {
			if ($model->save(false)) {
				$nodoc = $model->no_doc;
				$email_request = $model->email_requestor;
				$email_valid = $model->email_validator;
				$email_support = $model->email_support;
				$email_acknowledges = $model->email_acknowledges;
				$requestor = $model->requestor;
				$status = $model->status;
				$code = $model->code;
				$name = $model->name;				
				$remark = $model->remark_cancel;
				
				$emailv = explode(',',$email_valid);
				$emaila = explode(',',$email_acknowledges);
				$valid =[];
				$ackno =[];
				foreach($emailv as $key => $valuev) {
				   $valid[] = $valuev;
				}
				foreach($emaila as $key => $valuea) {
				   $ackno[] = $valuea;
				}
				$ackno[]=$model->email_support;
				
				Yii::$app->mailer->compose('@app/mail/layouts/reject.php',[
					'request' => $requestor,
					'master' => 'Master Client',
					'status' => $status,
					'code' => $code,
					'name' => $name,
					'nodoc' => $nodoc,
					'remark' => $remark,
					'image' => Yii::getAlias('@app/web/klog.png'),
				])
				 ->setFrom('it.mdm@kamadjaja.com')
				 ->setTo($email_request)
				 ->setCc(array_merge($valid,$ackno))
				 ->setSubject('Reject Master Client ['.$nodoc.']')
				 ->send();
				 
				 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success reject ".$nodoc);
				return $this->redirect(['support/client']);
			}else{
				Yii::$app->session->setFlash('error', 'Error On ');
				return $this->redirect(['cancelclient','id'=>$model->id]);
			}
		}
		else {
			return $this->render('cancel', [
				'model' => $model,
				$model->status = 'reject',
			]);
		}
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdatecountry($id)
    {
        $model = $this->findCountry($id);
		$model->scenario = "support_country";
		$valid = User::getUser($model->validator);
		$acknowledges = User::getUser($model->acknowledges);
		$support = User::getUser($model->support);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			$email_request = $_POST["MasterCountry"]["email_requestor"];
			$email_valid = $_POST["MasterCountry"]["email_validator"];
			$nodoc = $_POST["MasterCountry"]["no_doc"];
			$email_support = $_POST["MasterCountry"]["email_support"];
			$email_acknowledges = $_POST["MasterCountry"]["email_acknowledges"];
			$requestor = $_POST["MasterCountry"]["requestor"];
			$status = $_POST["MasterCountry"]["status"];
			$code = $_POST["MasterCountry"]["code"];
			$name = $_POST["MasterCountry"]["name"];			
			$remark = $_POST["MasterCountry"]["remark"];
			
			$emailv = explode(',',$email_valid);
			$emaila = explode(',',$email_acknowledges);
			$valid =[];
			$ackno =[];
			
			foreach($emailv as $key => $valuev) {
			   $valid[] = $valuev;
			}
			$valid[] = $email_request;
			$valid[] = $email_support;
			
			foreach($emaila as $key => $valuea) {
			   $ackno[] = $valuea;
			}
			$ackno[]="andra.wisata@kamadjaja.com";
			
			Yii::$app->mailer->compose('@app/mail/layouts/finish.php',[
				'request' => $requestor,
				'master' => 'Master Country',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc($ackno)
			 ->setSubject('Support Master Country ['.$nodoc.']')
			 ->send();
			 
			 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success support ".$nodoc);
            return $this->redirect(['country']);
        } else {
            return $this->render('country/update', [
                'model' => $model,
				'title' => 'Support Master Data Country',
				$model->validator_name = strtoupper($valid),
				$model->support_name = strtoupper($support),
				$model->acknowledges_name = strtoupper($acknowledges),
				$model->status = 'finish',
            ]);
        }
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdatelocation($id)
    {
        $model = $this->findLocation($id);
		$model->scenario = "support_location";
		$valid = User::getUser($model->validator);
		$acknowledges = User::getUser($model->acknowledges);
		$support = User::getUser($model->support);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			$email_request = $_POST["MasterLocation"]["email_requestor"];
			$email_valid = $_POST["MasterLocation"]["email_validator"];
			$nodoc = $_POST["MasterLocation"]["no_doc"];
			$email_support = $_POST["MasterLocation"]["email_support"];
			$email_acknowledges = $_POST["MasterLocation"]["email_acknowledges"];
			$requestor = $_POST["MasterLocation"]["requestor"];
			$status = $_POST["MasterLocation"]["status"];
			$code = $_POST["MasterLocation"]["code"];
			$name = $_POST["MasterLocation"]["name"];			
			$remark = $_POST["MasterLocation"]["remark"];
			
			$emailv = explode(',',$email_valid);
			$emaila = explode(',',$email_acknowledges);
			$valid =[];
			$ackno =[];
			
			foreach($emailv as $key => $valuev) {
			   $valid[] = $valuev;
			}
			$valid[] = $email_request;
			$valid[] = $email_support;
			
			foreach($emaila as $key => $valuea) {
			   $ackno[] = $valuea;
			}
			$ackno[]="andra.wisata@kamadjaja.com";
			
			Yii::$app->mailer->compose('@app/mail/layouts/finish.php',[
				'request' => $requestor,
				'master' => 'Master Location',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc($ackno)
			 ->setSubject('Support Master Location ['.$nodoc.']')
			 ->send();
			 
			 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success support ".$nodoc);
            return $this->redirect(['location']);
        } else {
            return $this->render('location/update', [
                'model' => $model,
				'title' => 'Support Master Data Location',
				$model->validator_name = strtoupper($valid),
				$model->acknowledges_name = strtoupper($acknowledges),
				$model->support_name = strtoupper($support),
				$model->status = 'finish',
            ]);
        }
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdateregion($id)
    {
        $model = $this->findRegion($id);
		$model->scenario = "support_region";
		$valid = User::getUser($model->validator);
		$acknowledges = User::getUser($model->acknowledges);
		$support = User::getUser($model->support);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			$email_request = $_POST["MasterRegion"]["email_requestor"];
			$email_valid = $_POST["MasterRegion"]["email_validator"];
			$nodoc = $_POST["MasterRegion"]["no_doc"];
			$email_support = $_POST["MasterRegion"]["email_support"];
			$email_acknowledges = $_POST["MasterRegion"]["email_acknowledges"];
			$requestor = $_POST["MasterRegion"]["requestor"];
			$status = $_POST["MasterRegion"]["status"];
			$code = $_POST["MasterRegion"]["code"];
			$name = $_POST["MasterRegion"]["name"];			
			$remark = $_POST["MasterRegion"]["remark"];
			
			$emailv = explode(',',$email_valid);
			$emaila = explode(',',$email_acknowledges);
			$valid =[];
			$ackno =[];
			
			foreach($emailv as $key => $valuev) {
			   $valid[] = $valuev;
			}
			$valid[] = $email_request;
			$valid[] = $email_support;
			
			foreach($emaila as $key => $valuea) {
			   $ackno[] = $valuea;
			}
			$ackno[]="andra.wisata@kamadjaja.com";
			
			Yii::$app->mailer->compose('@app/mail/layouts/finish.php',[
				'request' => $requestor,
				'master' => 'Master Region',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc($ackno)
			 ->setSubject('Support Master Region ['.$nodoc.']')
			 ->send();
			 
			 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success support ".$nodoc);
            return $this->redirect(['region']);
        } else {
            return $this->render('region/update', [
                'model' => $model,
				'title' => 'Support Master Data Region',
				$model->validator_name = strtoupper($valid),
				$model->acknowledges_name = strtoupper($acknowledges),
				$model->support_name = strtoupper($support),
				$model->status = 'finish',
            ]);
        }
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdateitem($id)
    {
        $model = $this->findItem($id);
		$model->scenario = "support_item";
		$valid = User::getUser($model->validator);
		$acknowledges = User::getUser($model->acknowledges);
		$support = User::getUser($model->support);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			$email_request = $_POST["MasterItem"]["email_requestor"];
			$email_valid = $_POST["MasterItem"]["email_validator"];
			$nodoc = $_POST["MasterItem"]["no_doc"];
			$email_support = $_POST["MasterItem"]["email_support"];
			$email_acknowledges = $_POST["MasterItem"]["email_acknowledges"];
			$requestor = $_POST["MasterItem"]["requestor"];
			$status = $_POST["MasterItem"]["status"];
			$code = $_POST["MasterItem"]["item_code"];
			$name = $_POST["MasterItem"]["description"];			
			$remark = $_POST["MasterItem"]["remark"];
			
			$desc = $_POST["MasterItem"]["description"];
			$item = new ItemList();
			$cekitem = ItemList::find()->where(['item_code'=>$code])->one();
			if($cekitem === null){
				$item->item_code = $code;
				$item->description = $desc;
				$item->save(false);
			}
			
			$emailv = explode(',',$email_valid);
			$emaila = explode(',',$email_acknowledges);
			$valid =[];
			$ackno =[];
			
			foreach($emailv as $key => $valuev) {
			   $valid[] = $valuev;
			}
			$valid[] = $email_request;
			$valid[] = $email_support;
			
			foreach($emaila as $key => $valuea) {
			   $ackno[] = $valuea;
			}
			$ackno[]="andra.wisata@kamadjaja.com";
			
			Yii::$app->mailer->compose('@app/mail/layouts/finish.php',[
				'request' => $requestor,
				'master' => 'Master Item',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc($ackno)
			 ->setSubject('Support Master Item ['.$nodoc.']')
			 ->send();
			 
			 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success support ".$nodoc);
            return $this->redirect(['item']);
        } else {
            return $this->render('item/update', [
                'model' => $model,
				'title' => 'Support Master Data Item',
				$model->validator_name = strtoupper($valid),
				$model->acknowledges_name = strtoupper($acknowledges),
				$model->support_name = strtoupper($support),
				$model->status = 'finish',
            ]);
        }
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdatecontract($id)
    {
        $model = $this->findContract($id);
		$modelsUpload = Upload::find()->where(['id_form'=>$model->no_doc])->all();
		$model->scenario = "support_contract";
		$valid = User::getUser($model->validator);
		$acknowledges = User::getUser($model->acknowledges);
		$support = User::getUser($model->support);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			$email_request = $_POST["MasterContract"]["email_requestor"];
			$email_valid = $_POST["MasterContract"]["email_validator"];
			$nodoc = $_POST["MasterContract"]["no_doc"];
			$email_support = $_POST["MasterContract"]["email_support"];
			$email_acknowledges = $_POST["MasterContract"]["email_acknowledges"];
			$requestor = $_POST["MasterContract"]["requestor"];
			$status = $_POST["MasterContract"]["status"];
			$code = $_POST["MasterContract"]["vendor_code"];
			$name = $_POST["MasterContract"]["vendor_name"];			
			$remark = $_POST["MasterContract"]["remark"];
			
			$emailv = explode(',',$email_valid);
			$emaila = explode(',',$email_acknowledges);
			$valid =[];
			$ackno =[];
			
			foreach($emailv as $key => $valuev) {
			   $valid[] = $valuev;
			}
			$valid[] = $email_request;
			$valid[] = $email_support;
			
			foreach($emaila as $key => $valuea) {
			   $ackno[] = $valuea;
			}
			$ackno[]="andra.wisata@kamadjaja.com";
			
			Yii::$app->mailer->compose('@app/mail/layouts/finish.php',[
				'request' => $requestor,
				'master' => 'Master Contract',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc($ackno)
			 ->setSubject('Support Master Contract ['.$nodoc.']')
			 ->send();
			 
			 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success support ".$nodoc);
            return $this->redirect(['contract']);
        } else {
            return $this->render('contract/update', [
                'model' => $model,
                'modelsUpload' => $modelsUpload,
				'title' => 'Support Master Data Contract',
				$model->validator_name = strtoupper($valid),
				$model->acknowledges_name = strtoupper($acknowledges),
				$model->support_name = strtoupper($support),
				$model->status = 'finish',
            ]);
        }
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdateequipment($id)
    {
        $model = $this->findEquipment($id);
		$model->scenario = "support_equipment";
		$modelsUpload = Upload::find()->where(['id_form'=>$model->no_doc])->all();
		
		$valid = User::getUser($model->validator);
		$acknowledges = User::getUser($model->acknowledges);
		$support = User::getUser($model->support);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			$email_request = $_POST["MasterEquipment"]["email_requestor"];
			$email_valid = $_POST["MasterEquipment"]["email_validator"];
			$nodoc = $_POST["MasterEquipment"]["no_doc"];
			$email_support = $_POST["MasterEquipment"]["email_support"];
			$email_acknowledges = $_POST["MasterEquipment"]["email_acknowledges"];
			$requestor = $_POST["MasterEquipment"]["requestor"];
			$status = $_POST["MasterEquipment"]["status"];
			$code = $_POST["MasterEquipment"]["code"];
			$name = $_POST["MasterEquipment"]["name"];			
			$remark = $_POST["MasterEquipment"]["remark"];
			
			$emailv = explode(',',$email_valid);
			$emaila = explode(',',$email_acknowledges);
			$valid =[];
			$ackno =[];
			
			foreach($emailv as $key => $valuev) {
			   $valid[] = $valuev;
			}
			$valid[] = $email_request;
			$valid[] = $email_support;
			
			foreach($emaila as $key => $valuea) {
			   $ackno[] = $valuea;
			}
			$ackno[]="andra.wisata@kamadjaja.com";
			
			Yii::$app->mailer->compose('@app/mail/layouts/finish.php',[
				'request' => $requestor,
				'master' => 'Master Equipment',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc($ackno)
			 ->setSubject('Support Master Equipment ['.$nodoc.']')
			 ->send();
			 
			 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success support ".$nodoc);
			 return $this->redirect(['equipment']);
        } else {
            return $this->render('equipment/update', [
                'model' => $model,
				'title' => 'Support Master Data Equipment',
				'modelsUpload' => $modelsUpload,
				$model->validator_name = strtoupper($valid),
				$model->acknowledges_name = strtoupper($acknowledges),
				$model->support_name = strtoupper($support),
				$model->status = 'finish',
            ]);
        }
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdateproduct($id)
    {
        $model = $this->findProduct($id);
		$model->scenario = "support_product";
		
		$valid = User::getUser($model->validator);
		$acknowledges = User::getUser($model->acknowledges);
		$support = User::getUser($model->support);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			$email_request = $_POST["MasterProduct"]["email_requestor"];
			$email_valid = $_POST["MasterProduct"]["email_validator"];
			$nodoc = $_POST["MasterProduct"]["no_doc"];
			$email_support = $_POST["MasterProduct"]["email_support"];
			$email_acknowledges = $_POST["MasterProduct"]["email_acknowledges"];
			$requestor = $_POST["MasterProduct"]["requestor"];
			$status = $_POST["MasterProduct"]["status"];
			$code = $_POST["MasterProduct"]["code"];
			$name = $_POST["MasterProduct"]["name"];			
			$remark = $_POST["MasterProduct"]["remark"];
			
			$emailv = explode(',',$email_valid);
			$emaila = explode(',',$email_acknowledges);
			$valid =[];
			$ackno =[];
			
			foreach($emailv as $key => $valuev) {
			   $valid[] = $valuev;
			}
			$valid[] = $email_request;
			$valid[] = $email_support;
			
			foreach($emaila as $key => $valuea) {
			   $ackno[] = $valuea;
			}
			$ackno[]="andra.wisata@kamadjaja.com";
			
			Yii::$app->mailer->compose('@app/mail/layouts/finish.php',[
				'request' => $requestor,
				'master' => 'Master Product',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc($ackno)
			 ->setSubject('Support Master Product ['.$nodoc.']')
			 ->send();
			 
			 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success support ".$nodoc);
			 return $this->redirect(['product']);
        } else {
            return $this->render('product/update', [
                'model' => $model,
				'title' => 'Support Master Data Product',
				$model->validator_name = strtoupper($valid),
				$model->acknowledges_name = strtoupper($acknowledges),
				$model->support_name = strtoupper($support),
				$model->status = 'finish',
            ]);
        }
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdateprinting($id)
    {
        $model = $this->findPrinting($id);
		$model->scenario = "support_printing";
		
		$valid = User::getUser($model->validator);
		$acknowledges = User::getUser($model->acknowledges);
		$support = User::getUser($model->support);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			$email_request = $_POST["MasterPrinting"]["email_requestor"];
			$email_valid = $_POST["MasterPrinting"]["email_validator"];
			$nodoc = $_POST["MasterPrinting"]["no_doc"];
			$email_support = $_POST["MasterPrinting"]["email_support"];
			$email_acknowledges = $_POST["MasterPrinting"]["email_acknowledges"];
			$requestor = $_POST["MasterPrinting"]["requestor"];
			$status = $_POST["MasterPrinting"]["status"];
			$code = $_POST["MasterPrinting"]["code"];
			$name = $_POST["MasterPrinting"]["description"];			
			$remark = $_POST["MasterPrinting"]["remark"];
			
			$emailv = explode(',',$email_valid);
			$emaila = explode(',',$email_acknowledges);
			$valid =[];
			$ackno =[];
			
			foreach($emailv as $key => $valuev) {
			   $valid[] = $valuev;
			}
			$valid[] = $email_request;
			$valid[] = $email_support;
			
			foreach($emaila as $key => $valuea) {
			   $ackno[] = $valuea;
			}
			$ackno[]="andra.wisata@kamadjaja.com";
			
			Yii::$app->mailer->compose('@app/mail/layouts/finish.php',[
				'request' => $requestor,
				'master' => 'Master Printing',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc($ackno)
			 ->setSubject('Support Master Printing ['.$nodoc.']')
			 ->send();
			 
			 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success support ".$nodoc);
			 return $this->redirect(['printing']);
        } else {
            return $this->render('printing/update', [
                'model' => $model,
				'title' => 'Support Master Data Printing',
				$model->validator_name = strtoupper($valid),
				$model->acknowledges_name = strtoupper($acknowledges),
				$model->support_name = strtoupper($support),
				$model->status = 'finish',
            ]);
        }
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdatecharge($id)
    {
        $model = $this->findCharge($id);
		$model->scenario = "support_charge";
		
		$valid = User::getUser($model->validator);
		$acknowledges = User::getUser($model->acknowledges);
		$support = User::getUser($model->support);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			$email_request = $_POST["MasterCharge"]["email_requestor"];
			$email_valid = $_POST["MasterCharge"]["email_validator"];
			$nodoc = $_POST["MasterCharge"]["no_doc"];
			$email_support = $_POST["MasterCharge"]["email_support"];
			$email_acknowledges = $_POST["MasterCharge"]["email_acknowledges"];
			$requestor = $_POST["MasterCharge"]["requestor"];
			$status = $_POST["MasterCharge"]["status"];
			$code = $_POST["MasterCharge"]["code"];
			$name = $_POST["MasterCharge"]["name"];			
			$remark = $_POST["MasterCharge"]["remark"];
			
			$emailv = explode(',',$email_valid);
			$emaila = explode(',',$email_acknowledges);
			$valid =[];
			$ackno =[];
			
			foreach($emailv as $key => $valuev) {
			   $valid[] = $valuev;
			}
			$valid[] = $email_request;
			$valid[] = $email_support;
			
			foreach($emaila as $key => $valuea) {
			   $ackno[] = $valuea;
			}
			$ackno[]="andra.wisata@kamadjaja.com";
			$ackno[]="it.mdm@kamadjaja.com";
			
			Yii::$app->mailer->compose('@app/mail/layouts/finish.php',[
				'request' => $requestor,
				'master' => 'Master Charge',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc($ackno)
			 ->setSubject('Support Master Charge ['.$nodoc.']')
			 ->send();
			 
			 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success support ".$nodoc);
			 return $this->redirect(['charge']);
        } else {
            return $this->render('charge/update', [
                'model' => $model,
				'title' => 'Support Master Data Charge',
				$model->validator_name = strtoupper($valid),
				$model->acknowledges_name = strtoupper($acknowledges),
				$model->support_name = strtoupper($support),
				$model->status = 'finish',
            ]);
        }
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdateform($id)
    {
        $model = $this->findUpdate($id);
		$modelsUpload = Upload::find()->where(['id_form'=>$model->no_doc])->all();
		$mast = $model->master;
		$model->scenario = "support_update";
		
		$valid = User::getUser($model->validator);
		$acknowledges = User::getUser($model->acknowledges);
		$support = User::getUser($model->support);

        if ($model->load(Yii::$app->request->post())) {
			$valid = $model->validate();
			if ($valid) {
				$model->save(false);
				$email_request = $_POST["UpdateForm"]["email_requestor"];
				$email_valid = $_POST["UpdateForm"]["email_validator"];
				$nodoc = $_POST["UpdateForm"]["no_doc"];
				$email_support = $_POST["UpdateForm"]["email_support"];
				$email_acknowledges = $_POST["UpdateForm"]["email_acknowledges"];
				$requestor = $_POST["UpdateForm"]["requestor"];
				$status = $_POST["UpdateForm"]["status"];
				$code = $_POST["UpdateForm"]["code"];
				$name = $_POST["UpdateForm"]["name"];
				$remark = $_POST["UpdateForm"]["remark"];
				$remark_support = $_POST["UpdateForm"]["remark_support"];
				$request = $_POST["UpdateForm"]["request"];
				$reason = $_POST["UpdateForm"]["reason"];
				$master = $_POST["UpdateForm"]["master"];
				
				$emailv = explode(',',$email_valid);
				$emaila = explode(',',$email_acknowledges);
				$valid =[];
				$ackno =[];
				
				foreach($emailv as $key => $valuev) {
				   $valid[] = $valuev;
				}
				$valid [] = $email_support;
				$valid [] = $email_request;
				
				foreach($emaila as $key => $valuea) {
				   $ackno[] = $valuea;
				}
				
				Yii::$app->mailer->compose('@app/mail/layouts/finish_update.php',[
					'requestor' => $requestor,
					'master' => 'Update Master '.$master,
					'status' => $status,
					'code' => $code,
					'name' => $name,
					'nodoc' => $nodoc,
					'remark' => $remark,
					'remark_support' => $remark_support,
					'reason' => $reason,
					'request' => $request,
					'image' => Yii::getAlias('@app/web/klog.png'),
				])
				 ->setFrom('it.mdm@kamadjaja.com')
				 ->setTo($valid)
				 ->setCc($ackno)
				 ->setSubject('Support Update Master '.$master.' ['.$nodoc.']')
				 ->send();
				 
				\cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success support ".$nodoc);
				$mast = strtolower($model->master);
				return $this->redirect([$mast]);
			}
			else{
				var_dump ($model->getErrors()); die();
			}
        } else {
            return $this->render('update-form/update', [
                'model' => $model,
                'modelsUpload' => $modelsUpload,
				'title' => 'Support Update Master '.$mast,
				$model->validator_name = strtoupper($valid),
				$model->acknowledges_name = strtoupper($acknowledges),
				$model->support_name = strtoupper($support),
				$model->status = 'finish',
				'master' => $mast,
            ]);
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findParty($id)
    {
        if (($model = MasterParty::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findClient($id)
    {
        if (($model = MasterClient::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findCountry($id)
    {
        if (($model = MasterCountry::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findLocation($id)
    {
        if (($model = MasterLocation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findDebtor($id)
    {
        if (($model = MasterDebtor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findRegion($id)
    {
        if (($model = MasterRegion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findVendor($id)
    {
        if (($model = MasterVendor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findItem($id)
    {
        if (($model = MasterItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findContract($id)
    {
        if (($model = MasterContract::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findEquipment($id)
    {
        if (($model = MasterEquipment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findProduct($id)
    {
        if (($model = MasterProduct::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findPrinting($id)
    {
        if (($model = MasterPrinting::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findCharge($id)
    {
        if (($model = MasterCharge::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findUpdate($id)
    {
        if (($model = UpdateForm::findOne(['no_doc'=>$id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php

namespace app\controllers;

use Yii;
use app\models\MasterProduct;
use app\models\MasterProductSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\User;

/**
 * MasterProductController implements the CRUD actions for MasterProduct model.
 */
class MasterProductController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterProduct models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MasterProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MasterProduct model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MasterProduct model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MasterProduct();
		// $validator = User::getValidator('validator','contract');
		$acknowledges = User::getValidator('acknowledges','product');
		$support = User::getValidator('support','product');
		$count = MasterProduct::find()->count();
		$nextCount = $count+1;
		$today = date("Y-m-d");
		if ($nextCount < 10 ){
			$noDoc = 'FM-PRD-0'.$nextCount;
		}else{
			$noDoc = 'FM-PRD-'.$nextCount;
		}

        if ($model->load(Yii::$app->request->post())) {
				$cekdoc = $_POST["MasterProduct"]["no_doc"];
				$existdoc = MasterProduct::find()->where(['no_doc'=>$cekdoc])->one();
				if($existdoc){
					$count = MasterProduct::find()->count();
					$nextCount = $count+1;
					$today = date("Y-m-d");
					if ($nextCount < 10 ){
						$noDoc = 'FM-PRD-0'.$nextCount;
					}else{
						$noDoc = 'FM-PRD-'.$nextCount;
					}
					$model->no_doc = $noDoc;
					
				}else{
					$model->no_doc = $cekdoc;
				}
			if($model->save()){
				$email_request = $_POST["MasterProduct"]["email_requestor"];
				$nodoc = $model->no_doc;
				\cakebake\actionlog\model\ActionLog::add('success', $email_request." success request ".$nodoc);						
				$email_valid = $_POST["MasterProduct"]["email_validator"];
				$validator = $_POST["MasterProduct"]["validator_name"];
				$status = $_POST["MasterProduct"]["status"];
				$code = $_POST["MasterProduct"]["code"];
				$name = $_POST["MasterProduct"]["name"];						
				$remark = $_POST["MasterProduct"]["remark"];
				
				$email = explode(',',$email_valid);
				$valid =[];
				foreach($email as $key => $value) {
				   $valid[] = $value;
				}
				$valid[] = $email_request;
				
				Yii::$app->mailer->compose('@app/mail/layouts/validator.php',[
					'request' => $validator,
					'master' => 'Master Product',
					'status' => $status,
					'code' => $code,
					'name' => $name,
					'nodoc' => $nodoc,
					'remark' => $remark,
					'image' => Yii::getAlias('@app/web/klog.png'),
				])
				 ->setFrom('it.mdm@kamadjaja.com')
				 ->setTo($valid)
				 ->setCc('andra.wisata@kamadjaja.com')
				 ->setSubject('Validation Master Product ['.$nodoc.']')
				 ->send();
				return $this->goHome();
			}else{
				\Yii::$app->session->setFlash('error','Sorry some error occurred, please contact IT BS Team :)');
				return $this->redirect(['create']);
			}
        } else {
            return $this->render('create', [
                'model' => $model,
				$model->no_doc = $noDoc,
				$model->status = 'validator',
				$model->req_date = $today,
				$model->acknowledges = strtoupper($acknowledges['id']),
				$model->acknowledges_name = strtoupper($acknowledges['username']),
				$model->email_acknowledges = $acknowledges['email'],
				$model->support = strtoupper($support['id']),
				$model->support_name = strtoupper($support['username']),
				$model->email_support = $support['email'],
            ]);
        }
    }

    /**
     * Updates an existing MasterProduct model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MasterProduct model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MasterProduct model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterProduct the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MasterProduct::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

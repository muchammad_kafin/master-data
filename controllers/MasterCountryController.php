<?php

namespace app\controllers;

use Yii;
use app\models\MasterCountry;
use app\models\MasterCountrySearch;
use app\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MasterCountryController implements the CRUD actions for MasterCountry model.
 */
class MasterCountryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterCountry models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MasterCountrySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MasterCountry model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MasterCountry model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MasterCountry();

        // $validator = User::getValidator('validator','country');
		$support = User::getValidator('support','country');
		$acknowledges = User::getValidator('acknowledges','country');
		$count = MasterCountry::find()->count();
		$nextCount = $count+1;
		$today = date("Y-m-d");
		if ($nextCount < 10 ){
			$noDoc = 'FM-COU-0'.$nextCount;
		}else{
			$noDoc = 'FM-COU-'.$nextCount;
		}
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			$cekdoc = $_POST["MasterCountry"]["no_doc"];
			$existdoc = MasterCountry::find()->where(['no_doc'=>$cekdoc])->one();
			if($existdoc){
				$count = MasterCountry::find()->count();
				$nextCount = $count+1;
				$today = date("Y-m-d");
				if ($nextCount < 10 ){
					$noDoc = 'FM-COU-0'.$nextCount;
				}else{
					$noDoc = 'FM-COU-'.$nextCount;
				}
				$model->no_doc = $noDoc;
				
			}else{
				$model->no_doc = $cekdoc;
			}
			$email_request = $_POST["MasterCountry"]["email_requestor"];
			$email_valid = $_POST["MasterCountry"]["email_validator"];
			$email_acknowledges = $_POST["MasterCountry"]["email_acknowledges"];
			$request = $_POST["MasterCountry"]["requestor"];
			$status = $_POST["MasterCountry"]["status"];
			$code = $_POST["MasterCountry"]["code"];
			$name = $_POST["MasterCountry"]["name"];
			$nodoc = $model->no_doc;
			$remark = $_POST["MasterCountry"]["remark"];
			
			$email = explode(',',$email_valid);
			$valid =[];
			foreach($email as $key => $value) {
			   $valid[] = $value;
			}
			
			Yii::$app->mailer->compose('@app/mail/layouts/validator.php',[
				'request' => $request,
				'master' => 'Master Country',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc('andra.wisata@kamadjaja.com')
			 ->setSubject('Validation Master Country ['.$nodoc.']')
			 ->send();
			 
			Yii::$app->mailer->compose('@app/mail/layouts/validator.php',[
				'request' => $request,
				'master' => 'Master Country',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($email_request)
			 ->setCc(['andra.wisata@kamadjaja.com',$email_acknowledges])
			 ->setSubject('Master Country ['.$nodoc.']')
			 ->send();
			 \cakebake\actionlog\model\ActionLog::add('success', $email_request." success request");
            return $this->goHome();
        } else {
            return $this->render('create', [
                'model' => $model,
				$model->no_doc = $noDoc,
				$model->status = 'validator',
				$model->req_date = $today,
				// $model->validator = strtoupper($validator['id']),
				// $model->validator_name = strtoupper($validator['username']),
				// $model->email_validator = $validator['email'],
				$model->acknowledges = strtoupper($acknowledges['id']),
				$model->acknowledges_name = strtoupper($acknowledges['username']),
				$model->email_acknowledges = $acknowledges['email'],
				$model->support = strtoupper($support['id']),
				$model->support_name = strtoupper($support['username']),
				$model->email_support = $support['email'],
            ]);
        }
    }

    /**
     * Updates an existing MasterCountry model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MasterCountry model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MasterCountry model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterCountry the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MasterCountry::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php

namespace app\controllers;

use Yii;
use app\models\MasterRegion;
use app\models\MasterRegionSearch;
use app\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MasterRegionController implements the CRUD actions for MasterRegion model.
 */
class MasterRegionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterRegion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MasterRegionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MasterRegion model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MasterRegion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MasterRegion();

        // $validator = User::getValidator('validator','region');
        $acknowledges = User::getValidator('acknowledges','region');
		$support = User::getValidator('support','region');
		$count = MasterRegion::find()->count();
		$nextCount = $count+1;
		$today = date("Y-m-d");
		if ($nextCount < 10 ){
			$noDoc = 'FM-REG-0'.$nextCount;
		}else{
			$noDoc = 'FM-REG-'.$nextCount;
		}
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
				$cekdoc = $_POST["MasterRegion"]["no_doc"];
				$existdoc = MasterRegion::find()->where(['no_doc'=>$cekdoc])->one();
				if($existdoc){
					$count = MasterRegion::find()->count();
					$nextCount = $count+1;
					$today = date("Y-m-d");
					if ($nextCount < 10 ){
						$noDoc = 'FM-REG-0'.$nextCount;
					}else{
						$noDoc = 'FM-REG-'.$nextCount;
					}
					$model->no_doc = $noDoc;
					
				}else{
					$model->no_doc = $cekdoc;
				}
			$email_request = $_POST["MasterRegion"]["email_requestor"];
			$email_valid = $_POST["MasterRegion"]["email_validator"];
			$request = $_POST["MasterRegion"]["requestor"];
			$status = $_POST["MasterRegion"]["status"];
			$code = $_POST["MasterRegion"]["code"];
			$name = $_POST["MasterRegion"]["name"];
			$nodoc = $model->no_doc;
			$remark = $_POST["MasterRegion"]["remark"];
			
			$email = explode(',',$email_valid);
			$valid =[];
			foreach($email as $key => $value) {
			   $valid[] = $value;
			}
			
			Yii::$app->mailer->compose('@app/mail/layouts/validator.php',[
				'request' => $request,
				'master' => 'Master Region',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc('andra.wisata@kamadjaja.com')
			 ->setSubject('Validation Master Region ['.$nodoc.']')
			 ->send();
			 
			Yii::$app->mailer->compose('@app/mail/layouts/validator.php',[
				'request' => $request,
				'master' => 'Master Region',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo([$email_request])
			 ->setCc('andra.wisata@kamadjaja.com')
			 ->setSubject('Master Region ['.$nodoc.']')
			 ->send();
			 \cakebake\actionlog\model\ActionLog::add('success', $email_request." success request");
            return $this->goHome();
        } else {
            return $this->render('create', [
                'model' => $model,
				$model->no_doc = $noDoc,
				$model->status = 'validator',
				$model->req_date = $today,
				// $model->validator = strtoupper($validator['id']),
				// $model->validator_name = strtoupper($validator['username']),
				// $model->email_validator = $validator['email'],
				$model->acknowledges = strtoupper($acknowledges['id']),
				$model->acknowledges_name = strtoupper($acknowledges['username']),
				$model->email_acknowledges = $acknowledges['email'],
				$model->support = strtoupper($support['id']),
				$model->support_name = strtoupper($support['username']),
				$model->email_support = $support['email'],
            ]);
        }
    }

    /**
     * Updates an existing MasterRegion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MasterRegion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MasterRegion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterRegion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MasterRegion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

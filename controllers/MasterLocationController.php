<?php

namespace app\controllers;

use Yii;
use app\models\MasterLocation;
use app\models\MasterLocationSearch;
use app\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MasterLocationController implements the CRUD actions for MasterLocation model.
 */
class MasterLocationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterLocation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MasterLocationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MasterLocation model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MasterLocation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MasterLocation();

        // $validator = User::getValidator('validator','location');
		$support = User::getValidator('support','location');
		$acknowledges = User::getValidator('acknowledges','location');
		$count = MasterLocation::find()->count();
		$nextCount = $count+1;
		$today = date("Y-m-d");
		if ($nextCount < 10 ){
			$noDoc = 'FM-LOC-0'.$nextCount;
		}else{
			$noDoc = 'FM-LOC-'.$nextCount;
		}		

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			$cekdoc = $_POST["MasterLocation"]["no_doc"];
			$existdoc = MasterLocation::find()->where(['no_doc'=>$cekdoc])->one();
			if($existdoc){
				$count = MasterLocation::find()->count();
				$nextCount = $count+1;
				$today = date("Y-m-d");
				if ($nextCount < 10 ){
					$noDoc = 'FM-LOC-0'.$nextCount;
				}else{
					$noDoc = 'FM-LOC-'.$nextCount;
				}
				$model->no_doc = $noDoc;
				
			}else{
				$model->no_doc = $cekdoc;
			}
			$email_request = $_POST["MasterLocation"]["email_requestor"];
			$email_valid = $_POST["MasterLocation"]["email_validator"];
			$request = $_POST["MasterLocation"]["requestor"];
			$status = $_POST["MasterLocation"]["status"];
			$code = $_POST["MasterLocation"]["code"];
			$name = $_POST["MasterLocation"]["name"];
			$nodoc = $model->no_doc;
			$remark = $_POST["MasterLocation"]["remark"];
			
			$email = explode(',',$email_valid);
			$valid =[];
			foreach($email as $key => $value) {
			   $valid[] = $value;
			}
			
			Yii::$app->mailer->compose('@app/mail/layouts/validator.php',[
				'request' => $request,
				'master' => 'Master Location',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc('andra.wisata@kamadjaja.com')
			 ->setSubject('Validation Master Location ['.$nodoc.']')
			 ->send();
			 
			Yii::$app->mailer->compose('@app/mail/layouts/validator.php',[
				'request' => $request,
				'master' => 'Master Location',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($email_request)
			 ->setCc('andra.wisata@kamadjaja.com')
			 ->setSubject('Master Location ['.$nodoc.']')
			 ->send();
			 \cakebake\actionlog\model\ActionLog::add('success', $email_request." success request");
            return $this->goHome();
        } else {
            return $this->render('create', [
                'model' => $model,
				$model->no_doc = $noDoc,
				$model->status = 'validator',
				$model->req_date = $today,
				// $model->validator = strtoupper($validator['id']),
				// $model->validator_name = strtoupper($validator['username']),
				// $model->email_validator = $validator['email'],
				$model->acknowledges = strtoupper($acknowledges['id']),
				$model->acknowledges_name = strtoupper($acknowledges['username']),
				$model->email_acknowledges = $acknowledges['email'],
				$model->support = strtoupper($support['id']),
				$model->support_name = strtoupper($support['username']),
				$model->email_support = $support['email'],
            ]);
        }
    }

    /**
     * Updates an existing MasterLocation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MasterLocation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MasterLocation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterLocation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MasterLocation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

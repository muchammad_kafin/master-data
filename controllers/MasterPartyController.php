<?php

namespace app\controllers;

use Yii;
use app\models\MasterParty;
use app\models\User;
use app\models\Model;
use app\models\Contact;
use app\models\Phone;
use app\models\Address;
use app\models\MasterPartySearch;
use app\models\AddressSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;

/**
 * MasterPartyController implements the CRUD actions for MasterParty model.
 */
class MasterPartyController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MasterPartySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MasterParty model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MasterParty model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MasterParty();
		$modelsContact = [new Contact()];
        $modelsPhone = [[new Phone()]];
		// $validator = User::getValidator('validator','party');
		$support = User::getValidator('support','party');
		$acknowledges = User::getValidator('acknowledges','party');
		$count = MasterParty::find()->count();
		$nextCount = $count+1;
		$today = date("Y-m-d");
		if ($nextCount < 10 ){
			$noDoc = 'FM-PRT-0'.$nextCount;
		}else{
			$noDoc = 'FM-PRT-'.$nextCount;
		}
		
        if ($model->load(Yii::$app->request->post())) {
			$modelsContact = Model::createMultiple(Contact::classname());
			Model::loadMultiple($modelsContact, Yii::$app->request->post());
	 
			// validate person and houses models
			$valid = $model->validate();
			$valid = Model::validateMultiple($modelsContact) && $valid;
	 
			if (isset($_POST['Phone'][0][0])) {
				foreach ($_POST['Phone'] as $indexHouse => $rooms) {
					foreach ($rooms as $indexRoom => $room) {
						$data['Phone'] = $room;
						$modelPhone = new Phone();
						$modelPhone->load($data);
						$modelsPhone[$indexHouse][$indexRoom] = $modelPhone;
						$valid = $modelPhone->validate();
						//die(var_dump($modelRoom->errors));
					}
				}
			}
			if ($valid) {
				$cekdoc = $_POST["MasterParty"]["no_doc"];
				$existdoc = MasterParty::find()->where(['no_doc'=>$cekdoc])->one();
				if($existdoc){
					$count = MasterParty::find()->count();
					$nextCount = $count+1;
					$today = date("Y-m-d");
					if ($nextCount < 10 ){
						$noDoc = 'FM-PRT-0'.$nextCount;
					}else{
						$noDoc = 'FM-PRT-'.$nextCount;
					}
					$model->no_doc = $noDoc;
					
				}else{
					$model->no_doc = $cekdoc;
				}
					$transaction = Yii::$app->db->beginTransaction();
					$model->office_mapping = implode(",",$_POST["MasterParty"]["office_mapping"]);
					try {
						if ($flag = $model->save(false)) {
							foreach ($modelsContact as $indexHouse => $modelContact) {
		 
								if ($flag === false) {
									break;
								}
		 
								$modelContact->id_party = $model->id;
		 
								if (!($flag = $modelContact->save(false))) {
									break;
								}
		 
								if (isset($modelsPhone[$indexHouse]) && is_array($modelsPhone[$indexHouse])) {
									foreach ($modelsPhone[$indexHouse] as $indexRoom => $modelPhone) {
										$modelPhone->id_contact = $modelContact->id;
										if (!($flag = $modelPhone->save(false))) {
											break;
										}
									}
								}
							}
						}
		 
						if ($flag) {
							$transaction->commit();
							$email_request = $_POST["MasterParty"]["email_requestor"];
							$nodoc = $model->no_doc;
							\cakebake\actionlog\model\ActionLog::add('success', $email_request." success request ".$nodoc);						
							$email_valid = $_POST["MasterParty"]["email_validator"];
							$request = $_POST["MasterParty"]["requestor"];
							$status = $_POST["MasterParty"]["status"];
							$code = $_POST["MasterParty"]["code"];
							$name = $_POST["MasterParty"]["name"];						
							$remark = $_POST["MasterParty"]["remark"];
							
							$email = explode(',',$email_valid);
							$valid =[];
							foreach($email as $key => $value) {
							   $valid[] = $value;
							}
							
							Yii::$app->mailer->compose('@app/mail/layouts/validator.php',[
								'request' => $request,
								'master' => 'Master Party',
								'status' => $status,
								'code' => $code,
								'name' => $name,
								'nodoc' => $nodoc,
								'remark' => $remark,
								'image' => Yii::getAlias('@app/web/klog.png'),
							])
							 ->setFrom('it.mdm@kamadjaja.com')
							 ->setTo($valid)
							 ->setCc('andra.wisata@kamadjaja.com')
							 ->setSubject('Validation Master Party ['.$nodoc.']')
							 ->send();
							 
							Yii::$app->mailer->compose('@app/mail/layouts/validator.php',[
								'request' => $request,
								'master' => 'Master Party',
								'status' => $status,
								'code' => $code,
								'name' => $name,
								'nodoc' => $nodoc,
								'remark' => $remark,
								'image' => Yii::getAlias('@app/web/klog.png'),
							])
							 ->setFrom('it.mdm@kamadjaja.com')
							 ->setTo($email_request)
							 ->setCc('andra.wisata@kamadjaja.com')
							 ->setSubject('Master Party ['.$nodoc.']')
							 ->send();
							return $this->goHome();
						} else {
							$transaction->rollBack();
							\Yii::$app->session->setFlash('error','Sorry some error occurred, please contact IT BS Team :)');
							return $this->redirect(['create']);
						}
					} catch (Exception $e) {
						$transaction->rollBack();
						\Yii::$app->session->setFlash('error','Sorry some error occurred, please contact IT BS Team :)');
						return $this->redirect(['create']);
					}
				}
        } else {
            return $this->render('create', [
                'model' => $model,
                'modelsContact' => $modelsContact,
                'modelsPhone' => $modelsPhone,
				$model->no_doc = $noDoc,
				$model->status = 'validator',
				$model->req_date = $today,
				// $model->validator = strtoupper($validator['id']),
				// $model->validator_name = strtoupper($validator['username']),
				// $model->email_validator = $validator['email'],
				$model->acknowledges = strtoupper($acknowledges['id']),
				$model->acknowledges_name = strtoupper($acknowledges['username']),
				$model->email_acknowledges = $acknowledges['email'],
				$model->support = strtoupper($support['id']),
				$model->support_name = strtoupper($support['username']),
				$model->email_support = $support['email'],
				$model->address_2 = '-',
				$model->code = '-',
            ]);
        }
    }

    /**
     * Updates an existing MasterParty model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		$modelsContact = $model->contact;
		$modelsAddress = $model->address;
        $modelsPhone = [];
        $oldPhone = [];
		$count = MasterParty::find()->count();
		$nextCount = $count+1;
		$today = date("Y-m-d");
		if ($nextCount < 10 ){
			$noDoc = 'FM-PRT-0'.$nextCount;
		}else{
			$noDoc = 'FM-PRT-'.$nextCount;
		}
		
		$address = Address::find()->where(['id_form'=>$model->no_doc]);
		$addressDP = new ActiveDataProvider([
			'query' => $address,
			'pagination' => [
				'pageSize' => 10,
			],
		]);
		
		if (!empty($modelsContact)) {
            foreach ($modelsContact as $indexContact => $modelContact) {
                $phones = $modelContact->phones;
                $modelsPhone[$indexContact] = $phones;
                $oldPhone = ArrayHelper::merge(ArrayHelper::index($phones, 'id'), $oldPhone);
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
			$model->type = explode(',',$model->type);
			$model->office_mapping = explode(',',$model->office_mapping);
            return $this->render('update', [
                'model' => $model,
                'modelsContact' => (empty($modelsContact)) ? [new Contact] : $modelsContact,
                'modelsPhone' => (empty($modelsPhone)) ? [new Phone] : $modelsPhone,
                'modelsAddress' => (empty($modelsAddress)) ? [new Address] : $modelsAddress,
				'addressDP'=>$addressDP,
				'id'=>$noDoc,
				$model->no_doc = $noDoc,
				$model->status_ubah = 'active',
            ]);
        }
    }
	
	public function actionSearch($q='',$master='')
    {	
		if($q == '' && $master == ''){
			$searchData = (new \yii\db\Query())
			->from(['master_party'])
			->orWhere(['like','master_party.code',$q])
			->orWhere(['like','master_party.name',$q])
			->all();
		}else{
			if($master == "code" ){
				$searchData = (new \yii\db\Query())
			->from(['master_party'])
			->andWhere(['like','master_party.code',$q])
			->andWhere(['=','master_party.status','finish'])
			->all();
			}elseif($master == "name"){
				$searchData = (new \yii\db\Query())
			->from(['master_party'])
			->andWhere(['like','master_party.name',$q])
			->andWhere(['=','master_party.status','finish'])
			->all();
			}
			
		}
        
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $searchData,
            'pagination' => ['pageSize' => 10],
			'key'=>'id',
			'sort' => [
				'attributes' => ['id', 'name'],
			],
        ]);
		$model=$dataProvider->getModels();
        return $this->render(
            'search',
            [
                'dataProvider' => $dataProvider,
                'model' => $model,
                'query' => $searchData,
				'q' => $q,
				'master' => $master,
            ]
        );
    }

    /**
     * Deletes an existing MasterParty model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MasterParty::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

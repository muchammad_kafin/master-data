<?php

namespace app\controllers;

use Yii;
use app\models\MasterEquipment;
use app\models\MasterEquipmentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\User;
use app\models\Upload;
use yii\web\UploadedFile;

/**
 * MasterEquipmentController implements the CRUD actions for MasterEquipment model.
 */
class MasterEquipmentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterEquipment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MasterEquipmentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MasterEquipment model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MasterEquipment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MasterEquipment();
		// $validator = User::getValidator('validator','contract');
		$acknowledges = User::getValidator('acknowledges','equipment');
		$support = User::getValidator('support','equipment');
		$count = MasterEquipment::find()->count();
		$nextCount = $count+1;
		$today = date("Y-m-d");
		if ($nextCount < 10 ){
			$noDoc = 'FM-EQM-0'.$nextCount;
		}else{
			$noDoc = 'FM-EQM-'.$nextCount;
		}
		
        if ($model->load(Yii::$app->request->post())) {
			$cekdoc = $_POST["MasterEquipment"]["no_doc"];
			$existdoc = MasterEquipment::find()->where(['no_doc'=>$cekdoc])->one();
			if($existdoc){
				$count = MasterEquipment::find()->count();
				$nextCount = $count+1;
				$today = date("Y-m-d");
				if ($nextCount < 10 ){
					$noDoc = 'FM-EQM-0'.$nextCount;
				}else{
					$noDoc = 'FM-EQM-'.$nextCount;
				}
				$model->no_doc = $noDoc;
				
			}else{
				$model->no_doc = $cekdoc;
			}
            if($model->save()){
				$file = UploadedFile::getInstances($model, 'getfile');
				if ($file !== false) {
				   foreach ($file as $files) {
						$tmp = explode('.', $files->extension);
						$ext = end($tmp);
						$upload = new Upload();
						$upload->filename = $files->baseName . '.' . $files->extension;
						$upload->stored_file = Yii::$app->security->generateRandomString().".{$ext}";
						$upload->filepath = Yii::$app->basePath . '/web/uploads/';
						$upload->id_form = $model->no_doc;
						$upload->save(false);
						$files->saveAs(Yii::$app->basePath . '/web/uploads/' . $upload->stored_file);
						\cakebake\actionlog\model\ActionLog::add('success', $upload->filename." uploaded");	
					}
				}
				else {
					var_dump ($upload->getErrors()); die();
				}
				$email_request = $_POST["MasterEquipment"]["email_requestor"];
				$nodoc = $_POST["MasterEquipment"]["no_doc"];
				\cakebake\actionlog\model\ActionLog::add('success', $email_request." success request ".$nodoc);						
				$email_valid = $_POST["MasterEquipment"]["email_validator"];
				$validator = $_POST["MasterEquipment"]["validator_name"];
				$status = $_POST["MasterEquipment"]["status"];
				$code = $_POST["MasterEquipment"]["code"];
				$name = $_POST["MasterEquipment"]["name"];						
				$remark = $_POST["MasterEquipment"]["remark"];
				
				$email = explode(',',$email_valid);
				$valid =[];
				foreach($email as $key => $value) {
				   $valid[] = $value;
				}
				$valid[] = $email_request;
				
				Yii::$app->mailer->compose('@app/mail/layouts/validator.php',[
					'request' => $validator,
					'master' => 'Master Equipment',
					'status' => $status,
					'code' => $code,
					'name' => $name,
					'nodoc' => $nodoc,
					'remark' => $remark,
					'image' => Yii::getAlias('@app/web/klog.png'),
				])
				 ->setFrom('it.mdm@kamadjaja.com')
				 ->setTo($valid)
				 ->setCc('andra.wisata@kamadjaja.com')
				 ->setSubject('Validation Master Equipment ['.$nodoc.']')
				 ->send();
				return $this->goHome();
			}else{
				\Yii::$app->session->setFlash('error','Sorry some error occurred, please contact IT BS Team :)');
				return $this->redirect(['create']);
			}
			
        } else {
            return $this->render('create', [
                'model' => $model,
				$model->no_doc = $noDoc,
				$model->status = 'validator',
				$model->req_date = $today,
				$model->acknowledges = strtoupper($acknowledges['id']),
				$model->acknowledges_name = strtoupper($acknowledges['username']),
				$model->email_acknowledges = $acknowledges['email'],
				$model->support = strtoupper($support['id']),
				$model->support_name = strtoupper($support['username']),
				$model->email_support = $support['email'],
            ]);
        }
    }

    /**
     * Updates an existing MasterEquipment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MasterEquipment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MasterEquipment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterEquipment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MasterEquipment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

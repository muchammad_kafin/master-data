<?php

namespace app\controllers;

use Yii;
use app\models\MasterParty;
use app\models\MasterPartySearch;
use app\models\MasterClient;
use app\models\MasterClientSearch;
use app\models\MasterCountry;
use app\models\MasterCountrySearch;
use app\models\MasterDebtor;
use app\models\MasterDebtorSearch;
use app\models\MasterLocation;
use app\models\MasterLocationSearch;
use app\models\MasterRegion;
use app\models\MasterRegionSearch;
use app\models\MasterVendor;
use app\models\MasterVendorSearch;
use app\models\MasterItem;
use app\models\MasterItemSearch;
use app\models\MasterContract;
use app\models\MasterContractSearch;
use app\models\MasterEquipment;
use app\models\MasterEquipmentSearch;
use app\models\MasterProduct;
use app\models\MasterProductSearch;
use app\models\MasterPrinting;
use app\models\MasterPrintingSearch;
use app\models\MasterCharge;
use app\models\MasterChargeSearch;
use app\models\UpdateForm;
use app\models\UpdateFormSearch;
use app\models\Country;
use app\models\Region;
use app\models\City;
use app\models\User;
use app\models\Clients;
use app\models\Party;
use app\models\Contact;
use app\models\Phone;
use app\models\Upload;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * ValidatorController implements the CRUD actions for MasterParty model.
 */
class ValidatorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
			// 'access' => [
                // 'class' => AccessControl::className(),
                // 'only' => ['party','client'],
                // 'rules' => [
                    // [
                        // 'actions' => ['party','client'],
                        // 'allow' => true,
                        // 'roles' => ['@'],
                    // ],
                // ],
            // ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionParty()
    {
        $searchModel = new MasterPartySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$searchUpdate = new UpdateFormSearch();
		$queryParams = array_merge(array(),Yii::$app->request->queryParams);
        $queryParams["UpdateFormSearch"]["master"] = "PARTY";
        $dataUpdate = $searchUpdate->search($queryParams);
		$title = "Master Party";

        return $this->render('party/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'updateProvider' => $dataUpdate,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionClient()
    {
        $searchModel = new MasterClientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$searchUpdate = new UpdateFormSearch();
		$queryParams = array_merge(array(),Yii::$app->request->queryParams);
        $queryParams["UpdateFormSearch"]["master"] = "CLIENT";
        $dataUpdate = $searchUpdate->search($queryParams);
		$title = "Master Client";
		
        return $this->render('client/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'updateProvider' => $dataUpdate,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionDebtor()
    {
        $searchModel = new MasterDebtorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$searchUpdate = new UpdateFormSearch();
		$queryParams = array_merge(array(),Yii::$app->request->queryParams);
        $queryParams["UpdateFormSearch"]["master"] = "DEBTOR";
        $dataUpdate = $searchUpdate->search($queryParams);
		$title = "Master Debtor";

        return $this->render('debtor/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'updateProvider' => $dataUpdate,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionCountry()
    {
        $searchModel = new MasterCountrySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Country";

        return $this->render('country/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionLocation()
    {
        $searchModel = new MasterLocationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Location";

        return $this->render('location/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionRegion()
    {
        $searchModel = new MasterRegionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Region";

        return $this->render('region/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionVendor()
    {
        $searchModel = new MasterVendorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$searchUpdate = new UpdateFormSearch();
		$queryParams = array_merge(array(),Yii::$app->request->queryParams);
        $queryParams["UpdateFormSearch"]["master"] = "VENDOR";
        $dataUpdate = $searchUpdate->search($queryParams);
		$title = "Master Vendor";

        return $this->render('vendor/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'updateProvider' => $dataUpdate,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionItem()
    {
        $searchModel = new MasterItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Item";

        return $this->render('item/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionContract()
    {
        $searchModel = new MasterContractSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Contract";

        return $this->render('contract/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionEquipment()
    {
        $searchModel = new MasterEquipmentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Equipment";

        return $this->render('equipment/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionProduct()
    {
        $searchModel = new MasterProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Product";

        return $this->render('product/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionPrinting()
    {
        $searchModel = new MasterPrintingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Printing";

        return $this->render('printing/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionCharge()
    {
        $searchModel = new MasterChargeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Charge";

        return $this->render('charge/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdateparty($id)
    {
        $model = $this->findParty($id);
		$modelsContact = Contact::find()->where(['id_party'=>$id])->all();
		$coun = Country::find()->where(['code'=>$model->country])->one();
		if($coun){
			$couname = $coun->name;
		}else{
			$couname = $model->country;
		}
		// if($model->state != ''){
			// $state = Region::find()->where(['country'=>$coun->code,'code'=>$model->state])->one();
			// $region = $state->name;
		// }else{
			// $region = '';
		// }
		// if($model->city != ''){
			// $city = City::find()->where(['id'=>$model->city])->one();
			// $namecity = $city->name;
		// }else{
			// $namecity = '';
		// }
		
		if ($model->validator == "Approve By System"){
			$valids = "Approve By System";
		}else{
			$valids = User::getUser($model->validator);
		}
		$acknowledges = User::getUser($model->acknowledges);
		$support = User::getUser($model->support);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			$email_request = $_POST["MasterParty"]["email_requestor"];
			$email_valid = $_POST["MasterParty"]["email_validator"];
			$email_support = $_POST["MasterParty"]["email_support"];
			$email_acknowledges = $_POST["MasterParty"]["email_acknowledges"];
			$support = $_POST["MasterParty"]["support_name"];
			$status = $_POST["MasterParty"]["status"];
			$code = $_POST["MasterParty"]["code"];
			$name = $_POST["MasterParty"]["name"];
			$nodoc = $_POST["MasterParty"]["no_doc"];
			$remark = $_POST["MasterParty"]["remark"];
			
			$emailv = explode(',',$email_valid);
			$emaila = explode(',',$email_acknowledges);
			$valid =[];
			$ackno =[];
			foreach($emailv as $key => $valuev) {
			   $valid[] = $valuev;
			}
			$valid [] = $email_support;
			$valid [] = $email_request;
			
			foreach($emaila as $key => $valuea) {
			   $ackno[] = $valuea;
			}
			$ackno[]="andra.wisata@kamadjaja.com";
			
			Yii::$app->mailer->compose('@app/mail/layouts/support.php',[
				'request' => $support,
				'master' => 'Master Party',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc($ackno)
			 ->setSubject('Support Master Party ['.$nodoc.']')
			 ->send();
			 
			 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success validation ".$nodoc);
            return $this->redirect('index');
        } else {
            return $this->render('party/update', [
                'model' => $model,
				'modelsContact' => $modelsContact,
				$model->country_name = $couname,
				$model->validator_name = strtoupper($valids),
				$model->acknowledges_name = strtoupper($acknowledges),
				$model->support_name = strtoupper($support),
				$model->status = 'support',
				$model->state_name = $model->state,
				$model->city_name = $model->city,
				$model->office_mapping = explode(',', $model->office_mapping),
            ]);
        }
    }
	
	public function actionCancelparty($id)
    {
        $model = $this->findParty($id);
		$modelsContact = Contact::find()->where(['id_party'=>$id])->all();
		$model->scenario = "cancel_party";
		if ($model->load(Yii::$app->request->post())) {
			if ($model->save(false)) {
				$email_request = $model->email_requestor;
				$email_valid = $model->email_validator;
				$email_support = $model->email_support;
				$email_acknowledges = $model->email_acknowledges;
				$requestor = $model->requestor;
				$status = $model->status;
				$code = $model->code;
				$name = $model->name;
				$nodoc = $model->no_doc;
				$remark = $model->remark_cancel;
				
				$emailv = explode(',',$email_valid);
				$emaila = explode(',',$email_acknowledges);
				$valid =[];
				$ackno =[];
				foreach($emailv as $key => $valuev) {
				   $valid[] = $valuev;
				}
				foreach($emaila as $key => $valuea) {
				   $ackno[] = $valuea;
				}
				$ackno[]="andra.wisata@kamadjaja.com";
				
				Yii::$app->mailer->compose('@app/mail/layouts/reject.php',[
					'request' => $requestor,
					'master' => 'Master Party',
					'status' => $status,
					'code' => $code,
					'name' => $name,
					'nodoc' => $nodoc,
					'remark' => $remark,
					'image' => Yii::getAlias('@app/web/klog.png'),
				])
				 ->setFrom('it.mdm@kamadjaja.com')
				 ->setTo($email_request)
				 ->setCc(array_merge($valid,$ackno))
				 ->setSubject('Reject Master Party ['.$nodoc.']')
				 ->send();
				 
				 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success Reject ".$nodoc);
				return $this->redirect(['validator/party']);
			}else{
				Yii::$app->session->setFlash('error', 'Error On ');
				return $this->redirect(['cancelparty','id'=>$model->id]);
			}
		}
		else {
			return $this->render('cancel', [
				'model' => $model,
				$model->status = 'reject',
			]);
		}
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdatevendor($id)
    {
        $model = $this->findVendor($id);
		$model->scenario = "validator_vendor";
		$modelsContact = Contact::find()->where(['id_vendor'=>$id])->all();
		$modelsUpload = Upload::find()->where(['id_form'=>$model->no_doc])->all();
		$coun = Country::find()->where(['code'=>$model->country])->one();
		if($coun){
			$couname = $coun->name;
		}else{
			$couname = $model->country;
		}
		// if($model->state != ''){
			// $state = Region::find()->where(['country'=>$coun->code,'code'=>$model->state])->one();
			// $region = $state->name;
		// }else{
			// $region = '';
		// }
		// if($model->city != ''){
			// $city = City::find()->where(['id'=>$model->city])->one();
			// $namecity = $city->name;
		// }else{
			// $namecity = '';
		// }
		
		$valid = User::getUser($model->validator);
		$acknowledges = User::getUser($model->acknowledges);
		$support = User::getUser($model->support);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			$email_request = $_POST["MasterVendor"]["email_requestor"];
			$email_valid = $_POST["MasterVendor"]["email_validator"];
			$nodoc = $_POST["MasterVendor"]["no_doc"];
			$email_support = $_POST["MasterVendor"]["email_support"];
			$email_acknowledges = $_POST["MasterVendor"]["email_acknowledges"];
			$support = $_POST["MasterVendor"]["support_name"];
			$status = $_POST["MasterVendor"]["status"];
			$code = $_POST["MasterVendor"]["code"];
			$name = $_POST["MasterVendor"]["name"];
			$address1 = $_POST["MasterVendor"]["address_1"];
			$address2 = $_POST["MasterVendor"]["address_2"];
			$address3 = $_POST["MasterVendor"]["address_3"];
			$address4 = $_POST["MasterVendor"]["address_4"];
			$tax = $_POST["MasterVendor"]["tax_address_code"];
			$npwp = $_POST["MasterVendor"]["npwp"];
			$vendor_group = $_POST["MasterVendor"]["vendor_group"];
			$account_code = $_POST["MasterVendor"]["account_code"];
			$credit = $_POST["MasterVendor"]["credit_term"];
			$billing_currency = $_POST["MasterVendor"]["billing_currency"];
			$remark = $_POST["MasterVendor"]["remark"];
			
			$emailv = explode(',',$email_valid);
			$emaila = explode(',',$email_acknowledges);
			$valid =[];
			$ackno =[];
			
			foreach($emailv as $key => $valuev) {
			   $valid[] = $valuev;
			}
			$valid [] = $email_support;
			$valid [] = $email_request;
			
			foreach($emaila as $key => $valuea) {
			   $ackno[] = $valuea;
			}
			$ackno[]="andra.wisata@kamadjaja.com";
			
			Yii::$app->mailer->compose('@app/mail/layouts/support.php',[
				'request' => $support,
				'master' => 'Master Vendor',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc($ackno)
			 ->setSubject('Support Master Vendor ['.$nodoc.']')
			 ->send();
			 
			 Yii::$app->mailer->compose('@app/mail/layouts/support_vendor.php',[
				'request' => $support,
				'master' => 'Master Vendor',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'address' => $address1.', '.$address2.', '.$address3.', '.$address4,
				'tax' => $tax,
				'npwp' => $npwp,
				'vendor_group' => $vendor_group,
				'account_code' => $account_code,
				'billing_currency' => $billing_currency.', Credit Term : '.$credit,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($email_support)
			 ->setSubject('Support Master Vendor ['.$nodoc.']')
			 ->send();
			 
			 $codes = substr_replace($code,"",5);
			 if($codes !== 'S.GEN'){
				 $this->createPartyvendor($nodoc);
			 }
			 
			 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success validation ".$nodoc);
            return $this->redirect(['vendor']);
        } else {
            return $this->render('vendor/update', [
                'model' => $model,
                'modelsContact' => $modelsContact,
                'modelsUpload' => $modelsUpload,
				$model->country_name = $couname,
				$model->validator_name = strtoupper($valid),
				$model->acknowledges_name = strtoupper($acknowledges),
				$model->support_name = strtoupper($support),
				$model->status = 'support',
				$model->state_name = $model->state,
				$model->city_name = $model->city,
				$model->office_code = explode(',', $model->office_code),
            ]);
        }
    }
	
	protected function createPartyvendor($vendor)
    {
		$model = MasterVendor::find()->where(['no_doc'=>$vendor])->one();
		$modelNew = new MasterParty();
		$code = substr_replace($model->code,"",11);
		$code_party = str_replace('S.', 'P.', substr($code,0,11));
		$party = Party::find()->where(['code'=>$code_party])->one();
		$count = MasterParty::find()->count();
		$nextCount = $count+1;
		$today = date("Y-m-d");
		if ($nextCount < 10 ){
			$noDoc = 'FM-PRT-0'.$nextCount;
		}else{
			$noDoc = 'FM-PRT-'.$nextCount;
		}		
		if($party){
				\cakebake\actionlog\model\ActionLog::add('error', 'Party '.$party->code.' already exists');
				// return $this->redirect(['validator/debtor']);
		}else{
				$code = substr_replace($model->code,"",11);
				$code_party = str_replace('S.', 'P.', substr($code,0,11));
				$modelNew->no_doc = $noDoc;
				$modelNew->req_date = $today;
				$modelNew->code = $code_party;
				$modelNew->requestor = $model->requestor;
				$modelNew->status = "support";
				$modelNew->email_requestor = $model->email_requestor;
					$validator = \app\models\User::getValid($model->email_requestor,"validator","party");
					$support = \app\models\User::getValidator("support","party");
				$modelNew->validator = "Approve By System";
				$modelNew->email_validator = $validator["email"];
				$modelNew->acknowledges = $model->acknowledges;
				$modelNew->email_acknowledges = $model->email_acknowledges;
				$modelNew->support = $model->support;
				$modelNew->email_support = $model->email_support;
				$modelNew->support_date = "";
				$modelNew->remark = "";
				$modelNew->remark_cancel = "";
				$modelNew->client = "-";
				$modelNew->name = $model->name;
				$modelNew->office = $model->address_type;
				$modelNew->address_1 = $model->address_1;
				$modelNew->address_2 = $model->address_2;
				$modelNew->country = $model->country;
				$modelNew->state = $model->state;
				$modelNew->city = $model->city;
				$modelNew->zip_code = $model->zip_code;
				$modelNew->email = $model->email;
				$modelNew->type = ["-"=>"-"];
				$modelNew->office_mapping = explode(",",$model->office_code);
				$modelNew->location = "-";
				
				$modelParty = New Party();
				$modelParty->code = $code_party;
				$modelParty->name = $model->name;
				$modelParty->status = '1';
				$modelParty->save(false);
				
			if ($modelNew->save(false)) {
					$modelsContact = Contact::find()->where(['id_vendor'=>$model->id])->all();
					foreach ($modelsContact as $contact) {
							$modelContact = NEW Contact();
							$modelContact->id_party = $modelNew->id;
							$modelContact->firstname = $contact->firstname;
							$modelContact->lastname = $contact->lastname;
							if($modelContact->save(false)){
								$modelsPhone = Phone::find()->where(['id_contact'=>$contact->id])->all();
								foreach ($modelsPhone as $phone) {
										$modelPhone = NEW Phone();
										$modelPhone->id_contact = $modelContact->id;
										$modelPhone->phone_type = $phone->phone_type;
										$modelPhone->phone_number = $phone->phone_number;							
										$modelPhone->save(false);
								}
							}
					}
				 \cakebake\actionlog\model\ActionLog::add('success', "System success create ".$modelNew->no_doc);
				$emailv = explode(',',$validator["email"]);
				$emaila = explode(',',$model->email_acknowledges);
				$valid =[];
				$ackno =[];
				foreach($emailv as $key => $valuev) {
				   $valid[] = $valuev;
				}
				foreach($emaila as $key => $valuea) {
				   $ackno[] = $valuea;
				}
				$ackno[]=$model->email_requestor;
				Yii::$app->mailer->compose('@app/mail/layouts/support.php',[
					'request' => $support["username"],
					'master' => 'Master Party',
					'status' => "support",
					'code' => $modelNew->code,
					'name' => $modelNew->name,
					'nodoc' => $modelNew->no_doc,
					'remark' => $modelNew->remark,
					'image' => Yii::getAlias('@app/web/klog.png'),
				])
				 ->setFrom('it.mdm@kamadjaja.com')
				 ->setTo($model->email_support)
				 ->setCc(array_merge($valid,$ackno))
				 ->setSubject('Support Master Party ['.$modelNew->no_doc.']')
				 ->send();
				 
				// return $this->redirect(['validator/debtor']);
			}else{
				Yii::$app->session->setFlash('error', 'Error On create Party');
				// return $this->redirect(['validator/debtor']);
			}
		}
    }
	
	public function actionCancelvendor($id)
    {
        $model = $this->findVendor($id);
		$model->scenario = "cancel_vendor";
		$modelsUpload = Upload::find()->where(['id_form'=>$model->no_doc])->all();
		if ($model->load(Yii::$app->request->post())) {
			/*foreach ($modelsUpload as $stored) {
				$stored->delete();
				unlink($stored->filepath.''.$stored->stored_file);
			}*/
			if ($model->save(false)) {
				$nodoc = $model->no_doc;
				$email_request = $model->email_requestor;
				$email_valid = $model->email_validator;
				$email_support = $model->email_support;
				$email_acknowledges = $model->email_acknowledges;
				$requestor = $model->requestor;
				$status = $model->status;
				$code = $model->code;
				$name = $model->name;				
				$remark = $model->remark_cancel;
				
				$emailv = explode(',',$email_valid);
				$emaila = explode(',',$email_acknowledges);
				$valid =[];
				$ackno =[];
				foreach($emailv as $key => $valuev) {
				   $valid[] = $valuev;
				}
				foreach($emaila as $key => $valuea) {
				   $ackno[] = $valuea;
				}
				$ackno[]="andra.wisata@kamadjaja.com";
				
				Yii::$app->mailer->compose('@app/mail/layouts/reject.php',[
					'request' => $requestor,
					'master' => 'Master Vendor',
					'status' => $status,
					'code' => $code,
					'name' => $name,
					'nodoc' => $nodoc,
					'remark' => $remark,
					'image' => Yii::getAlias('@app/web/klog.png'),
				])
				 ->setFrom('it.mdm@kamadjaja.com')
				 ->setTo($email_request)
				 ->setCc(array_merge($ackno,$valid))
				 ->setSubject('Reject Master Vendor ['.$nodoc.']')
				 ->send();
				 
				 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success reject ".$nodoc);
				return $this->redirect(['validator/vendor']);
			}else{
				Yii::$app->session->setFlash('error', 'Error On ');
				return $this->redirect(['cancelvendor','id'=>$model->id]);
			}
		}
		else {
			return $this->render('cancel', [
				'model' => $model,
				$model->status = 'reject',
			]);
		}
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdatedebtor($id)
    {
        $model = $this->findDebtor($id);
		$model->scenario = "validator_debtor";
		$modelsContact = Contact::find()->where(['id_debtor'=>$id])->all();
		$modelsUpload = Upload::find()->where(['id_form'=>$model->no_doc])->all();
		$coun = Country::find()->where(['code'=>$model->country])->one();
		if($coun){
			$couname = $coun->name;
		}else{
			$couname = $model->country;
		}
		// if($model->state != ''){
			// $state = Region::find()->where(['country'=>$coun->code,'code'=>$model->state])->one();
			// $region = $state->name;
		// }else{
			// $region = '';
		// }
		// if($model->city != ''){
			// $city = City::find()->where(['id'=>$model->city])->one();
			// $namecity = $city->name;
		// }else{
			// $namecity = '';
		// }
		
		$valid = User::getUser($model->validator);
		$acknowledges = User::getUser($model->acknowledges);
		$support = User::getUser($model->support);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			$email_request = $_POST["MasterDebtor"]["email_requestor"];
			$email_valid = $_POST["MasterDebtor"]["email_validator"];
			$nodoc = $_POST["MasterDebtor"]["no_doc"];
			$email_support = $_POST["MasterDebtor"]["email_support"];
			$email_acknowledges = $_POST["MasterDebtor"]["email_acknowledges"];
			$support = $_POST["MasterDebtor"]["support_name"];
			$status = $_POST["MasterDebtor"]["status"];
			$code = $_POST["MasterDebtor"]["code"];
			$name = $_POST["MasterDebtor"]["name"];
			$address1 = $_POST["MasterDebtor"]["address_1"];
			$address2 = $_POST["MasterDebtor"]["address_2"];
			$address3 = $_POST["MasterDebtor"]["address_3"];
			$address4 = $_POST["MasterDebtor"]["address_4"];
			$tax = $_POST["MasterDebtor"]["tax_address_code"];
			$npwp = $_POST["MasterDebtor"]["npwp"];
			$debtor_group = $_POST["MasterDebtor"]["debtor_group"];
			$account_code = $_POST["MasterDebtor"]["account_code"];
			$credit = $_POST["MasterDebtor"]["credit_term"];
			$billing_currency = $_POST["MasterDebtor"]["billing_currency"];
			$remark = $_POST["MasterDebtor"]["remark"];
			
			$emailv = explode(',',$email_valid);
			$emaila = explode(',',$email_acknowledges);
			$valid =[];
			$ackno =[];
			
			foreach($emailv as $key => $valuev) {
			   $valid[] = $valuev;
			}
			$valid [] = $email_support;
			$valid [] = $email_request;
			
			foreach($emaila as $key => $valuea) {
			   $ackno[] = $valuea;
			}
			// $ackno[]="andra.wisata@kamadjaja.com";
			
			$this->createClient($nodoc);
			$this->createParty($nodoc);
			
			Yii::$app->mailer->compose('@app/mail/layouts/support.php',[
				'request' => $support,
				'master' => 'Master Debtor',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc($ackno)
			 ->setSubject('Support Master Debtor ['.$nodoc.']')
			 ->send();
			 
			 Yii::$app->mailer->compose('@app/mail/layouts/support_debtor.php',[
				'request' => $support,
				'master' => 'Master Debtor',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'address' => $address1.', '.$address2.', '.$address3.', '.$address4,
				'tax' => $tax,
				'npwp' => $npwp,
				'debtor_group' => $debtor_group,
				'account_code' => $account_code,
				'billing_currency' => $billing_currency.', Credit Term : '.$credit,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($email_support)
			 ->setSubject('Support Master Debtor ['.$nodoc.']')
			 ->send();
			 
			 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success validation ".$nodoc);
            return $this->redirect(['debtor']);
        } else {
            return $this->render('debtor/update', [
                'model' => $model,
                'modelsContact' => $modelsContact,
                'modelsUpload' => $modelsUpload,
				'title' => 'Validation Master Data Debtor',
				$model->country_name = $couname,
				$model->validator_name = strtoupper($valid),
				$model->acknowledges_name = strtoupper($acknowledges),
				$model->support_name = strtoupper($support),
				$model->status = 'support',
				$model->state_name = $model->state,
				$model->city_name = $model->city,
				$model->office_code = explode(',', $model->office_code),
            ]);
        }
    }
	
	public function actionCanceldebtor($id)
    {
        $model = $this->findDebtor($id);
		$modelsUpload = Upload::find()->where(['id_form'=>$model->no_doc])->all();
		$model->scenario = "cancel_debtor";
		if ($model->load(Yii::$app->request->post())) {
			/*foreach ($modelsUpload as $stored) {
				$stored->delete();
				unlink($stored->filepath.''.$stored->stored_file);
			}*/
			if ($model->save(false)) {
				$nodoc = $model->no_doc;
				$email_request = $model->email_requestor;
				$email_valid = $model->email_validator;
				$email_support = $model->email_support;
				$email_acknowledges = $model->email_acknowledges;
				$requestor = $model->requestor;
				$status = $model->status;
				$code = $model->code;
				$name = $model->name;				
				$remark = $model->remark_cancel;
				
				$emailv = explode(',',$email_valid);
				$emaila = explode(',',$email_acknowledges);
				$valid =[];
				$ackno =[];
				foreach($emailv as $key => $valuev) {
				   $valid[] = $valuev;
				}
				foreach($emaila as $key => $valuea) {
				   $ackno[] = $valuea;
				}
				$ackno[]="andra.wisata@kamadjaja.com";
				
				Yii::$app->mailer->compose('@app/mail/layouts/reject.php',[
					'request' => $requestor,
					'master' => 'Master Debtor',
					'status' => $status,
					'code' => $code,
					'name' => $name,
					'nodoc' => $nodoc,
					'remark' => $remark,
					'image' => Yii::getAlias('@app/web/klog.png'),
				])
				 ->setFrom('it.mdm@kamadjaja.com')
				 ->setTo($email_request)
				 ->setCc(array_merge($valid,$ackno))
				 ->setSubject('Reject Master Debtor ['.$nodoc.']')
				 ->send();
				 
				 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success reject ".$nodoc);
				return $this->redirect(['validator/debtor']);
			}else{
				Yii::$app->session->setFlash('error', 'Error On ');
				return $this->redirect(['canceldebtor','id'=>$model->id]);
			}
		}
		else {
			return $this->render('cancel', [
				'model' => $model,
				$model->status = 'reject',
			]);
		}
    }
	
	public function actionClonedebtor($id)
    {
        $model = $this->findDebtor($id);
		$modelNew = new MasterDebtor();
		$modelsUpload = Upload::find()->where(['id_form'=>$model->no_doc])->all();
		$count = MasterDebtor::find()->count();
		$nextCount = $count+1;
		$today = date("Y-m-d");
		if ($nextCount < 10 ){
			$noDoc = 'FM-DBT-0'.$nextCount;
		}else{
			$noDoc = 'FM-DBT-'.$nextCount;
		}
		if($model->code != "C.".$model->sector.".XXXXX.12" && $model->account_code != "1.1.99.04.00100"){
			foreach ($modelsUpload as $stored) {
				$modelsNew = NEW Upload();
				$modelsNew->filename = $stored->filename;
				$modelsNew->stored_file = $stored->stored_file;
				$modelsNew->filepath = $stored->filepath;
				$modelsNew->id_form = $noDoc;
				$modelsNew->save(false);
			}
				$modelNew->no_doc = $noDoc;
				$modelNew->req_date = $today;
				$modelNew->code = "C.".$model->sector.".XXXXX.12";
				$modelNew->account_code = "1.1.99.04.00100";
				$modelNew->requestor = $model->requestor;
				$modelNew->status = "validator";
				$modelNew->email_requestor = $model->email_requestor;
				$modelNew->validator = $model->validator;
				$modelNew->email_validator = $model->email_validator;
				$modelNew->acknowledges = $model->acknowledges;
				$modelNew->email_acknowledges = $model->email_acknowledges;
				$modelNew->support = $model->support;
				$modelNew->email_support = $model->email_support;
				$modelNew->support_date = $model->support_date;
				$modelNew->remark = $model->remark;
				$modelNew->remark_cancel = $model->remark_cancel;
				$modelNew->name = $model->name;
				$modelNew->special_intru = $model->special_intru;
				$modelNew->tax_address_code = $model->tax_address_code;
				$modelNew->npwp = $model->npwp;
				$modelNew->debtor_group = $model->debtor_group;
				$modelNew->address_type = $model->address_type;
				$modelNew->address_1 = $model->address_1;
				$modelNew->address_2 = $model->address_2;
				$modelNew->address_3 = $model->address_3;
				$modelNew->address_4 = $model->address_4;
				$modelNew->country = $model->country;
				$modelNew->state = $model->state;
				$modelNew->city = $model->city;
				$modelNew->zip_code = $model->zip_code;
				$modelNew->email = $model->email;
				$modelNew->office_code = $model->office_code;
				$modelNew->credit_term = $model->credit_term;
				$modelNew->billing_currency = $model->billing_currency;
				$modelNew->sector = $model->sector;
				$modelNew->bank_name = $model->bank_name;
				$modelNew->account_name = $model->account_name;
				$modelNew->branch = $model->branch;
				$modelNew->swift_code = $model->swift_code;
				$modelNew->account_no = $model->account_no;
				
			if ($modelNew->save(false)) {
					$modelsContact = Contact::find()->where(['id_debtor'=>$model->id])->all();
					foreach ($modelsContact as $contact) {
							$modelContact = NEW Contact();
							$modelContact->id_debtor = $modelNew->id;
							$modelContact->firstname = $contact->firstname;
							$modelContact->lastname = $contact->lastname;
							if($modelContact->save(false)){
								$modelsPhone = Phone::find()->where(['id_contact'=>$contact->id])->all();
								foreach ($modelsPhone as $phone) {
										$modelPhone = NEW Phone();
										$modelPhone->id_contact = $modelContact->id;
										$modelPhone->phone_type = $phone->phone_type;
										$modelPhone->phone_number = $phone->phone_number;							
										$modelPhone->save(false);
								}
							}												
					}
				 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success clone ".$modelNew->no_doc);
				return $this->redirect(['validator/debtor']);
			}else{
				Yii::$app->session->setFlash('error', 'Error On ');
				return $this->redirect(['validator/debtor']);
			}
		}else{
			Yii::$app->session->setFlash('error', 'Debtor have been cloned on '.$model->created_at);
			return $this->redirect(['validator/debtor']);
		}
    }
	
    protected function createClient($debtor)
    {
		$model = MasterDebtor::find()->where(['no_doc'=>$debtor])->one();
		$modelNew = new MasterClient();
		$code = substr_replace($model->code,"",11);
		$client = Clients::find()->where(['code'=>$code])->one();
		$count = MasterClient::find()->count();
		$nextCount = $count+1;
		$today = date("Y-m-d");
		if ($nextCount < 10 ){
			$noDoc = 'FM-CLI-0'.$nextCount;
		}else{
			$noDoc = 'FM-CLI-'.$nextCount;
		}		
		if($client){
				\cakebake\actionlog\model\ActionLog::add('error', 'Client '.$client->code.' already exists');
				// return $this->redirect(['validator/debtor']);
		}else{
				$code = substr_replace($model->code,"",11);
				$code_party = str_replace('C.', 'P.', substr($code,0,11));
				$modelNew->no_doc = $noDoc;
				$modelNew->req_date = $today;
				$modelNew->code = $code;
				$modelNew->requestor = $model->requestor;
				$modelNew->status = "support";
				$modelNew->email_requestor = $model->email_requestor;
					$validator = \app\models\User::getValid($model->email_requestor,"validator","client");
					$support = \app\models\User::getValidator("support","client");
				$modelNew->validator = "Approve By System";
				$modelNew->email_validator = $validator["email"];
				$modelNew->acknowledges = $model->acknowledges;
				$modelNew->email_acknowledges = $model->email_acknowledges;
				$modelNew->support = $model->support;
				$modelNew->email_support = $model->email_support;
				$modelNew->support_date = "";
				$modelNew->remark = "";
				$modelNew->remark_cancel = "";
				$modelNew->name = $model->name;
				$modelNew->address_1 = $model->address_1;
				$modelNew->address_2 = $model->address_2;
				$modelNew->country = $model->country;
				$modelNew->state = $model->state;
				$modelNew->city = $model->city;
				$modelNew->zip_code = $model->zip_code;
				$modelNew->email = $model->email;
				$modelNew->web = "-";
				$modelNew->base_currency = $model->billing_currency;
				
				$modelClient = New Clients();
				$modelClient->code = $code;
				$modelClient->name = $model->name;
				$modelClient->status = '1';
				$modelClient->save(false);
				
			if ($modelNew->save(false)) {
					$modelsContact = Contact::find()->where(['id_debtor'=>$model->id])->all();
					foreach ($modelsContact as $contact) {
							$modelContact = NEW Contact();
							$modelContact->id_client = $modelNew->id;
							$modelContact->firstname = $contact->firstname;
							$modelContact->lastname = $contact->lastname;
							if($modelContact->save(false)){
								$modelsPhone = Phone::find()->where(['id_contact'=>$contact->id])->all();
								foreach ($modelsPhone as $phone) {
										$modelPhone = NEW Phone();
										$modelPhone->id_contact = $modelContact->id;
										$modelPhone->phone_type = $phone->phone_type;
										$modelPhone->phone_number = $phone->phone_number;							
										$modelPhone->save(false);
								}
							}
					}
				 \cakebake\actionlog\model\ActionLog::add('success', "System success create ".$modelNew->no_doc);
				$emailv = explode(',',$validator["email"]);
				$emaila = explode(',',$model->email_acknowledges);
				$valid =[];
				$ackno =[];
				foreach($emailv as $key => $valuev) {
				   $valid[] = $valuev;
				}
				foreach($emaila as $key => $valuea) {
				   $ackno[] = $valuea;
				}
				$ackno[]=$model->email_requestor;
				Yii::$app->mailer->compose('@app/mail/layouts/support.php',[
					'request' => $support["username"],
					'master' => 'Master Client',
					'status' => "support",
					'code' => $modelNew->code,
					'name' => $modelNew->name,
					'nodoc' => $modelNew->no_doc,
					'remark' => $modelNew->remark,
					'image' => Yii::getAlias('@app/web/klog.png'),
				])
				 ->setFrom('it.mdm@kamadjaja.com')
				 ->setTo($model->email_support)
				 ->setCc(array_merge($valid,$ackno))
				 ->setSubject('Support Master Client ['.$modelNew->no_doc.']')
				 ->send();
				 
				// return $this->redirect(['validator/debtor']);
			}else{
				Yii::$app->session->setFlash('error', 'Error On create Client');
				// return $this->redirect(['validator/debtor']);
			}
		}
    }
	
	protected function createParty($debtor)
    {
		$model = MasterDebtor::find()->where(['no_doc'=>$debtor])->one();
		$modelNew = new MasterParty();
		$code = substr_replace($model->code,"",11);
		$code_party = str_replace('C.', 'P.', substr($code,0,11));
		$party = Party::find()->where(['code'=>$code_party])->one();
		$count = MasterParty::find()->count();
		$nextCount = $count+1;
		$today = date("Y-m-d");
		if ($nextCount < 10 ){
			$noDoc = 'FM-PRT-0'.$nextCount;
		}else{
			$noDoc = 'FM-PRT-'.$nextCount;
		}		
		if($party){
				\cakebake\actionlog\model\ActionLog::add('error', 'Party '.$party->code.' already exists');
				// return $this->redirect(['validator/debtor']);
		}else{
				$code = substr_replace($model->code,"",11);
				$code_party = str_replace('C.', 'P.', substr($code,0,11));
				$modelNew->no_doc = $noDoc;
				$modelNew->req_date = $today;
				$modelNew->code = $code_party;
				$modelNew->requestor = $model->requestor;
				$modelNew->status = "support";
				$modelNew->email_requestor = $model->email_requestor;
					$validator = \app\models\User::getValid($model->email_requestor,"validator","party");
					$support = \app\models\User::getValidator("support","party");
				$modelNew->validator = "Approve By System";
				$modelNew->email_validator = $validator["email"];
				$modelNew->acknowledges = $model->acknowledges;
				$modelNew->email_acknowledges = $model->email_acknowledges;
				$modelNew->support = $model->support;
				$modelNew->email_support = $model->email_support;
				$modelNew->support_date = "";
				$modelNew->remark = "";
				$modelNew->remark_cancel = "";
				$modelNew->client = $model->name;
				$modelNew->name = $model->name;
				$modelNew->office = $model->address_type;
				$modelNew->address_1 = $model->address_1;
				$modelNew->address_2 = $model->address_2;
				$modelNew->country = $model->country;
				$modelNew->state = $model->state;
				$modelNew->city = $model->city;
				$modelNew->zip_code = $model->zip_code;
				$modelNew->email = $model->email;
				$modelNew->type = ["CONSIGNEE"=>"CONSIGNEE","SHIPPER"=>"SHIPPER"];
				$modelNew->office_mapping = explode(",",$model->office_code);
				$modelNew->location = "-";
				
				$modelParty = New Party();
				$modelParty->code = $code_party;
				$modelParty->name = $model->name;
				$modelParty->status = '1';
				$modelParty->save(false);
				
			if ($modelNew->save(false)) {
					$modelsContact = Contact::find()->where(['id_debtor'=>$model->id])->all();
					foreach ($modelsContact as $contact) {
							$modelContact = NEW Contact();
							$modelContact->id_party = $modelNew->id;
							$modelContact->firstname = $contact->firstname;
							$modelContact->lastname = $contact->lastname;
							if($modelContact->save(false)){
								$modelsPhone = Phone::find()->where(['id_contact'=>$contact->id])->all();
								foreach ($modelsPhone as $phone) {
										$modelPhone = NEW Phone();
										$modelPhone->id_contact = $modelContact->id;
										$modelPhone->phone_type = $phone->phone_type;
										$modelPhone->phone_number = $phone->phone_number;							
										$modelPhone->save(false);
								}
							}
					}
				 \cakebake\actionlog\model\ActionLog::add('success', "System success create ".$modelNew->no_doc);
				$emailv = explode(',',$validator["email"]);
				$emaila = explode(',',$model->email_acknowledges);
				$valid =[];
				$ackno =[];
				foreach($emailv as $key => $valuev) {
				   $valid[] = $valuev;
				}
				foreach($emaila as $key => $valuea) {
				   $ackno[] = $valuea;
				}
				$ackno[]=$model->email_requestor;
				Yii::$app->mailer->compose('@app/mail/layouts/support.php',[
					'request' => $support["username"],
					'master' => 'Master Party',
					'status' => "support",
					'code' => $modelNew->code,
					'name' => $modelNew->name,
					'nodoc' => $modelNew->no_doc,
					'remark' => $modelNew->remark,
					'image' => Yii::getAlias('@app/web/klog.png'),
				])
				 ->setFrom('it.mdm@kamadjaja.com')
				 ->setTo($model->email_support)
				 ->setCc(array_merge($valid,$ackno))
				 ->setSubject('Support Master Party ['.$modelNew->no_doc.']')
				 ->send();
				 
				// return $this->redirect(['validator/debtor']);
			}else{
				Yii::$app->session->setFlash('error', 'Error On create Party');
				// return $this->redirect(['validator/debtor']);
			}
		}
    }
	
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdateclient($id)
    {        
		$model = $this->findClient($id);
		$modelsContact = Contact::find()->where(['id_client'=>$model->id])->all();
		$coun = Country::find()->where(['code'=>$model->country])->one();
		if($coun){
			$couname = $coun->name;
		}else{
			$couname = $model->country;
		}
		// if($model->state != ''){
			// $state = Region::find()->where(['country'=>$coun->code,'code'=>$model->state])->one();
			// $region = $state->name;
		// }else{
			// $region = '';
		// }
		// if($model->city != ''){
			// $city = City::find()->where(['id'=>$model->city])->one();
			// $namecity = $city->name;
		// }else{
			// $namecity = '';
		// }
		if ($model->validator == "Approve By System"){
			$valid = "Approve By System";
		}else{
			$valid = User::getUser($model->validator);
		}
		
		$acknowledges = User::getUser($model->acknowledges);
		$support = User::getUser($model->support);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			$email_request = $_POST["MasterClient"]["email_requestor"];
			$email_valid = $_POST["MasterClient"]["email_validator"];
			$nodoc = $_POST["MasterClient"]["no_doc"];
			$email_support = $_POST["MasterClient"]["email_support"];
			$email_acknowledges = $_POST["MasterClient"]["email_acknowledges"];
			$support = $_POST["MasterClient"]["support_name"];
			$status = $_POST["MasterClient"]["status"];
			$code = $_POST["MasterClient"]["code"];
			$name = $_POST["MasterClient"]["name"];			
			$remark = $_POST["MasterClient"]["remark"];
			
			$emailv = explode(',',$email_valid);
			$emaila = explode(',',$email_acknowledges);
			$valid =[];
			$ackno =[];
			
			foreach($emailv as $key => $valuev) {
			   $valid[] = $valuev;
			}
			$valid [] = $email_support;
			$valid [] = $email_request;
			
			foreach($emaila as $key => $valuea) {
			   $ackno[] = $valuea;
			}
			$ackno[]="andra.wisata@kamadjaja.com";
			
			Yii::$app->mailer->compose('@app/mail/layouts/support.php',[
				'request' => $support,
				'master' => 'Master Client',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc($ackno)
			 ->setSubject('Support Master Client ['.$nodoc.']')
			 ->send();
			 
			 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success validation ".$nodoc);
            return $this->redirect(['validator/client']);
        } else {
            return $this->render('client/update', [
                'model' => $model,
                'modelsContact' => $modelsContact,
				'title' => 'Validation Master Data Client',
				$model->country_name = $couname,
				$model->validator_name = strtoupper($valid),
				$model->support_name = strtoupper($support),
				$model->acknowledges_name = strtoupper($acknowledges),
				$model->status = 'support',
				$model->state_name = $model->state,
				$model->city_name = $model->city,
            ]);
        }
    }
	
	public function actionCancelclient($id)
    {
        $model = $this->findClient($id);
		$model->scenario = "cancel_client";
		if ($model->load(Yii::$app->request->post())) {
			if ($model->save(false)) {
				$nodoc = $model->no_doc;
				$email_request = $model->email_requestor;
				$email_valid = $model->email_validator;
				$email_support = $model->email_support;
				$email_acknowledges = $model->email_acknowledges;
				$requestor = $model->requestor;
				$status = $model->status;
				$code = $model->code;
				$name = $model->name;				
				$remark = $model->remark_cancel;
				
				$emailv = explode(',',$email_valid);
				$emaila = explode(',',$email_acknowledges);
				$valid =[];
				$ackno =[];
				foreach($emailv as $key => $valuev) {
				   $valid[] = $valuev;
				}
				foreach($emaila as $key => $valuea) {
				   $ackno[] = $valuea;
				}
				$ackno[]="andra.wisata@kamadjaja.com";
				
				Yii::$app->mailer->compose('@app/mail/layouts/reject.php',[
					'request' => $requestor,
					'master' => 'Master Client',
					'status' => $status,
					'code' => $code,
					'name' => $name,
					'nodoc' => $nodoc,
					'remark' => $remark,
					'image' => Yii::getAlias('@app/web/klog.png'),
				])
				 ->setFrom('it.mdm@kamadjaja.com')
				 ->setTo($email_request)
				 ->setCc(array_merge($valid,$ackno))
				 ->setSubject('Reject Master Client ['.$nodoc.']')
				 ->send();
				 
				 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success reject ".$nodoc);
				return $this->redirect(['validator/client']);
			}else{
				Yii::$app->session->setFlash('error', 'Error On ');
				return $this->redirect(['cancelclient','id'=>$model->id]);
			}
		}
		else {
			return $this->render('cancel', [
				'model' => $model,
				$model->status = 'reject',
			]);
		}
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdatelocation($id)
    {
        $model = $this->findLocation($id);
		
		$valid = User::getUser($model->validator);
		$acknowledges = User::getUser($model->acknowledges);
		$support = User::getUser($model->support);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			$email_request = $_POST["MasterLocation"]["email_requestor"];
			$email_valid = $_POST["MasterLocation"]["email_validator"];
			$nodoc = $_POST["MasterLocation"]["no_doc"];
			$email_support = $_POST["MasterLocation"]["email_support"];
			$email_acknowledges = $_POST["MasterLocation"]["email_acknowledges"];
			$support = $_POST["MasterLocation"]["support_name"];
			$status = $_POST["MasterLocation"]["status"];
			$code = $_POST["MasterLocation"]["code"];
			$name = $_POST["MasterLocation"]["name"];			
			$remark = $_POST["MasterLocation"]["remark"];
			
			$emailv = explode(',',$email_valid);
			$emaila = explode(',',$email_acknowledges);
			$valid =[];
			$ackno =[];
			
			foreach($emailv as $key => $valuev) {
			   $valid[] = $valuev;
			}
			$valid [] = $email_support;
			$valid [] = $email_request;
			
			foreach($emaila as $key => $valuea) {
			   $ackno[] = $valuea;
			}
			$ackno[]="andra.wisata@kamadjaja.com";
			
			Yii::$app->mailer->compose('@app/mail/layouts/support.php',[
				'request' => $support,
				'master' => 'Master Location',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc($ackno)
			 ->setSubject('Support Master Location ['.$nodoc.']')
			 ->send();
			 
			 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success validation ".$nodoc);
            return $this->redirect(['validator/location']);
        } else {
            return $this->render('location/update', [
                'model' => $model,
				'title' => 'Validation Master Data Location',
				$model->validator_name = strtoupper($valid),
				$model->acknowledges_name = strtoupper($acknowledges),
				$model->support_name = strtoupper($support),
				$model->status = 'support',
            ]);
        }
    }
	
	public function actionCancellocation($id)
    {
        $model = $this->findLocation($id);
		$model->scenario = "cancel_location";
		if ($model->load(Yii::$app->request->post())) {
			if ($model->save(false)) {
				$nodoc = $model->no_doc;
				$email_request = $model->email_requestor;
				$email_valid = $model->email_validator;
				$email_support = $model->email_support;
				$email_acknowledges = $model->email_acknowledges;
				$requestor = $model->requestor;
				$status = $model->status;
				$code = $model->code;
				$name = $model->name;				
				$remark = $model->remark_cancel;
				
				$emailv = explode(',',$email_valid);
				$emaila = explode(',',$email_acknowledges);
				$valid =[];
				$ackno =[];
				foreach($emailv as $key => $valuev) {
				   $valid[] = $valuev;
				}
				foreach($emaila as $key => $valuea) {
				   $ackno[] = $valuea;
				}
				$ackno[]="andra.wisata@kamadjaja.com";
				
				Yii::$app->mailer->compose('@app/mail/layouts/reject.php',[
					'request' => $requestor,
					'master' => 'Master Location',
					'status' => $status,
					'code' => $code,
					'name' => $name,
					'nodoc' => $nodoc,
					'remark' => $remark,
					'image' => Yii::getAlias('@app/web/klog.png'),
				])
				 ->setFrom('it.mdm@kamadjaja.com')
				 ->setTo($email_request)
				 ->setCc(array_merge($valid,$ackno))
				 ->setSubject('Reject Master Location ['.$nodoc.']')
				 ->send();
				 
				 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success reject ".$nodoc);
				return $this->redirect(['validator/location']);
			}else{
				Yii::$app->session->setFlash('error', 'Error On ');
				return $this->redirect(['cancellocation','id'=>$model->id]);
			}
		}
		else {
			return $this->render('cancel', [
				'model' => $model,
				$model->status = 'reject',
			]);
		}
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdatecountry($id)
    {
        $model = $this->findCountry($id);
		
		$valid = User::getUser($model->validator);
		$acknowledges = User::getUser($model->acknowledges);
		$support = User::getUser($model->support);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			$email_request = $_POST["MasterCountry"]["email_requestor"];
			$email_valid = $_POST["MasterCountry"]["email_validator"];
			$nodoc = $_POST["MasterCountry"]["no_doc"];
			$email_support = $_POST["MasterCountry"]["email_support"];
			$email_acknowledges = $_POST["MasterCountry"]["email_acknowledges"];
			$support = $_POST["MasterCountry"]["support_name"];
			$status = $_POST["MasterCountry"]["status"];
			$code = $_POST["MasterCountry"]["code"];
			$name = $_POST["MasterCountry"]["name"];			
			$remark = $_POST["MasterCountry"]["remark"];
			
			$emailv = explode(',',$email_valid);
			$emaila = explode(',',$email_acknowledges);
			$valid =[];
			$ackno =[];
			
			foreach($emailv as $key => $valuev) {
			   $valid[] = $valuev;
			}
			$valid [] = $email_support;
			$valid [] = $email_request;
			
			foreach($emaila as $key => $valuea) {
			   $ackno[] = $valuea;
			}
			$ackno[]="andra.wisata@kamadjaja.com";
			
			Yii::$app->mailer->compose('@app/mail/layouts/support.php',[
				'request' => $support,
				'master' => 'Master Country',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc($ackno)
			 ->setSubject('Support Master Country ['.$nodoc.']')
			 ->send();
			 
			 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success validation ".$nodoc);
            return $this->redirect(['validator/country']);
        } else {
            return $this->render('country/update', [
                'model' => $model,
				'title' => 'Validation Master Data Country',
				$model->validator_name = strtoupper($valid),
				$model->support_name = strtoupper($support),
				$model->acknowledges_name = strtoupper($acknowledges),
				$model->status = 'support',
            ]);
        }
    }
	
	public function actionCancelcountry($id)
    {
        $model = $this->findCountry($id);
		$model->scenario = "cancel_country";
		if ($model->load(Yii::$app->request->post())) {
			if ($model->save(false)) {
				$nodoc = $model->no_doc;
				$email_request = $model->email_requestor;
				$email_valid = $model->email_validator;
				$email_support = $model->email_support;
				$email_acknowledges = $model->email_acknowledges;
				$requestor = $model->requestor;
				$status = $model->status;
				$code = $model->code;
				$name = $model->name;				
				$remark = $model->remark_cancel;
				
				$emailv = explode(',',$email_valid);
				$emaila = explode(',',$email_acknowledges);
				$valid =[];
				$ackno =[];
				foreach($emailv as $key => $valuev) {
				   $valid[] = $valuev;
				}
				foreach($emaila as $key => $valuea) {
				   $ackno[] = $valuea;
				}
				$ackno[]="andra.wisata@kamadjaja.com";
				
				Yii::$app->mailer->compose('@app/mail/layouts/reject.php',[
					'request' => $requestor,
					'master' => 'Master Country',
					'status' => $status,
					'code' => $code,
					'name' => $name,
					'nodoc' => $nodoc,
					'remark' => $remark,
					'image' => Yii::getAlias('@app/web/klog.png'),
				])
				 ->setFrom('it.mdm@kamadjaja.com')
				 ->setTo($email_request)
				 ->setCc(array_merge($valid,$ackno))
				 ->setSubject('Reject Master Country ['.$nodoc.']')
				 ->send();
				 
				 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success reject ".$nodoc);
				return $this->redirect(['validator/country']);
			}else{
				Yii::$app->session->setFlash('error', 'Error On ');
				return $this->redirect(['cancelcountry','id'=>$model->id]);
			}
		}
		else {
			return $this->render('cancel', [
				'model' => $model,
				$model->status = 'reject',
			]);
		}
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdateregion($id)
    {
        $model = $this->findRegion($id);
		
		$valid = User::getUser($model->validator);
		$acknowledges = User::getUser($model->acknowledges);
		$support = User::getUser($model->support);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			$email_request = $_POST["MasterRegion"]["email_requestor"];
			$email_valid = $_POST["MasterRegion"]["email_validator"];
			$nodoc = $_POST["MasterRegion"]["no_doc"];
			$email_support = $_POST["MasterRegion"]["email_support"];
			$email_acknowledges = $_POST["MasterRegion"]["email_acknowledges"];
			$support = $_POST["MasterRegion"]["support_name"];
			$status = $_POST["MasterRegion"]["status"];
			$code = $_POST["MasterRegion"]["code"];
			$name = $_POST["MasterRegion"]["name"];			
			$remark = $_POST["MasterRegion"]["remark"];
			
			$emailv = explode(',',$email_valid);
			$emaila = explode(',',$email_acknowledges);
			$valid =[];
			$ackno =[];
			
			foreach($emailv as $key => $valuev) {
			   $valid[] = $valuev;
			}
			$valid [] = $email_support;
			$valid [] = $email_request;
			
			foreach($emaila as $key => $valuea) {
			   $ackno[] = $valuea;
			}
			$ackno[]="andra.wisata@kamadjaja.com";
			
			Yii::$app->mailer->compose('@app/mail/layouts/support.php',[
				'request' => $support,
				'master' => 'Master Region',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc($ackno)
			 ->setSubject('Support Master Region ['.$nodoc.']')
			 ->send();
			 
			 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success validation ".$nodoc);
            return $this->redirect(['validator/region']);
        } else {
            return $this->render('region/update', [
                'model' => $model,
				'title' => 'Validation Master Data Region',
				$model->validator_name = strtoupper($valid),
				$model->support_name = strtoupper($support),
				$model->acknowledges_name = strtoupper($acknowledges),
				$model->status = 'support',
            ]);
        }
    }
	
	public function actionCancelregion($id)
    {
        $model = $this->findRegion($id);
		$model->scenario = "cancel_region";
		if ($model->load(Yii::$app->request->post())) {
			if ($model->save(false)) {
				$nodoc = $model->no_doc;
				$email_request = $model->email_requestor;
				$email_valid = $model->email_validator;
				$email_support = $model->email_support;
				$email_acknowledges = $model->email_acknowledges;
				$requestor = $model->requestor;
				$status = $model->status;
				$code = $model->code;
				$name = $model->name;				
				$remark = $model->remark_cancel;
				
				$emailv = explode(',',$email_valid);
				$emaila = explode(',',$email_acknowledges);
				$valid =[];
				$ackno =[];
				foreach($emailv as $key => $valuev) {
				   $valid[] = $valuev;
				}
				foreach($emaila as $key => $valuea) {
				   $ackno[] = $valuea;
				}
				$ackno[]="andra.wisata@kamadjaja.com";
				
				Yii::$app->mailer->compose('@app/mail/layouts/reject.php',[
					'request' => $requestor,
					'master' => 'Master Region',
					'status' => $status,
					'code' => $code,
					'name' => $name,
					'nodoc' => $nodoc,
					'remark' => $remark,
					'image' => Yii::getAlias('@app/web/klog.png'),
				])
				 ->setFrom('it.mdm@kamadjaja.com')
				 ->setTo($email_request)
				 ->setCc(array_merge($valid,$ackno))
				 ->setSubject('Reject Master Region')
				 ->send();
				 
				 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success reject ".$nodoc);
				return $this->redirect(['validator/region']);
			}else{
				Yii::$app->session->setFlash('error', 'Error On ');
				return $this->redirect(['cancelregion','id'=>$model->id]);
			}
		}
		else {
			return $this->render('cancel', [
				'model' => $model,
				$model->status = 'reject',
			]);
		}
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdateitem($id)
    {
        $model = $this->findItem($id);
		
		$valid = User::getUser($model->validator);
		$acknowledges = User::getUser($model->acknowledges);
		$support = User::getUser($model->support);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			$email_request = $_POST["MasterItem"]["email_requestor"];
			$email_valid = $_POST["MasterItem"]["email_validator"];
			$nodoc = $_POST["MasterItem"]["no_doc"];
			$email_support = $_POST["MasterItem"]["email_support"];
			$email_acknowledges = $_POST["MasterItem"]["email_acknowledges"];
			$support = $_POST["MasterItem"]["support_name"];
			$status = $_POST["MasterItem"]["status"];
			$code = $_POST["MasterItem"]["item_code"];
			$name = $_POST["MasterItem"]["description"];			
			$remark = $_POST["MasterItem"]["remark"];
			
			$emailv = explode(',',$email_valid);
			$emaila = explode(',',$email_acknowledges);
			$valid =[];
			$ackno =[];
			
			foreach($emailv as $key => $valuev) {
			   $valid[] = $valuev;
			}
			$valid [] = $email_support;
			$valid [] = $email_request;
			
			foreach($emaila as $key => $valuea) {
			   $ackno[] = $valuea;
			}
			$ackno[]="andra.wisata@kamadjaja.com";
			
			Yii::$app->mailer->compose('@app/mail/layouts/support.php',[
				'request' => $support,
				'master' => 'Master Item',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc($ackno)
			 ->setSubject('Support Master Item ['.$nodoc.']')
			 ->send();
			 
			 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success validation ".$nodoc);
            return $this->redirect(['validator/item']);
        } else {
            return $this->render('item/update', [
                'model' => $model,
				'title' => 'Validation Master Data Item',
				$model->validator_name = strtoupper($valid),
				$model->support_name = strtoupper($support),
				$model->acknowledges_name = strtoupper($acknowledges),
				$model->status = 'support',
            ]);
        }
    }
	
	public function actionCancelitem($id)
    {
        $model = $this->findItem($id);
		$model->scenario = "cancel_item";
		if ($model->load(Yii::$app->request->post())) {
			if ($model->save(false)) {
				$nodoc = $model->no_doc;
				$email_request = $model->email_requestor;
				$email_valid = $model->email_validator;
				$email_support = $model->email_support;
				$email_acknowledges = $model->email_acknowledges;
				$requestor = $model->requestor;
				$status = $model->status;
				$code = $model->item_code;
				$name = $model->description;				
				$remark = $model->remark_cancel;
				
				$emailv = explode(',',$email_valid);
				$emaila = explode(',',$email_acknowledges);
				$valid =[];
				$ackno =[];
				foreach($emailv as $key => $valuev) {
				   $valid[] = $valuev;
				}
				foreach($emaila as $key => $valuea) {
				   $ackno[] = $valuea;
				}
				$ackno[]="andra.wisata@kamadjaja.com";
				
				Yii::$app->mailer->compose('@app/mail/layouts/reject.php',[
					'request' => $requestor,
					'master' => 'Master Item',
					'status' => $status,
					'code' => $code,
					'name' => $name,
					'nodoc' => $nodoc,
					'remark' => $remark,
					'image' => Yii::getAlias('@app/web/klog.png'),
				])
				 ->setFrom('it.mdm@kamadjaja.com')
				 ->setTo($email_request)
				 ->setCc(array_merge($valid,$ackno))
				 ->setSubject('Reject Master Item ['.$nodoc.']')
				 ->send();
				 
				 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success reject ".$nodoc);
				return $this->redirect(['validator/item']);
			}else{
				Yii::$app->session->setFlash('error', 'Error On ');
				return $this->redirect(['cancelitem','id'=>$model->id]);
			}
		}
		else {
			return $this->render('cancel', [
				'model' => $model,
				$model->status = 'reject',
			]);
		}
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdatecontract($id)
    {
        $model = $this->findContract($id);
		$modelsUpload = Upload::find()->where(['id_form'=>$model->no_doc])->all();
		
		$valid = User::getUser($model->validator);
		$acknowledges = User::getUser($model->acknowledges);
		$support = User::getUser($model->support);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			
			$file = UploadedFile::getInstances($model, 'getfile');
			if ($file !== false) {
			   foreach ($file as $files) {
					$tmp = explode('.', $files->extension);
					$ext = end($tmp);
					$upload = new Upload();
					$upload->filename = $files->baseName . '.' . $files->extension;
					$upload->stored_file = Yii::$app->security->generateRandomString().".{$ext}";
					$upload->filepath = Yii::$app->basePath . '/web/uploads/';
					$upload->id_form = $_POST["MasterContract"]["no_doc"];
					$upload->save(false);
					$files->saveAs(Yii::$app->basePath . '/web/uploads/' . $upload->stored_file);
				}
			}
			else {
				var_dump ($upload->getErrors()); die();
			}
			
			$email_request = $_POST["MasterContract"]["email_requestor"];
			$email_valid = $_POST["MasterContract"]["email_validator"];
			$nodoc = $_POST["MasterContract"]["no_doc"];
			$email_support = $_POST["MasterContract"]["email_support"];
			$email_acknowledges = $_POST["MasterContract"]["email_acknowledges"];
			$support = $_POST["MasterContract"]["support_name"];
			$status = $_POST["MasterContract"]["status"];
			$code = $_POST["MasterContract"]["vendor_code"];
			$name = $_POST["MasterContract"]["vendor_name"];			
			$remark = $_POST["MasterContract"]["remark"];
			
			$emailv = explode(',',$email_valid);
			$emaila = explode(',',$email_acknowledges);
			$valid =[];
			$ackno =[];
			
			foreach($emailv as $key => $valuev) {
			   $valid[] = $valuev;
			}
			$valid [] = $email_support;
			$valid [] = $email_request;
			
			foreach($emaila as $key => $valuea) {
			   $ackno[] = $valuea;
			}
			$ackno[]="andra.wisata@kamadjaja.com";
			
			Yii::$app->mailer->compose('@app/mail/layouts/support.php',[
				'request' => $support,
				'master' => 'Master Contract',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc($ackno)
			 ->setSubject('Support Master Contract ['.$nodoc.']')
			 ->send();
			 
			 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success validation ".$nodoc);
            return $this->redirect(['validator/contract']);
        } else {
            return $this->render('contract/update', [
                'model' => $model,
                'modelsUpload' => $modelsUpload,
				'title' => 'Validation Master Data Contract',
				$model->validator_name = strtoupper($valid),
				$model->support_name = strtoupper($support),
				$model->acknowledges_name = strtoupper($acknowledges),
				$model->status = 'support',
            ]);
        }
    }
	
	public function actionCancelcontract($id)
    {
        $model = $this->findContract($id);
		$model->scenario = "cancel_contract";
		if ($model->load(Yii::$app->request->post())) {
			if ($model->save(false)) {
				$nodoc = $model->no_doc;
				$email_request = $model->email_requestor;
				$email_valid = $model->email_validator;
				$email_support = $model->email_support;
				$email_acknowledges = $model->email_acknowledges;
				$requestor = $model->requestor;
				$status = $model->status;
				$code = $model->vendor_code;
				$name = $model->vendor_name;				
				$remark = $model->remark_cancel;
				
				$emailv = explode(',',$email_valid);
				$emaila = explode(',',$email_acknowledges);
				$valid =[];
				$ackno =[];
				foreach($emailv as $key => $valuev) {
				   $valid[] = $valuev;
				}
				foreach($emaila as $key => $valuea) {
				   $ackno[] = $valuea;
				}
				$ackno[]="andra.wisata@kamadjaja.com";
				
				Yii::$app->mailer->compose('@app/mail/layouts/reject.php',[
					'request' => $requestor,
					'master' => 'Master Contract',
					'status' => $status,
					'code' => $code,
					'name' => $name,
					'nodoc' => $nodoc,
					'remark' => $remark,
					'image' => Yii::getAlias('@app/web/klog.png'),
				])
				 ->setFrom('it.mdm@kamadjaja.com')
				 ->setTo($email_request)
				 ->setCc(array_merge($valid,$ackno))
				 ->setSubject('Reject Master Contract ['.$nodoc.']')
				 ->send();
				 
				 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success reject ".$nodoc);
				return $this->redirect(['validator/contract']);
			}else{
				Yii::$app->session->setFlash('error', 'Error On ');
				return $this->redirect(['cancelcontract','id'=>$model->id]);
			}
		}
		else {
			return $this->render('cancel', [
				'model' => $model,
				$model->status = 'reject',
			]);
		}
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdateequipment($id)
    {
        $model = $this->findEquipment($id);
		$modelsUpload = Upload::find()->where(['id_form'=>$model->no_doc])->all();
		
		$valid = User::getUser($model->validator);
		$acknowledges = User::getUser($model->acknowledges);
		$support = User::getUser($model->support);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			$email_request = $_POST["MasterEquipment"]["email_requestor"];
			$email_valid = $_POST["MasterEquipment"]["email_validator"];
			$nodoc = $_POST["MasterEquipment"]["no_doc"];
			$email_support = $_POST["MasterEquipment"]["email_support"];
			$email_acknowledges = $_POST["MasterEquipment"]["email_acknowledges"];
			$support = $_POST["MasterEquipment"]["support_name"];
			$status = $_POST["MasterEquipment"]["status"];
			$code = $_POST["MasterEquipment"]["code"];
			$name = $_POST["MasterEquipment"]["name"];			
			$remark = $_POST["MasterEquipment"]["remark"];
			
			$emailv = explode(',',$email_valid);
			$emaila = explode(',',$email_acknowledges);
			$valid =[];
			$ackno =[];
			
			foreach($emailv as $key => $valuev) {
			   $valid[] = $valuev;
			}
			$valid [] = $email_support;
			$valid [] = $email_request;
			
			foreach($emaila as $key => $valuea) {
			   $ackno[] = $valuea;
			}
			$ackno[]="andra.wisata@kamadjaja.com";
			
			Yii::$app->mailer->compose('@app/mail/layouts/support.php',[
				'request' => $support,
				'master' => 'Master Equipment',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc($ackno)
			 ->setSubject('Support Master Equipment ['.$nodoc.']')
			 ->send();
			 
			 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success validation ".$nodoc);
            return $this->redirect(['validator/equipment']);
        } else {
            return $this->render('equipment/update', [
                'model' => $model,
                'modelsUpload' => $modelsUpload,
				'title' => 'Validation Master Data Equipment',
				$model->validator_name = strtoupper($valid),
				$model->support_name = strtoupper($support),
				$model->acknowledges_name = strtoupper($acknowledges),
				$model->status = 'support',
            ]);
        }
    }
	
	public function actionCancelequipment($id)
    {
        $model = $this->findEquipment($id);
		$model->scenario = "cancel_equipment";
		if ($model->load(Yii::$app->request->post())) {
			if ($model->save(false)) {
				$nodoc = $model->no_doc;
				$email_request = $model->email_requestor;
				$email_valid = $model->email_validator;
				$email_support = $model->email_support;
				$email_acknowledges = $model->email_acknowledges;
				$requestor = $model->requestor;
				$status = $model->status;
				$code = $model->code;
				$name = $model->name;				
				$remark = $model->remark_cancel;
				
				$emailv = explode(',',$email_valid);
				$emaila = explode(',',$email_acknowledges);
				$valid =[];
				$ackno =[];
				foreach($emailv as $key => $valuev) {
				   $valid[] = $valuev;
				}
				foreach($emaila as $key => $valuea) {
				   $ackno[] = $valuea;
				}
				$ackno[]="andra.wisata@kamadjaja.com";
				
				Yii::$app->mailer->compose('@app/mail/layouts/reject.php',[
					'request' => $requestor,
					'master' => 'Master Equipment',
					'status' => $status,
					'code' => $code,
					'name' => $name,
					'nodoc' => $nodoc,
					'remark' => $remark,
					'image' => Yii::getAlias('@app/web/klog.png'),
				])
				 ->setFrom('it.mdm@kamadjaja.com')
				 ->setTo($email_request)
				 ->setCc(array_merge($valid,$ackno))
				 ->setSubject('Reject Master Equipment ['.$nodoc.']')
				 ->send();
				 
				 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success reject ".$nodoc);
				return $this->redirect(['validator/equipment']);
			}else{
				Yii::$app->session->setFlash('error', 'Error On ');
				return $this->redirect(['cancelequipment','id'=>$model->id]);
			}
		}
		else {
			return $this->render('cancel', [
				'model' => $model,
				$model->status = 'reject',
			]);
		}
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdateproduct($id)
    {
        $model = $this->findProduct($id);
		
		$valid = User::getUser($model->validator);
		$acknowledges = User::getUser($model->acknowledges);
		$support = User::getUser($model->support);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			$email_request = $_POST["MasterProduct"]["email_requestor"];
			$email_valid = $_POST["MasterProduct"]["email_validator"];
			$nodoc = $_POST["MasterProduct"]["no_doc"];
			$email_support = $_POST["MasterProduct"]["email_support"];
			$email_acknowledges = $_POST["MasterProduct"]["email_acknowledges"];
			$support = $_POST["MasterProduct"]["support_name"];
			$status = $_POST["MasterProduct"]["status"];
			$code = $_POST["MasterProduct"]["code"];
			$name = $_POST["MasterProduct"]["name"];			
			$remark = $_POST["MasterProduct"]["remark"];
			
			$emailv = explode(',',$email_valid);
			$emaila = explode(',',$email_acknowledges);
			$valid =[];
			$ackno =[];
			
			foreach($emailv as $key => $valuev) {
			   $valid[] = $valuev;
			}
			$valid [] = $email_support;
			$valid [] = $email_request;
			
			foreach($emaila as $key => $valuea) {
			   $ackno[] = $valuea;
			}
			$ackno[]="andra.wisata@kamadjaja.com";
			
			Yii::$app->mailer->compose('@app/mail/layouts/support.php',[
				'request' => $support,
				'master' => 'Master Product',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc($ackno)
			 ->setSubject('Support Master Product ['.$nodoc.']')
			 ->send();
			 
			 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success validation ".$nodoc);
            return $this->redirect(['validator/product']);
        } else {
            return $this->render('product/update', [
                'model' => $model,
				'title' => 'Validation Master Data Product',
				$model->validator_name = strtoupper($valid),
				$model->support_name = strtoupper($support),
				$model->acknowledges_name = strtoupper($acknowledges),
				$model->status = 'support',
            ]);
        }
    }
	
	public function actionCancelproduct($id)
    {
        $model = $this->findProduct($id);
		$model->scenario = "cancel_product";
		if ($model->load(Yii::$app->request->post())) {
			if ($model->save(false)) {
				$nodoc = $model->no_doc;
				$email_request = $model->email_requestor;
				$email_valid = $model->email_validator;
				$email_support = $model->email_support;
				$email_acknowledges = $model->email_acknowledges;
				$requestor = $model->requestor;
				$status = $model->status;
				$code = $model->code;
				$name = $model->name;				
				$remark = $model->remark_cancel;
				
				$emailv = explode(',',$email_valid);
				$emaila = explode(',',$email_acknowledges);
				$valid =[];
				$ackno =[];
				foreach($emailv as $key => $valuev) {
				   $valid[] = $valuev;
				}
				foreach($emaila as $key => $valuea) {
				   $ackno[] = $valuea;
				}
				$ackno[]="andra.wisata@kamadjaja.com";
				
				Yii::$app->mailer->compose('@app/mail/layouts/reject.php',[
					'request' => $requestor,
					'master' => 'Master Product',
					'status' => $status,
					'code' => $code,
					'name' => $name,
					'nodoc' => $nodoc,
					'remark' => $remark,
					'image' => Yii::getAlias('@app/web/klog.png'),
				])
				 ->setFrom('it.mdm@kamadjaja.com')
				 ->setTo($email_request)
				 ->setCc(array_merge($valid,$ackno))
				 ->setSubject('Reject Master Product ['.$nodoc.']')
				 ->send();
				 
				 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success reject ".$nodoc);
				return $this->redirect(['validator/product']);
			}else{
				Yii::$app->session->setFlash('error', 'Error On ');
				return $this->redirect(['cancelproduct','id'=>$model->id]);
			}
		}
		else {
			return $this->render('cancel', [
				'model' => $model,
				$model->status = 'reject',
			]);
		}
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdateprinting($id)
    {
        $model = $this->findPrinting($id);
		
		$valid = User::getUser($model->validator);
		$acknowledges = User::getUser($model->acknowledges);
		$support = User::getUser($model->support);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			$email_request = $_POST["MasterPrinting"]["email_requestor"];
			$email_valid = $_POST["MasterPrinting"]["email_validator"];
			$nodoc = $_POST["MasterPrinting"]["no_doc"];
			$email_support = $_POST["MasterPrinting"]["email_support"];
			$email_acknowledges = $_POST["MasterPrinting"]["email_acknowledges"];
			$support = $_POST["MasterPrinting"]["support_name"];
			$status = $_POST["MasterPrinting"]["status"];
			$code = $_POST["MasterPrinting"]["code"];
			$name = $_POST["MasterPrinting"]["description"];			
			$remark = $_POST["MasterPrinting"]["remark"];
			
			$emailv = explode(',',$email_valid);
			$emaila = explode(',',$email_acknowledges);
			$valid =[];
			$ackno =[];
			
			foreach($emailv as $key => $valuev) {
			   $valid[] = $valuev;
			}
			$valid [] = $email_support;
			$valid [] = $email_request;
			
			foreach($emaila as $key => $valuea) {
			   $ackno[] = $valuea;
			}
			$ackno[]="andra.wisata@kamadjaja.com";
			
			Yii::$app->mailer->compose('@app/mail/layouts/support.php',[
				'request' => $support,
				'master' => 'Master Printing',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc($ackno)
			 ->setSubject('Support Master Printing ['.$nodoc.']')
			 ->send();
			 
			 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success validation ".$nodoc);
            return $this->redirect(['validator/printing']);
        } else {
            return $this->render('printing/update', [
                'model' => $model,
				'title' => 'Validation Master Data Printing',
				$model->validator_name = strtoupper($valid),
				$model->support_name = strtoupper($support),
				$model->acknowledges_name = strtoupper($acknowledges),
				$model->status = 'support',
            ]);
        }
    }
	
	public function actionCancelprinting($id)
    {
        $model = $this->findPrinting($id);
		$model->scenario = "cancel_printing";
		if ($model->load(Yii::$app->request->post())) {
			if ($model->save(false)) {
				$nodoc = $model->no_doc;
				$email_request = $model->email_requestor;
				$email_valid = $model->email_validator;
				$email_support = $model->email_support;
				$email_acknowledges = $model->email_acknowledges;
				$requestor = $model->requestor;
				$status = $model->status;
				$code = $model->code;
				$name = $model->description;				
				$remark = $model->remark_cancel;
				
				$emailv = explode(',',$email_valid);
				$emaila = explode(',',$email_acknowledges);
				$valid =[];
				$ackno =[];
				foreach($emailv as $key => $valuev) {
				   $valid[] = $valuev;
				}
				foreach($emaila as $key => $valuea) {
				   $ackno[] = $valuea;
				}
				$ackno[]="andra.wisata@kamadjaja.com";
				
				Yii::$app->mailer->compose('@app/mail/layouts/reject.php',[
					'request' => $requestor,
					'master' => 'Master Printing',
					'status' => $status,
					'code' => $code,
					'name' => $name,
					'nodoc' => $nodoc,
					'remark' => $remark,
					'image' => Yii::getAlias('@app/web/klog.png'),
				])
				 ->setFrom('it.mdm@kamadjaja.com')
				 ->setTo($email_request)
				 ->setCc(array_merge($valid,$ackno))
				 ->setSubject('Reject Master Printing ['.$nodoc.']')
				 ->send();
				 
				 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success reject ".$nodoc);
				return $this->redirect(['validator/printing']);
			}else{
				Yii::$app->session->setFlash('error', 'Error On ');
				return $this->redirect(['cancelprinting','id'=>$model->id]);
			}
		}
		else {
			return $this->render('cancel', [
				'model' => $model,
				$model->status = 'reject',
			]);
		}
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdatecharge($id)
    {
        $model = $this->findCharge($id);
		
		$valid = User::getUser($model->validator);
		$acknowledges = User::getUser($model->acknowledges);
		$support = User::getUser($model->support);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			$email_request = $_POST["MasterCharge"]["email_requestor"];
			$email_valid = $_POST["MasterCharge"]["email_validator"];
			$nodoc = $_POST["MasterCharge"]["no_doc"];
			$email_support = $_POST["MasterCharge"]["email_support"];
			$email_acknowledges = $_POST["MasterCharge"]["email_acknowledges"];
			$support = $_POST["MasterCharge"]["support_name"];
			$status = $_POST["MasterCharge"]["status"];
			$code = $_POST["MasterCharge"]["code"];
			$name = $_POST["MasterCharge"]["name"];			
			$remark = $_POST["MasterCharge"]["remark"];
			
			$emailv = explode(',',$email_valid);
			$emaila = explode(',',$email_acknowledges);
			$valid =[];
			$ackno =[];
			
			foreach($emailv as $key => $valuev) {
			   $valid[] = $valuev;
			}
			$valid [] = $email_support;
			$valid [] = $email_request;
			
			foreach($emaila as $key => $valuea) {
			   $ackno[] = $valuea;
			}
			$ackno[]="andra.wisata@kamadjaja.com";
			$ackno[]="it.mdm@kamadjaja.com";
			
			Yii::$app->mailer->compose('@app/mail/layouts/support.php',[
				'request' => $support,
				'master' => 'Master Charge',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc($ackno)
			 ->setSubject('Support Master Charge ['.$nodoc.']')
			 ->send();
			 
			 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success validation ".$nodoc);
            return $this->redirect(['validator/charge']);
        } else {
            return $this->render('charge/update', [
                'model' => $model,
				'title' => 'Validation Master Data Charge',
				$model->validator_name = strtoupper($valid),
				$model->support_name = strtoupper($support),
				$model->acknowledges_name = strtoupper($acknowledges),
				$model->status = 'support',
				$model->office_map = explode(',', $model->office_map),
				$model->job_type = explode(',', $model->job_type),
            ]);
        }
    }
	
	public function actionCancelcharge($id)
    {
        $model = $this->findCharge($id);
		$model->scenario = "cancel_charge";
		if ($model->load(Yii::$app->request->post())) {
			if ($model->save(false)) {
				$nodoc = $model->no_doc;
				$email_request = $model->email_requestor;
				$email_valid = $model->email_validator;
				$email_support = $model->email_support;
				$email_acknowledges = $model->email_acknowledges;
				$requestor = $model->requestor;
				$status = $model->status;
				$code = $model->code;
				$name = $model->name;				
				$remark = $model->remark_cancel;
				
				$emailv = explode(',',$email_valid);
				$emaila = explode(',',$email_acknowledges);
				$valid =[];
				$ackno =[];
				foreach($emailv as $key => $valuev) {
				   $valid[] = $valuev;
				}
				foreach($emaila as $key => $valuea) {
				   $ackno[] = $valuea;
				}
				$ackno[]="andra.wisata@kamadjaja.com";
				
				Yii::$app->mailer->compose('@app/mail/layouts/reject.php',[
					'request' => $requestor,
					'master' => 'Master Charge',
					'status' => $status,
					'code' => $code,
					'name' => $name,
					'nodoc' => $nodoc,
					'remark' => $remark,
					'image' => Yii::getAlias('@app/web/klog.png'),
				])
				 ->setFrom('it.mdm@kamadjaja.com')
				 ->setTo($email_request)
				 ->setCc(array_merge($valid,$ackno))
				 ->setSubject('Reject Master Charge ['.$nodoc.']')
				 ->send();
				 
				 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success reject ".$nodoc);
				return $this->redirect(['validator/charge']);
			}else{
				Yii::$app->session->setFlash('error', 'Error On ');
				return $this->redirect(['cancelcharge','id'=>$model->id]);
			}
		}
		else {
			return $this->render('cancel', [
				'model' => $model,
				$model->status = 'reject',
			]);
		}
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionUpdateform($id)
    {
        $model = $this->findUpdate($id);
		$modelsUpload = Upload::find()->where(['id_form'=>$model->no_doc])->all();
		$mast = $model->master;
		$model->scenario = "validator_update";
		
		$valid = User::getUser($model->validator);
		$acknowledges = User::getUser($model->acknowledges);
		$support = User::getUser($model->support);

        if ($model->load(Yii::$app->request->post())) {
			$valid = $model->validate();
			if ($valid) {
				$model->save(false);
				$file = UploadedFile::getInstances($model, 'getfile');
				if ($file !== false) {
				   foreach ($file as $files) {
						$tmp = explode('.', $files->extension);
						$ext = end($tmp);
						$upload = new Upload();
						$upload->filename = $files->baseName . '.' . $files->extension;
						$upload->stored_file = Yii::$app->security->generateRandomString().".{$ext}";
						$upload->filepath = Yii::$app->basePath . '/web/uploads/';
						$upload->id_form = $_POST["UpdateForm"]["no_doc"];
						$upload->save(false);
						$files->saveAs(Yii::$app->basePath . '/web/uploads/' . $upload->stored_file);
					}
				}
				else {
					var_dump ($upload->getErrors()); die();
				}
				$email_request = $_POST["UpdateForm"]["email_requestor"];
				$email_valid = $_POST["UpdateForm"]["email_validator"];
				$nodoc = $_POST["UpdateForm"]["no_doc"];
				$email_support = $_POST["UpdateForm"]["email_support"];
				$email_acknowledges = $_POST["UpdateForm"]["email_acknowledges"];
				$support = $_POST["UpdateForm"]["support_name"];
				$status = $_POST["UpdateForm"]["status"];
				$code = $_POST["UpdateForm"]["code"];
				$name = $_POST["UpdateForm"]["name"];
				$remark = $_POST["UpdateForm"]["remark"];
				$remark_validator = $_POST["UpdateForm"]["remark_validator"];
				$request = $_POST["UpdateForm"]["request"];
				$reason = $_POST["UpdateForm"]["reason"];
				$master = $_POST["UpdateForm"]["master"];
				
				$emailv = explode(',',$email_valid);
				$emaila = explode(',',$email_acknowledges);
				$valid =[];
				$ackno =[];
				
				foreach($emailv as $key => $valuev) {
				   $valid[] = $valuev;
				}
				$valid [] = $email_support;
				$valid [] = $email_request;
				
				foreach($emaila as $key => $valuea) {
				   $ackno[] = $valuea;
				}
				
				Yii::$app->mailer->compose('@app/mail/layouts/support_update.php',[
					'support' => $support,
					'master' => 'Update Master '.$master,
					'status' => $status,
					'code' => $code,
					'name' => $name,
					'nodoc' => $nodoc,
					'remark' => $remark,
					'remark_validator' => $remark_validator,
					'reason' => $reason,
					'request' => $request,
					'image' => Yii::getAlias('@app/web/klog.png'),
				])
				 ->setFrom('it.mdm@kamadjaja.com')
				 ->setTo($valid)
				 ->setCc($ackno)
				 ->setSubject('Support Update Master '.$master.'['.$nodoc.']')
				 ->send();
				 
				 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success validation ".$nodoc);
				return $this->redirect([$master]);
			}
			else{
				var_dump ($model->getErrors()); die();
			}
        } else {
            return $this->render('update-form/update', [
                'model' => $model,
                'modelsUpload' => $modelsUpload,
				'title' => 'Validation Update Master '.$mast,
				$model->validator_name = strtoupper($valid),
				$model->acknowledges_name = strtoupper($acknowledges),
				$model->support_name = strtoupper($support),
				$model->status = 'support',
				'master' => $mast,
            ]);
        }
    }
	
	public function actionCancelupdate($id)
    {
        $model = $this->findUpdate($id);
		$model->scenario = "cancel_update";
		if ($model->load(Yii::$app->request->post())) {
			if ($model->save(false)) {
				$nodoc = $model->no_doc;
				$email_request = $model->email_requestor;
				$email_valid = $model->email_validator;
				$email_support = $model->email_support;
				$email_acknowledges = $model->email_acknowledges;
				$requestor = $model->requestor;
				$status = $model->status;
				$code = $model->code;
				$name = $model->name;				
				$remark = $model->remark_cancel;
				
				$emailv = explode(',',$email_valid);
				$emaila = explode(',',$email_acknowledges);
				$valid =[];
				$ackno =[];
				foreach($emailv as $key => $valuev) {
				   $valid[] = $valuev;
				}
				foreach($emaila as $key => $valuea) {
				   $ackno[] = $valuea;
				}
				// $ackno[]="andra.wisata@kamadjaja.com";
				
				Yii::$app->mailer->compose('@app/mail/layouts/reject.php',[
					'request' => $requestor,
					'master' => 'Update Master '.$model->master,
					'status' => $status,
					'code' => $code,
					'name' => $name,
					'nodoc' => $nodoc,
					'remark' => $remark,
					'image' => Yii::getAlias('@app/web/klog.png'),
				])
				 ->setFrom('it.mdm@kamadjaja.com')
				 ->setTo($email_request)
				 ->setCc(array_merge($valid,$ackno))
				 ->setSubject('Reject Update Master '.$model->master.'['.$nodoc.']')
				 ->send();
				 $mast = strtolower($model->master);
				 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success reject ".$nodoc);
				return $this->redirect(['validator/'.$mast]);
			}else{
				Yii::$app->session->setFlash('error', 'Error On ');
				return $this->redirect(['cancelupdate','id'=>$model->no_doc]);
			}
		}
		else {
			return $this->render('cancel', [
				'model' => $model,
				$model->status = 'reject',
			]);
		}
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findParty($id)
    {
        if (($model = MasterParty::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findClient($id)
    {
        if (($model = MasterClient::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findCountry($id)
    {
        if (($model = MasterCountry::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findLocation($id)
    {
        if (($model = MasterLocation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findDebtor($id)
    {
        if (($model = MasterDebtor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findRegion($id)
    {
        if (($model = MasterRegion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findVendor($id)
    {
        if (($model = MasterVendor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findItem($id)
    {
        if (($model = MasterItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findContract($id)
    {
        if (($model = MasterContract::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findEquipment($id)
    {
        if (($model = MasterEquipment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findProduct($id)
    {
        if (($model = MasterProduct::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findPrinting($id)
    {
        if (($model = MasterPrinting::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findCharge($id)
    {
        if (($model = MasterCharge::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findUpdate($id)
    {
        if (($model = UpdateForm::findOne(['no_doc'=>$id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
<?php

namespace app\controllers;

use Yii;
use app\models\MasterContract;
use app\models\MasterContractSearch;
use app\models\User;
use app\models\Upload;
use yii\web\UploadedFile;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MasterContractController implements the CRUD actions for MasterContract model.
 */
class MasterContractController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterContract models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MasterContractSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MasterContract model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MasterContract model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MasterContract();
		// $validator = User::getValidator('validator','contract');
		$acknowledges = User::getValidator('acknowledges','contract');
		$support = User::getValidator('support','contract');
		$count = MasterContract::find()->count();
		$nextCount = $count+1;
		$today = date("Y-m-d");
		if ($nextCount < 10 ){
			$noDoc = 'FM-CTC-0'.$nextCount;
		}else{
			$noDoc = 'FM-CTC-'.$nextCount;
		}
		
        if ($model->load(Yii::$app->request->post())) {
			$cekdoc = $_POST["MasterContract"]["no_doc"];
			$existdoc = MasterContract::find()->where(['no_doc'=>$cekdoc])->one();
			if($existdoc){
				$count = MasterContract::find()->count();
				$nextCount = $count+1;
				$today = date("Y-m-d");
				if ($nextCount < 10 ){
					$noDoc = 'FM-CTC-0'.$nextCount;
				}else{
					$noDoc = 'FM-CTC-'.$nextCount;
				}
				$model->no_doc = $noDoc;
				$model->save();
			}else{
				$model->no_doc = $cekdoc;
				$model->save();
			}
			$file = UploadedFile::getInstances($model, 'getfile');
			if ($file !== false) {
			   foreach ($file as $files) {
					$tmp = explode('.', $files->extension);
					$ext = end($tmp);
					$upload = new Upload();
					$upload->filename = $files->baseName . '.' . $files->extension;
					$upload->stored_file = Yii::$app->security->generateRandomString().".{$ext}";
					$upload->filepath = Yii::$app->basePath . '/web/uploads/';
					$upload->id_form = $model->no_doc;
					$upload->save(false);
					$files->saveAs(Yii::$app->basePath . '/web/uploads/' . $upload->stored_file);
				}
			}
			else {
				var_dump ($upload->getErrors()); die();
			}
			$email_request = $_POST["MasterContract"]["email_requestor"];
			$email_valid = $_POST["MasterContract"]["email_validator"];
			$email_acknowledges = $_POST["MasterContract"]["email_acknowledges"];
			$request = $_POST["MasterContract"]["requestor"];
			$validator = $_POST["MasterContract"]["validator_name"];
			$status = $_POST["MasterContract"]["status"];
			$code = $_POST["MasterContract"]["vendor_code"];
			$name = $_POST["MasterContract"]["vendor_name"];
			$nodoc = $model->no_doc;
			$remark = $_POST["MasterContract"]["remark"];
			
			$email = explode(',',$email_valid);
			$valid =[];
			foreach($email as $key => $value) {
			   $valid[] = $value;
			}
			$valid[] = $email_request;
			
			Yii::$app->mailer->compose('@app/mail/layouts/validator.php',[
				'request' => $validator,
				'master' => 'Master Contract',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc('andra.wisata@kamadjaja.com')
			 ->setSubject('Validation Master Contract ['.$nodoc.']')
			 ->send();
			 
			 \cakebake\actionlog\model\ActionLog::add('success', $email_request." success request");
            return $this->goHome();
        } else {
            return $this->render('create', [
                'model' => $model,
				$model->no_doc = $noDoc,
				$model->status = 'validator',
				$model->req_date = $today,
				// $model->validator = strtoupper($validator['id']),
				// $model->validator_name = strtoupper($validator['username']),
				// $model->email_validator = $validator['email'],
				$model->acknowledges = strtoupper($acknowledges['id']),
				$model->acknowledges_name = strtoupper($acknowledges['username']),
				$model->email_acknowledges = $acknowledges['email'],
				$model->support = strtoupper($support['id']),
				$model->support_name = strtoupper($support['username']),
				$model->email_support = $support['email'],
            ]);
        }
    }

    /**
     * Updates an existing MasterContract model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MasterContract model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MasterContract model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterContract the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MasterContract::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

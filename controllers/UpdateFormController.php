<?php

namespace app\controllers;

use Yii;
use app\models\UpdateForm;
use app\models\UpdateFormSearch;
use app\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\MasterParty;
use app\models\MasterPartySearch;
use app\models\MasterClient;
use app\models\MasterClientSearch;
use app\models\MasterCountry;
use app\models\MasterCountrySearch;
use app\models\MasterDebtor;
use app\models\MasterDebtorSearch;
use app\models\MasterLocation;
use app\models\MasterLocationSearch;
use app\models\MasterRegion;
use app\models\MasterRegionSearch;
use app\models\MasterVendor;
use app\models\MasterVendorSearch;
use app\models\MasterItem;
use app\models\MasterItemSearch;
use app\models\MasterContract;
use app\models\MasterContractSearch;
use app\models\MasterEquipment;
use app\models\MasterEquipmentSearch;
use app\models\MasterProduct;
use app\models\MasterProductSearch;
use app\models\MasterPrinting;
use app\models\MasterPrintingSearch;
use app\models\MasterCharge;
use app\models\MasterChargeSearch;
use app\models\Upload;
use yii\web\UploadedFile;

/**
 * UpdateFormController implements the CRUD actions for UpdateForm model.
 */
class UpdateFormController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
	
	public function actionSearch($q='',$master='')
    {	
		if($q == '' && $master == ''){
			$master = "client";
			$searchData = (new \yii\db\Query())
			->select(['clients.code','clients.name','master_list.name as master'])
			->from(['clients','master_list'])
			->orWhere(['like','clients.code',$q])
			->orWhere(['like','clients.name',$q])
			->andWhere(['=','clients.status',1])
			->andWhere(['=','master_list.name',$master])
			->distinct()->all();
			
		}else{
			if($master == "client" ){
				$masters = new MasterClient();
			}elseif($master == "contract"){
				$masters = new MasterContract();
			}elseif($master == "country"){
				$masters = new MasterCountry();
			}elseif($master == "debtor"){
				$masters = new MasterDebtor();
			}elseif($master == "equipment"){
				$masters = new MasterEquipment();
			}elseif($master == "item"){
				$masters = new MasterItem();
			}elseif($master == "location"){
				$masters = new MasterLocation();
			}elseif($master == "party"){
				$masters = new MasterParty();
			}elseif($master == "printing"){
				$masters = new MasterPrinting();
			}elseif($master == "product"){
				$masters = new MasterProduct();
			}elseif($master == "region"){
				$masters = new MasterRegion();
			}elseif($master == "vendor"){
				$masters = new MasterVendor();
			}elseif($master == "charge"){
				$masters = new MasterCharge();
			}
			
			// $models = $masters->attributes();
			$data = (new \yii\db\Query())
					->select([$master.'.code',$master.'.name','master_list.name as master'])
					->from([$master,'master_list'])
					->orWhere(['like',$master.'.code',$q])
					->orWhere(['like',$master.'.name',$q])
					->andWhere(['=','master_list.name',$master])
					->distinct();
			// foreach( $models as $key => $value){
				// $data->orWhere(['like',$models[$key],$q]);
			// }
			$searchData= $data->all();
			
		}
        
        //$searchData = $search->find($q, ['model' => 'page']); // Search by index provided only by model `page`.
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $searchData,
            'pagination' => ['pageSize' => 10],
        ]);
		
		$model=$dataProvider->getModels();
		
        return $this->render(
            'search',
            [
                'dataProvider' => $dataProvider,
                'model' => $model,
                'query' => $searchData,
				'q' => $q,
				'master' => $master,
            ]
        );
    }

    /**
     * Lists all UpdateForm models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UpdateFormSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UpdateForm model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UpdateForm model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($code,$master)
    {
        $model = new UpdateForm();
		$support = User::getValidator('support',$master);
		$acknowledges = User::getValidator('acknowledges',$master);
		$count = UpdateForm::find()->count();
		$nextCount = $count+1;
		$today = date("Y-m-d");
		if ($nextCount < 10 ){
			$noDoc = 'FM-UPD-0'.$nextCount;
		}else{
			$noDoc = 'FM-UPD-'.$nextCount;
		}
		
		$data = (new \yii\db\Query())
					->select(['name'])
					->from([$master])
					->orWhere(['=',$master.'.code',$code])
					->distinct();
		$name = $data->all();

        if ($model->load(Yii::$app->request->post())) {
			$valid = $model->validate();
			if ($valid) {
				$cekdoc = $_POST["UpdateForm"]["no_doc"];
				$existdoc = UpdateForm::find()->where(['no_doc'=>$cekdoc])->one();
				if($existdoc){
					$count = UpdateForm::find()->count();
					$nextCount = $count+1;
					$today = date("Y-m-d");
					if ($nextCount < 10 ){
						$noDoc = 'FM-UPD-0'.$nextCount;
					}else{
						$noDoc = 'FM-UPD-'.$nextCount;
					}
					$model->no_doc = $noDoc;
					
				}else{
					$model->no_doc = $cekdoc;
				}
				$model->save(false);
				$file = UploadedFile::getInstances($model, 'getfile');
				if ($file !== false) {
				   foreach ($file as $files) {
						$tmp = explode('.', $files->extension);
						$ext = end($tmp);
						$upload = new Upload();
						$upload->filename = $files->baseName . '.' . $files->extension;
						$upload->stored_file = Yii::$app->security->generateRandomString().".{$ext}";
						$upload->filepath = Yii::$app->basePath . '/web/uploads/';
						$upload->id_form = $model->no_doc;
						$upload->save(false);
						$files->saveAs(Yii::$app->basePath . '/web/uploads/' . $upload->stored_file);
					}
				}
				else {
					var_dump ($upload->getErrors()); die();
				}
				
				$email_request = $_POST["UpdateForm"]["email_requestor"];
				$nodoc = $model->no_doc;
				$master = $_POST["UpdateForm"]["master"];
				\cakebake\actionlog\model\ActionLog::add('success', $email_request." success request ".$nodoc);						
				$email_valid = $_POST["UpdateForm"]["email_validator"];
				$validator = $_POST["UpdateForm"]["validator_name"];
				$status = $_POST["UpdateForm"]["status"];
				$code = $_POST["UpdateForm"]["code"];
				$name = $_POST["UpdateForm"]["name"];						
				$remark = $_POST["UpdateForm"]["remark"];
				$request = $_POST["UpdateForm"]["request"];
				$reason = $_POST["UpdateForm"]["reason"];
				
				$email = explode(',',$email_valid);
				$valid =[];
				foreach($email as $key => $value) {
				   $valid[] = $value;
				}
				$valid[] = $email_request;
				
				Yii::$app->mailer->compose('@app/mail/layouts/validator_update.php',[
					'validator' => $validator,
					'master' => 'Update Master '.$master,
					'status' => $status,
					'code' => $code,
					'name' => $name,
					'nodoc' => $nodoc,
					'reason' => $reason,
					'request' => $request,
					'remark' => $remark,
					'image' => Yii::getAlias('@app/web/klog.png'),
				])
				 ->setFrom('it.mdm@kamadjaja.com')
				 ->setTo($valid)
				 // ->setCc('andra.wisata@kamadjaja.com')
				 ->setSubject('Validation Master '.$master)
				 ->send();
				 
				return $this->goHome();
			}else {
					var_dump ($model->getErrors()); die();
				}
			
        } else {
            return $this->render('create', [
                'model' => $model,
				'master' => $master,
				$model->code = $code,
				$model->name = $name[0]['name'],
				$model->no_doc = $noDoc,
				$model->req_date = $today,
				$model->master = $master,
				$model->status = 'validator',
				$model->acknowledges = strtoupper($acknowledges['id']),
				$model->acknowledges_name = strtoupper($acknowledges['username']),
				$model->email_acknowledges = $acknowledges['email'],
				$model->support = strtoupper($support['id']),
				$model->support_name = strtoupper($support['username']),
				$model->email_support = $support['email'],
            ]);
        }
    }

    /**
     * Updates an existing UpdateForm model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UpdateForm model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UpdateForm model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UpdateForm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UpdateForm::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

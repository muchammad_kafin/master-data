<?php

namespace app\controllers;

use Yii;
use app\models\RequestorValidator;
use app\models\RequestorValidatorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RequestorValidatorController implements the CRUD actions for RequestorValidator model.
 */
class RequestorValidatorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RequestorValidator models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RequestorValidatorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RequestorValidator model.
     * @param string $id_validator
     * @param string $requestor
     * @return mixed
     */
    public function actionView($id_validator, $requestor)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_validator, $requestor),
        ]);
    }

    /**
     * Creates a new RequestorValidator model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RequestorValidator();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_validator' => $model->id_validator, 'requestor' => $model->requestor]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing RequestorValidator model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id_validator
     * @param string $requestor
     * @return mixed
     */
    public function actionUpdate($id_validator, $requestor)
    {
        $model = $this->findModel($id_validator, $requestor);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_validator' => $model->id_validator, 'requestor' => $model->requestor]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RequestorValidator model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id_validator
     * @param string $requestor
     * @return mixed
     */
    public function actionDelete($id_validator, $requestor)
    {
        $this->findModel($id_validator, $requestor)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RequestorValidator model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id_validator
     * @param string $requestor
     * @return RequestorValidator the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_validator, $requestor)
    {
        if (($model = RequestorValidator::findOne(['id_validator' => $id_validator, 'requestor' => $requestor])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

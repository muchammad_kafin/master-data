<?php

namespace app\controllers;

use Yii;
use app\models\MasterCharge;
use app\models\MasterChargeSearch;
use app\models\User;
use app\models\ChargeList;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use yii\data\ArrayDataProvider;

/**
 * MasterChargeController implements the CRUD actions for MasterCharge model.
 */
class MasterChargeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterCharge models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MasterChargeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MasterCharge model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MasterCharge model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MasterCharge();
		$support = User::getValidator('support','charge');
		$acknowledges = User::getValidator('acknowledges','charge');
		$count = MasterCharge::find()->count();
		$nextCount = $count+1;
		$today = date("Y-m-d");
		if ($nextCount < 10 ){
			$noDoc = 'FM-CRG-0'.$nextCount;
		}else{
			$noDoc = 'FM-CRG-'.$nextCount;
		}

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
					$cekdoc = $_POST["MasterCharge"]["no_doc"];
					$existdoc = MasterCharge::find()->where(['no_doc'=>$cekdoc])->one();
					if($existdoc){
						$count = MasterCharge::find()->count();
						$nextCount = $count+1;
						$today = date("Y-m-d");
						if ($nextCount < 10 ){
							$noDoc = 'FM-CRG-0'.$nextCount;
						}else{
							$noDoc = 'FM-CRG-'.$nextCount;
						}
						$model->no_doc = $noDoc;
						
					}else{
						$model->no_doc = $cekdoc;
					}
					$transaction = Yii::$app->db->beginTransaction();
					$model->office_map = implode(",",$_POST["MasterCharge"]["office_map"]);
					try {						
						if ($flag = $model->save(false)) {
							$transaction->commit();
							$email_request = $_POST["MasterCharge"]["email_requestor"];
							$nodoc = $model->no_doc;
							\cakebake\actionlog\model\ActionLog::add('success', $email_request." success request ".$nodoc);						
							$email_valid = $_POST["MasterCharge"]["email_validator"];
							$request = strtoupper($_POST["MasterCharge"]["requestor"]);
							$validator = strtoupper($_POST["MasterCharge"]["validator_name"]);
							$status = strtoupper($_POST["MasterCharge"]["status"]);
							$code = strtoupper($_POST["MasterCharge"]["code"]);
							$name = strtoupper($_POST["MasterCharge"]["name"]);
							$remark = strtoupper($_POST["MasterCharge"]["remark"]);
							
							$email = explode(',',$email_valid);
							$valid =[];
							foreach($email as $key => $value) {
							   $valid[] = $value;
							}
							$valid[]="it.mdm@kamadjaja.com";
							$valid[]=$email_request;
							
							Yii::$app->mailer->compose('@app/mail/layouts/validator.php',[
								'request' => $validator,
								'master' => 'Master Charge',
								'status' => $status,
								'code' => $code,
								'name' => $name,
								'nodoc' => $nodoc,
								'remark' => $remark,
								'image' => Yii::getAlias('@app/web/klog.png'),
							])
							 ->setFrom('it.mdm@kamadjaja.com')
							 ->setTo($valid)
							 ->setCc('andra.wisata@kamadjaja.com')
							 ->setSubject('Validation Master Charge ['.$nodoc.']')
							 ->send();
							 
							return $this->goHome();
						} else {
							$transaction->rollBack();
							\Yii::$app->session->setFlash('error','Sorry some error occurred, please contact IT BS Team :)');
							return $this->redirect(['create']);
						}
					} catch (Exception $e) {
						$transaction->rollBack();
						\Yii::$app->session->setFlash('error','Sorry some error occurred, please contact IT BS Team :)');
						return $this->redirect(['create']);
					}
        } else {
            return $this->render('create', [
                'model' => $model,
				$model->no_doc = $noDoc,
				$model->status = 'validator',
				$model->req_date = $today,
				$model->acknowledges = strtoupper($acknowledges['id']),
				$model->acknowledges_name = strtoupper($acknowledges['username']),
				$model->email_acknowledges = $acknowledges['email'],
				$model->support = strtoupper($support['id']),
				$model->support_name = strtoupper($support['username']),
				$model->email_support = $support['email'],
				$model->code = '-',
            ]);
        }
    }

    /**
     * Updates an existing MasterCharge model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MasterCharge model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
	public function actionExists($name){		
		Yii::$app->response->format = Response::FORMAT_JSON;
		$posts = [];
        $posts = ChargeList::find()->where(['name'=>$name])->one();
		if($posts['job_type']){
			$posts['job_type'] = explode(",",$posts['job_type']);
		} 
		if($posts['office_map']){
			$posts['office_map'] = explode(",",$posts['office_map']);
		}
		
        if($posts){
			return $posts;
        }else{
			return false;
		}
	}

    /**
     * Finds the MasterCharge model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterCharge the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MasterCharge::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

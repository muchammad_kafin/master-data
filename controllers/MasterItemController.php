<?php

namespace app\controllers;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;
use app\models\MasterItem;
use app\models\MasterItemSearch;
use app\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MasterItemController implements the CRUD actions for MasterItem model.
 */
class MasterItemController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MasterItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MasterItem model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MasterItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MasterItem();
		// $validator = User::getValidator('validator','item');
		$acknowledges = User::getValidator('acknowledges','item');
		$support = User::getValidator('support','item');
		$count = MasterItem::find()->count();
		$nextCount = $count+1;
		$today = date("Y-m-d");
		if ($nextCount < 10 ){
			$noDoc = 'FM-ITM-0'.$nextCount;
		}else{
			$noDoc = 'FM-ITM-'.$nextCount;
		}
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			$cekdoc = $_POST["MasterItem"]["no_doc"];
			$existdoc = MasterItem::find()->where(['no_doc'=>$cekdoc])->one();
			if($existdoc){
				$count = MasterItem::find()->count();
				$nextCount = $count+1;
				$today = date("Y-m-d");
				if ($nextCount < 10 ){
					$noDoc = 'FM-ITM-0'.$nextCount;
				}else{
					$noDoc = 'FM-ITM-'.$nextCount;
				}
				$model->no_doc = $noDoc;
				
			}else{
				$model->no_doc = $cekdoc;
			}
			$email_request = $_POST["MasterItem"]["email_requestor"];
			$email_valid = $_POST["MasterItem"]["email_validator"];
			$email_acknowledges = $_POST["MasterItem"]["email_acknowledges"];
			$request = $_POST["MasterItem"]["requestor"];
			$validator = $_POST["MasterItem"]["validator_name"];
			$status = $_POST["MasterItem"]["status"];
			$code = $_POST["MasterItem"]["item_code"];
			$name = $_POST["MasterItem"]["description"];
			$nodoc = $model->no_doc;
			$remark = $_POST["MasterItem"]["remark"];
			
			$email = explode(',',$email_valid);
			$valid =[];
			foreach($email as $key => $value) {
			   $valid[] = $value;
			}
			$valid[] = $email_request;
			
			/*Yii::$app->mailer->compose('@app/mail/layouts/validator.php',[
				'request' => $request,
				'master' => 'Master Item',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc('andra.wisata@kamadjaja.com')
			 ->setSubject('Validation Master Item')
			 ->send();*/
			 
			Yii::$app->mailer->compose('@app/mail/layouts/validator.php',[
				'request' => $validator,
				'master' => 'Master Item',
				'status' => $status,
				'code' => $code,
				'name' => $name,
				'nodoc' => $nodoc,
				'remark' => $remark,
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo($valid)
			 ->setCc('andra.wisata@kamadjaja.com')
			 ->setSubject('Validator Master Item ['.$nodoc.']')
			 ->send();
			 
			 \cakebake\actionlog\model\ActionLog::add('success', $email_request." success request");
            return $this->goHome();
        } else {
            return $this->render('create', [
                'model' => $model,
				$model->no_doc = $noDoc,
				$model->status = 'validator',
				$model->req_date = $today,
				// $model->validator = strtoupper($validator['id']),
				// $model->validator_name = strtoupper($validator['username']),
				// $model->email_validator = $validator['email'],
				$model->acknowledges = strtoupper($acknowledges['id']),
				$model->acknowledges_name = strtoupper($acknowledges['username']),
				$model->email_acknowledges = $acknowledges['email'],
				$model->support = strtoupper($support['id']),
				$model->support_name = strtoupper($support['username']),
				$model->email_support = $support['email'],
            ]);
        }
    }

    /**
     * Updates an existing MasterItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MasterItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MasterItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MasterItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

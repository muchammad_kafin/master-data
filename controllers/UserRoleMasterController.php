<?php

namespace app\controllers;

use Yii;
use app\models\UserRoleMaster;
use app\models\UserRoleMasterSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserRoleMasterController implements the CRUD actions for UserRoleMaster model.
 */
class UserRoleMasterController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserRoleMaster models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserRoleMasterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserRoleMaster model.
     * @param integer $id_user
     * @param integer $id_master
     * @param integer $id_role
     * @return mixed
     */
    public function actionView($id_user, $id_master, $id_role)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_user, $id_master, $id_role),
        ]);
    }

    /**
     * Creates a new UserRoleMaster model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserRoleMaster();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_user' => $model->id_user, 'id_master' => $model->id_master, 'id_role' => $model->id_role]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing UserRoleMaster model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id_user
     * @param integer $id_master
     * @param integer $id_role
     * @return mixed
     */
    public function actionUpdate($id_user, $id_master, $id_role)
    {
        $model = $this->findModel($id_user, $id_master, $id_role);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_user' => $model->id_user, 'id_master' => $model->id_master, 'id_role' => $model->id_role]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UserRoleMaster model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id_user
     * @param integer $id_master
     * @param integer $id_role
     * @return mixed
     */
    public function actionDelete($id_user, $id_master, $id_role)
    {
        $this->findModel($id_user, $id_master, $id_role)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserRoleMaster model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id_user
     * @param integer $id_master
     * @param integer $id_role
     * @return UserRoleMaster the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_user, $id_master, $id_role)
    {
        if (($model = UserRoleMaster::findOne(['id_user' => $id_user, 'id_master' => $id_master, 'id_role' => $id_role])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php

namespace app\controllers;

use Yii;
use app\models\MasterPrinting;
use app\models\MasterPrintingSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\User;

/**
 * MasterPrintingController implements the CRUD actions for MasterPrinting model.
 */
class MasterPrintingController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterPrinting models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MasterPrintingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MasterPrinting model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MasterPrinting model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MasterPrinting();
		// $validator = User::getValidator('validator','contract');
		$acknowledges = User::getValidator('acknowledges','printing');
		$support = User::getValidator('support','printing');
		$count = MasterPrinting::find()->count();
		$nextCount = $count+1;
		$today = date("Y-m-d");
		if ($nextCount < 10 ){
			$noDoc = 'FM-PTG-0'.$nextCount;
		}else{
			$noDoc = 'FM-PTG-'.$nextCount;
		}

        if ($model->load(Yii::$app->request->post())) {
				$cekdoc = $_POST["MasterPrinting"]["no_doc"];
				$existdoc = MasterPrinting::find()->where(['no_doc'=>$cekdoc])->one();
				if($existdoc){
					$count = MasterPrinting::find()->count();
					$nextCount = $count+1;
					$today = date("Y-m-d");
					if ($nextCount < 10 ){
						$noDoc = 'FM-PTG-0'.$nextCount;
					}else{
						$noDoc = 'FM-PTG-'.$nextCount;
					}
					$model->no_doc = $noDoc;
					
				}else{
					$model->no_doc = $cekdoc;
				}
            if($model->save()){
				$email_request = $_POST["MasterPrinting"]["email_requestor"];
				$nodoc = $model->no_doc;
				\cakebake\actionlog\model\ActionLog::add('success', $email_request." success request ".$nodoc);						
				$email_valid = $_POST["MasterPrinting"]["email_validator"];
				$validator = $_POST["MasterPrinting"]["validator_name"];
				$status = $_POST["MasterPrinting"]["status"];
				$code = $_POST["MasterPrinting"]["code"];
				$name = $_POST["MasterPrinting"]["description"];						
				$remark = $_POST["MasterPrinting"]["remark"];
				
				$email = explode(',',$email_valid);
				$valid =[];
				foreach($email as $key => $value) {
				   $valid[] = $value;
				}
				$valid[] = $email_request;
				
				Yii::$app->mailer->compose('@app/mail/layouts/validator.php',[
					'request' => $validator,
					'master' => 'Master Printing',
					'status' => $status,
					'code' => $code,
					'name' => $name,
					'nodoc' => $nodoc,
					'remark' => $remark,
					'image' => Yii::getAlias('@app/web/klog.png'),
				])
				 ->setFrom('it.mdm@kamadjaja.com')
				 ->setTo($valid)
				 ->setCc('andra.wisata@kamadjaja.com')
				 ->setSubject('Validation Master Printing ['.$nodoc.']')
				 ->send();
				return $this->goHome();
			}else{
				\Yii::$app->session->setFlash('error','Sorry some error occurred, please contact IT BS Team :)');
				return $this->redirect(['create']);
			}
        } else {
            return $this->render('create', [
                'model' => $model,
				$model->no_doc = $noDoc,
				$model->status = 'validator',
				$model->req_date = $today,
				$model->acknowledges = strtoupper($acknowledges['id']),
				$model->acknowledges_name = strtoupper($acknowledges['username']),
				$model->email_acknowledges = $acknowledges['email'],
				$model->support = strtoupper($support['id']),
				$model->support_name = strtoupper($support['username']),
				$model->email_support = $support['email'],
            ]);
        }
    }

    /**
     * Updates an existing MasterPrinting model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MasterPrinting model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MasterPrinting model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterPrinting the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MasterPrinting::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

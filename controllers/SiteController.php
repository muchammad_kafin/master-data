<?php

namespace app\controllers;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Upload;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use yii\data\ArrayDataProvider;
use app\models\MasterParty;
use app\models\MasterPartySearch;
use app\models\MasterClient;
use app\models\MasterClientSearch;
use app\models\MasterCountry;
use app\models\MasterCountrySearch;
use app\models\MasterDebtor;
use app\models\MasterDebtorSearch;
use app\models\MasterLocation;
use app\models\MasterLocationSearch;
use app\models\MasterRegion;
use app\models\MasterRegionSearch;
use app\models\MasterVendor;
use app\models\MasterVendorSearch;
use app\models\MasterItem;
use app\models\MasterItemSearch;
use app\models\MasterContract;
use app\models\MasterContractSearch;
use app\models\MasterEquipment;
use app\models\MasterEquipmentSearch;
use app\models\MasterProduct;
use app\models\MasterProductSearch;
use app\models\MasterPrinting;
use app\models\MasterPrintingSearch;
use app\models\MasterCharge;
use app\models\MasterChargeSearch;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            // 'access' => [
                // 'class' => AccessControl::className(),
                // 'only' => ['logout','about','email','contact'],
                // 'only' => ['logout'],
                // 'rules' => [
                    // [
                        // 'actions' => ['logout','about','email','contact'],
                        // 'actions' => ['logout'],
                        // 'allow' => true,
                        // 'roles' => ['@'],
                    // ],
                // ],
            // ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
	
	/**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
	
	
    /**
     * Displays dashboard.
     *
     * @return string
     */
    public function actionDashboard($start="",$end="")
    {
			if ($start && $end) {
				$day1 = $start;
				$day2 = $end;
			}else{
				$year = date("Y");
				$month = date("n");
				$last = date("t");
				$day1 = date("Y-m-d",strtotime($year."-".$month."-01"));				
				$day2 = date("Y-m-d",strtotime($year."-".$month."-".$last));
			}
		
		$query1 = (new \yii\db\Query())
			->select(["CONCAT('Master',' ','Charge') as master",	"COUNT(req_date) AS 'created',
						SUM( CASE	WHEN ( status = 'validator' ) THEN 1 ELSE	0	END	) AS 'validator',
						SUM( CASE	WHEN ( status = 'support' ) THEN 1 ELSE	0	END	) AS 'support',
						SUM( CASE	WHEN ( status = 'finish' ) THEN 1 ELSE	0	END	) AS 'finish',
						SUM( CASE	WHEN ( status = 'reject' ) THEN 1 ELSE	0	END	) AS 'reject'"])
			->from('master_charge')->andWhere(['between','req_date',$day1,$day2]);
		
		$query2 = (new \yii\db\Query())
			->select(["CONCAT('Master',' ','Client') as master",	"COUNT(req_date) AS 'created',
						SUM( CASE	WHEN ( status = 'validator' ) THEN 1 ELSE	0	END	) AS 'validator',
						SUM( CASE	WHEN ( status = 'support' ) THEN 1 ELSE	0	END	) AS 'support',
						SUM( CASE	WHEN ( status = 'finish' ) THEN 1 ELSE	0	END	) AS 'finish',
						SUM( CASE	WHEN ( status = 'reject' ) THEN 1 ELSE	0	END	) AS 'reject'"])
			->from('master_client')->andWhere(['between','req_date',$day1,$day2]);

		$query3 = (new \yii\db\Query())
			->select(["CONCAT('Master ','Contract') as master",	"COUNT(req_date) AS 'created',
						SUM( CASE	WHEN ( status = 'validator' ) THEN 1 ELSE	0	END	) AS 'validator',
						SUM( CASE	WHEN ( status = 'support' ) THEN 1 ELSE	0	END	) AS 'support',
						SUM( CASE	WHEN ( status = 'finish' ) THEN 1 ELSE	0	END	) AS 'finish',
						SUM( CASE	WHEN ( status = 'reject' ) THEN 1 ELSE	0	END	) AS 'reject'"])
			->from('master_contract')->andWhere(['between','req_date',$day1,$day2]);
			
		$query4 = (new \yii\db\Query())
			->select(["CONCAT('Master ','Country') as master",	"COUNT(req_date) AS 'created',
						SUM( CASE	WHEN ( status = 'validator' ) THEN 1 ELSE	0	END	) AS 'validator',
						SUM( CASE	WHEN ( status = 'support' ) THEN 1 ELSE	0	END	) AS 'support',
						SUM( CASE	WHEN ( status = 'finish' ) THEN 1 ELSE	0	END	) AS 'finish',
						SUM( CASE	WHEN ( status = 'reject' ) THEN 1 ELSE	0	END	) AS 'reject'"])
			->from('master_country')->andWhere(['between','req_date',$day1,$day2]);
		
		$query5 = (new \yii\db\Query())
			->select(["CONCAT('Master ','Debtor') as master",	"COUNT(req_date) AS 'created',
						SUM( CASE	WHEN ( status = 'validator' ) THEN 1 ELSE	0	END	) AS 'validator',
						SUM( CASE	WHEN ( status = 'support' ) THEN 1 ELSE	0	END	) AS 'support',
						SUM( CASE	WHEN ( status = 'finish' ) THEN 1 ELSE	0	END	) AS 'finish',
						SUM( CASE	WHEN ( status = 'reject' ) THEN 1 ELSE	0	END	) AS 'reject'"])
			->from('master_debtor')->andWhere(['between','req_date',$day1,$day2]);
		
		$query6 = (new \yii\db\Query())
			->select(["CONCAT('Master ','Equipment') as master",	"COUNT(req_date) AS 'created',
						SUM( CASE	WHEN ( status = 'validator' ) THEN 1 ELSE	0	END	) AS 'validator',
						SUM( CASE	WHEN ( status = 'support' ) THEN 1 ELSE	0	END	) AS 'support',
						SUM( CASE	WHEN ( status = 'finish' ) THEN 1 ELSE	0	END	) AS 'finish',
						SUM( CASE	WHEN ( status = 'reject' ) THEN 1 ELSE	0	END	) AS 'reject'"])
			->from('master_equipment')->andWhere(['between','req_date',$day1,$day2]);
			
		$query7 = (new \yii\db\Query())
			->select(["CONCAT('Master ','Item') as master",	"COUNT(req_date) AS 'created',
						SUM( CASE	WHEN ( status = 'validator' ) THEN 1 ELSE	0	END	) AS 'validator',
						SUM( CASE	WHEN ( status = 'support' ) THEN 1 ELSE	0	END	) AS 'support',
						SUM( CASE	WHEN ( status = 'finish' ) THEN 1 ELSE	0	END	) AS 'finish',
						SUM( CASE	WHEN ( status = 'reject' ) THEN 1 ELSE	0	END	) AS 'reject'"])
			->from('master_item')->andWhere(['between','req_date',$day1,$day2]);
			
		$query8 = (new \yii\db\Query())
			->select(["CONCAT('Master ','Location') as master",	"COUNT(req_date) AS 'created',
						SUM( CASE	WHEN ( status = 'validator' ) THEN 1 ELSE	0	END	) AS 'validator',
						SUM( CASE	WHEN ( status = 'support' ) THEN 1 ELSE	0	END	) AS 'support',
						SUM( CASE	WHEN ( status = 'finish' ) THEN 1 ELSE	0	END	) AS 'finish',
						SUM( CASE	WHEN ( status = 'reject' ) THEN 1 ELSE	0	END	) AS 'reject'"])
			->from('master_location')->andWhere(['between','req_date',$day1,$day2]);
			
		$query9 = (new \yii\db\Query())
			->select(["CONCAT('Master ','Party') as master",	"COUNT(req_date) AS 'created',
						SUM( CASE	WHEN ( status = 'validator' ) THEN 1 ELSE	0	END	) AS 'validator',
						SUM( CASE	WHEN ( status = 'support' ) THEN 1 ELSE	0	END	) AS 'support',
						SUM( CASE	WHEN ( status = 'finish' ) THEN 1 ELSE	0	END	) AS 'finish',
						SUM( CASE	WHEN ( status = 'reject' ) THEN 1 ELSE	0	END	) AS 'reject'"])
			->from('master_party')->andWhere(['between','req_date',$day1,$day2]);
			
		$query10 = (new \yii\db\Query())
			->select(["CONCAT('Master ','Printing') as master",	"COUNT(req_date) AS 'created',
						SUM( CASE	WHEN ( status = 'validator' ) THEN 1 ELSE	0	END	) AS 'validator',
						SUM( CASE	WHEN ( status = 'support' ) THEN 1 ELSE	0	END	) AS 'support',
						SUM( CASE	WHEN ( status = 'finish' ) THEN 1 ELSE	0	END	) AS 'finish',
						SUM( CASE	WHEN ( status = 'reject' ) THEN 1 ELSE	0	END	) AS 'reject'"])
			->from('master_printing')->andWhere(['between','req_date',$day1,$day2]);
			
		$query11 = (new \yii\db\Query())
			->select(["CONCAT('Master ','Product') as master",	"COUNT(req_date) AS 'created',
						SUM( CASE	WHEN ( status = 'validator' ) THEN 1 ELSE	0	END	) AS 'validator',
						SUM( CASE	WHEN ( status = 'support' ) THEN 1 ELSE	0	END	) AS 'support',
						SUM( CASE	WHEN ( status = 'finish' ) THEN 1 ELSE	0	END	) AS 'finish',
						SUM( CASE	WHEN ( status = 'reject' ) THEN 1 ELSE	0	END	) AS 'reject'"])
			->from('master_product')->andWhere(['between','req_date',$day1,$day2]);
			
		$query12 = (new \yii\db\Query())
			->select(["CONCAT('Master ','Region') as master",	"COUNT(req_date) AS 'created',
						SUM( CASE	WHEN ( status = 'validator' ) THEN 1 ELSE	0	END	) AS 'validator',
						SUM( CASE	WHEN ( status = 'support' ) THEN 1 ELSE	0	END	) AS 'support',
						SUM( CASE	WHEN ( status = 'finish' ) THEN 1 ELSE	0	END	) AS 'finish',
						SUM( CASE	WHEN ( status = 'reject' ) THEN 1 ELSE	0	END	) AS 'reject'"])
			->from('master_region')->andWhere(['between','req_date',$day1,$day2]);
		
		$query13 = (new \yii\db\Query())
			->select(["CONCAT('Master ','Vendor') as master",	"COUNT(req_date) AS 'created',
						SUM( CASE	WHEN ( status = 'validator' ) THEN 1 ELSE	0	END	) AS 'validator',
						SUM( CASE	WHEN ( status = 'support' ) THEN 1 ELSE	0	END	) AS 'support',
						SUM( CASE	WHEN ( status = 'finish' ) THEN 1 ELSE	0	END	) AS 'finish',
						SUM( CASE	WHEN ( status = 'reject' ) THEN 1 ELSE	0	END	) AS 'reject'"])
			->from('master_vendor')->andWhere(['between','req_date',$day1,$day2]);

		$unionQuery = (new \yii\db\Query())
			->from(['dash' => $query1->union($query2)->union($query3)->union($query4)->union($query5)->union($query6)->union($query7)->union($query8)->union($query9)->union($query10)->union($query11)->union($query12)->union($query13)]);

		$provider = new ActiveDataProvider([
			'query' => $unionQuery,
			'pagination' => [
				'pageSize' => 5,
			],
		]);

		$rows = $provider->getModels();
		
		//-------update request data provider-----------
		
		$querya = (new \yii\db\Query())
			->select(["CONCAT('Master',' ',b.`name`) AS 'master'",
						"COUNT(req_date) as 'created',
						SUM(CASE WHEN ( a.`status` = 'validator') THEN 1 ELSE 0 END) as 'validator',
						SUM(CASE WHEN ( a.`status` = 'support') THEN 1 ELSE 0 END) as 'support',
						SUM(CASE WHEN ( a.`status` = 'finish') THEN 1 ELSE 0 END) as 'finish',
						SUM(CASE WHEN ( a.`status` = 'reject') THEN 1 ELSE 0 END) as 'reject'"])
			->from('update_form a')->join('inner join','master_list b','b.name =a.master')->where(['a.master'=>'CLIENT'])
			->andWhere(['between','a.req_date',$day1,$day2]);
			
		$queryb = (new \yii\db\Query())
			->select(["CONCAT('Master',' ',b.`name`) AS 'master'",
						"COUNT(req_date) as 'created',
						SUM(CASE WHEN ( a.`status` = 'validator') THEN 1 ELSE 0 END) as 'validator',
						SUM(CASE WHEN ( a.`status` = 'support') THEN 1 ELSE 0 END) as 'support',
						SUM(CASE WHEN ( a.`status` = 'finish') THEN 1 ELSE 0 END) as 'finish',
						SUM(CASE WHEN ( a.`status` = 'reject') THEN 1 ELSE 0 END) as 'reject'"])
			->from('update_form a')->join('inner join','master_list b','b.name =a.master')->where(['a.master'=>'DEBTOR'])
			->andWhere(['between','a.req_date',$day1,$day2]);
			
		$queryc = (new \yii\db\Query())
			->select(["CONCAT('Master',' ',b.`name`) AS 'master'",
						"COUNT(req_date) as 'created',
						SUM(CASE WHEN ( a.`status` = 'validator') THEN 1 ELSE 0 END) as 'validator',
						SUM(CASE WHEN ( a.`status` = 'support') THEN 1 ELSE 0 END) as 'support',
						SUM(CASE WHEN ( a.`status` = 'finish') THEN 1 ELSE 0 END) as 'finish',
						SUM(CASE WHEN ( a.`status` = 'reject') THEN 1 ELSE 0 END) as 'reject'"])
			->from('update_form a')->join('inner join','master_list b','b.name =a.master')->where(['a.master'=>'PARTY'])
			->andWhere(['between','a.req_date',$day1,$day2]);
		
		$queryd = (new \yii\db\Query())
			->select(["CONCAT('Master',' ',b.`name`) AS 'master'",
						"COUNT(req_date) as 'created',
						SUM(CASE WHEN ( a.`status` = 'validator') THEN 1 ELSE 0 END) as 'validator',
						SUM(CASE WHEN ( a.`status` = 'support') THEN 1 ELSE 0 END) as 'support',
						SUM(CASE WHEN ( a.`status` = 'finish') THEN 1 ELSE 0 END) as 'finish',
						SUM(CASE WHEN ( a.`status` = 'reject') THEN 1 ELSE 0 END) as 'reject'"])
			->from('update_form a')->join('inner join','master_list b','b.name =a.master')->where(['a.master'=>'VENDOR'])
			->andWhere(['between','a.req_date',$day1,$day2]);

		$unionQuerya = (new \yii\db\Query())
			->from(['dasha' => $querya->union($queryb)->union($queryc)->union($queryd)]);

		$providera = new ActiveDataProvider([
			'query' => $unionQuerya,
			'pagination' => [
				'pageSize' => 5,
			],
		]);

		$rowsa = $providera->getModels();
		
        return $this->render('dashboard',[
			'dataProvider' => $provider,
			'updateProvider' => $providera,
			'start' => $day1,
			'end' => $day2,
		]);
    }
	
	public function actionSearch($q='',$master='')
    {	
		if($q == '' && $master == ''){
			$searchData = (new \yii\db\Query())
			->from(['master_client'])
			->orWhere(['like','master_client.requestor',$q])
			->orWhere(['like','master_client.name',$q])
			->all();
			$master = "client";
		}else{
			if($master == "client" ){
				$masters = new MasterClient();
			}elseif($master == "contract"){
				$masters = new MasterContract();
			}elseif($master == "country"){
				$masters = new MasterCountry();
			}elseif($master == "debtor"){
				$masters = new MasterDebtor();
			}elseif($master == "equipment"){
				$masters = new MasterEquipment();
			}elseif($master == "item"){
				$masters = new MasterItem();
			}elseif($master == "location"){
				$masters = new MasterLocation();
			}elseif($master == "party"){
				$masters = new MasterParty();
			}elseif($master == "printing"){
				$masters = new MasterPrinting();
			}elseif($master == "product"){
				$masters = new MasterProduct();
			}elseif($master == "region"){
				$masters = new MasterRegion();
			}elseif($master == "vendor"){
				$masters = new MasterVendor();
			}elseif($master == "charge"){
				$masters = new MasterCharge();
			}
			
			$models = $masters->attributes();
			$data = (new \yii\db\Query())->from('master_'.$master);
			foreach( $models as $key => $value){
				$data->orWhere(['like',$models[$key],$q]);
			}
			$searchData= $data->all();
			
		}
        
        //$searchData = $search->find($q, ['model' => 'page']); // Search by index provided only by model `page`.
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $searchData,
            'pagination' => ['pageSize' => 10],
        ]);
        return $this->render(
            'search',
            [
                'hits' => $dataProvider->getModels(),
                'pagination' => $dataProvider->getPagination(),
                'query' => $searchData,
				'q' => $q,
				'master' => $master,
            ]
        );
    }
	
	/**
     * Displays homepage.
     *
     * @return string
     */
    public function actionDownload($file,$path)
    {
       return \Yii::$app->response->sendFile($path.''.$file);
    }
	
	/**
     * Displays homepage.
     *
     * @return string
     */
    public function actionEmail()
    {
		Yii::$app->mailer->compose('@app/mail/layouts/finish.php',[
				'request' => 'Test',
				'master' => 'Test',
				'status' => 'Test',
				'code' => 'Test',
				'name' => 'Test',
				'nodoc' => 'Test',
				'remark' => 'Test',
				'image' => Yii::getAlias('@app/web/klog.png'),
			])
			 ->setFrom('it.mdm@kamadjaja.com')
			 ->setTo('it.bs@kamadjaja.com')
			 ->setCc('it.mdm@kamadjaja.com')
			 ->setSubject('(Test Email)')
			 ->send();
        return $this->goHome();
    }
	
    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }
	
	/**
     * Displays contact page.
     *
     * @return string
     */
    public function actionUpload($id='')
    {
        $model = new Upload();
		 if ($model->load(Yii::$app->request->post())) {
			$file = UploadedFile::getInstances($model, 'getfile');
			if ($file !== false) {
			   foreach ($file as $files) {
					$tmp = explode('.', $files->extension);
					$ext = end($tmp);
					$upload = new Upload();
					$upload->filename = $files->baseName . '.' . $files->extension;
					$upload->stored_file = Yii::$app->security->generateRandomString().".{$ext}";
					$upload->filepath = Yii::$app->basePath . '/web/uploads/';
					$upload->id_form = $_POST["Upload"]["id_form"];
					$upload->save(false);
					$files->saveAs(Yii::$app->basePath . '/web/uploads/' . $upload->stored_file);
				}
			}
			else {
				var_dump ($upload->getErrors()); die();
			}
			return $this->render('upload', [
				'model' => $model,
				$model->id_form = "",
				
			]);
		 }
		 else{
			 return $this->render('upload', [
				'model' => $model,
				$model->id_form = $id,
			]);
		 }
        
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
	
	public function actionState() {
		$out = [];
		if (isset($_POST['depdrop_parents'])) {
			$parents = $_POST['depdrop_parents'];
			if ($parents != null) {
				$cat_id = $parents[0];
				$out = \app\models\Country::getState($cat_id); 
				// the getSubCatList function will query the database based on the
				// cat_id and return an array like below:
				// [
				//    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
				//    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
				// ]
				echo Json::encode(['output'=>$out, 'selected'=>'']);
				return;
			}
		}
		echo Json::encode(['output'=>'', 'selected'=>'']);
	}
	
	public function actionCity() {
		$out = [];
		if (isset($_POST['depdrop_parents'])) {
			$ids = $_POST['depdrop_parents'];
			$cat_id = empty($ids[0]) ? null : $ids[0];
			$subcat_id = empty($ids[1]) ? null : $ids[1];
			if ($cat_id != null) {
			   $data = \app\models\Country::getCity($cat_id, $subcat_id);
				/**
				 * the getProdList function will query the database based on the
				 * cat_id and sub_cat_id and return an array like below:
				 *  [
				 *      'out'=>[
				 *          ['id'=>'<prod-id-1>', 'name'=>'<prod-name1>'],
				 *          ['id'=>'<prod_id_2>', 'name'=>'<prod-name2>']
				 *       ],
				 *       'selected'=>'<prod-id-1>'
				 *  ]
				 */
			   
			   echo Json::encode(['output'=>$data['output'], 'selected'=>$data['selected']]);
			   return;
			}
		}
		echo Json::encode(['output'=>'', 'selected'=>'']);
	}
	
	public function actionVendor() {
		$out = [];
		if (isset($_POST['depdrop_parents'])) {
			$parents = $_POST['depdrop_parents'];
			if ($parents != null) {
				$cat_id = $parents[0];
				$out = \app\models\Vendor::getVendor($cat_id); 
				// the getSubCatList function will query the database based on the
				// cat_id and return an array like below:
				// [
				//    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
				//    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
				// ]
				echo Json::encode(['output'=>$out, 'selected'=>'']);
				return;
			}
		}
		echo Json::encode(['output'=>'', 'selected'=>'']);
	}
	
	public function actionValidator($requestor,$role,$master)
    {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$posts = array();
        $posts = \app\models\User::getValid($requestor,$role,$master);
		
        if($posts){
			return $posts;
        }
        else{
			return $post = ['id'=>'0','username'=>"Need Assign user",'email'=>"Need Assign user"];
        }
 
    }
	
	public function actionVendor1($code) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$out = array();
		$out = \app\models\Vendor::getVendor1($code);
		return $out;
	}
	
	public function actionGetstate($term,$country){
		$state = \app\models\Region::find()->where(['country' => $country])
		->andWhere(['like','name',$term])
		->select('name as value, code as id')->asArray()->all();
		return Json::Encode($state);
	}
	
	public function actionGetcity($term,$country,$state){
		$city = \app\models\City::find()->where(['country' => $country])
		->andWhere(['region' => $state])->andWhere(['like','name',$term])
		->select('name as value, id')->asArray()->all();
		return Json::Encode($city);
	}
}

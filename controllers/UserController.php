<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\Signup;
use app\models\ChangePassword;
use app\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
	
	/**
     * Signup new user
     * @return string
     */
    public function actionSignup()
    {
        $model = new Signup();
        $user = new User();
        if ($model->load(Yii::$app->getRequest()->post())) {
            if ($user = $model->signup()) {
               return $this->redirect(['index']);
            }
        }

        return $this->render('signup', [
                'model' => $model,
				'user' => $user,
        ]);
    }
	
	/**
     * Reset password
     * @return string
     */
    public function actionResetpassword($id)
    {
		
       $model = $this->findModel($id);
		$password = "password.99";
		$model->password_hash = $password;
        if ($model->password_hash && $model->save()) {
			Yii::$app->getSession()->setFlash(
                            'success','Password reset '.$model->username
                        );
            return $this->redirect(['index']);
        } else {
			Yii::$app->getSession()->setFlash(
                            'error','Password not changed'
                        );
            return $this->redirect(['index']);
        }
    }
	
    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

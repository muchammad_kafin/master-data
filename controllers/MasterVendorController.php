<?php

namespace app\controllers;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;
use app\models\MasterVendor;
use app\models\User;
use app\models\Model;
use app\models\Contact;
use app\models\Phone;
use app\models\MasterVendorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Upload;
use yii\web\UploadedFile;

/**
 * MasterVendorController implements the CRUD actions for MasterVendor model.
 */
class MasterVendorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterVendor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MasterVendorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MasterVendor model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MasterVendor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MasterVendor();
        $modelsContact = [new Contact()];
        $modelsPhone = [[new Phone()]];
		// $validator = User::getValidator('validator','vendor');
		$acknowledges = User::getValidator('acknowledges','vendor');
		$support = User::getValidator('support','vendor');
		$count = MasterVendor::find()->count();
		$nextCount = $count+1;
		$today = date("Y-m-d");
		if ($nextCount < 10 ){
			$noDoc = 'FM-VND-0'.$nextCount;
		}else{
			$noDoc = 'FM-VND-'.$nextCount;
		}

        if ($model->load(Yii::$app->request->post())) {
			$modelsContact = Model::createMultiple(Contact::classname());
			Model::loadMultiple($modelsContact, Yii::$app->request->post());
	 
			// validate person and houses models
			$valid = $model->validate();
			$valid = Model::validateMultiple($modelsContact) && $valid;
			
			if (isset($_POST['Phone'][0][0])) {
				foreach ($_POST['Phone'] as $indexHouse => $rooms) {
					foreach ($rooms as $indexRoom => $room) {
						$data['Phone'] = $room;
						$modelPhone = new Phone();
						$modelPhone->load($data);
						$modelsPhone[$indexHouse][$indexRoom] = $modelPhone;
						$valid = $modelPhone->validate();
						//die(var_dump($modelRoom->errors));
					}
				}
			}
			
			if ($valid) {
				$cekdoc = $_POST["MasterVendor"]["no_doc"];
				$existdoc = MasterVendor::find()->where(['no_doc'=>$cekdoc])->one();
				if($existdoc){
					$count = MasterVendor::find()->count();
					$nextCount = $count+1;
					$today = date("Y-m-d");
					if ($nextCount < 10 ){
						$noDoc = 'FM-VND-0'.$nextCount;
					}else{
						$noDoc = 'FM-VND-'.$nextCount;
					}
					$model->no_doc = $noDoc;
					
				}else{
					$model->no_doc = $cekdoc;
				}
				$transaction = Yii::$app->db->beginTransaction();
					try {
						if ($flag = $model->save(false)) {
							foreach ($modelsContact as $indexHouse => $modelContact) {
		 
								if ($flag === false) {
									break;
								}
		 
								$modelContact->id_vendor = $model->id;
		 
								if (!($flag = $modelContact->save(false))) {
									break;
								}
		 
								if (isset($modelsPhone[$indexHouse]) && is_array($modelsPhone[$indexHouse])) {
									foreach ($modelsPhone[$indexHouse] as $indexRoom => $modelPhone) {
										$modelPhone->id_contact = $modelContact->id;
										if (!($flag = $modelPhone->save(false))) {
											break;
										}
									}
								}
							}
							$file = UploadedFile::getInstances($model, 'getfile');
							if ($file !== false) {
							   foreach ($file as $files) {
									$tmp = explode('.', $files->extension);
									$ext = end($tmp);
									$upload = new Upload();
									$upload->filename = $files->baseName . '.' . $files->extension;
									$upload->stored_file = Yii::$app->security->generateRandomString().".{$ext}";
									$upload->filepath = Yii::$app->basePath . '/web/uploads/';
									$upload->id_form = $model->no_doc;
									$upload->save(false);
									$files->saveAs(Yii::$app->basePath . '/web/uploads/' . $upload->stored_file);
								}
							}
							else {
								var_dump ($upload->getErrors()); die();
							}
						}
	 
						if ($flag) {
							$transaction->commit();
							$email_request = $_POST["MasterVendor"]["email_requestor"];
							$nodoc = $model->no_doc;
							\cakebake\actionlog\model\ActionLog::add('success', $email_request." success request ".$nodoc);						
							$email_valid = $_POST["MasterVendor"]["email_validator"];
							$validator = $_POST["MasterVendor"]["validator_name"];
							$status = $_POST["MasterVendor"]["status"];
							$code = $_POST["MasterVendor"]["code"];
							$name = $_POST["MasterVendor"]["name"];						
							$remark = $_POST["MasterVendor"]["remark"];
							
							$email = explode(',',$email_valid);
							$valid =[];
							foreach($email as $key => $value) {
							   $valid[] = $value;
							}
							$valid[] = $email_request;
							
							Yii::$app->mailer->compose('@app/mail/layouts/validator.php',[
								'request' => $validator,
								'master' => 'Master Vendor',
								'status' => $status,
								'code' => $code,
								'name' => $name,
								'nodoc' => $nodoc,
								'remark' => $remark,
								'image' => Yii::getAlias('@app/web/klog.png'),
							])
							 ->setFrom('it.mdm@kamadjaja.com')
							 ->setTo($valid)
							 ->setCc('andra.wisata@kamadjaja.com')
							 ->setSubject('Validation Master Vendor ['.$nodoc.']')
							 ->send();
							 
							return $this->goHome();
							} else {
								$transaction->rollBack();
								\Yii::$app->session->setFlash('error','Sorry some error occurred, please contact IT BS Team :)');
								return $this->redirect(['create']);
							}
						} catch (Exception $e) {
							$transaction->rollBack();
							\Yii::$app->session->setFlash('error','Sorry some error occurred, please contact IT BS Team :)');
							return $this->redirect(['create']);
						}
				
			}
        } else {
            return $this->render('create', [
                'model' => $model,
                'modelsContact' => (empty($modelsContact)) ? [new ContactVendor()] : $modelsContact,
                'modelsPhone' => (empty($modelsPhone)) ? [new PhoneVendor()] : $modelsPhone,
				$model->no_doc = $noDoc,
				$model->status = 'validator',
				$model->req_date = $today,
				// $model->validator = strtoupper($validator['id']),
				// $model->validator_name = strtoupper($validator['username']),
				// $model->email_validator = $validator['email'],
				$model->acknowledges = strtoupper($acknowledges['id']),
				$model->acknowledges_name = strtoupper($acknowledges['username']),
				$model->email_acknowledges = $acknowledges['email'],
				$model->support = strtoupper($support['id']),
				$model->support_name = strtoupper($support['username']),
				$model->email_support = $support['email'],
				$model->address_2 = '-',
				$model->address_3 = '-',
				$model->address_4 = '-',
				$model->code = 'S',
            ]);
        }
    }

    /**
     * Updates an existing MasterVendor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MasterVendor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MasterVendor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterVendor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MasterVendor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

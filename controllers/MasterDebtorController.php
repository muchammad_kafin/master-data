<?php

namespace app\controllers;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;
use app\models\MasterDebtor;
use app\models\MasterDebtorSearch;
use app\models\MasterClient;
use app\models\MasterClientSearch;
use app\models\MasterParty;
use app\models\MasterPartySearch;
use yii\web\Controller;
use app\models\User;
use app\models\Contact;
use app\models\Phone;
use app\models\Model;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Upload;
use yii\web\UploadedFile;

/**
 * MasterDebtorController implements the CRUD actions for MasterDebtor model.
 */
class MasterDebtorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterDebtor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MasterDebtorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MasterDebtor model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MasterDebtor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MasterDebtor();
		$modelsContact = [new Contact()];
        $modelsPhone = [[new Phone()]];
		// $validator = User::getValidator('validator','debtor');
		$acknowledges = User::getValidator('acknowledges','debtor');
		$support = User::getValidator('support','debtor');
		$count = MasterDebtor::find()->count();
		$nextCount = $count+1;
		$today = date("Y-m-d");
		if ($nextCount < 10 ){
			$noDoc = 'FM-DBT-0'.$nextCount;
		}else{
			$noDoc = 'FM-DBT-'.$nextCount;
		}
		
        if ($model->load(Yii::$app->request->post())) {
			$modelsContact = Model::createMultiple(Contact::classname());
			Model::loadMultiple($modelsContact, Yii::$app->request->post());
	 
			// validate person and houses models
			$valid = $model->validate();
			$valid = Model::validateMultiple($modelsContact) && $valid;
	 
			if (isset($_POST['Phone'][0][0])) {
				foreach ($_POST['Phone'] as $indexHouse => $rooms) {
					foreach ($rooms as $indexRoom => $room) {
						$data['Phone'] = $room;
						$modelPhone = new Phone();
						$modelPhone->load($data);
						$modelsPhone[$indexHouse][$indexRoom] = $modelPhone;
						$valid = $modelPhone->validate();
						//die(var_dump($modelRoom->errors));
					}
				}
			}
			if ($valid) {
				$cekdoc = $_POST["MasterDebtor"]["no_doc"];
				$existdoc = MasterDebtor::find()->where(['no_doc'=>$cekdoc])->one();
				if($existdoc){
					$count = MasterDebtor::find()->count();
					$nextCount = $count+1;
					$today = date("Y-m-d");
					if ($nextCount < 10 ){
						$noDoc = 'FM-DBT-0'.$nextCount;
					}else{
						$noDoc = 'FM-DBT-'.$nextCount;
					}
					$model->no_doc = $noDoc;
					
				}else{
					$model->no_doc = $cekdoc;
				}
					$transaction = Yii::$app->db->beginTransaction();
					try {
							$model->office_code = implode(",",$_POST["MasterDebtor"]["office_code"]);
						if ($flag = $model->save(false)) {
							
							foreach ($modelsContact as $indexHouse => $modelContact) {
		 
								if ($flag === false) {
									break;
								}
		 
								$modelContact->id_debtor = $model->id;
		 
								if (!($flag = $modelContact->save(false))) {
									break;
								}
		 
								if (isset($modelsPhone[$indexHouse]) && is_array($modelsPhone[$indexHouse])) {
									foreach ($modelsPhone[$indexHouse] as $indexRoom => $modelPhone) {
										$modelPhone->id_contact = $modelContact->id;
										if (!($flag = $modelPhone->save(false))) {
											break;
										}
									}
								}
							}
							$file = UploadedFile::getInstances($model, 'getfile');
							if ($file !== false) {
							   foreach ($file as $files) {
									$tmp = explode('.', $files->extension);
									$ext = end($tmp);
									$upload = new Upload();
									$upload->filename = $files->baseName . '.' . $files->extension;
									$upload->stored_file = Yii::$app->security->generateRandomString().".{$ext}";
									$upload->filepath = Yii::$app->basePath . '/web/uploads/';
									$upload->id_form = $model->no_doc;
									$upload->save(false);
									$files->saveAs(Yii::$app->basePath . '/web/uploads/' . $upload->stored_file);
								}
							}
							else {
								var_dump ($upload->getErrors()); die();
							}
						}
		 
						if ($flag) {
							$transaction->commit();
							$email_request = $_POST["MasterDebtor"]["email_requestor"];
							$nodoc = $model->no_doc;
							\cakebake\actionlog\model\ActionLog::add('success', $email_request." success request ".$nodoc);						
							$email_valid = $_POST["MasterDebtor"]["email_validator"];
							$validator = $_POST["MasterDebtor"]["validator_name"];
							$status = $_POST["MasterDebtor"]["status"];
							$code = $_POST["MasterDebtor"]["code"];
							$name = $_POST["MasterDebtor"]["name"];						
							$remark = $_POST["MasterDebtor"]["remark"];
							
							$email = explode(',',$email_valid);
							$valid =[];
							foreach($email as $key => $value) {
							   $valid[] = $value;
							}
							$valid[] = $email_request;
							
							// $this->createClient($nodoc);
							// $this->createParty($nodoc);
							
							Yii::$app->mailer->compose('@app/mail/layouts/validator.php',[
								'request' => $validator,
								'master' => 'Master Debtor',
								'status' => $status,
								'code' => $code,
								'name' => $name,
								'nodoc' => $nodoc,
								'remark' => $remark,
								'image' => Yii::getAlias('@app/web/klog.png'),
							])
							 ->setFrom('it.mdm@kamadjaja.com')
							 ->setTo($valid)
							 ->setCc('andra.wisata@kamadjaja.com')
							 ->setSubject('Validation Master Debtor ['.$nodoc.']')
							 ->send();
							 
							return $this->goHome();
						} else {
							$transaction->rollBack();
							\Yii::$app->session->setFlash('error','Sorry some error occurred, please contact IT BS Team :)');
							return $this->redirect(['create']);
						}
					} catch (Exception $e) {
						$transaction->rollBack();
						\Yii::$app->session->setFlash('error','Sorry some error occurred, please contact IT BS Team :)');
						return $this->redirect(['create']);
					}
				}
        } else {
            return $this->render('create', [
                'model' => $model,
                'modelsContact' => $modelsContact,
                'modelsPhone' => $modelsPhone,
				$model->no_doc = $noDoc,
				$model->status = 'validator',
				$model->req_date = $today,
				// $model->validator = strtoupper($validator['id']),
				// $model->validator_name = strtoupper($validator['username']),
				// $model->email_validator = $validator['email'],
				$model->acknowledges = strtoupper($acknowledges['id']),
				$model->acknowledges_name = strtoupper($acknowledges['username']),
				$model->email_acknowledges = $acknowledges['email'],
				$model->support = strtoupper($support['id']),
				$model->support_name = strtoupper($support['username']),
				$model->email_support = $support['email'],
				$model->address_2 = '-',
				$model->address_3 = '-',
				$model->address_4 = '-',
				$model->code = 'C',
            ]);
        }
    }
	
	protected function createClient($debtor)
    {
		$model = MasterDebtor::find()->where(['no_doc'=>$debtor])->one();
		$modelNew = new MasterClient();
		$code = substr_replace($model->code,"",11);
		$client = MasterClient::find()->where(['code'=>$code])->one();
		$count = MasterClient::find()->count();
		$nextCount = $count+1;
		$today = date("Y-m-d");
		if ($nextCount < 10 ){
			$noDoc = 'FM-CLI-0'.$nextCount;
		}else{
			$noDoc = 'FM-CLI-'.$nextCount;
		}		
		if($client){
				\cakebake\actionlog\model\ActionLog::add('error', 'Client '.$client->code.' already exists');
				// return $this->redirect(['validator/debtor']);
		}else{
				$code = substr_replace($model->code,"",11);
				$code_party = str_replace('C.', 'P.', substr($code,0,11));
				$modelNew->no_doc = $noDoc;
				$modelNew->req_date = $today;
				$modelNew->code = $code;
				$modelNew->requestor = $model->requestor;
				$modelNew->status = "validator";
				$modelNew->email_requestor = $model->email_requestor;
					$validator = \app\models\User::getValid($model->email_requestor,"validator","client");
				$modelNew->validator = $validator["id"];
				$modelNew->email_validator = $validator["email"];
				$modelNew->acknowledges = $model->acknowledges;
				$modelNew->email_acknowledges = $model->email_acknowledges;
				$modelNew->support = $model->support;
				$modelNew->email_support = $model->email_support;
				$modelNew->support_date = "";
				$modelNew->remark = "";
				$modelNew->remark_cancel = "";
				$modelNew->name = $model->name;
				$modelNew->address_1 = $model->address_1;
				$modelNew->address_2 = $model->address_2;
				$modelNew->country = $model->country;
				$modelNew->state = $model->state;
				$modelNew->city = $model->city;
				$modelNew->zip_code = $model->zip_code;
				$modelNew->email = $model->email;
				$modelNew->web = "-";
				$modelNew->base_currency = $model->billing_currency;
				
			if ($modelNew->save(false)) {
					$modelsContact = Contact::find()->where(['id_debtor'=>$model->id])->all();
					foreach ($modelsContact as $contact) {
							$modelContact = NEW Contact();
							$modelContact->id_client = $modelNew->id;
							$modelContact->firstname = $contact->firstname;
							$modelContact->lastname = $contact->lastname;
							if($modelContact->save(false)){
								$modelsPhone = Phone::find()->where(['id_contact'=>$contact->id])->all();
								foreach ($modelsPhone as $phone) {
										$modelPhone = NEW Phone();
										$modelPhone->id_contact = $modelContact->id;
										$modelPhone->phone_type = $phone->phone_type;
										$modelPhone->phone_number = $phone->phone_number;							
										$modelPhone->save(false);
								}
							}
					}
				 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success create ".$modelNew->no_doc);
				$emailv = explode(',',$validator["email"]);
				$emaila = explode(',',$model->email_acknowledges);
				$valid =[];
				$ackno =[];
				foreach($emailv as $key => $valuev) {
				   $valid[] = $valuev;
				}
				foreach($emaila as $key => $valuea) {
				   $ackno[] = $valuea;
				}
				$ackno[]="andra.wisata@kamadjaja.com";
				Yii::$app->mailer->compose('@app/mail/layouts/validator.php',[
					'request' => $modelNew->requestor,
					'master' => 'Master Client',
					'status' => "validator",
					'code' => $modelNew->code,
					'name' => $modelNew->name,
					'nodoc' => $modelNew->no_doc,
					'remark' => $modelNew->remark,
					'image' => Yii::getAlias('@app/web/klog.png'),
				])
				 ->setFrom('it.mdm@kamadjaja.com')
				 ->setTo($model->email_requestor)
				 ->setCc(array_merge($valid,$ackno))
				 ->setSubject('Validation Master Client')
				 ->send();
				 
				// return $this->redirect(['validator/debtor']);
			}else{
				Yii::$app->session->setFlash('error', 'Error On create Client');
				// return $this->redirect(['validator/debtor']);
			}
		}
    }
	
	protected function createParty($debtor)
    {
		$model = MasterDebtor::find()->where(['no_doc'=>$debtor])->one();
		$modelNew = new MasterParty();
		$code = substr_replace($model->code,"",11);
		$code_party = str_replace('C.', 'P.', substr($code,0,11));
		$party = MasterParty::find()->where(['code'=>$code_party])->one();
		$count = MasterParty::find()->count();
		$nextCount = $count+1;
		$today = date("Y-m-d");
		if ($nextCount < 10 ){
			$noDoc = 'FM-PRT-0'.$nextCount;
		}else{
			$noDoc = 'FM-PRT-'.$nextCount;
		}		
		if($party){
				\cakebake\actionlog\model\ActionLog::add('error', 'Party '.$party->code.' already exists');
				// return $this->redirect(['validator/debtor']);
		}else{
				$code = substr_replace($model->code,"",11);
				$code_party = str_replace('C.', 'P.', substr($code,0,11));
				$modelNew->no_doc = $noDoc;
				$modelNew->req_date = $today;
				$modelNew->code = $code_party;
				$modelNew->requestor = $model->requestor;
				$modelNew->status = "validator";
				$modelNew->email_requestor = $model->email_requestor;
					$validator = \app\models\User::getValid($model->email_requestor,"validator","party");
				$modelNew->validator = $validator["id"];
				$modelNew->email_validator = $validator["email"];
				$modelNew->acknowledges = $model->acknowledges;
				$modelNew->email_acknowledges = $model->email_acknowledges;
				$modelNew->support = $model->support;
				$modelNew->email_support = $model->email_support;
				$modelNew->support_date = "";
				$modelNew->remark = "";
				$modelNew->remark_cancel = "";
				$modelNew->client = $model->name;
				$modelNew->name = $model->name;
				$modelNew->office = $model->address_type;
				$modelNew->address_1 = $model->address_1;
				$modelNew->address_2 = $model->address_2;
				$modelNew->country = $model->country;
				$modelNew->state = $model->state;
				$modelNew->city = $model->city;
				$modelNew->zip_code = $model->zip_code;
				$modelNew->email = $model->email;
				$modelNew->type = ["CONSIGNEE"=>"CONSIGNEE","SHIPPER"=>"SHIPPER"];
				$modelNew->office_mapping = $model->office_code;
				$modelNew->location = "-";
				
			if ($modelNew->save(false)) {
					$modelsContact = Contact::find()->where(['id_debtor'=>$model->id])->all();
					foreach ($modelsContact as $contact) {
							$modelContact = NEW Contact();
							$modelContact->id_party = $modelNew->id;
							$modelContact->firstname = $contact->firstname;
							$modelContact->lastname = $contact->lastname;
							if($modelContact->save(false)){
								$modelsPhone = Phone::find()->where(['id_contact'=>$contact->id])->all();
								foreach ($modelsPhone as $phone) {
										$modelPhone = NEW Phone();
										$modelPhone->id_contact = $modelContact->id;
										$modelPhone->phone_type = $phone->phone_type;
										$modelPhone->phone_number = $phone->phone_number;							
										$modelPhone->save(false);
								}
							}
					}
				 \cakebake\actionlog\model\ActionLog::add('success', Yii::$app->user->identity->username." success create ".$modelNew->no_doc);
				$emailv = explode(',',$validator["email"]);
				$emaila = explode(',',$model->email_acknowledges);
				$valid =[];
				$ackno =[];
				foreach($emailv as $key => $valuev) {
				   $valid[] = $valuev;
				}
				foreach($emaila as $key => $valuea) {
				   $ackno[] = $valuea;
				}
				$ackno[]="andra.wisata@kamadjaja.com";
				Yii::$app->mailer->compose('@app/mail/layouts/validator.php',[
					'request' => $model->requestor,
					'master' => 'Master Party',
					'status' => "validator",
					'code' => $modelNew->code,
					'name' => $modelNew->name,
					'nodoc' => $modelNew->no_doc,
					'remark' => $modelNew->remark,
					'image' => Yii::getAlias('@app/web/klog.png'),
				])
				 ->setFrom('it.mdm@kamadjaja.com')
				 ->setTo($model->email_requestor)
				 ->setCc(array_merge($valid,$ackno))
				 ->setSubject('Validation Master Party')
				 ->send();
				 
				// return $this->redirect(['validator/debtor']);
			}else{
				Yii::$app->session->setFlash('error', 'Error On create Party');
				// return $this->redirect(['validator/debtor']);
			}
		}
    }

    /**
     * Updates an existing MasterDebtor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MasterDebtor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MasterDebtor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterDebtor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MasterDebtor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

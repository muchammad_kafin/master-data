<?php

namespace app\controllers;

use Yii;
use app\models\MasterClient;
use app\models\MasterClientSearch;
use yii\web\Controller;
use app\models\User;
use app\models\Contact;
use app\models\Phone;
use app\models\Model;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MasterClientController implements the CRUD actions for MasterClient model.
 */
class MasterClientController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterClient models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MasterClientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MasterClient model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MasterClient model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MasterClient();
		$modelsContact = [new Contact()];
        $modelsPhone = [[new Phone()]];
		// $validator = User::getValidator('validator','client');
		$support = User::getValidator('support','client');
		$ackno = User::getValidator('acknowledges','client');
		$count = MasterClient::find()->count();
		$nextCount = $count+1;
		$today = date("Y-m-d");
		if ($nextCount < 10 ){
			$noDoc = 'FM-CLI-0'.$nextCount;
		}else{
			$noDoc = 'FM-CLI-'.$nextCount;
		}		

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			$modelsContact = Model::createMultiple(Contact::classname());
			Model::loadMultiple($modelsContact, Yii::$app->request->post());
	 
			// validate person and houses models
			$valid = $model->validate();
			$valid = Model::validateMultiple($modelsContact) && $valid;
	 
			if (isset($_POST['Phone'][0][0])) {
				foreach ($_POST['Phone'] as $indexHouse => $rooms) {
					foreach ($rooms as $indexRoom => $room) {
						$data['Phone'] = $room;
						$modelPhone = new Phone();
						$modelPhone->load($data);
						$modelsPhone[$indexHouse][$indexRoom] = $modelPhone;
						$valid = $modelPhone->validate();
						//die(var_dump($modelRoom->errors));
					}
				}
			}
			if ($valid) {
				$cekdoc = $_POST["MasterClient"]["no_doc"];
				$existdoc = MasterClient::find()->where(['no_doc'=>$cekdoc])->one();
				if($existdoc){
					$count = MasterClient::find()->count();
					$nextCount = $count+1;
					$today = date("Y-m-d");
					if ($nextCount < 10 ){
						$noDoc = 'FM-CLI-0'.$nextCount;
					}else{
						$noDoc = 'FM-CLI-'.$nextCount;
					}
					$model->no_doc = $noDoc;
					
				}else{
					$model->no_doc = $cekdoc;
				}
				$transaction = Yii::$app->db->beginTransaction();
				try {
					if ($flag = $model->save(false)) {
						foreach ($modelsContact as $indexHouse => $modelContact) {
	 
							if ($flag === false) {
								break;
							}
	 
							$modelContact->id_client = $model->id;
	 
							if (!($flag = $modelContact->save(false))) {
								break;
							}
	 
							if (isset($modelsPhone[$indexHouse]) && is_array($modelsPhone[$indexHouse])) {
								foreach ($modelsPhone[$indexHouse] as $indexRoom => $modelPhone) {
									$modelPhone->id_contact = $modelContact->id;
									if (!($flag = $modelPhone->save(false))) {
										break;
									}
								}
							}
						}
					}
	 
					if ($flag) {
						$transaction->commit();
						$email_request = $_POST["MasterClient"]["email_requestor"];
						$nodoc = $model->no_doc;
						\cakebake\actionlog\model\ActionLog::add('success', $email_request." success request ".$nodoc);						
						$email_valid = $_POST["MasterClient"]["email_validator"];
						$request = $_POST["MasterClient"]["requestor"];
						$status = $_POST["MasterClient"]["status"];
						$code = $_POST["MasterClient"]["code"];
						$name = $_POST["MasterClient"]["name"];						
						$remark = $_POST["MasterClient"]["remark"];
						
						$email = explode(',',$email_valid);
						$valid =[];
						foreach($email as $key => $value) {
						   $valid[] = $value;
						}
						
						Yii::$app->mailer->compose('@app/mail/layouts/validator.php',[
							'request' => $request,
							'master' => 'Master Client',
							'status' => $status,
							'code' => $code,
							'name' => $name,
							'nodoc' => $nodoc,
							'remark' => $remark,
							'image' => Yii::getAlias('@app/web/klog.png'),
						])
						 ->setFrom('it.mdm@kamadjaja.com')
						 ->setTo($valid)
						 ->setCc('andra.wisata@kamadjaja.com')
						 ->setSubject('Validation Master Client ['.$nodoc.']')
						 ->send();
						 
						Yii::$app->mailer->compose('@app/mail/layouts/validator.php',[
							'request' => $request,
							'master' => 'Master Client',
							'status' => $status,
							'code' => $code,
							'name' => $name,
							'nodoc' => $nodoc,
							'remark' => $remark,
							'image' => Yii::getAlias('@app/web/klog.png'),
						])
						 ->setFrom('it.mdm@kamadjaja.com')
						 ->setTo($email_request)
						 ->setCc('andra.wisata@kamadjaja.com')
						 ->setSubject('Master Client ['.$nodoc.']')
						 ->send();
						return $this->goHome();
					} else {
						$transaction->rollBack();
						\Yii::$app->session->setFlash('error','Sorry some error occurred, please contact IT BS Team :)');
						return $this->redirect(['create']);
					}
				} catch (Exception $e) {
					$transaction->rollBack();
					\Yii::$app->session->setFlash('error','Sorry some error occurred, please contact IT BS Team :)');
					return $this->redirect(['create']);
				}
			}
        } else {
            return $this->render('create', [
                'model' => $model,
                'modelsContact' => $modelsContact,
                'modelsPhone' => $modelsPhone,
				$model->no_doc = $noDoc,
				$model->status = 'validator',
				$model->req_date = $today,
				// $model->validator = strtoupper($validator['id']),
				// $model->validator_name = strtoupper($validator['username']),
				// $model->email_validator = $validator['email'],
				$model->acknowledges = strtoupper($ackno['id']),
				$model->acknowledges_name = strtoupper($ackno['username']),
				$model->email_acknowledges = $ackno['email'],
				$model->support = strtoupper($support['id']),
				$model->support_name = strtoupper($support['username']),
				$model->email_support = $support['email'],
				$model->address_2 = '-',
            ]);
        }
    }

    /**
     * Updates an existing MasterClient model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MasterClient model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MasterClient model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterClient the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MasterClient::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

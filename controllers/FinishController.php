<?php

namespace app\controllers;

use Yii;
use app\models\MasterParty;
use app\models\MasterPartySearch;
use app\models\MasterClient;
use app\models\MasterClientSearch;
use app\models\MasterCountry;
use app\models\MasterCountrySearch;
use app\models\MasterDebtor;
use app\models\MasterDebtorSearch;
use app\models\MasterLocation;
use app\models\MasterLocationSearch;
use app\models\MasterRegion;
use app\models\MasterRegionSearch;
use app\models\MasterVendor;
use app\models\MasterVendorSearch;
use app\models\MasterItem;
use app\models\MasterItemSearch;
use app\models\MasterContract;
use app\models\MasterContractSearch;
use app\models\MasterEquipment;
use app\models\MasterEquipmentSearch;
use app\models\MasterProduct;
use app\models\MasterProductSearch;
use app\models\MasterPrinting;
use app\models\MasterPrintingSearch;
use app\models\MasterCharge;
use app\models\MasterChargeSearch;
use app\models\UpdateForm;
use app\models\UpdateFormSearch;
use app\models\Country;
use app\models\Region;
use app\models\City;
use app\models\User;
use app\models\Contact;
use app\models\Phone;
use app\models\Upload;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * MasterPartyController implements the CRUD actions for MasterParty model.
 */
class FinishController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
			// 'access' => [
                // 'class' => AccessControl::className(),
                // 'only' => ['party','client'],
                // 'rules' => [
                    // [
                        // 'actions' => ['party','client'],
                        // 'allow' => true,
                        // 'roles' => ['@'],
                    // ],
                // ],
            // ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionParty()
    {
        $searchModel = new MasterPartySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$searchUpdate = new UpdateFormSearch();
		$queryParams = array_merge(array(),Yii::$app->request->queryParams);
        $queryParams["UpdateFormSearch"]["master"] = "PARTY";
        $dataUpdate = $searchUpdate->search($queryParams);
		$title = "Master Party";

        return $this->render('party/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'updateProvider' => $dataUpdate,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionClient()
    {
        $searchModel = new MasterClientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$searchUpdate = new UpdateFormSearch();
		$queryParams = array_merge(array(),Yii::$app->request->queryParams);
        $queryParams["UpdateFormSearch"]["master"] = "CLIENT";
        $dataUpdate = $searchUpdate->search($queryParams);
		$title = "Master Client";
		
        return $this->render('client/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'updateProvider' => $dataUpdate,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionDebtor()
    {
        $searchModel = new MasterDebtorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$searchUpdate = new UpdateFormSearch();
		$queryParams = array_merge(array(),Yii::$app->request->queryParams);
        $queryParams["UpdateFormSearch"]["master"] = "DEBTOR";
        $dataUpdate = $searchUpdate->search($queryParams);
		$title = "Master Debtor";

        return $this->render('debtor/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'updateProvider' => $dataUpdate,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionCountry()
    {
        $searchModel = new MasterCountrySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Country";

        return $this->render('country/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionLocation()
    {
        $searchModel = new MasterLocationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Location";

        return $this->render('location/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionRegion()
    {
        $searchModel = new MasterRegionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Region";

        return $this->render('region/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionVendor()
    {
        $searchModel = new MasterVendorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$searchUpdate = new UpdateFormSearch();
		$queryParams = array_merge(array(),Yii::$app->request->queryParams);
        $queryParams["UpdateFormSearch"]["master"] = "VENDOR";
        $dataUpdate = $searchUpdate->search($queryParams);
		$title = "Master Vendor";

        return $this->render('vendor/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'updateProvider' => $dataUpdate,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionItem()
    {
        $searchModel = new MasterItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Item";

        return $this->render('item/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionContract()
    {
        $searchModel = new MasterContractSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Contract";

        return $this->render('contract/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionEquipment()
    {
        $searchModel = new MasterEquipmentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Equipment";

        return $this->render('equipment/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionProduct()
    {
        $searchModel = new MasterProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Product";

        return $this->render('product/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionPrinting()
    {
        $searchModel = new MasterPrintingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Printing";

        return $this->render('printing/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionCharge()
    {
        $searchModel = new MasterChargeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$title = "Master Charge";

        return $this->render('charge/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'title' => $title,
        ]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionDetailparty($id)
    {
        $model = $this->findParty($id);
		$modelsContact = \app\models\Contact::find()->where(['id_party'=>$id])->all();
		$coun = Country::find()->where(['code'=>$model->country])->one();
		if($coun){
			$couname = $coun->name;
		}else{
			$couname = $model->country;
		}
		// if($model->city != ''){
			// $city = City::find()->where(['id'=>$model->city])->one();
			// $namecity = $city->name;
		// }else{
			// $namecity = '';
		// }
		return $this->renderPartial('party/update', [
					'model' => $model,
					'modelsContact' => $modelsContact,
					$model->country = $couname,
					$model->state_name = $model->state,
					$model->city_name = $model->city,
		]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionDetailvendor($id)
    {
        $model = $this->findVendor($id);
		$modelsContact = \app\models\Contact::find()->where(['id_vendor'=>$id])->all();
		$modelsUpload = Upload::find()->where(['id_form'=>$model->no_doc])->all();
		$coun = Country::find()->where(['code'=>$model->country])->one();
		if($coun){
			$couname = $coun->name;
		}else{
			$couname = $model->country;
		}
		// if($model->state != ''){
			// $state = Region::find()->where(['country'=>$coun->code,'code'=>$model->state])->one();
			// $region = $state->name;
		// }else{
			// $region = '';
		// }
		// if($model->city != ''){
			// $city = City::find()->where(['id'=>$model->city])->one();
			// $namecity = $city->name;
		// }else{
			// $namecity = '';
		// }
		return $this->renderPartial('vendor/update', [
					'model' => $model,
					'modelsContact' => $modelsContact,
					'modelsUpload' => $modelsUpload,
					$model->country = $couname,
					$model->state_name = $model->state,
					$model->city_name = $model->city,
		]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionDetaildebtor($id)
    {
        $model = $this->findDebtor($id);
		$modelsContact = \app\models\Contact::find()->where(['id_debtor'=>$id])->all();
		$modelsUpload = Upload::find()->where(['id_form'=>$model->no_doc])->all();
		$coun = Country::find()->where(['code'=>$model->country])->one();
		if($coun){
			$couname = $coun->name;
		}else{
			$couname = $model->country;
		}
		// if($model->state != ''){
			// $state = Region::find()->where(['country'=>$coun->code,'code'=>$model->state])->one();
			// $region = $state->name;
		// }else{
			// $region = '';
		// }
		// if($model->city != ''){
			// $city = City::find()->where(['id'=>$model->city])->one();
			// $namecity = $city->name;
		// }else{
			// $namecity = '';
		// }
		return $this->renderPartial('debtor/update', [
					'model' => $model,
					'modelsContact' => $modelsContact,
					'modelsUpload' => $modelsUpload,
					$model->country = $couname,
					$model->state_name = $model->state,
					$model->city_name = $model->city,
		]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionDetailclient($id)
    {
        $model = $this->findClient($id);
		$modelsContact = Contact::find()->where(['id_client'=>$id])->all();
		$coun = Country::find()->where(['code'=>$model->country])->one();
		if($coun){
			$couname = $coun->name;
		}else{
			$couname = $model->country;
		}
		// if($model->state != ''){
			// $state = Region::find()->where(['country'=>$coun->code,'code'=>$model->state])->one();
			// $region = $state->name;
		// }else{
			// $region = '';
		// }
		// if($model->city != ''){
			// $city = City::find()->where(['id'=>$model->city])->one();
			// $namecity = $city->name;
		// }else{
			// $namecity = '';
		// }
		return $this->renderPartial('client/update', [
					'model' => $model,
					'modelsContact' => $modelsContact,
					$model->country = $couname,
					$model->state_name = $model->state,
					$model->city_name = $model->city,
		]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionDetailcountry($id)
    {
        $model = $this->findCountry($id);
		return $this->renderPartial('country/update', [
					'model' => $model,
		]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionDetaillocation($id)
    {
        $model = $this->findLocation($id);
		return $this->renderPartial('location/update', [
					'model' => $model,
		]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionDetailregion($id)
    {
        $model = $this->findRegion($id);
		return $this->renderPartial('region/update', [
					'model' => $model,
		]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionDetailitem($id)
    {
        $model = $this->findItem($id);
		return $this->renderPartial('item/update', [
					'model' => $model,
		]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionDetailcontract($id)
    {
        $model = $this->findContract($id);
		$modelsUpload = Upload::find()->where(['id_form'=>$model->no_doc])->all();
		return $this->renderPartial('contract/update', [
					'model' => $model,
					'modelsUpload' => $modelsUpload,
		]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionDetailequipment($id)
    {
        $model = $this->findEquipment($id);
		$modelsUpload = Upload::find()->where(['id_form'=>$model->no_doc])->all();
		
		return $this->renderPartial('equipment/update', [
					'model' => $model,
					'modelsUpload' => $modelsUpload,
		]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionDetailproduct($id)
    {
        $model = $this->findProduct($id);
		
		return $this->renderPartial('product/update', [
					'model' => $model,
		]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionDetailprinting($id)
    {
        $model = $this->findPrinting($id);
		
		return $this->renderPartial('printing/update', [
					'model' => $model,
		]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionDetailcharge($id)
    {
        $model = $this->findCharge($id);
		
		return $this->renderPartial('charge/update', [
					'model' => $model,
		]);
    }
	
	/**
     * Lists all MasterParty models.
     * @return mixed
     */
    public function actionDetailupdate($id)
    {
        $model = $this->findUpdate($id);
		
		return $this->renderPartial('update_form', [
					'model' => $model,
		]);
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findParty($id)
    {
        if (($model = MasterParty::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findClient($id)
    {
        if (($model = MasterClient::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findCountry($id)
    {
        if (($model = MasterCountry::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findLocation($id)
    {
        if (($model = MasterLocation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findDebtor($id)
    {
        if (($model = MasterDebtor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findRegion($id)
    {
        if (($model = MasterRegion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findVendor($id)
    {
        if (($model = MasterVendor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findItem($id)
    {
        if (($model = MasterItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findContract($id)
    {
        if (($model = MasterContract::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findEquipment($id)
    {
        if (($model = MasterEquipment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findProduct($id)
    {
        if (($model = MasterProduct::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findPrinting($id)
    {
        if (($model = MasterPrinting::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findCharge($id)
    {
        if (($model = MasterCharge::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * Finds the MasterParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findUpdate($id)
    {
        if (($model = UpdateForm::findOne(['no_doc'=>$id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
